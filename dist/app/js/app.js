/*!
 * 
 * Angle - Bootstrap Admin App + AngularJS
 * 
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: http://support.wrapbootstrap.com/knowledge_base/topics/usage-licenses
 * 
 */

if (typeof $ === 'undefined') { throw new Error('This application\'s JavaScript requires jQuery'); }


// APP START
// ----------------------------------- 

var App = angular.module('angle', ['ngRoute', 'ngAnimate', 'ngStorage', 'ngCookies', 'pascalprecht.translate', 'ui.bootstrap', 'ui.router', 'oc.lazyLoad', 'cfp.loadingBar', 'ngSanitize', 'ngResource', 'tmh.dynamicLocale'])
          .run(["$rootScope", "$state", "$stateParams",  '$window', '$templateCache', function ($rootScope, $state, $stateParams, $window, $templateCache) {
              // Set reference to access them from any scope
              $rootScope.$state = $state;
              $rootScope.$stateParams = $stateParams;
              $rootScope.$storage = $window.localStorage;

              // Uncomment this to disables template cache
              /*$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                  if (typeof(toState) !== 'undefined'){
                    $templateCache.remove(toState.templateUrl);
                  }
              });*/

              // Scope Globals
              // ----------------------------------- 
              $rootScope.app = {
                name: 'Angle',
                description: 'Angular Bootstrap Admin Template',
                year: ((new Date()).getFullYear()),
                layout: {
                  isFixed: true,
                  isCollapsed: false,
                  isBoxed: false,
                  isRTL: false
                },
                useFullLayout: false,
                hiddenFooter: false,
                viewAnimation: 'ng-fadeInUp'
              };
              $rootScope.user = {
                name:     'John',
                job:      'ng-Dev',
                picture:  'app/img/user/02.jpg'
              };
            }
          ]);

/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/

App.config(['$stateProvider','$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'APP_REQUIRES', 'RouteHelpersProvider',
function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, appRequires, helper) {
  'use strict';

  App.controller = $controllerProvider.register;
  App.directive  = $compileProvider.directive;
  App.filter     = $filterProvider.register;
  App.factory    = $provide.factory;
  App.service    = $provide.service;
  App.constant   = $provide.constant;
  App.value      = $provide.value;

  // LAZY MODULES
  // ----------------------------------- 

  $ocLazyLoadProvider.config({
    debug: false,
    events: true,
    modules: appRequires.modules
  });


  // defaults to dashboard
  $urlRouterProvider.otherwise('/app/home');

  // 
  // Application Routes
  // -----------------------------------   
  $stateProvider
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: helper.basepath('app.html'),
        controller: 'AppController',
        resolve: helper.resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'toaster', 'whirl')
    })
    .state('app.home', {
        url: '/home',
        title: 'Home',
        templateUrl: helper.basepath('main.html'),
        controller: 'AppController'
      })
    .state('app.about', {
        url: '/about',
        title: 'About',
        templateUrl: helper.basepath('about.html'),
        controller: 'AboutCtrl'
      })
    .state('app.resetPassword', {
        url: '/resetPassword/:accessToken',
        title: 'Reset Password',
        templateUrl: helper.basepath('reset-password.html'),
        controller: 'AppController'
      })
    .state('app.scoreboard', {
        url: '/scoreboard/:roundId',
        title: 'Scoreboard',
        templateUrl: helper.basepath('scoreboard.html'),
        controller: 'ScoreboardCtrl',
        resolve: helper.resolveFor('flot-chart','flot-chart-plugins')
    })

    // 
    // CUSTOM RESOLVES
    //   Add your own resolves properties
    //   following this object extend
    //   method
    // ----------------------------------- 
    // .state('app.someroute', {
    //   url: '/some_url',
    //   templateUrl: 'path_to_template.html',
    //   controller: 'someController',
    //   resolve: angular.extend(
    //     helper.resolveFor(), {
    //     // YOUR RESOLVES GO HERE
    //     }
    //   )
    // })
    ;


}]).config(['$translateProvider', function ($translateProvider) {

    $translateProvider.useStaticFilesLoader({
        prefix : 'app/i18n/',
        suffix : '.json'
    });
    $translateProvider.preferredLanguage('en');
    $translateProvider.useLocalStorage();
    $translateProvider.usePostCompiling(true);

}]).config(['tmhDynamicLocaleProvider', function (tmhDynamicLocaleProvider) {

    tmhDynamicLocaleProvider.localeLocationPattern('vendor/angular-i18n/angular-locale_{{locale}}.js');

    // tmhDynamicLocaleProvider.useStorage('$cookieStore');

}]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 500;
    cfpLoadingBarProvider.parentSelector = '.wrapper > section';
  }])
.controller('NullController', function() {});

/**=========================================================
 * Module: constants.js
 * Define constants to inject across the application
 =========================================================*/
App
  .constant('APP_COLORS', {
    'primary':                '#5d9cec',
    'success':                '#27c24c',
    'info':                   '#23b7e5',
    'warning':                '#ff902b',
    'danger':                 '#f05050',
    'inverse':                '#131e26',
    'green':                  '#37bc9b',
    'pink':                   '#f532e5',
    'purple':                 '#7266ba',
    'dark':                   '#3a3f51',
    'yellow':                 '#fad732',
    'gray-darker':            '#232735',
    'gray-dark':              '#3a3f51',
    'gray':                   '#dde6e9',
    'gray-light':             '#e4eaec',
    'gray-lighter':           '#edf1f2'
  })
  .constant('APP_MEDIAQUERY', {
    'desktopLG':             1200,
    'desktop':                992,
    'tablet':                 768,
    'mobile':                 480
  })
  .constant('APP_REQUIRES', {
    // jQuery based and standalone scripts
    scripts: {
      'whirl':              ['vendor/whirl/dist/whirl.css'],
      'classyloader':       ['vendor/jquery-classyloader/js/jquery.classyloader.min.js'],
      'animo':              ['vendor/animo.js/animo.js'],
      'fastclick':          ['vendor/fastclick/lib/fastclick.js'],
      'modernizr':          ['vendor/modernizr/modernizr.js'],
      'animate':            ['vendor/animate.css/animate.min.css'],
      'icons':              ['vendor/skycons/skycons.js',
                             'vendor/fontawesome/css/font-awesome.min.css',
                             'vendor/simple-line-icons/css/simple-line-icons.css',
                             'vendor/weather-icons/css/weather-icons.min.css'],
      'sparklines':         ['app/vendor/sparklines/jquery.sparkline.min.js'],
      'slider':             ['vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js',
                             'vendor/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css'],
      'wysiwyg':            ['vendor/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                             'vendor/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
      'slimscroll':         ['vendor/slimScroll/jquery.slimscroll.min.js'],
      'screenfull':         ['vendor/screenfull/dist/screenfull.min.js'],
      'vector-map':         ['vendor/ika.jvectormap/jquery-jvectormap-1.2.2.min.js',
                             'vendor/ika.jvectormap/jquery-jvectormap-world-mill-en.js',
                             'vendor/ika.jvectormap/jquery-jvectormap-us-mill-en.js',
                             'vendor/ika.jvectormap/jquery-jvectormap-1.2.2.css'],
      'loadGoogleMapsJS':   ['app/vendor/gmap/load-google-maps.js'],
      'google-map':         ['vendor/jQuery-gMap/jquery.gmap.min.js'],
      'flot-chart':         ['vendor/Flot/jquery.flot.js'],
      'flot-chart-plugins': ['vendor/flot.tooltip/js/jquery.flot.tooltip.min.js',
                             'vendor/Flot/jquery.flot.resize.js',
                             'vendor/Flot/jquery.flot.pie.js',
                             'vendor/Flot/jquery.flot.time.js',
                             'vendor/Flot/jquery.flot.categories.js',
                             'vendor/flot-spline/js/jquery.flot.spline.min.js'],
                            // jquery core and widgets
      'jquery-ui':          ['vendor/jquery-ui/ui/core.js',
                             'vendor/jquery-ui/ui/widget.js'],
                             // loads only jquery required modules and touch support
      'jquery-ui-widgets':  ['vendor/jquery-ui/ui/core.js',
                             'vendor/jquery-ui/ui/widget.js',
                             'vendor/jquery-ui/ui/mouse.js',
                             'vendor/jquery-ui/ui/draggable.js',
                             'vendor/jquery-ui/ui/droppable.js',
                             'vendor/jquery-ui/ui/sortable.js',
                             'vendor/jqueryui-touch-punch/jquery.ui.touch-punch.min.js'],
      'moment' :            ['vendor/moment/min/moment-with-locales.min.js'],
      'inputmask':          ['vendor/jquery.inputmask/dist/jquery.inputmask.bundle.min.js'],
      'flatdoc':            ['vendor/flatdoc/flatdoc.js'],
      'codemirror':         ['vendor/codemirror/lib/codemirror.js',
                             'vendor/codemirror/lib/codemirror.css'],
      'codemirror-plugins':  ['vendor/codemirror/addon/mode/overlay.js',
                              'vendor/codemirror/mode/markdown/markdown.js',
                              'vendor/codemirror/mode/xml/xml.js',
                              'vendor/codemirror/mode/gfm/gfm.js',
                              'vendor/marked/lib/marked.js'],
      // modes for common web files
      'codemirror-modes-web': ['vendor/codemirror/mode/javascript/javascript.js',
                               'vendor/codemirror/mode/xml/xml.js',
                               'vendor/codemirror/mode/htmlmixed/htmlmixed.js',
                               'vendor/codemirror/mode/css/css.js'],
      'taginput' :          ['vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                             'vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js'],
      'filestyle':          ['vendor/bootstrap-filestyle/src/bootstrap-filestyle.js'],
      'parsley':            ['vendor/parsleyjs/dist/parsley.min.js'],
      'datatables':         ['vendor/datatables/media/js/jquery.dataTables.min.js',
                             'app/vendor/datatable-bootstrap/css/dataTables.bootstrap.css'],
      'datatables-pugins':  ['app/vendor/datatable-bootstrap/js/dataTables.bootstrap.js',
                             'app/vendor/datatable-bootstrap/js/dataTables.bootstrapPagination.js',
                             'vendor/datatables-colvis/js/dataTables.colVis.js',
                             'vendor/datatables-colvis/css/dataTables.colVis.css'],
      'fullcalendar':       ['vendor/fullcalendar/dist/fullcalendar.min.js',
                             'vendor/fullcalendar/dist/fullcalendar.css'],
      'gcal':               ['vendor/fullcalendar/dist/gcal.js'],
      'nestable':           ['vendor/nestable/jquery.nestable.js']
    },
    // Angular based script (use the right module name)
    modules: [
      {name: 'toaster',                   files: ['vendor/angularjs-toaster/toaster.js',
                                                 'vendor/angularjs-toaster/toaster.css']},
      {name: 'localytics.directives',     files: ['vendor/chosen_v1.2.0/chosen.jquery.min.js',
                                                 'vendor/chosen_v1.2.0/chosen.min.css',
                                                 'vendor/angular-chosen-localytics/chosen.js']},
      {name: 'ngDialog',                  files: ['vendor/ngDialog/js/ngDialog.min.js',
                                                 'vendor/ngDialog/css/ngDialog.min.css',
                                                 'vendor/ngDialog/css/ngDialog-theme-default.min.css'] },
      {name: 'ngWig',                     files: ['vendor/ngWig/dist/ng-wig.min.js'] },
      {name: 'ngTable',                   files: ['vendor/ng-table/ng-table.min.js',
                                                  'vendor/ng-table/ng-table.min.css']},
      {name: 'ngTableExport',             files: ['vendor/ng-table-export/ng-table-export.js']},
      {name: 'angularBootstrapNavTree',   files: ['vendor/angular-bootstrap-nav-tree/dist/abn_tree_directive.js',
                                                  'vendor/angular-bootstrap-nav-tree/dist/abn_tree.css']},
      {name: 'htmlSortable',              files: ['vendor/html.sortable/dist/html.sortable.js',
                                                  'vendor/html.sortable/dist/html.sortable.angular.js']},
      {name: 'xeditable',                 files: ['vendor/angular-xeditable/dist/js/xeditable.js',
                                                  'vendor/angular-xeditable/dist/css/xeditable.css']},
      {name: 'angularFileUpload',         files: ['vendor/angular-file-upload/angular-file-upload.js']},
      {name: 'ngImgCrop',                 files: ['vendor/ng-img-crop/compile/unminified/ng-img-crop.js',
                                                  'vendor/ng-img-crop/compile/unminified/ng-img-crop.css']},
      {name: 'ui.select',                 files: ['vendor/angular-ui-select/dist/select.js',
                                                  'vendor/angular-ui-select/dist/select.css']},
      {name: 'ui.codemirror',             files: ['vendor/angular-ui-codemirror/ui-codemirror.js']},
      {name: 'angular-carousel',          files: ['vendor/angular-carousel/dist/angular-carousel.css',
                                                  'vendor/angular-carousel/dist/angular-carousel.js']}
        
    ]

  })
;


/**
 * @ngdoc function
 * @name golfCourseAmericaWebAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the golfCourseAmericaWebAppApp
 */
App.controller('AboutCtrl', ["$scope", function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  }]);

/**=========================================================
 * Module: main.js
 * Main Application Controller
 =========================================================*/

App.controller('AppController', ['$rootScope', '$scope', '$state', '$http', '$stateParams',
    function($rootScope, $scope, $state, $http, $stateParams) {
        "use strict";

        var infoWindow = new google.maps.InfoWindow();
        var createMarker = function(info) {

            var marker = new google.maps.Marker({
                map: $scope.map,
                position: new google.maps.LatLng(info.latitude, info.longitude),
                title: info.courseName
            });
            marker.content = '<div class="infoWindowContent">' + info.courseAddress + ', ' + info.cityName + ', ' + info.stateName + '<p> <i class="glyphicon glyphicon-phone"></i> ' + info.phone + '</p> </div>';

            google.maps.event.addListener(marker, 'click', function() {
                infoWindow.setContent('<h5>' + marker.title + '</h5>' + marker.content);
                infoWindow.open($scope.map, marker);
            });
            $scope.markers.push(marker);
        };

        $scope.openInfoWindow = function(e, selectedMarker) {
            e.preventDefault();
            google.maps.event.trigger(selectedMarker, 'click');
        };


        $scope.createMap = function() {
            var cities = [];

            $http.get('http://54.174.167.36/api/GolfCourses/allCourseDetails')
                .success(function(response) {
                    cities = response;
                    console.log('Courses: ' + JSON.stringify(cities));
                    console.log('Create Map called');
                    var mapOptions = {
                        zoom: 10,
                        center: new google.maps.LatLng(40.7143528, -74.0059731),
                        mapTypeId: google.maps.MapTypeId.TERRAIN
                    };

                    $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

                    $scope.markers = [];

                    for (var i = 0; i < cities.length; i++) {
                        createMarker(cities[i]);
                    }

                    //var markerCluster = new MarkerClusterer($scope.map, $scope.markers);
                    google.maps.event.addListener($scope.map, 'idle', function() {
                        $scope.newMarkers = [];

                        for (var i = 0; i < $scope.markers.length; i++) {
                            if ($scope.map.getBounds().contains($scope.markers[i].getPosition())) {
                                // markers[i] in visible getBounds
                                console.log('Visible Marker: ' + $scope.markers[i].getTitle());
                                $scope.newMarkers.push($scope.markers[i]);
                            } else {
                                // markers[i] is not in visible bounds
                            }

                        }
                        $scope.$apply();
                    });
                });
        };

        $scope.resetPassword = function() {

            var pwd = $scope.pwd;
            var cPwd = $scope.cPwd;

            console.log('In savePassword ' + JSON.stringify(pwd));
            if (angular.isUndefined(pwd) || pwd === null || pwd === '') {
                window.alert('Please enter passwords', 'Reset Password');
            } else {
                if (pwd !== cPwd) {
                    window.alert('Passwords don\'t match.', 'Reset Password');
                } else {

                    var accessToken = $stateParams.accessToken;

                    console.log(pwd + ' - ' + accessToken);

                    $http.post('http://54.174.167.36/reset-password', {
                            password: pwd,
                            confirmation: cPwd,
                            accessToken: accessToken
                        })
                        .success(function(data) { //success
                            console.log('Success: ' + JSON.stringify(data)); //data from server
                            window.alert('Passwords reset successfully.');
                        })
                        .error(function(error) { //error
                            console.log('Error: ' + JSON.stringify(error)); //data from server
                            window.alert('Passwords not reset, please try again.');
                        });
                }
            }
        };


    }
]);

/*/**
 * @ngdoc function
 * @name gcaControllers:ScoreboardCtrl
 * @description
 * # ScoreboardController
 * Scoreboard Controller of GCA app
 */

App.controller('ScoreboardCtrl', ['$scope', '$http', '$stateParams', function($scope, $http, $stateParams) {

    var url = 'http://api.qa.golfcourseamerica.com/api';


    /*function countDown(i) {
        var int = setInterval(function() {
            document.getElementById('displayDiv').innerHTML = 'Round still in progress! <br> Checking for updates in ' + i + ' seconds' ;
            i-- || clearInterval(int); //if i is 0, then stop the interval
        }, 1000);
        setTimeout(function() {
            window.location.reload(1);
        }, 60000);
    }
    countDown(60);*/


    function scoreGraph(scoreCard, playersName) {

        var courseHoles = [];

        var players = [];

        var playersScore = [];

        var colorCode = [];

        var nickName = '';

        var str = '';

        if ((!angular.isUndefined(scoreCard)) && (scoreCard.length > 0)) {
            for (var i = 0; i < scoreCard[0].jsonScore.length; i++) {

                playersScore = [];

                colorCode = [];

                if ((!angular.isUndefined(playersName[i].groupName))) {

                    nickName = playersName[i].groupName;
                } else {
                    nickName = scoreCard[0].jsonScore[i].nickName;
                }

                for (var j = 0; j < scoreCard.length; j++) {

                    if (scoreCard[j].jsonScore[i].score === '-') {
                        playersScore.push(0);
                        colorCode.push("tie-row");
                    } else {
                        playersScore.push(scoreCard[j].jsonScore[i].score);
                        colorCode.push("" + scoreCard[j].colorCode);
                    }
                    if (i === 0) {
                        courseHoles.push(j + 1);
                    }
                }

                console.log('Player Score :' + (playersScore.toString()));

                str = playersScore.toString();
                str = '[' + str + ']';
                console.log('str : ' + str);

                console.log('Color Code :' + (colorCode.toString()));

                players.push({
                    'name': nickName,
                    'data': JSON.parse(str),
                    'colorCode': JSON.parse(JSON.stringify(colorCode))
                });
            }
        }

        courseHoles = '[' + courseHoles.toString() + ']';

        console.log('Course Holes : ' + courseHoles);

        console.log('Players : ' + JSON.stringify(players));

        $scope.playerWiseScore = players;

        //$scope.players = players;

        $(function() {
            $('#container').highcharts({
                title: {
                    text: 'Score to Par',
                    //x: 20 //center
                },
                xAxis: {
                    title: {
                        text: 'Hole'
                    },
                    categories: JSON.parse(courseHoles)
                },
                yAxis: {
                    title: {
                        text: 'Par'
                    },
                    min: 0 // this sets minimum values of y to 0

                    /*plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]*/
                },
                // tooltip: {
                //     valueSuffix: '°C'
                // },
                /*legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },*/
                series: JSON.parse(JSON.stringify(players))
            });

        });
    }

    //base64: 18 flags icons
    var HOLES = [
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAJgElEQVR42u3Ye1BTVx4HcJWivJFnIBAICSE867irdX00nepM7e5Wx9a19mXU1tGqiAUE5f1+ibyUNwREBBEQgSJCeCVgeAeCCCriY7fdnXZ2/+9fu9/93dsLdZy1u7pAhemd+c0N9yaX88k5v3N+J8uW/Xr8ery6R/SE18qAektBQL2h4ETzslWLF9JhI8voFyO6zQYHsg0fHy+zGPWrtND5V1vqTjfY6EKaeLrwGw666BaRLr7dXZfcuUZ3TrVel9G9WZd9a6sur+8dXWH/Dp1i8ANd2fBeig91Zdo9FPS39n1d2cguip26slEmdtDr93Spau/h6BZH1zmFxHfYy3K0nkjotIc8wxhHC63hW2aFgKu2CP2aj+hWZ8S3uSK50wPn1K8j89Y6XOjbiPxBGYqHt+Hi6O9x+fZOVE3sRu29j1A/tQ83Hn2OlidH0P6NL1R/9UfPd8Ho/T4Umu/P4MbjzxDb6fzJnPdIslogyx9ZSxA7yDONcazYGicv2yCw1gbUG4i4aY+YNgHdJ4yKMD0+OE+YHMIUDMmgGHkbZWPvouLODtTcfR/X7+9Fw8PP0Pzng2j7y2F0/c0X3d/5o/nbvah9uA1nu6XB8zK0ktUuMoVuCwvZn2WM4yXW8K+0RdA1W0Q02yOylSBKB8R1OCGpU4x9fl7YvGYNsvt+i9z+DSgkTMnIVly6vR1VkztRN7UbjQ8+QvMTOZTffoHmb/6EqkdvoObRW0hSi3LmLUfOqT1k5ePbWciB8yY4cZF64woPIQ32iGphesMRMe1OiO90RkCehBA+LCS92xsXbhGmdwMKBjajVLsVFePvoubeDtQ//ADXHr6Dyul1uDztg5rHbyJN41E/r8meqfGRMeObgRzMNsHJSzwE1fAQ1kT5oeQjVulI95zgn+OGN9evYRFMnFW5I63LG5nqtcjVbEDR4BaUjFKMrYPijicu3fdG+QMfXH20EZl93n3VNDvOM2St7NpdOX3jDMQI/hV2OH2NhlSzA2LbBYjvcManvh7YsnYN3tvlPQtJanVH0k1PJLd4IKVVilQadpm9YuRp3VA6SZApb1yZXo+84Temc3ucLOZ9+s3WbJI1Th1lIZ9mLceXl4wQXGeD6BbKCxpSiZ1CtuEfH/ZEYqtkFhJxXYDQWj7CKCLrBXRPiPQeCfK1Hii9Q70xtQZFI7/7e1KXULgg60j2oEzWOh3IQvZdWEEQffjVGCC4yQihLWaIUFrAL5+PKKUVzWBWs5DgKh5OldvjdIUjQqup55pcaKi5UQ94omTcGwrdph/ilfbr6F8sXxBI/uBWWeejKILwIM/Rw7GKlQi4boSQFmOEKc0R0bYakW2WiGq1QlSz9SwksNwOASV8BCgcceayANF1IqR1SJHT74nC4Q3/Cmng7aTH6y0opPtJMgs5kPcaTlQZIqjRBOFKM0S1myOmwwKxHVaIbbOlWcz2J0iZPU4WO8C/yBFBpc6IviZGqlKKrB4p5ZmlPz16FQdZsSAYBtL1OJaFfF6gj69qjHDmhgki21cjutMScZ3WiO+yobMd4pR2T0H4LCKgSIDTZULE1LribKsbsjVeCGvkN9KjDRYUwuSI8mEwCzlUtBKBdSYIazFnEbEqS8SrbJFAkaTm0zTMn4UwuXGqVIBAhTNCy0UsJPmmG7I0UhQMv47QJl7+wkJo1roxdZxN9sOKVQhuMKOcoOGkskKcygbJ3bZI0dgjpccRZ9WOs5Dwq0KcKXfGmTIXhF4myDVXpLS4US0mRe6wFKW3N9LULQlfMAizjtTdO8BCviw1oPpqNWI6LZDQbYWkbh5Seu2QOuCI9F4nnNM4zUJi60WIuCpGWIUI4ZVigogJIkFmjxtyBiXIG3XDlYk/IEEpObBAEB/Z1ck9LORYmSHCb9JwUlsj8ZYNUvrskTbAR+aQEzIHhMgacJmFJNxwZXsh4oqYwoVei5DU4spCzhMkV+uKXJ0IdZPyfyZ3e22fdwhTa10e/yML8S03QlSbFRJ7bHFWwyOEA9KHHJGlFSJ7RIQLQ+KfShRazeOuuyKySoTIajFi68S0youRrpLgwgAFQXJGxLgwLET1uPyHhA7n38wrhKl+S8feYiF+lSY0O1FPaJjhZI/0QQHOa52QMypC7pgr8nXSWci5dikSGiWIrBFTiGgdEVOPiGnPIkJWP6GH6Tziggx6RvqAEwoGt/0jrpXnMo8QgaxgdB1bNH5VZYpENQ+p/XxkDDng/LAzsnVC5BOiYFyK4nEPFOq82EUvvdMdiU0Sdv2IqhUhtsFlFpLZS4hBZ2TS59MHHdk8S+l1QIrK/VFIu6nVvEDiOwS0Q/RiIQHVZuwMlT7Ip+HkRAgX5N8WofCOGxQTHiiZ9ILitjfyBj2RoZLSdCtBXIOYTfw4KlGSlS4EcaGJQUg5JWB7lPlSkmmYxtPkEUfr0elmC61/9TLDue+RVh9Z9rA7CzlVY47UXj47HM5TXjBDquC2BEV33Kmi9aLwRvEMRC2lqldCNRZBGl3oLESSUkjlvTPtIik0jkjV8JHUY0eTBxWhHZZstRB60xyB9cZte6rZNWbujvTG3V7Z7bsQenETThQLIwMq7ORBdWbyoHoz+emvV8tDKUKarPaHNVjtD663PBhYZXnoWInFkS8yzXz3JZj5fxxjGvghxScJxv4H0018DxeYHzlaZnrIr8r0oH+t8X4mTtYYyJk4UaUvP1ahLz9aqbfft9pgbvNFoUgxbVN2IDomAfElxwTPedtybmFjvkVmg2RMwewxbCnsubDlrhlz71m4Vf1HiMK0q0uFqJh4pOWkCX7mrTMYfa4gZBpsxjXegnttzN3TX1DEDESlYiBxBMn5XyBPY5ikNeLC8BnEKwt5FvMa1+iVXOhz1xYeMQNRq7tZSM5/hzyLeV4sLGIG0tNz60UgM5ifi2W/CESj6X1RyPNAv9zBQPr6+l8W8uocDGRgYGBpQIaGhhY/pKGhwVSr1S5+CHOMjo5ykJLFDRkbG1sakPHx8aUBmZiY+BFSssghdyfvLg3I/Xv3lgbkwdQUCylZ7JDp6emlAXnw4MFcQGYKR70XqIiXz3GO3P9/cmSm8czGyoDbJRpwm61VT22+ZjZiek9twGb2NnM0a9196VlrZtvLbHVNKcy5MOOCuWbC3Td+Zks8g5u7n4Vm15EXWxBXcA0y5X58sKaw4c5W3DVL7ryaA5pyIIOnIHM3vF5iZdfjGmPONZzH/STEnO24s+1/gJk91SOzufRvT8wW+7VRawwAAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKG0lEQVR42u2YeVCTdxrHVRQlQLgJkINwEyhSVrq7tTW22la3atejra1WBCttRRQ5lSsJISByqChUaDmkIGKUSw5DOMMhAgJxrfWitd3Zzm5ndv/e/Wf3u8/78tJhmHa3WuJUp7+Z77wh7zt5nw/P+fstWPDr+nX9cpfqVqB5XJO9OK7JQnygfcHSJxek20l+4poXVJ1OCC+yeLC/ym7yYK2dMVZrbzzc7GRMbhUY09qERpXO06jp8jfm9AQb8/ueM57of8FYNLDGeGboNeMnw5uMZSNbjZWj20lvGyuvv2WsGt9K2mKsmthMesNYNcloE33eaMwzPHNdpRN5zyuIpttVXjwegKweV4SdsMS+TxwRXeWAuAvOSGlxg6rDHZpOb+T0yJBvWI4TA6E4Pfw8SkblKBtbg7MT61FzYxPqPt+KS7e3o+nue2idisCVBx+i88/R6Ps2FgN/S8LV71Iw9N0RtD14D+oe9x3z7pEcg1heMhFCIC4IO2mJqDJHxNQ4If6SE8gbSL/iioxOMd0nmD6CGQjCqcFQFDMwY6tQNv4yKifXo/pPG3Hh1mbU334bTfd3ksHh0H8diZ5vo2H4ayza/7Idl75ci9x+vySThFaOwUNebnyRBdldaIn9FY6IrXVGYr0z0ttdoeggEL0Qmd0SHDnrg207ArFmZTBWhSzHa2uD8WFiKEoGX0LlxKuoMRLM51vQQJ5pmXoPum/2oO2bbTg/9Vtov1yNowbPYpPlSL5BJq++uY4FCT9lhQNnyRvnBUhudoVSx3hDhIwuCeI+8cKqFcGQhwbh3chARCQsxxubg/FCcDA2bXwWJYbVqBglmMnXKcz+iPNfvILqOytQdfcZgliFgkFZE71uoclATg4FyetubWNBIoqsEPOZAIkXBUhtpfzQu0GtF9E9Cda9FoQXQ4ORXO1D+ULq9kNeVyB2RS1nYfYrQlHU/3ucHgjB6at+KB2XofxmIGru/I7CMeCaqlK6zMQgIfL622HQsCA8xJ5zweF6Cql2IdRdYmi63ZF63os19u3wABzr9cGxLj9k6/yR3S6DUiub9sqWAGjapMjp8MJJgy+KrxHIZAhO9od+FXPSWUCvMiMtMhlM0dBK+eV7+1iQnYUL8dFnPCQ1OEGlo7ygkMrulULZ6I3INH/EnfHD0S5vqHUeUDRLkNrghqQaIQuy7vVAKBs8kNXijTy9Hwr7A5CvX/GPnemCIHoNj7TEtCCjcnnHVDwLsuv0IgJZgoMXlyGplYcUHR/pejsouxyozzhC2eFAnnJCeguFXqMLkrVuiEiZ9ta2MH8otJ7IaPDG0RYKvZaQf21L5L9Or3AiWZPMTeqVktE18p6vlAQiQFixGaLOmSOukYdknSVS9TZI77SFstseKoJR68lTV1ygoPxJbXRDYpUYq1+YzpGDBV5Ir/OE8qI3MrTL/7Mhhh9FP+9FciHZkJgcWWxSkP6vc1iQ8DOLcaDOAomXrZCm55MnbJDRbYfMPge674xMekbd5YbMdhHSLoqx7g/PTOfO+zIoLnhCUeeFlHMe2BhjmUs//RuSL0lIsiNZmByk94GaBdlTugSHLvJwpM0Kii5bqHrsyXhHZPU7IbvfBTkGVxztFVEuuGP9+kAW4o03A9lwyqj3ZmHSz3sj7KhjH/30CpI/SUSyN3meMDmi/zKJBdn7qTniG6yQqrNhIdR99tAYnHF0QIDcQTfkDgmhqPfAKy8HsRBb3wmE5jLlRKsv1ASTTh5JPU+FoM4XOzR22sfqEaZqtd3bzyb7B+VLkdTMh6LTjiAonAxOyBlwRt6wG/KHxVRqpXhp5TTEu3tlyKUZLLfDF9mU3AyI4gIDQl7ReiC7+VlEFgoL6BWujyVHmD7ScCecBfmochnNV7bI6LGjcHIgCAGODbsgf0QETYsHXnp+OiciEgJwot8XBb1+OKbzhabZh5Kckp0g0urIIwSsvixFUfcaRJdIDjyWqsV09gtfvMWCRFVZIO0KE06OyB50IghXFIy44cSoBBs2TefEnqQAFA754LjBj+0XWeQNJZXdNAqplFqPaZAGKTSt7sihPlQ98s6/Uy/5b+JATNdHmFmr5uYGFiS6mgdlpwOyKZxyhwTIvybE8TEREkqne8Wq54IRmSxD5JEAvJ8YgPBDAdgV7Y8dUf7YHeeD5Bp3ApFC1UggOilNAFLk9Utw7vrOf6bXi58zKQgz/VbeWM2CHKy1oipFnhhyQd4I4w0xTl2XYNdBfxbkf+nVtYE4Uk3jjFaCDOr6WTp3ZNOMdqxfhLxBMc4Mr/l7WqOzlwlBxPLSyVB2aDxUZ41sgwB51yicxoQoHHPH6QkpPp70QvGED4rH/HBqyB/5nX7IbPJBaq0nkiqlSChzR0KFGIerJCyIqlGCTJ0I2d0iKtlCKt0CqnxC5PT6fxXfYu1oEhBNt5h2iIEsSJyWT95wxfFRNxSOS3B60gNnjJ4kH5Qa/fDxmAynBmRsgqsueSH5Mw8WIu5TCeLLxWynT7kghqJRDHW7G+0shcjqdYWmzxnqXpoMyNuHW+3GY7VsKZ5nj3QEyYuu+7MgCRdtkHeVSW4KqXGqOhOeKCGIEoIonZThzFgATbYy5LT5UGXyIg9IEV8mQeynYsSViQhEhCO1QqTVU7+57MaOMxkdAnZOU+jtkdrBR0q7DeKbLDvf0rIVbP7W8cvbAou6NiPl7EocKJMq4s65hCU28MMSm/hhSc22u5MbbHcnNNhHHKq23/tRqd2+8Hx+zJvp1kkbDlqlr/uAp167h5f1MumVSF7m+iieckMsL2VLikXi9iyLmLCTvKi9pbwPqKzv2Ve9LGJ/7ZLd+2pItWa7o7XLPOYVpLz8mHWnvhuqjCxoKqLEc24v5CrNYq4z80nOJHeSH2k5KYRTMCmA5EOSkARcR7c0eQ+ZBim37u3tgzJDg4Ligh8DMeOMYeYlWw6GedaDm3CZox1PDlDI3bfnGqHFrBnLtCB9fQxIJoEUi3/gkdleWcrB8DlDnbn/vID77MR9z4wkVhyE6b3xkCAzMObc3MTjjLXmwKy5v3kcwFLOE7MhTAtiMPSzIMU/DLJglhEzYbaYM9L8R7Rk1oBoeogZkIGBwf8HMhdmtszmaO5900PMgAwNXf0pIHOBfooe32JAhoevPQzITwVa8NhBRkZGHgXkl7UYkLGxsScfpLm52Xp8fPzJB2HW5OQkB1LxZIPcuHHj6QC5efPm0wFy69ataZCKJxzk9he3nw6Qu3fuPB0g9+/dY0EqnnSQqamppwPk/v378wEyM2OZPcTstXCec+Tuz8mRGeOXcBuupdzVfNbmynzWHsZszmH2ovmrWrcfuWot4ozjcTtEG078H9g1Ws7ZPc7Azd+x0Pd95OEa4iLOIGvutMSR27MzVwfuO3vuassBWnNAy2aBzF94PUJnN+OMseEMZw4fXLmry5zDiNlg/Fke+T6X/gvUHPhuXhqFEAAAAABJRU5ErkJggg==',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKEUlEQVR42u2Zd1CUdxrHgyhSl7qUhYVlWXpxvMRMim4MiSYXO1juollQc1gQCChL7wtSBKRaQZAiTYqR3pbeBJYzdo2Xu8tMMnP3f/65+97zvrze7Dg6c/FYE528M995X9733ff9febpL2+88dv22/br3ZJveepEtJoJI1r1hCEdb6x8dUH6+dL8SSck9/IRWKz3OLjSdD601lQV3mCmimrjq2KuW6ni221VyV1ilaLPTZU5sEp1SrlGlT/8vqp4xFd1Zmyj6vzEFtXFKT/Vpek9pN2qytldJD/SDlXl3HbSVlXlPKMtdLxZlTPkdSO5y06ypCCKfhtpyawH0gdsIMs3wJHzFjhWaY6IekvEfi1AcrcDFL0SZA6449SQD/JH3kLRxLs4OyXFxRlfVMx9iuqFLaj7xg9Nd/ag9d4+XH90AJ2PD6Hvb8eg/D4cIz/IMf5jLMZ+jEb7431IHXD4fMktkjkklJ6dW00g1pCdNsDRixYIq+bjeBMfZA0kdNogpVcIRT/BKAlm2BuFo2+heOIdnJ1ehws3PsSl+U9R9efNqL+9A813dqPt4V5acCB6vgvCwPfHMPxDODr+vgdNjz5C9rCrXCOulTnkKC1TrWVBAgoMEFxugfBaS0RetURChw0Su23IKrZI6bHH/mh3bPzYB+tW+8D3fR/IglejoG8tWYZg5jagZmEzGm77oeXuHrLKF+j+6wG0f+ePK4/eRuO3H+DkkLhEYzFyashdWnXzExYksNAQIRVkjStWiGmzQVIXQfTYIaXPHts/d8f7q1Zh01YvBB73xO5Ab/rbBxvWr0Ju57u4MLEeFTc2EswmgtmO+nsbUH3/TVTd90bDo3XIHXVv1Wiwnx7zltbd8mdB9hcbIuyyFSIbrRB3neKjR4BUAgktlLAQ/jIPZPSJcbLPmeSK4Awv9nxAyGoUK99F6Shp/HconXbDhQUPXLrthdoH7yBvzGuisEPCZEQtDYKsll69I4OCBdFHeI01oq6SS3XYcrHhgIPxrvBd54OYChdk9hNEjysyOt2Q1rJopc8+80Lq145I73RETr8TisZdcZYSSMU3b9Lxmkcx5dZ8etUyjYIUj70nvXb/CAuyt0ALhy/rQ97MR3KXLdLIpTIGRDg56ISsQQmyBiTI6KVU3S5CQosQoaX2LMjmne6Ib7JHSpsYmV3OyFe6oXTSEwXKNf8IKhK40Wt0NA8yLZV2PzzOgnxRtIxAViC0URfy6/qI7eIhoccUSb3mSOoxR2KnORLaKZs1WeNQlhAbfD2xlgL/cJYIsbUOSGwQQ3HNGTndLsjtefMnWZr5x/QKIxLjVtoahTk77Ssd+DaJQKwgK9HG0RodRLToI6bLAHE9xkjoNUFSvxkVTHOkdPMpXhatwGjtaooPuQTR1UJEV4kQf0WMpEYJ0ltX/dsv3vggPV5AMiHpkZZrHGT4L5ksSOCZ5Qip00PkNUPE9/CQ1GeMlH5TpA2aU6xYIrXfChHnHbA32AV7Drviow8Wg90/wA3RlWSVajHJEdsiDDLp0R4kIcmMpE9aoXGQwcepLMiBcyvwVaM+otsNkdhnguQBM6QNWCB9mI+MYWtkDtlQvNhB0W2P1A4hkptF2OLnwcLsl7sg5rIjYqskkJ20UNKjvUkikgXJQONxwsRIzyM5C/LlBR0cbzZEXJcxC5GqNINCaYmMEStkjwqQNWqLrBF7ZDMJoFdEmUsMeZnzYsBv80RUhRhRlykRXHHGvgzTenq8mGTJxYmGQShrtd8PZoM9qGwl5G08JPaaEoQ50pR8ZI5YIqbOHmGFTsibFCJvXITcMTFyKYtlUw+Wfm0R5PefepJ7OULOuFitCIrmVfhTgW0uvcKaxNN4wDN1pPluIAty+JIu9VcmSBkwJXcyx8lhK2RNWGPbbjd2samtYpyeEqNgQoLToy7IU7oi+sLitR1/dGddK4pAoqvskdDogNNd63GoyD6MXmNM0tVowDOVvf72LhbkaKUe4jvJnYYskDFK1hi3Qe6kAKH5i5V9J7UppwmiaNIFBWNuyO5yx6ZNiwEfmuPMZq6oSgfICSSunmKoTYiyoV3/Crkg9tN45mJ6reqbm1iQY1X6bM3IIHfKHrPCqUlb5E3bIX/aATtlHpwLeeHgCQ9qSzzhu9Z7sUUJd0dinRgx1QRR4YAoAokhkCQqmopOO5SP7vkptFzwnkYzF9P9Xlr4gAUJrTWkLMVH1pg1cqbIGlNCFN6wR9GsI+0lCFaQBT7zYYug9G0fbPf3wld5rkhtcUJcnSMLEnlZSBaxo7iyQ3yLHZI7BEjvs0Ph8If/jKzhO2sQRCg9N/8W2zR+VWeEjCEr5JA75c/YomDGAUVzIpTOO6FkzhmlM64onnRHwbAbTlHTmNnhDEWbBElNYsRdcSDXIoswIJftEF0nIBBqPDuYecYSikEBNZyuj0MaDPkaAVH0C2lC9GRBIhp4ZA0bcicBCmbJEvOOOKMSk5xxTsU0gu4omaA4oV4qm9qQ9K8lVEvESGgga1CLEl2zaI2oWlvENtgggRkFOq1plrFEcp8ZjQN8an1MZ4POsQVyiS3S7S0tvuHGgpxoNEbOOFljmlxqVoTiOTHOMhALrjivcse5OQ+aDD2QP0jW6HRGWpsTkq6SNepEiKmxJ5BFiOgrAsQ2MSBWSGy3pOHMAgndZlSfeIjtMMbxFoPeXQ1sKl66Le+av2dx33bEVryHkIuixIgaa1lkM08W2cqTydtMAmKaTQJONJvtj6w3OxBaZRYUVMoLlp3ihf0h2eiEf5RR9LYIw7jNYYbxm8P0WW09YRCzPUpPvjtFL/zzHL1jB0v0DwVd1P3ySJXu/uDaFQFHqkm12gHHGnQdlxSkrCzLqLenH8kp6VCUHxU+dVmLC05trjLrczWBqda2JGYxTAC7cHLmztlx9xhzv9HRePdbVlZmNDioRFKKArklucJn3PIEZrkaDFOpzUlWXIdrx0nAnTPn7nk5EE9AlEoGJI1ASp4H8jQMU9wMucWach2uGXfM467pPQPiFwV5GkabK2w6XNuh/5R0uWsrXhrEE5ChoWEWpOT5IE/DPAFazi1YXcvVAF4OxBOQkZHR/wXkeUDPktZLA1AHGRsb/zkgz4LS+kUW/zTIxMTki4L8ejYGZGpq6vUAmZmZefVB2trajGZnZ199EGabn5/nQMpfbZCFhYXXA+TmzZuvB8itW7cWQcpfcZA7t++8HiD37t59PUAe3L/PgpS/6iAPHz58PUAePHiwFCBPGkbtn9FIai1xjNz7f2JES23YYoaqlWrD1Uq1IWy52rCl/ul02dJlrTsvnLWWcYtjJkMj7mODMTfu8rhzhtx1A26vxwHqqA1hS1xHfl5BXMYtyIib1Zl/6PC5vbnaLM/sTThAIw5IVw1k6dzrBSq7NrcYY27hzJcTG25vze0tnwHGU7PIf2PpP4wT+XCP2l0CAAAAAElFTkSuQmCC',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAJwklEQVR42u3YeVAUdxYHcBVBuQUcrpmei0uC6CYhp9kxMRHd3WhFjZpKFAhlYhRB7kuE4T4VUEZALglyCIii3NfMICMCAuMSI0Giu9mjkqr8v/vP7ndfN63FWsmm4jJUpNJVr3pqGKb7M+/3+73362XLfj1+PX65h/Kul0n4VVsm/KopE9yxbNWzC+kXKPJvuUDZK0BAkenDoGqbyZA6G31Yo60+plWgj2tz0Ce0C/XKLrk+rW+dPmtgoz5P85I+f3CTvmhoi7542Fd/fniHvnxkt75qdD/FPn31+F6K3RS79NUT71Hs1FdPsrGDXr+rz9Wuv63sErkuKCSt30mhGn8O6QNO8Ms3x5Hza3Gs2g7hl+wRf90Zym4J0npdkTXgiTztBhQM+aDo1msoHVOg/PbbuDD5O1z840403N2D5ukPcHXmINq+DkTnw8Po+8sxaP4WhhvfRuPmd/HQfReL9ocHkDIg+XDBM5KlZRQlE88TxBF+BeY4Wr4Wxy8KENEsAGUDJzudkNzL0N9dka31xKkhb5zR+UDFYyom3kL1ne24OLUDjV/uwpXpfWid/YhuOAA9f/4U6r8fw+C3Yej46340f/02cgY9og0ytLK0MkWF/g0O4l9ojqDKtQirs0fUZXuc7HBCYjdBeoRIGxAjS+1CWVmHfN1G7Nj5G2zauBFltxWonNhCGF/Uf7GDsrKbsvIBOh4eRPc3gWj/5n00PHgZTQ82I1MrVxlsjuRpPRU1U9s4SMAZCwRfoGzUOyCu1QlJXWw2REjpFyNdLUGmRoYcrSsOJ3lxCDaKb72K0pFNqBzbghr9NtRPvYvmr3ah6b4vamdeRM2MNxof/BandJ5XDTrZC3TeCnZ8s5CPiyxw/HMHRDU54EQbzY8eZ6QSJEMtRuaAHFkaVyQ1u+ENn42PIWd1L0A19ApKbm7C+ZHXUTr6IkomPFH5hReqp9ejYfY1FOjWDzfS6mhgyPOKy/f8aOiwEDOE1Toi5jINqQ4hUvrYuSEhgAxZWjlytW7YuXs9fN/eAN93vDlIXr8Xsrs9kNElR3afHPmDrii+vQ7lU16omfbBubGXZjNahHZ0qeUGhRTpXldcmznCQT4qXI7PPjdDdIsAyi6aFzSkMjVSgrggd9ANRzOe424+qswN27fPDa+ka0IkXmGgbKXVrV2OvAF3qIY9Ua73pvPL3ydcYVzoMisMDxlVKLpnIzjIwbMrCGKMkKbViG4zQ3y3FRJ7baDst0N8iwMUr2zAvk/caLg5Ytu2OVRCE4O4ehFONEigbKHh1+WOM4PrcHbwpX8eLxW8QZcwWRRIyegWxcCDJII4wE9lhKO1Jgi/Yoa4LnOc6LUmyBooB2yxc78H3trkBSXVlpRuZ/j6zkGiaxhEVYkQXS3ByXo50q+5Ibf7hX8HFNqwtcKKhxgtCmTwT1kcJKB4JYIbTBF1zQIJPVZI6rNGcr8NDueIuZsOyhcjtZ8gHSL4bp2DRFYxCCsVI7JMgvgaORIb5fgw1UpJX+1MYU2xiocYNissRP0whYMElhojtMkMse0WSOyby8TJq/bY/Jo3dh+gCa12oiovpHkhwtZ35iDhZQxCSySIKJUipkqKpEY3HDln30tfLaJYQ7F6USDsHOn5OpqDHCozQUSLBU50WSOp3xYpalvsOuiON19Zz918Wp8zkjtFSGxhHkNCS8QIPSdBWLGEsiJDQoMcmW2e1OrYVdPX21CYUqw0PIRWrfaZIG6yf1qxCtGtcxM8WW2HVLXgcb34qWAxkWVSxNfJkHpdjkK1D6LqZal0CbNFgbB1pGU6gIN8VrWa+qs1SB6wQZrWjloKmjexLhSuCIxyg1+EOw4cd8eHQZSlV+fqyN5AD7wf4I7gs2KEl0oQVyNF8hVasnvkKB/eirhm2Sd0GeNFgHgrLn25l4McrTZFQqctUjVrkT4oQKbOCTlDFDeouveJkdbBUM2Q4ES9FO9smasjIWcltAgwOFYoQsg5MWLY1euyFBmdUuRqJKif+Ohfyk7P3xscwvZaF6f+wEGO1ZghqdeOEPbIvuGA3GEhcm8KCSNGtlpKEAn92jLE18uwlYcEn2EQVMBwmOAiESIrxEholCClXYrsfinybogJc+AfynbGx6AQtvuturOZg4TUWSB1QICsIUfkDDsh7xaDfIrTN+nXHaRi1+OClFY5kppcqE3hM6KijBQyOJIv4iDhFSLEN1Clv06NJmUxSytEno6h5nLL98o2B7nBMOx+pHTSh2saQxsskUHzImfYGadHhCgYlaBwTIIzI9RD6VxxWuOGnB53ruglNbog7nMZVz+O05AKUYlwvFiEiKo5SPJ1EUFEHCRz0AGZQ0LK6roHcX2WdgaBpPUztEP04iDhjVbIpnlxetQZBbfFODshg4qNcTdq/jxoZ7gOhdR+5PV6IP26G5TNLoivlVFllyC8nEFEpQgxF0VIaBYhud2ZIEJkaJyQrrGneWdHO0MBYjpsxsMauSV5gTPS7a0oom6VhUQ2WdOccEb+KIPC21IUjctRrHdDid4D5/WeKKUtserWcyigzVVurzsy2ly5/upEgxSxF8WIraVsXBLhZIsQSoKk9jgitc+BugMBdQm2SKDeLb7TGhFXzXv3NnJFcuGO09f2eBX1vYf4C68juFyaGF7r6BfVYuUX0WLpH926xj+uZY1/ZIvtx1GXbAND62wPBVfaHP6s1OZIYL5V8MF0q7APlJaRu2ItY96LNo/bFWUeuyfBPGp/mmmof75F0KEys0+Dqs0Cg2tXB4RcWu0fVGfsf7TW2O9InZH/scbVsgWFVFRkW/b29EOZnI60yqPME39ezscKvqiZ8C2HOd8QsuPdgUJIIaZg/9+Jf9+Kr+qGryFzkApLtVqDpOQ0nFKdYn7gI/MxPwZiWxFbPtj+ypJHmCxKn/UIotGwkFSCqJgf+diTGKMnQGY8yoyPVXwmFgfxMyA/BTLmUSb865XzPmN4xCOIVjvIQVQ/DflfqCdj+aIhHkFu3Bh6Gsh8zI/FskWF6HQ3nxbyyzlYyPDwraUBGRkZWRqQsbGxZx/S2tpqOT4+/uxD2GNycpKHVD7bkDt37iwNyNTU1NKA3L17dw5S+YxD7n15b2lAvpqeXhqQ+zMzHKTyWYfMzs4uDcj9+/cXAvKo4zX6GZ3w8gWeI1/9P3Pk0c0b87vFVfzZZN5O0WTeBsxo2X8/1F6xcKvWvadetVbwN2fG79Ot+bDig33P4omtsCkPfIRbuMdCj+vIzyuIK/gbsuQfPqylEPBnu3kPJGz4BxLW/GfN52Vs5YIOr6eo7Eb8zVjzN+7APwZiz4782f4HYFbzMvJ4Lv0HHXr13/PCE1sAAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKD0lEQVR42u2YeVCTdxrHRaXKLUcIgYSQgyNc1inVsdpYtV7r0lpdtYcV1NoqhzaAIGcChEPlEAVPQFFBjoByg9yXyCVR12LFo7Pd3Wln9//+tfvd53196WQcO912CVudvjPfSeYN8/J88vye5/s8mTXr9+v367d7ae77vBZx3U4Ucd1MFN40a97LC9LBU+bekkHTxkNwvtnT0BLb8QNltnpVpZ0+ppanj23g6xMaXfSaFqle2+6lz+xcqM/qelOf271Mn9+3Sn96YK3+3GCgvnBos/7C8HZ9yeg2fcnYVtJm0gf6ktubSO/pS8YZBdL7P+qP9fiOalqE8mkF0XYIlAVj3kjrFGBnrgX2n3NAWIk9IiocEVfvDE2rGNo2OTI7Fcjq8UduXwBODi7FmWElCkdX4+L4Bly5+x7K72+B7sGHqJ38FI1PdqPlmy/Q/m0Yuv+mQt930bj5fRwGvj+Mxqc7kNIp/njaM5LZI1Keub2IQJyw87gFQgodcPAKD5E6HigbSGwWQHNDBG0HwXQTTK8fTvQHoIBgzo4oUXR7JUrurEfpnwNRNfEBrj3YhtrHO9D4zS60/eVzdP09DL3fqdD01+3QPV6No72e0UY5Wpk9EmWRfjkLEpRngdBiB6jKHHGo2hGJTQIktRBIqwtSO1yR0SnDsW4vZPf548TAGzh1awnOEUzx7VW4dHcdyr96D9Vfb0Hd5IdoevopWr/dg6Zv/4TyJ4tR9WQFMnqkBUarkawehfLyvXUsSPAJS4RfpGxc5SO2VgA1ZUPdJMSy1/2wbOHCFyqzZjHODS/HhbFVKL27HpUTgah5tBnVj9eibDIAlyf9UPX0bWQPKK4btdiPD/gpmfPNgOzKt8TBS3wcquIjvsEZ6lZnRBWL2YDf3+aD4CgvBJGCoxTYfcgbn0X74WjTIpwaWILzBFN8ezmK9AEovOeNiw98cPmhH8ofL8XxAd/BSuqORgZZpKye2AktC2IOVakTYqrpSDW6ILlNhH0Z7ixIeK470lpkJDnSmjxInkhvUiCzRYFjbZ44Rscud0CGghEPFN1VEIgvm5HTo4sf5bQI7YzefvMH3lLWPdzPgnySZ4J9l8wRXcOjY0V10e6KHeGeLEj8VTlSm6hN10kRXyNGbKWI5Iz4aups9SJk3HBDdq87To0oCMQXJRP+1AyW/CO1VSyhf2NifJBhpbL1USQL8unJ2QRiigOV8xFdb47YZmv8YbMH3g7wo5qxQ7TODlHlDohkutoVPqIuCxBbLkTSNVekN0uR0+2BU8PeKNT74Pzo0h9idfwl9C9mcyDGhTkzvErZ+URNIHzsLJiDkNLXEFFjjsPNFohtscGKpT54d7U3tn4hwcrlvli+yB+rV3jjozA3RFxwRvRlEZKqXKFtkCGny5PasoI8ZvG/o3WOm+jxTF3MMYAxLkjvN5ksSPDpuQgvN8OhWkvEt1ojRmf7Y3da+Y4vtuyR4oMgOd55y4e9F7jNA9GXXJFYIUF6vRw5nV7I6/Okzrcgkh5tMeMgXU9TWJDdZ03xZRVlo8ESiW0LEFHCx5o1Cmz62B0xVeQtZTyoLvARmi/AmrUKFmZvkhRJlVICcUdWuwdO9vsgtsa5nh5tOaMgTI3ceBzNgnx2/jVE1lA2mm2g7rCDpt2OWjAPiY08xF9zwiHyF9VFAQ6edcZuteuztrzdG0kVMmhr3ZHZ7MHWScGgH7Vw3nl6/HzS3JkBoa7V+DCULfbPi+YhutYaSTdskdxpj+R2HlJpmNS0OCHhugCHKwSIKnGGqsgFX54WsSDr1vkg8aoMydVypDe44yhl5Xi/B9OxkFAvVXNZMT4I4yM1D4JZkH0X5tN8tQCaDltou+2RSMGrzguRUCukFutCrdYFMWVCti4iz7uxIBs2eiOhTAZ1pQwp12TkLXJkdZCn9MtxcWwdkurke2bkeDHOXvHVVhYkpMQMCc12SO12QFovD0HRMjbYz1MoyGYRkutESNCJkVDhhn1pHuxnH+5VEIiEsiJBkk6KVPKZjFYZsrqlyL3phso7O/6V3Oq9weggzKx15d5GFiTssjnUbfYE4YgjfTSmlLti+esL8e5KX6TUSZDaKKZAJWwG1qx+1oqjTsmQcJVMspTul7tBrZOQ60to7JeQQUqQc1OMcv2OHzSNogCjgjDT74U7K1iQA2WWSO3kIbPfCUcHBTh2U4jgyGff/CrykE9CFfh4nwKrlvk/61jxXuQhMiSWSxF7RULu74ZEylhKPRlkmxhHesQ42i9E1k0Rzgyt/qemgS81IohIeXY8gB0avyy3QnoPnyCckTPkgtxbrsi5JcbBLHds3Eje8YY/lG/6I3CTLw5meyKlRg51lZQyIUFcqZhVYpUYybWuSGsVIrNLRJl1QUafE305LjjS5fUktt3K3igg2g4RbYg+LEhEpTWODAiQPeSM4yOuyBt1w4lhCfIGqXj7yCc6PXC01QMZDR5IvS6HplrGmiFzpGLLCOQqmaNOxM5eWtphMrpIvQKk9/Kp7uxpM+Qhpsl2TFU5y2z6M9Lqp8wf9WJBoqps6Dgx2RAhb4QgRiQ4OSzHiVvuyOv3RHanJ+sVjGdodAwE1QYVeSxl4nCpK4EwzUAIdZ0QqbQCaNsF0HbxkdJFQyh5UgJNC3HkUZHXLdq2VrKdbPqunLotPvntmxB38S2EF7olRZQ67YzUWQUxUlUt2KUqtdkTUmj7xZ7j1mEfpVtFbEuwink/yjJ+Y7hl0voQSw2jtaQN4eZJgSrzuC1xFoe2p5kd3HXCcv/eIvO9oSXmuw9UzA9iFFpmGhRSarpzf9mcoLDK+ZJpBSkqOmLVdqMDmuQ0aItDRNxtE65dzuEMjRk3mJ3CmcQUrILkT3qdE/Pem+ROYp7BI1nPqLMXFRVZdXV1Q52sRXZBtiHIFIwpyYwLzIGDkXBBe3By5+65cBA2JHMDV58ZkO5uBiSVQApEBh89nxVzDobJDJ8DEnLBM+8duc+sDSBmZmD8GZAXwZhx4zkTrC0XOKMF3D0L7jjNLMQUSE9PLwtS8GIQQxhTLsj53Lc+JTMDANPnIGYOpK+v/6dAns/MFNBcLmBDzTUAmFmIKZCBgZs/B/J8dn5KJjMOYAgyOHjrvwF5EZTJ/zX450GGhoZ+Kchv72JARkZGXn6Q2tpaq7GxsZcfhLnGx8c5kOKXG+TOnTuvBsi9e/deDZD79+8/Ayl+yUEmvpp4NUC+fvDg1QCZfPiQBSl+2UEePXr0aoBMTk5OB8jUvDXnF8xhJtNcI1//LzViYrCrMDvJPIPdZJ7BDjPXYFcx3ONnT1/XmvjVXWtqp2eWKytuV7fhtkVr7p4l97mFwRI2zwBu+n4W+tFHfpkhzuYCsuLWXgfuhwfm1d5gFbblVmEb7m8N1+G503q8foWzz+GCseECZ36MEHCvTtyr4wvArA0y8mMt/Qcsl/ljPS94DAAAAABJRU5ErkJggg==',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKDUlEQVR42u2YaVBUVxqGVURZm81u6I1eZRHBuFY00hr3lGLcAjNGwS1RdllsoAHZN8ENGhEVEBoQQYgoCM2+CohIq6MobjOZSVVSlf8zf2beOfd6meqinJqJQzPRyql66946p+ve7+nvfMu5M2b8Nn4bv96R+MRtTvhNW2H4TVNh8J0Zcz9ckHa24uygDImtbBxQm74JLLUZDam00YVV2+qi6tm6mAZ7XVwjX5fYLNWltrnoMjsW6XK6luvOdn2mU/eu0xX0b9JdGvDSXRnapSu556Mrve+tKx35imgX0U5d6YMdRNt1paOUvMj9Nl1298L7ic0C+ZSCpLZzFfkjC5DWwYXvWXP4X5qHoFI7hF/nQHWbh0StCKmtcmR2uCK7ywNne5chb2AlLg4pcGV4Ha4+2ILyh16o+sMu3Bjzwc3n+9Dw6hCa3hxF25+D0PVDGHp/VOLuTyr0/xSNxjf7kNwh2jvlHsnsFiouPlhMQBzge84cAVfmIbScjYgbbBBvIL6Ji6RWIVLbCUyXK3J63JHbtwzqgU9x8Z4nLt//HCWjW6B5tA3Xn+5E3Zg36l9+TQw+gJY/fYuOH4LQ82MY7vzFBzdercepHmelQbZWZrdEUaRbTYP4nTdHYPE8hFVycKKWg/g7XJzUcolX+IiqlGCP7wKsW+0Bz8Ue2LR+Eb5VLUF+72riGQLzYCMqH3mhZmwXvnvug8bX+6H9/hAav9+Da69WoOb1GmR0S/MNFiM53a4KzePNNMiBXAsEXyXeuGaPmHouEpoJRIsAoYUSKJa60/I+7Abf4wuwZYs7Plu0CHv2fkJgVuHywFqUjmwiMFsJzA5Uj29ExfhSaMbdUf3aE6f7XG8aNNjP9bsrqp7spkEOqi0QWmaPEzX2iG3gIUFL1CAkXliINSs9EF02H2ktUqRp5UhrdsYOHw8aJkq9BOrulSjoJxpcgoL7rih6tABXxxbi2otPcaZ/4UDuHflcA4MsVtSO+SKVBjFDWIUDomrJlmrk07FxLENOG/tNIoFolhHJkdrohORbTghXu8Lbzw2hOc5Ivi1BhlaC011yXBhyxuWHbih7uhTqoeWv0uv4duRVMw0Kou5fpbg17k+DfH1+Jo6VmUFZx0ZCEx8pbY7Ytd8VqxcvQlK9HClNJE3fkkJVK0LUNQFOVHChJIqp4iO+1hEpDVLktDtBfdcVlx64Q92/4udoDZ1mjYhmGRRGfU+h0L6MoEH2580iIMYIqTGB8rYZYppZ2LCeBLinG5Qki3n7S/D5ald8tsQD6z93w++DxIgo4UOpESC2SoSk72TI0jrhfLcL8rqX/y34AtuTvMKEaLbBQS7eW6foeJ1AQOzhm2+EgIo5CK8zQ3STOQGxgucKD6xbswAbN7li7Sp37PSTYfcBGdYSOGrLbfdxQmSJANHlYiTVypBBtl1Oy5J/+Gbb7COPtyYyIzKeFpCeP2bSIAcKZiO4yhQn6i0Qq2UhXmuF1Z8sog3esIHEQjEHoVc5CC60R0CeAw1HrR2KkyNGI0FCjRzJxCt701gJ5NECIlsGZM60gHS+SaZBDhUa43gN8UaDBeJbrXGy1ZYY+jbNfpPuiMgKNkKu2CMw3wH+BORgvIhe+9LbBSqNFCevS5F60wkBFzkt5NGORFSQm08LCBUjLa+UNMiRy3MQUUe80WSFhHZbJLbZYs2nC2ljw0t4CNdwEFTogMA8LgJyuQg8J6DXNm1yQ0yZFLGVJBmQ7ZXZ6IpjBXZl5PEcIksGxLABT2WtxvFAOti/LZoLZT0LJ1tskNhhR0Dm4Quvt9sntICPyDIHhBQSCDUPgef5CDonpNc2b3ZDdCkFQrxSI0XKbSlOty7F8TLHdPIKFhPwhgWh6kjdswM0yLESE9JfWSOx3QYpXXZkjoN9YTLaWN9IKSI1XIRc4iE4n4+QC0Icjn9bY3b7uhAQCVTlEsRVSZBYJ0baHQku9qwnXhT7k9eYGjxzUZX9+tOvaJCAUlPENdkSiHlI7WEjrccBcTV8urdas3IhQvIECC8WIKxQiNB8MTaSBEAlg6AcGdlaYkRrxIi9Jkb8DTHxiggZrY7QDP3u77E3XLwMnrmoXqv88VYaJEhjhoRWOwLAQVavPU718Uijx8PRFBldFD2XkfT7tQv2HHSh2xbKGz5HnBFTLqWzVrRGRO7FiKsWkcIpQnqzCKc6HVE+vPevymrBCoOCUN1vycM1NEhIpQVSOtjI7HPAqQEusvsFyOkTIqtThLACGbbvWQDFcnfiBQ/aG4einWgIVYWE9gYFEl0hRGy1EAn1pNJrhUjv4COrR4gLd9f9HPcdR2ZAEKGicHQZ3TQer7JEejfxxAAPZ4b4ODvoiDODIpzpkyCrXYr0xrdZKf6aDLEVBICCIHFBeSFa44ioMkcC4ojY6xQIaXG0AqS185DWZY+MXj4yO11eR9y2nGcQkNR2ITkhutEg4dUsZPVzcXqIh3PDjsgdESNvWILcQTk5Gc5HdpsTXbmTauWIr6KylIT2Bg1S/tYbqioh4moFSCCny5QWHvE0F6mdHCR32pGTIRtRDTYjYdV08E+xR7TuCvV9FxokssYK2XcpbwhxfphAjEiRPyJH/jBpBAddcK7HBdktzki7NR+JN2R0hlJVihFTIaI9EVNJQKoFiKvjk/afh+RmByS32SOpjU0X1zjSLajuWCHipnnrV9V0Op66cebWbjd12w6orq5C8BXxyfAKB98TdSzfiDpLP2WdtV9kjfXBkErbIwFXbI4ezmUF7c9ghe2Jt1R+GWGp2hZqEbc12OLkF0TbQs3ivCLMVDtizJXeSaZh+7Itgg4XmB31LzY74q8xORhy3cQvsNLYL6DC2Ne/0sgvqNpEMqUgRUVZlq0t7UhMSkNqcYBQb2kmk2WMmMpM9UxWTLWm+igpkRORMyMnZk7A/MZar88yfBtfVFRk2dnZhYSkVJzOPy2ctPwuGBbTDNoT8RjDBcy9PdNfsaYVYgKkq4sCSSEg+cJ3/GQyjAnTCLKYf92WkQ0zZ8FU8skQvwoQfZjZekCmzD8/IRNmzXhaISZAurt7aJD8d4NMhtEHMp6k2XoA0wcxAdLb2/efQCYD6UO9S9MHoA/S33/3vwX5d1Az/y/GTwYZGBh8H5Bf16BAhoaGPg6Q4eHhDx+kvr7ecmRk5MMHocbo6CgDUvxhgzx8+PDjAHn8+PHHAfLkyZO3IMUfOMjY07GPA+T5s2cfB8iL8XEapPhDB3n58uXHAfLixYupAJloGo1+QTM5c4pj5Pn/EiMTxhszB6u5egesucz8HL3zi9Gk78Czpi5rjb131prFGEedDi2ZjxNWzJGXxcxZMOvmzNWUAZyjdxCb4jryywriLMYgS+a8Tn1BZDNXO2Zu4ixvzQBaMkAmeiBTt73eo7IbMcZYMYZTX0+4zNWBuXLeAcbS88i/YumfHQrkaWRSeO0AAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAJtUlEQVR42u2YeVRTZxrGVQRlSVjDKmFJAqSITt1mqjbtqNVxrJ461tpOJaBHbV1QAUFBVknYkS0IKqIIKLIEUAl72GTfLoML4jqd5bTn9P+Zf2aeeXO9zOE4elo7BKun3znv+W5yyc3343ne732/zJr1y/hl/HxHzB1vo6BqK+egamPnAM2seW8uSItAlt4rQkyTAP4q4ycHCy1HDl+xZALLrJjjNQIm7KYdE1HrxMTUuzOKZi8mUbuYSW1bzqR3rGJUnWuY3K71zLnezcyF/j8whYM7KD5jCoe2U9Droa1M4fAnFFuYwhFdbKbrj5mU9oWDMfULxNMKomhxkOUMvQOl1gHydFPsP2eDQ4XWCLpmi/AbjohpcIGiSYxErRSp7YuQ3rkM2T3vIa9fhvzBtbg0shHFf9qC0jvbUDH+OaonfHHz0W7UP/0KzX85hLa/BaLz21B0fxeOru9OoPbJTpzSuvxx2hVJbHeW5Q2/SyD2kGeY4kC+DY4UCxBcIQCpgcg6B8Q0OkPRQjCtBNPhg6xby5Cjgxl4H/nDv8Wl0d+h5PZmlN/biqrxz1Dz8EvUPt2Fxj/vQ+vfD6Hj20Bo/roDFY/WIrnDM1Qv1kpsd5NdYFazIH6ZpjhYYIPAK7YIqbRFpMYBUfUE0uCEuBYhErQipLR6Iq1zEbK6luJM769xbkCGguE1KBxdj6sEU3F/G2omPofmiS8avtmN2m8+RenjFSh//AES2t1z9JYjqe1SWdHYBhbEP8sMAZdIjat2CKtxQDSpsWrx4h+MvJ5VKBhYgyJmA0pvf4zKia0of7AeVx4sQ9GED8oev4+0Lmm1XpM9o8tHpvO3DmSXygxHLtshpNwOJyk/ohscIQ+WwD/EA37HvOAX7MXO/sek2Obrw0Js2boY2R0rkHtrJc71rcTZ/qXIG5ai4LY3CscXovThezjdtbAnSyOep2eQd2WV9+RQsCAmCCyxx/FKslStE2KbnBHX7AJlkyslvBuU9SLEN4qhuOGBdesWQbZiEaKuShFf6wFlnTuSmt1pMxAjd9AL+WPeKBqnXOpb/ihe7WRNXzVbryCqrpWy6xP7WZAvM2fj68smCFULyFaUF81CKFsIhBaobBJB2SDCKY07dh72ZNXwC3PHiWsOOFm+ANFVQig0IqRqPWgjkOL8iA/NK74/UcRus3P0D9IvkzU8DGZBfLPnEIghDpfPR+gNE4TV8xFRb4nIeitE1FqT3awQRNZbvcQHH62T4liRA0KLnSgW4GSpC2KrREhu8ERWp5TstvyfR84KVtNXGFEY6B0mr3+NTPs4mkDsIM8xwIESIwRVEUSdKcIbzRHRYIHIRoKpsya72WD7HjGrxt44IUJLnHDs0gKEUpwodkVMhQgJZLO0piX/3pNl+QU9nk8xb8ZAOp4msiD+uXMRUGqMkOtmBMBHVLM5VXxLxLZY4VSjLU5W27NqrP/IG+FljgRCEJeFCLnogvBiN0SXU/5cF8EvlR9Bj7adcZDWJ6dYkN1nDXG03AQnas0Q1WSBGC0BaG2gaBVQ2GNPtDurxp5IESKryE7XhAgrdqFwQ+RVyh+1CEkaLwRetq+jR9tQmHHWmpkcaXwUyoLsOW+EYLUZTtabP4Nos4KizRbxHXZI6nTExk1SrFq6CLE1ZKPrQkRXuiKi1A1RpSJWjbhqCZKbJJQjCxFUKsinx5tQGM4MCO1atRMH2WTfd2EeQmvIUmSnU23WiGsTILHDFsk9jojTCFk1Pt0pRUILbcU33RGjdkNsJTWcFQRRRS2MRoLUVg9k9Xjg/NBvEKMRR82IrSbriHrcnwX5+uJ86q8sEKu1hLLDGgk6JbrtkdK3AEfSnyX5wURPJFOrklivywcx2ekZRPwNCVIaJcjo9ICqV4IzwxKUjG2EslHir3eIycp+7e52FuRAoTEi6shO7TaIv0VqdDsgrdcR6QNC+B6WsiCxFbr/uhgJ9RIoafE6OymqCaxOjBQt2arbA9kDHsgdFuPMiBvUd+X/Suzw3qB3EF2vVTy2iQU5VGSC6CZrxHeSnbrskNrrhNP9C5Ax5EqtiDdkSxezlVu34OQGqubXCYRqR/xN3WsJTreLqcX3YNVQjRDIsAjZ9Nny2/J/UGFdolcQXfd7cfQDFuTwFTPEaQVI6tLZidToc0bWoBCqYTesWU1F8EMfZNySIE3riaS6ZyCnashWtWI2yXX3svrEyBkSIWfEHVn0ufQBZ6T3C6kHW/t9XIOdmx5BnGVnR5axTePRUh7i2+2QwtrJCZkDLsgedkUuI8KqXy3Cpt9T+97thdNaL0psDzZHlDfEbL6ktoqQSSCqATFUgyL2c5lDLqyiyT32lGtOSGrzehzWzLPWC4iixZlOiN4sSFAZn9RwoC93pEUIkU0ez2XckTcqwVnGk5pBKVTdUg5EwkIob4qRRI1kGtkqs4cABnR2cmOV1KmR2ueIRLKpgjaPOKpHxzWWQ4Fls4ynX5EGH5mKulUdyLFyc6R0kxr9ZCnytmqYIBiCGPXEOUaKvMF36EAlRVqLJxJ0IAQRryFFGkUEIkJGtzsy+twoSIleZ/ZZibfsafMQIIa6g4hGPsLrzBFcbdq0vYzdlqdvnL6+zVvV/AnCL61EQL5rVFCJvTxEzZeHVPPloTUWfmFqC79jaqtdR69Y7TlQYPnV3kx+gG8CP3BHFC9kewQv5ItYXrBvCv/wvlzz/QEXzfcevWqx+2gZzz9YzfMLVJv6BalN5UfK57MRUGooP1BiKN9/xcDvUNn86c2XCxeSeE2NLYiJVUJRcMD5uduzuWI2l+uZeBQ6jztSCLnQXQu4vsp4xir5/4Jc4LW2tiE6VoG0nLSXgRhwPZMJt2Brrim05Xoqc+6e0YxV8heBtLXpQOIIJMf5BX8yVZVJGB63eHMOzIRTbO5rgXhFkKkw8zkbGXPXRlMgXh9Ie3sHC5LzYpDnYQy4RRtyMXeKnV4PxCRIZ+etHwKZhJkK9HzMfm0QkyBdXd0/BmQqzMti1msF6enp/bEgLwN6/UMH0tfX96ogP7+hAxkYGHjzQWpqanhDQ0NvPohujIyMcCAFbzbI6Ojo2wEyNjb2doDcuXPnGUjBGw5y7+69twPk/vj42wHyYGKCBSl400EePnz4doA8ePBgOkAm+y6DV2goZ09zjtz/f3JkcvGG3CFr3pTD1jzufaMpZxiD506Sc6Zv17r3k3etOdziXnT85XPvmXH3TbnZmAM0mnIom+Y68moFcc6UX1YsuR8hBNxszb1nxc0WHCCPA5p6PJ4+e/2Eym7ALcacW7gdhQM323Oz7QvA+FMU+W8u/QdWWhHGOs2zwgAAAABJRU5ErkJggg==',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKAElEQVR42u2YeVCTdxrHqyhyBBAkhAAhp8ohWlvqWsV4om0t1Wq11iqisooKaDiCIJBwqMghoiAqcsl9SAEBAUEIGBQBDWMtVjw66+5OO7v/96/d7z55ednJuO3O2iW0Ov3NfCfJm+HN8+G537fe+v38fn67R/3Q0zSs3k4QVm8uCGl5a8brC9LJlWfekUJ9g4uAbPPnh4pt74eW2+oU1Xa6qAauLrqJp4ttdtapWyW65A43XUrnAl1613u6zO6luuzeVbpc7Vrdpdt+usv9m3TFg5+TtuqKh7aQ6PPQp7riextJn+iK7+vlR+8/1qVp5g2qW11kEwqS3MmX5wx54PhNPvwzLXHgkj2Ci2chrMoBMdecoG4TIvmGDCc73ZHWPR+ZPd441/c+LvTLcXlgFYrufYDSYT9Ufr0JtY+2of7xTjQ/24PW7/aj40Uwuv+iQO/3SvT9EAPtD0fR/HwHEm8Kt0+4R1I0AvmFewsJxBH+Zyxx8LI9DpdyEV7LBXkDcS18qNsFSO6UIaWLYHrmIav3XZzTLkZu/zLkDa5EoY5gHvihZuRTfPVoKxqe7kDzd7tx40/70PXXYPR8r0DLnz9H7dPVSO2ZqzRKaKVoxPJ8nQ8DsivLEocK7KEod0DkVQfENfOhrHLGZ3vnYKXPPCxbOB+r6HV7kBdOtbyDbO0iXLizDJcHyTO6taj42g+1325G4+g2tDzfibYXe9Hy4jNUPluEmmfLcVIjyTFajqRr3OUlD9YxIAFnOQgpIm9U8BBdz0cUQegBli5YgA3b3LFDMRcbtnoyn9esmI+TjQtxtnsRzmuXoODuSpTo1qHqGz/UPdmEq0/XonzUGyWjXqh5vgwZWvd6oyb7Ga2XvPLhZgZkdzYHh6/wEFnNwzHKj637ZzNG74mVIbFFBHWTBOprYuyKcGOu7zw0D2ntC5B18z3k3lqCvIGlyLv3LvKGPVA04omSx16ofPo+zmjn3a6m6mhkkIXyqyP+SGZALKAoc0RULZ/CyhkfrvdgDFY1CqFqEkFVL0b8VxIcq5Qx1z/8yAuJ9W44fm0OTrRKkN4twbk7swnEHYUj81BGHskdXPTkdKuLndHLb7Z2ibzx8QEG5MusKQi6YgHlVS5U153ht8WdMTiyUIr4BjFia8U4WinCoSzRGMgGN0SW8hFT7YIEgk2h6na2zw2X7nmi8OF8XBz4w9+S2oRi+pkpxge5K5e3PQlnQHaem4qg4ukIqTZDZKMF9mXw4PP2Avh+4I79WTwcLrbF3hRHrFnjDp+FC+AfLUBYgROUpS6IrxHhRIsMmRo3XLjrgYv9i39UVvIW00+YkKYaHebC3VXym89UBMKDf44JDpaaQlFngahmS0Q1WWNfuiPk740l/Lh83vHCl0pXBJ93ROgFZ0QUuiCmQoTkBhnSO+bibI/3P0MKuJ/R7c1J0yYNpOe7FAYkIHcaQirMEdHAQUyLNSJrbOHrOxZea9Z64NPdYnyyXYrliz3hs2g+th0WIfS8MxQXXRF9RYzEOhlONMuwN8smmm6tzwsL0vRJA+l6nsiA7Lk4HUeqyRtNHMS2zsSWQCkDsXmvFApqkqHULA/kOCAwjQ9ffXhR2AXEiglEiKNFYqhqpEhpdoPiiuN1urU9yZJkOikg+hxpf6pkQALzTBF+lbxx3QZx7XZYscQTy7y9EFnFRUSZA0IucwmEh6AzNAXEujCQH1HCh10UQVlIxaBCiuRrMmR0euBwCbeAbs+ZPBCqWs2PDzHJvi9/BpT11gRhC1XHLCx92wu+q90RU89DRLkjjuTzcCjHEUFZfARlODMgvqs9xkAK9CASJNRLkdohQw51/eg6SQILYsKCTDFqH6l7FMCABBWaIfraTKg7bZHYOQurV3gyiX20hrp8OR+KAkcE5zohOMcZgYlCBmT9RneEMyAiHCsTI6FOgpPXJcjQSFE0uI7+CbLASalc+s5e9c0WBuRgsTliW+yQ1GVP4mJnxFjj2/rHuQRC1anICUfyXBCaLaBmOVYEAqJkiMgTUmgJEU0gqqtiJLdIkHZTjNNaEap0O/6R0ObxodFB9LNW6YP1DEhwiQVU7bNwXOOAk90OSGp3xnq/MYPXrvFkhsfN/nOxUu7FXNu03Z3xhJISPapQhOhSIeKrCaRJjBQCyegRI/O2EJXDO39UNwu8jQqin34Lh5czIKHlHAopLlJ6HZFyi6RxwYl2FwQo51AZpl5C06+P93x8sM4DeyJnU8mVIIrKblSxHkaEmDIhYmtckdjkipMdQpzqESL1lgvS+wS0v6z+u7qJJzEiiEB+8b43MzQeqbTC8W4eUvuckH6bdMsVqRoyql2EBBoWVbVjCX2sTIKYUlKJGNGko1dEOFpCvYRA4mqFzLhy4oYApzQCAnEe+6fQ66kut2fRHVazjAKS3CmgDdGTAQmrtqYf5CPjjhMy77ois1/ExHlalwQpbVRaG2VIqJUhvlI6BlQuJi8QTCmFVbkQx6pcEV9HHmkWEAgBaEi9fJzo4SGpexZthlxEtdgOKaqZjj/BHmnzkmcPujEgETU2SNU64fQdAc4MiJDVL0ZWnxSZvbORcXMOTrXOoTFkNtS1UsRVSgiGQMoppAgipsIVcRRWqnoBEppdaD12Ztbn4108JFLhUHXYIbbNmulR4fWWN7ZUM5Vs4s7pxs2e2R0bEVO0hBqeKD6szNE/vNZql15HKmbuCS22Cdx/yTZoT6Z1yBdJVuFbY6yiNkRwjq0P5cStD+HE6/XxYU6sX7hFzKZoS+W2JEvFjjRO8N5czv4DxRaBweWWu0OrzHbpFVI53f9g2XT/A+Umu4KrzcQTCpKff8rqRnsn1AnHkVxwUMBensKWSxO2oVmys5MjyZUkJc0hubHSv9cnsv7vHUgz2TnLdNKm3/z8fKuurm6oEpKRkZNhCDIOo59eZ7AwegO5JD7JmTVcLycWUp/INgYQkzP5joN0d+tBkggkR2Dw1TjIOIwpa6AVa6ydgWxJ1uxsZf4TEL8qyMueMWFHclPWWIuXZMZ+N/2lcDI+xDiIRtPDgOT8J8jLMONA01iDDTXNAGByIcZBentv/TeQnwP6OU0ugCGIVtv3v4C8DPRT+vWOHuT27TuvAvLbPHqQ/v7+NwNkYGDg9QdpaGiwGhoaev1B9Of+/fssSMHrDTI8PPxmgDx48ODNAHn48OEYSMFrDjLyzcibAfLto0dvBsjo48cMSMHrDvLkyZM3A2R0dHQiQMYHR5NXGCinTHCOfPv/5MgUg6XLjF2Lx5esGQbL2DSDpctwe5w6cVVr5BdXramscYYrsA279lqz1zjs95bsqzkLaGqwjE1wH3m1hjiVNciK3dnt2QcT9uxDCFuDfX4mC2jFApkZgExceP2Czm7CGmPDGs5jn6zw2KcpPPax0Mtg1gYe+Xcu/QtwXOQbBb/XCAAAAABJRU5ErkJggg==',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAJ/ElEQVR42u2ZZ1BUWRqGFVHJSmxC50bJOsY10ZY6hlFxDWsWRHZWEQFJTW5oJRsISlDJkpsgIDkHG0RE2jILhlp3t2aq5v/un913z71cXIp1tsZZmhmsOVVv3eZ2ce/38J0vHWbN+m39tn69S/bMbp5ftQHHr1qT49Uwa/7MBWk3FifdF0HWagzXVM135/L1h72L9ZW+cgNlUI2xMqSOpQyvt1DKmoTK6DZrZXz7UuWVzlXKpK71ytTezcoMxTblrX4nZdbAfmX+w8NEh5T5QweJyM9D+5T5j/YS7VHmD1NyIp93Ky932z+UNbEtpxQkut1MnDZki5gOM7gkaePsLSN45hvCr8wEoXfNIWvmIbrVEnHtNrjctQRJPStxvW8tbgyIkTW4GXmPdqDwsRNKn+5HxcsjqH7tjLo3bmh6fwZtHzzR9Vdf9H4XiL7vQ6H4Phj1707gYgfv2JR7JL6bI77xaBkBMYVLsjY8soxwvtAY/hXGIN6AtMEMshYOotstEd9JYHrskdK7AtcVa5Ax4IjMh5uQqyQwT5xQ/mIf7rw8hJo3J1D//hRa/3wanX/zRM93vmj4y2FUvNmCSz1WgSrZWvHdAnG2cgMNcjJFG+dyjOBbbAJJpQmk9WYIJ3KRiLB9mwMcly/FpnUOOOzmgOjK5UhVrMaN+47Iekg8o9yGkqdOqHh1ALUjR9DwzhnNH/6Ihg9/QOnb1Sh/uxFx3cI0lcXIlW4bccGT7TSI6zUdeOURb5SwEFJtBuldM+zeZ4v1S5dii9geR89Z45iHLbY4EqhVSxCWtQzXu1cjo28dch5sQoFyO8qeO6FqZB8qRreheGQlCkYcUP7OEVcVNtUqDfZkhYO49NkBGuRUqg7O32ZBUs5CGIkPZ4mQhthz0AaRtQLI6oWQ1QkQUSnCN7vsCdASJDR+hWtdq5ChWI/MQaJHK5D52BZ5L+xw+5U9SkfXIllh3y8n2VHFIMvElS9cEE2DaMG3yBRBFcQb9RbYutUOG75aijC5AJF1fMhqhIisJqqyxPkUaxrS/aItYusWI65ZiCtdQlzrX4RMpQ1yn9uj6PVKZDxcPZrYxDYgr5qtUpBUxTpx7euzNMjxlNlwv62FwEpjRDZa0DHx9WZ74gkCUitEeKUAIWV8SIq5CMhj0yD7nReR+2xcrOMhnmS3FIU1bg7ZIefpEpIMfvdD+B2OiLxGTfUgD8Ti5lF/GsT5uhrc8+fCS64BSa0WHFcvwSZHWwRW6cO/zAA+BfrwzjWEN0kInummNMg3TjYILeNAdoeP2AZLJHZZI73fBumK1f/wKzBZR14xb1pAbjzYLO54G0lAWHBJmwOPwnnwrdJCUL02du5bRBt7JsmEbDl9+OQZwJukZ68sExw5z6e/27bVFqHEQ9JyAaJrLXGp2QpJHSv+9adU/SPk8bpEVLcwR+UwFEjP+3gaxDVDHV4lmgio0UFogx48rrHoGNm41g6uUjY8SbE8m8bCUR8BuecwBrLNDiGFfEhLhbhIYieqRoTj8XoR5NHmRAuJNInUpwWk891FGsTt5lz4yIk36nQQ1rQQYY36OBXBwYblY0aPa+Mae7hFcMe21k7ikSIBwotFiCgTIqpqMc6mm7SSR3OJqCDXIpqrchAqRlreBNIg32bOg38l8UbjAkhbDCBt1kdYnRH8b5vAJZiPg2cFOCHhwzvdAp7JnI+pObRQiNACIUKIpHIhYu9a43S6YQF5vDGRzrTECZW16l+fo4P9dPZ8BFbrEQh9RLYZIrLZCJENRgivZSFIbgpJoSn888zhl8uGa9BYjTnqboXgPAGCcgX0NbREgAskRSe2rYBvAS+GiRPVg1B1pOqlKw3inquBkLsLIWvXJ42dIY55iiBe44BguQXCq8wRVGIOSQEbkjwOdu+1oUE84kSQZAsQkEnScjafjhdZJclgTQLcUmxFQDH/zDSBOIjLnh+kQTzyNRHeYICoTiNEdxvDPXYsMzn7LCJ1hA1pBRshJVy4hVjS93fssqUh/G/x4HeLC/8sHoILSOBTII180i3zUPLo+D9ljTY7VQ5C9VqFT3bRIJ4FWohsMURMtwnie1mI67LAjt12tNF7j9jguOdi/H6/DZ3JNq13wPnrIuIJCoIPv5s8BGRzCQgPkaSmRDcRkE4+EhUUzIm/y+o5K1UKQnW/uY830iDexTq42G5MIEyR0G+Gywo24tq4OOFphc0b7LFuqT25OuCQmw0CsywRnC9EQA4f/hQMUWAel/aYrJqLGDLHJHTzcOkeG1f6OGR+2fKDrI4lVCEIR3xzeCXdNPqU6iKmi4VL/eZIHLBAUj8Xif08JPYKkNAhQnyTJV30ZBWWkJaISKYSIDCXxEYOD5JcDoJuU4WRhwu1PMS2cggIh4BYIP6eKZEFEjqt34a06RqqBCS6nUMmRDsaxE+uhwSFGa4OmCN5kIuUQT6uPRAguU9EJsNFuNRKmsP6xXThowpgGKkfwbf59HYKKuQitJRLthUXUY1cxLYRw3vYSLhnhliyTaO6DEkCMUZQg/6Qr5wuklPskWYHcepDaxokoHwBLvdR3uCMQQwKkDpoiWv3FyHlnhWudlghvnExou5YIkIuQjhJtSFFJFMV8RBGIKQVXFy4yyEgHMS0WyCu04x42JQkD9KEthkgvEWPrlH+1dqtB+V02zJ1K7H2gF1q216E5q0jPRQ/wq/I1MW/QvckJd/yhaf8Sha6eeYZnP42Ve+c82U9n8MRupK9wbohe3x1wnd56URQ2k0+7w3UDjkk1Q44Fq993jVZx+NMpu7pc4Vabj4l2q7eZRonKXmVznXxKJrrcrZ4zklPuYZgSkGysxN0W1vaIbsQg+gcDw5zezaTLtWYGkC1GQuITIjYRFTQLiayYkSdiPCY/sqQKYIa09Ka/AckW7ezswuRF6JxNe0qZ8JX4zDqk2CMiEwZoykoCyIzBlL/ExDTB9LVRYFEEZC0ySCTYTSZ3mkBY7QBc13AAGgxbfvcaWndfyLIZJg5jIHzmL+41gRpMgDzJrTs0wcxDtLd3UODpP03yGSYcSB1Bmpc6hM8oDbhd2ZNK0hv773/BfJjQJ/S7F8EYhxEoej7KSCTgT6lX25RIP399z8H5Ne5KJCBgYEvA2RwcHDmg9TU1OgODQ3NfBBqDQ8PMyA5Mxvk8ePHXwbIkydPvgyQZ8+ejYHkzHCQF89ffBkgr16+/DJARl6/pkFyZjrI6OjolwEyMjIyFSDjjeOcz2goZ09xjLz6f2Jk9oShS4MZsDSYIWv+hGFMfcLkOPH/JWpTl7Ve/OyspcYYp8WMugsY6THSZUZj6nvtH5kmp+5Y6GMd+byCqMYYpMvM7dShhDFzNZw00y+cMNdrT/CY+pRur59R2ecwxoyfqrCYkxQWc8LCYk5VJoPpTfDIx1j6N8tB6PIpXLDjAAAAAElFTkSuQmCC',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKPUlEQVR42u2YaVBUVxbHjchOQ7M0NEuzNAjNjlHHBKhWQNFoSERHNG64E0CWBgFBlmaTRQgRWSSRTVkaWZRNEW0RFcWVNkRxdzKpmuXLzOd8yPznvte3rR4qk5kk3ZnRyq3611vu63fvr889557z5sz5rf3W/n+b9KG3XspZC0HKWUNBwrk5+m8uiJwnrpp0hfQiDztqDF/Ft5pPJXaYKySnLRQZ/TxF5pCNInvYXiEdESqKLokUpZf9FRVXFiuqrgYpaq6HKupvhiu+mIxQNN5ep2i+vZEoStF8dwMRub4bqWi9v5boI0XrFKMIcv6h4si4z13piIObRkGK5Lbi2nteKL5si+1Vxoj9wgr7Wy2R0mWNrEE75I04oXDUDSVyTxy54oeqa4tw7Ob7OH5bjBN3w9Ay9QHavvoIsq/XoWdmI84+2Yqh5ztx/uU+XPwmHpe/TcbVP6dj4i9ZmPjrQQy/2oqCy06bNW6R0nGB+Pj9BQSEj+2fGyPuhBWS2nhI7eYhc9AG2edsIR0VoEjuhtIxT1Rc9UX19UWoJTANd8RovB+C1ger0DYdga5HkeidicKZp5sx+CIaIy/3QP5tPK78KRnDf4xC9/MwlI17ZGhlaZWOu4gbFcEsSPRRY8Q3WUHSbo20HmtEbvVAeLgX8s7bI5/AFF8UolzugcprfgRmIRIL3kV4aADECwOwasUCpJcvhWw6Ej1fR6H/yRYMvdiBgZfr0P50MbqeickYwjoy5DtUmm0V457iU9MrWZAd1SZIaOYhpcMGWzOECPL3R/hKL+ScE0A64oiCC84oHhWibMwD0cm+bP/HawOwL30RIiIWstex6YFoV0TgpCIUTQ8W4MRX3mifCSLW9Bggw80jmqsVmM8nfMWyh+tZkJ3HTJDYwseGfa7spFgQYpHcIQfkDjgjb9AFBedckHnKHcEB/ojcyFjmXdTdWIK660GI/P0CBC/wR2G3CNUTItROeuLEvcU4PCK6vWwtl0uG0yPSUYPRJMgCce/MdhTK+fgkzxzLl3uxABHrROxxxQovHOp1RGaPI7J7XCDtd8W2BOUzacd9UDnmjQq5CCUXhDhQP5+9v0PiRe65o5oEh+KhgD8ER3LcyVAMiJHWYGomAsUDT2NZkNXRNgha5ItPJORf77V7DZLe6Yi0NidkdDgjs8sZH6zxYv/53LMCYiF7FAyT45ATpH1CBPr7Ys0ab5QMe0Da5fe399aaLCPDOBJZE5kSGRLpah7ktlg88iyVBYnKMcauGgPEtRkgsdOQBQkLFZFIxsWnx7mIbeAiockCywJ9sCzIG9l9dpAOOCLvrCOkZ5wh7RUilOkL9ENOh993geuNt5Ih/IgYizgQWRIZq1lFc75y/HaoWP4ij4DYYNsxHcS06GF/hyGSZEYsSGioB/bWm2JPtRlijnERX29FrOFHAL3IUnNADllyuT1OyOl2Qd5pV6wI8WH7l0Ubp5LXv0+0iMiHSEhkQ61iMMvxNQMy/qqEBdleOw9xpxgIYyTLOEqQEBH21ZhhT5U59hw1R2wdj3X0FWFeZJkJkH3aCdldLsjtEhIRkOU+zO/+QV4dTBREtIQogFrFnsiC+oquxkEuv8xnQXbU6xJrGEHSbYyU06avl1ZMHZdAWGDfUUvE1VkrLbLME1nEd7KJz+RQCAYmPNQHgQG+35NXB1IxVllI5EV9xUpteWkOhPGRC8/TWZBdDXpI7jJB2hlTpPVxlSBhxEcaLBBba4VPa6wQV2+Dpe/5ICTIB4c6iTVkLhSCkRtCiI+EBPpio5TbowaymC4vF+r0JpoHIVFr6Ek86+x7v9RHaq8pMoe4yBywYEGWLxchqYWH+AZrJBAlfWmLlau8SHTyQWabsxJApgTJ6XRVRq0IH+R3+yPqEL+egvyOyJf6CQPCmeXwmtlH+h7vYEFimgyQ0c9FzggXuectaPj1xAGy00ua+Ehq5JOjPTbsdmf7EivdWQdnQHJkQiRWKO9vjvVgfaeoKwgfxvMKKYiflkF8xV2PNrAgsS2GODRsgfxLViStt1Lu7AQks9sWB07Z4UCrHdJaBUiscmH7PvzYG3lkOeV0CpHd4YLVEcr9RVLvSvxHgIw2e5R2rv4+fC8vQesWYXKttuk1KCIpyv6TRsgdtUThmDWK5dbsZFeSXCv7jD2JUA5kYg7IbFf6xabdnmz/qnBvbIoREWA2WmHTHhGyZGTzbBcQaHukNNvicMfq70K3mW3Qqo8w2W/zg6UsSGK7CQrkPJRc5aPkGv81SP6gADl9ygh1SOaM/F5X5Pe4YWeqCGFLfVm/CAv2wZb97gRWwAKnnSQQrbZIbrJBykk+8jqW/D1kCydIa1GLqUcaphaxSWNyJwfFYzYou2GHIxN2KB8XoOQySd/Pk/SjnwCQFCS/zw1F/fNRPDgfRQNuKDhDfKRbyEawg6eckH5SgAMtDsQS5H3Er5KbrZF80hJpMhsc7HL95v2t+m5a2UeK5AJSIXqzIMzeUXrdFpWTdvhsUoDKG06kkHJC2SUhSs67ksm74TABKBl2R+m5+ayKBl1ZQMZaB9nlxEDYsZZIarJmI57kFKk4ZVxk9FmS8M5VBG+eY67xnb30gq+45q6IBTnQbYbyCSVE1aQTqm664LNrQlSMueHIxfkoH3VH2YgHyi+4K89H5+PweWKVAReSohCLdBBrkCUlabKlIDwktVohuc2CBUnt4bB7VNJpY7n3BnZpaS7X+mxgvXfNpbXIaglEwgnnXEkbPzq1ixOdKOPsTGo32x3XYBaz66hp/PZy06RPDnNSthRyUpnjtsOmydvKiIpNJVH5nNTIg8YZERKjrNVJRtkr9xvkrEowyP5Aopf1UZpeemSuvmRjmf7+7dW6MTvrdPfsbtbZta9F31Wj2W9jYxnn4qgc0vxiFDXFCejL59LQqEfXsxl1Uibp49PIw6P3mHM7mn4IaU6lEnMtoM9otx5pbGzkjI1dQV5+ESprKwX0tgqGWcf6dAKmdDJcCmZK9wPmaE7B+BTKnh75FNZMDWLeHG1UiAzIlSsMSCEBqZ0NooLRo6m3EZUhvTag50YUyoxCWdCjKd0zDH8A4lcBmQ2jQ8OlLp2QLp3UPLV7erPAVLCq53W0BqECGR+/yoLU/ivIHLVB5/4X0lED01WD1VF7RjsQKpBr167/OxB1mP+kH4PUHoA6yMTEjR8D+SnthwB/ncaA3Lw5qSmQ/11jQG7duvV2gNy5c+fNB+nv7+fcu3fvzQdh2tTUFAVperNBHjx48HaATE9Pvx0gDx8+VII0veEgM49m3g6QJ48fvx0gz54+ZUGa3nSQ58+fvx0gz5490wSIKknU+QnJ4zsa9pEnv8RH3lErvAxoaawqqPTVCq95agWWeqU4V3NRa+ZnR625dHLqpa6qnlfV9Ca031itctRXg9PR/D7y0zbEuXRCHFqfW6l9WbGcVburPlhwKJCBGojmltfP2Nl16GTUPxPZqn0uspn1ychS7WOEyiKvfemfgIvGbAQCK1IAAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAJq0lEQVR42u2ZaVBUVxqGlU3WBhqapaWh2Vc1Ri2TqO3E3VEyaiRWXIkiyqYtEJB9VzYFZBEt9p1mkX21BUQQBKQdhijKaM2kapYfM79nfsy88/Xtq9PljEmZ0KlA5Va9dZp7D/ech+98W7Ns2S/XL9fP94qfddcKbuEKglt0BEFdy1YsXhApT5Q1Zo/4fh6883ReBZQbT1+oMZZdknBl4a08WUS7uSy6Y6UsvttOltznIkuVrpFlDmyQZQ1ukuUNb5PdHNklu/3QU1Y0fkhW+ugI6QtZ6aQXiX6ePCgrnTpA+kxWPi2Xp6z88X5ZxpDHZHyPlcOCgiRLLUX5U25IuWeJk1l68LttisByEwTXmSGyjY+4Hhsk9TngqtQVGQOrkXV/PXJHP0bhuAhFE9tQNr0XVb/9DHW/O4TGp0fQMnccHfNfofulL/r/EIB734px/89hGPlLJEb+ehmdr44j8Z7N0QW3SOqQQFT4eC2BWOBkth78i0xxsZKHkAYefnPMCbt2uRGMAIl99rhy1wXpAx7Ivr8OeaMfoXBiC4off4oTvhvhuX8d6mcPoHHWC81zR9E2f4pgfCD9NgCDfxKj849foGF+O9LvO4ep5GilDtmKimWbGZBTOXoIKDaFuNoMR0OF2LRmDQMS3U6W6SaYbltc7XNCxuAq3HiwDgVjGyG+spGZ57n/Q1Q/2Y+6mYNomPFCy7OjaJ/3RtvLQ6h+sQGS+a20hl0BLblcJSCZQ66iypndDIh3jj4CS8xw+Kw9s7nXIJFtVohuEyC23QYJHQTT74yMex44E7zmzbz9+9aidGInKibpqD32RMX0NhTL1qJ4xh01c5uQNuTSSsupqQwke2SVqG72cyRLLfBlnAm273BTbOygCzPu3OmGiMaViKi3RoREiJhmIUJvO2LPbg/m+aEjCpB9ez9AwYOPkT3wAa4NOJHFnFEw7oqS6Q1Ik7qNe/nz9Gk5dZXBZI+sFTU9PYkkAvH0tsLm9avxpdge4Y18BQiBfV1jhZBKAcIqbRBZb4sTF1yxheb5Rnog8547M2/Pbnek0NFL6bBDarcDMu86IW94DdJ6177y9DW2pqW0SRoqA8kb+UTUOufHgByO1sXpPANcqjVFeIMls8Ed290gLhHgYpEAl4qtEVphg4BsW4QTVELnSiR1WzHzdu9yR3yLLeKb7JHY7IAr7c5IbFrz9x3e3I9oGS5Jl6SpOpBHIlHPixAG5HiOGs6WaOJ8hTYCq3SYDW7b5gLfAkOcyTGEzw1D+BVycamch8sN5ohrI5BO6zcgsRI7RNfaI5YUXbXqn786pn+ElrAjmZHkR0tLZSCFj7aJpL+PIxBznMhVx7kyLQRW6+BCra4C5FNnnMnl4KvrBJJlBL98E4jLzBEusUQ8BYAECgAMyE53xNTaIbLSDuGl7v/eelzva3r9hyRHkjmJw4Koqwxk6NVVBuRkvgb8K3VwsU4P4joDFoQskieHMIZPjjH8CngILrVApISP+HZrpHTZshbxQGydPcJKhdjuo5dDr95KWk9yIlmyICtUCnLvZQID4n1TE0E1ughu1ENIoyGzwe10tPwKjeCba4LzeSYIum2GkAo+opuskNwpRFqfIlTvIZC4BnuyiANFP+4wvVr0PSDLF9xHeufDGJAzt7QglugjrJWD8FZj1tldEFTCRcAtHqOLxRYIr7JCXIs1UnvtkUmhVhG1PJDQ5ICoWluCccThKKNm9mj9RCAUtTqfBzDOfrZoBUKbOYjsNEJUJ5fNI64IqSaAEnOIi80RWs6nY2WN5HY7ZEgdkDOsyDd793hQpHJEbAP5Sa0QcXWrcSyBn8f6iIXKQeR5pPmZNwNyrkQbl9uMENtnjLje/4KESQigwhLB5ZYIq7ZCbJMQqT0OVHO5UM3l+gYktcsR8c12ZBXKN3U2SG/ZghPJ/LCfCGSVqP4bLyRTieJfpoOoLi4SpKZIuGvKliiuiGrmI7xmJS6TIuqskUj5Ir3fCbkjrpS93d6AXCW4hDsEUi8kqwgQ02iFwr5D/zqXLTyichB5rVU1s48BCazQJWuYIHnQDFcGzBTRaLcbYihfRDcK6K9M9VajEMkd9rg+oLBGwZjiaP16rwfj+HKQaIkNc/xi6Hfi261QNOj1D/98/maVhl959Vv6ZCsDcqFaH4lSHq4OWzC6MrCSSndKel3WiG2xQRzVWQl3bJHW44TsQRfkj7mhYNIN+Y9cqU9xJiuRRdqEiG4QIEpiRZakY9hmiaReK+QMbv1b8G1TJ5UlRHk/cmt6PVP9ynNHyqA50kf5yCRl3KfINCig0l1IGdyWscRV8oPMu864MezKFIU3J2mccEHOiCOB2COpQ0g+ZI0oqtWi71D530GJs8eMfJCPlD7nV0ESfZ5KQJKlAuoQ3RmQYAkHqQ8scW2Mj+tjApIQ14aF1BnaMX/t9D4FhNwauaMuuDnhisIpGicJ5KEjMy+l24bJ+DEEEUPWiO2kUqaHh7g+LtNOh3UYT/neYuquBbZI7ypRHm1EDhLaYIj0EQVE9iMbZI/bImfUHlnDjsgaouM05EJypRKd/OOhHIREIPkT9GzUgUCo+u21pp5FfqT41L+YI6bTDDFdpojuNkZkNwcRXYYIuaPX7yVhfGXhruttn7vn3T2AyLJPEFQkjL1UZXEqpN7glFhi4C2uNTp9odzQ53yR8TmfPE7A6RxOkO8NTqBPASfAn+5drOCeFddwfS6QAssMfc8VGp6nOQHH0vWDjmfoB57I0g04k6977kyRts/5cu3T/hWa3n5VmqfOV6t7B0q0bRcUpLg4zaC/T4r4hBQkl/gL2NvLWafUYEOmLhs+DVlx2HvabCTSYj+/a56WSnsRBUixwcDAIOISknEt/9rbIK9hlDeqDKDJPtdgP79rnobS+1QHMjgoB0kikHyB0iNlGHV2o8pSV3r+rnkab81TDcT3gCxTysBq36H3mbNMpSBDQ/cZkPz/BVGG+bFapnKQ4eEH3wWyOC45yMjI6NIAefhwbGmAjI+PLw2QiYmJxQ/S2tpqMDU1tfhB5Nf09DQLUrK4QZ48ebI0QGZmZpYGyOzsrAKkZJGDPP3m6dIAmXv2bGmAvHj+nAEpWewg8/PzSwPkxYsXCwHyulxXf48yfvkC+8jcj/GR5UqdoTbb479ucVcotcAaSp2lcv+utnBR6+kPjlpq7Obk/bnBW186cNh7+uxzPXbUYQG1lFrhBc4j75cQ1dgNyTdrTDIl8djRhL3HZUcjFtCABVL+UmLhjtcPyOzq7GYM2Y3L/0doyY4W7Gj2f8A4ShZ540v/AZJh3+QZBEe5AAAAAElFTkSuQmCC',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKTklEQVR42u2YaVRTZxrHrShbCJuBkEBYIwEiUJV2OqDRInUZixuDjlYEl6IsAiGsIiFsigpSKygomwqyI7tUjICKK0o41p0uMz1nli8zX2c+zPznvTcvnAzHdqZt0o6e3nP+h+Ry732fX571vbNm/XL8cvz/HqrHUuPkTltRcqeZaH//LJPXF0RtJyu94wHVoB2iysy+ijtnM55w0UYjb7HVpHfZaTJ7+ZqDfY4a1YC7puCql6bomr+mePgdTen1IE3ZzWDN6dsrNWfuhGqq7m7S1N7bQrRZUzsWTkS+j23U1D7YoDn3cJ3m3DijUPL5Q82xkQVjqgEnsV5BCtQCWfkDHxReE2BHKQcxZ3iIPzcPyc32ONAjRM6AC/KviHFY7Y1jw34ovRGAk7d/jYp7MlSNrUDd+BrUT4Si6fNNaHu6BZ3Pt6N3cicufxmNwd/H4do3Sbj+pzSM/vkARv+Sgb6vtiPvmss2vXukaEQkq3i4kIA4YMcnHMRW8ZBYbwdFqx0ye/g42C+A6ooIBWoxioa8UXzdF5/eDEA5gam8L0P1w/dRN7EaFx6FovnJRrQ/CcelF9vQ/UUkBr7cA/U3cRj+YxL6/rAZrZMrcGREkm6Q0CoacZNVa5awIJEnOIir4UHeYI/UNnts3C7BypU+yLnsiFwCUzjojqNqCUqu+yGvcRG2RLyNFYFvY+lCP6wKWYj4zCDUj61H2+eb0flsG3omo9D1xSbUP38HzS9lZA33U2TJ2URv6R2keMRbduHRKhYk6lML7K+1Q/JFPranuyPI3x8rV/kgu18E1YAz8j5zReEVd6Sd9cbSxf5YFuCHiL0LEZ0egA2bFrHXr1+3GOfvfYjz48Go1ixE1YQU9U+CiDcl3WS5OTog+oX5ZNRX1vQ4jAXZedICCXUOCI/2YI1iQYhHlL1OUHa7Erkhr8+N/Pq+CFrsB1UjE2aLUH7zVzh9MxA792th9uV4k/MSlN3yxtmxABT0e91fstbKhixnMgNGnyALZe1PdyBf7YCtOTYICfFhjQnd5MX+/eADH2S1OyOzzRkH29ygODufPR8eKcXRQSmK1VIcG5Tg0IA7ctq0XlwfRs5f9cQJUhwKuv2/DlrHlZClGBAOkTGRkd5hykYDZd0vYliQ30TyERTgi61yN2S2C6dB0i46I7XeBekNrkiuckOEQoKYYneoeknusBIhr9cF2a1aT65ZuwCHeiRQNvn97d1Qi2CyjCuRA5E1kTnRXP2D3JPJBl4qWJDN2RzsKjNFbL0pEhrNWKNWBHuRSmaNfRXWiKm0Rny1LRSkqmW0korWIYSq25lVbqcbopUSrbeipMhu8P1H4CZOBFnibSIvImciOyKuQbxScS9Ypv4ih4DwEXHSCHvrjBF/0QyJTeasUcHBEnx82hJ7PrXC3pPWiDvNQ1IdH+nNAmRfckLOJdJnSP5kNXhgeZAfe09iyYJ/LY/kpJDHBxK9S+RH5EG9YkVkqvdcYUBGvjrMguwon4PYCwwEB0lNXC3I+16ILrPCnk9ssOeEDWJO2UFe44CMJiGUl5yR3+sGVZsYq9f4stdv2e2NDxM5R8mjl1IxMIuIPIkcaa6YGQTk2pe5LEjU6bnEG+aQt3KQ3GI5HVp7T1kTj9gSj8xDXIU9kmsFyGwhuUE8wUCsXbOAvXZD+ALkd0gQdYw3RB69hEhGQRbT8HIisjVInjA58tlkGguyq9IYSc0WSL1kidQOay3ICpIjlbaIKedhH1F8pQNSzzsiu12ErPNihARrwylsmy8OdXuioEdM4CQIz7bu+Ek9wlSt3udxbLJ/fNYEinZLZPZaI7PbljUwJMQLiXV2iD9jj4RK4o0aITIaRVCcEmP5e9pw2rZHihK1F44MeJJ8EZPq5QYl6TFhafxKssR7RP5EzJAoMFiOMH2k41kUC7K3xhTpXdbIHrCG8rItLb/eSCGdPrnWgc2NlPNOUFS4TUNEyaUoHfZC8aAnDveJkdvhjqxGUqrrSf40BmJDkr2KLCOlJdjeYFWL6ezNZNBjQGLqzJDVZ4vcqzwy1vO0nZ2AZLYKSB8RkpASIu2CCKtXSbUQiT44PkQgrsxHUT+BuOSBg01uLETaOSdyrRDHO0L/GZYh3EXDaio/jPXeR5hZq/7RWhSQESX+vDmUV+Yhf8gehWp71thVZNY62OmIAy1OyGhwQnSOtuktISPKzmQf7FZ4I0ohxY4Eb3wUK8Hv9nliu9wDijoGxBGZzUKUXd74961K2w9oWJkZZExhpt/aiWUsSEKDBfLUdjh83QGHbzhMg+T2iJDdQUaUZlfS7Lym57BvU0iwDwlFIVIaSD61OEDZJcSxy0v/GlFos8Bg8xazH6kcD2CHxqRGLgqH+Dhyiyw8KsTREREOXyPj+2UXqLpc2fjP7RCjoGs+Cnu0yuvQhlNanTPkZ0RIrHBEQoUQ8mqST/UCAsLHgU47KMm+JrfX8+vtpeYCg4AUqEVkhyhlQZjeUXRTgJI7JLbviFByywXHRlxw5Ko7DvV7EMPF0wCM8js92EExs94VimotxP5TQuyvECCxig/FBT5SyU4zo4NHqqANDvbxoOi0ebgujU14PXvkM19Z2ZgXC5LSaoWjo1qI0jsuKL3thuM33FE8JCYT7ny2vBb1k+rUS0B6xSxIdgtJ7gsuZJh0Yj0Rf0qA+NMkTKvsISdlW3GRh5SWeUhtt0FqJxdpXVaQt3GuLo5mG6L+QI53h0nLrm7AgbpA7K9yVcrrHSIVzdzIhCbuzsQGq92xlVZ7d52wjIs4Ypm0NZ+r2JbHTQlXclN/m8VNC0vnZqxPtshaHWuRE/Kxef77UeaFy4iW7zTND9lnmrNGbnxgXYZJaniuSeJHxSaxkWVzo3efmbtrd63Rrug6Ew+9glRXH+EOXlFDlVuIgppYEX34bFrnjWm5tKaTq4CWUSc6zTK9YT6RD216i6j86bn59Dq+wfcj1dXV3KGhYeTkFqCkvERET0/BzKFVhoGxpMYwvWAeBWMMFFJj3Wn3FtPPLhTant7DpaV3asbSP8jwMAOST0DKZ4JMwRhTI8zpr2pBDZuCm0cN5lPZU1Bb2jss6P2G8cZ3gMyEMaK/pLGOTOjM9Co4Lv1uTgFM6P1GBnv5wICMjFxnQcr/E2SWzoK6QLqa8wrAmZqr0zcMAzEFcuPGzW8D0YWZCfUqzQSd+X/DQEyBjI7e+i6Q/wb2v8qwBwNy+/ad7wPyQ+Bm/SQgd+/e/bEgP//BgNy/f//1B+nq6uI+ePDg9QdhjvHxcQpS83qDTExMvBkgjx49ejNAHj9+rAWpec1Bnj55+maAPH/27M0AefniBQtS87qDTE5OvhkgL1++1AfI1Fxl9D3mrbf0nCPPf0yOvKWz8TLV2XAZ62yojHX2LkYz3mvN1l/VevqDq9Zsapw53RVaUVm+YqfImbFjnIIz0n8f+X4NcTY1iEv37Ty6T+fRPfzUiwob+hbGil7L0fHYHL2G1w/o7EbUGCtqOJ++KmL+Osx4AaELZqnjkelc+jc7ycknJwTq1wAAAABJRU5ErkJggg==',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKTElEQVR42u2YaVBUVxqGVWS3m7XZm33pZhFNYlKJpBnEqCEBtzEmLoCgEtmaBkFAQUAIuyiihoRmEVAWNbKoLLKLxi20IcaoRCs1qZnKn5nf+THzzndvn3a6rMnMJNOdGa3cqrf69r197zlPf+c773fOvHm/Hb8d/79Hwb0Ao/QL1uL0C6bilEvzjJ9fkBGRrOZzLxQMixBbZ/okqcVqJvW0lUrRZa3a1yNS5fTbqw5cdFYVDHiqiq9IVGWjwaqq8WWqmsnlqrqrK1Qnr69SffJ5pKrhxgZV083NpPdUTbc3kej77fWqli/WkaJULTOcIun8XVXlRODtggEXb52CFI84yo7f8UfJqCOia8yx5xNbJLfYIL3TDrm9Tjg44IZDQ94oHZGicnwxaqZewbHrr+PjmzI03A5H88zbaLsbiY6vNuDs/c248GAb+ud24PLj3Rj+Lglj3ysw+acsXPshF9M/ZOPik20oGnXbovOIlE2IZR9/sZRAHBB9xByJDbaQt4qQ0S1Cdp89DlxyRMGQGMUj3igbk6JqMgi1V1/BcYKpvyWD8oswNN9dg9bZSHTeW4dzX2/CZw+3oO/bGAw+2YXR75Mw/sc0XPzDZnTPhaNi0i9LL0OrbMJDplSF8CAxR82RpLSFot0OUR/4YtUqfx4k/5ITCgmmZNgTFaN+qBxbjD15S7E6PBhvLl2M8JAliJe/BuXVCHTMrsfZr95Dz4OtuPg4Fr2PN6D90TJ0zYVSG54nqMn5egGpmpDKWmdX8yCxRxchuUmEDxQeWB4crAbpd0beJTEOXnZF0aA7Phrywoatgfz9yLXB2LXvZWyNfwnLlyzGqrAlqB9bjXZVJNq+DEfjl0vROBuA0w+Wo3xC0kPNLWDSPcyR6SBZx72NNHQIpFaADXFefCc1IPsvOCGv1wX5PW442OeBpGpf/t6mmEDUTAbSMHsJdVOvIqPyVf56rDwYNRO+OHZNghM3pWhULUPpsORmVJytgJpbqAUyX8cgS2Vnv45GZrMLwsIkfGfeWav+fOstf2R3uyCnS4ycblfknfNAbJYEYcsXY3+LP6pGA3hVDPnhUK+n+tl3AlA55INqGoJ1U8EovbzkycotFp7U1CKS8TMwujvqpt+Q9TzYg+hMisTLQVif6Iy0NpEaZKU/9p52QcYpMTLb3JDT4Y688x7I73VH4UU3FF12QdElZxT2i7H3UzXI2vcCUdLng7JLfijqDv5z6PuCN6kZR5IVyYxkpBeQ2mtvhg48TOcj8n6pEXY2GGJ3kzHfqRUr/JBQL0TCSQuSJZIaLKFotaEo2dFwc0AB5U/BBTekVHthVXggQijxUw/7oqDbG3ntQT/K3jfnpll/khvJjiRgUTHQOcyJa2GhI3P5KLpij61HDbBLaYQPm02fgsTXCRB3VIj4oxbYc9IKimYR9nU5IK/HCWm1nk/zKWRpMHbulyC/0wu5rYF/C91mnkmvf4O0hORDciJZkkz1Mrw4kPHHpSgatsf22oVIaDJFYou5GiSMQI4SSA2BHLZCwglryJvtkNNJRtnrjPSTntieKsWWD6VY+bsg/pnfx0rxVoL5EXr1CpKM9CqLiphkzYaXoV5ARr8t5EFi6gyReMoUKW0aEAl2HbNA/BFO1kg8SY7f4kDJr86LsiEPVJJRVgz7orTPF2s3BqgTfovrQ3r1SgbDRSWI5E6yJZnrJU+4HBl8lMmD7DhphJR2c6R1CPkOhYdLKDessKuWonHMFkn15PitZJDnyBypdKka88JhmmqPTEhQNeyHbKUf/9y7UQFYmyHspdeHk0LY8PLUyhM9gEy+Htr/TSLliAN21htD0SVA5nlLvkMrCSRVaU0AthQNO8gb7JF42A0Jh7xQPuhFPuLD+0XtVSkOj0pQ0uvDP7dmdQCymwIQlSpSUhPcrPUSiSsSHUhCvSQ85yPn7sfiEBnibqUJQVggt9+STb9SKNpskdpIEJ860LByRESUlL+Xf9oPtQRx/LoUddcIZMwP++rVEVm3WYK9TWLkNb2GNQmij6iZl0m+LOEtSCY6T3i1s2/iQfZQouf0WSN/wIYZohT7qArOaCWIJkdktTohLtebv7d+cwBqp9QQtVN+5Bu+iIhQly6J5V4E4gJFgyNKWtf8dfVuh2RqSkJyZn6i+5mLq7XaZt/hQZJOmSHvsg0Kh0WsRJEi97wDss44IavdGdntVKp0u9M/rk7qt1cFIk7hj+3JgeT26llre6ofck67IqOZA3GCQumA0jMRP66MsdhIzbnobebiqt+mu6E8SEr7Ih6iZMz+aa2VT36xn8qU7A4xDnS6o4Ccnfv3E/L8EbE6iDdB2bLFiNwQiJQKHxygUoYDzmh2hlzpSLJD+ikyz87X/hIeLXhdjyBiWf3MK3zRKD8jQDFBlE45onTcCSVDLuTeLjj4GdVZZ91QeN6TLz8qBn1RPeLHi6urSi97U63lwUcrt8MVmS0uNBTpfRQNOZdftFDL7LCnadvru5A4E0+9DK3iETGtEAP4Mj69U4gygqicdkTFFPnEuCs+GnTFoX53vigs6fNG+YAvqq6oIaqu+KKcQEr6vQjWHfu71NHY2+JEIPZqCFoWcGVNeocl9p23QVqnpSpkC58nugUpGwyS1d2W8CB7uyxQcdUJVdMuJDdUjLujfMQDpTTVltG/Xk6RqBxSQ1SP0vkVisaAN4q4aFDEcs9QcdnqTMPKEQoCSSMIeYstFaHWPEjGWZraPxNC3mU+ErCJ9xLdlfOHezcG1F1Zh9zmN5D8qftBRZtDTOppwY7kU4L4D+stEqjOStpWIZR/UCxI57S9VKiIrhLKYyqFqdHs+sYDgsx1WeY5kWlm+yPkZgfWpJjwikg3yonKNMpan2+s2FxunBxda5iw44Thzvgmg7jdzcZeOjVEpbJcMDw0goLCEhTU73Zl4TZg7mvG5n2utLBnhsa5s4hdE7FrYubcPswvfNm5B5up7Nh7NGW87qtfpVIpGBsbx8HCYlQfrxazly9gY9iYNS5klasl65CQlRpC5gu2DMiJddyFnXPwNux3+oPQgIyPcyCHCOS4NogGxog5sRmTKftuws7NGJQFg7JmsmIAi9jvnoXQO8i8Z2AM2JxvyDpjyAAXal0zfgZWIxOtZ/QHoQGZmJjkQY7/A2SeVoML/gMZaIFpa6EWgP4gNCBTU1f/GYg2zL/Tv4Kcr1cAbZDp6Ws/BfJzjp+C/HUODuT69c91AfK/PTiQGzduvBggt27dev5Benp6BHfu3Hn+QbhjZmaGgTQ+3yB37959MUBmZ2dfDJB79+6pQRqfc5D7X99/MUAefPPNiwHy6OFDHqTxeQeZm5t7MUAePXqkCxBNkWjwM4rH+TrOkQf/TY7M11p8mWgtsjQLLs3ia6HWAkt7K2iB7mat+7941lrAOqe93NWs6TXr+kXsvrnWUtlYC85A9z7y8wxxAeuQQGsDQrO7YqO1frfS2rQQMCATLRDdDa9f4OwGrDPaW0WOWltG9s9sG9lobUZoIvI0l/4OHlTK7w9jSrwAAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKMUlEQVR42u2YaVBUVxbHFRBoGpq1uwFp2aEBwSyQxK2TIEI0wREsTTSi4AKybwqi7IvsgrERURtEAVHEyCIgyCagRCW0Q+GGiVPJ1Ew+Tc3XfMl/zns8qB5rTCoz3VUDlVf1r1f1+r53z6/PPcu9S5b8cf1x/f9e2dMeukk3zSRJN3mS2K4legsXpF8oqxh3RHafEKFy3qvoetPJuCZTZeI1M2Vqm1CZ1ilWpt9arszucVDm35EqiwZWKcuGfJQVd9cq5aO+yur7/spz44FKxYNgZf2jz0k7lPUT25V1j4JJQcq6ia2kLcr6SUaByvpvP1OWDq98lN1j46RWkPx+K1nVhDsKBqywp4KPyHMWiKk3R9JVEY51WCOrxxZ5vU4o7HdD6ZAXKka8cfr+apx9IMOFRxtQr9yExqktuPpkG1qff4GbL0LQ+d0+dL+KQN8PMRj8ayLu/j0FYz8dw+hPR3Hr1W7kDtjuUrtHioYlsrPfvk0glthTyUfUBQvENwiR3CJEWocY6V1WyO6VIL/fCUWDbii764mvRr1RRTA1j2RQTPqi/s8EM70FLc+C8fWzHWh7+SUZHIrbrw6i/8doDP0tAbd+2IGWGV8UD7umamRpFQ3byxTKdSzI3lN8RNdaILFRhCPXRQja7Qp/f3dkdS9HDsEU9DmgpN8V5SNekI+/i5qHH0Dx7YcE4ofLU58gKMgHa1etwtfPP0fHy93oehWG9u+D0fjCB80zMprD4QxNqUVaqnaQsmE32eWpABYk9CtDxNYJkdQkxu5UB9Yo/wB3ZHRJkN2zArm37XCi1xElQ1JUjnrizLg3zk+sRp1ShuTCtex4Ri1Pt6Jp2g91U+9AMeWBhidraWm6ttN0y0jaGoGpHPOUNU9vY0HCThsi7qIltoc7zhvFeCSz0waZ7XbI6rBHbpc9CvucaYlJySteqJnwRuXt1Vjv89b8O9UPpSQ3VhcmfJDfJX24OkBgRtPpczCaAHlb1vp0D/L6LbEzyxR+fu6sMYHBUva+caM7jreuQNr1FUi/bo+cDkecuO2C0gE3nBrzRNUDTwRt84S/nycCNq5k35Hfc8WpUQIdWYX89lV/WR1oJKWpGBBDkp5GvCIfWyNrfxHJgmzeK8Zab0/sTLRHWqv1PEjKlRU40mCL1CY7HG+xQ2abHfIomxUNShB1wokdl17vjE2bZkFODkpR2itFzjWvf/h8xt9A0ziQrEgmJAONeEX+QCbrmUlmQXZk8LFPro+oBn3EXeGxRm3wlVImM8GhGhNEkuLqzJFCqTmz0woZ122x/j0vfBnlhtPjztjMgRTdckFmk9fPHwTx99IU3iQPki1JyHlFV+0gZx/4yvq/yyIQMUJOayPioi5imniIbzZgjfL1dUV4tQAHvjLGIbkpYqjOHG6yREa7Nf70uQd8161E2R0nVIy6YNMnsyCZzZ6/rNvFZ9KsL2k96R2SM8mSJOBAtNUOMvyqkAXZU6WDqMsMBB8JzUazIB9LEV5ljAOVpjh42gzR54Q4fMkKB3JmE0LiKRdU3CUNueKTgFkQ/wh+BX16E2kziVla75OYOLEmGWskThiQge9zWJDQ6mXkDQMktvCRdE0wv7QizpiQR8wQcdocMTUixFfbQPa+J4J3uaN8kLwx7EpeIRD/WZAd6Waj9OktnAJIa0juJBsuTvTVDsLEyO2XKSzIvhpdJFw1xJGvBThyw2QWZAPFSI0ZIqssEHlGiNjzlgjcJoXMxxPZLU4o7XdGORXJoi6X+ayVdNYBAZFG3fT5IM4r60grSRKSKYlH0lEvCGWtzufRbLAfPK+H5FYB0jpNkNZuxhrl5ydFQr2QYkOEOPJGUq31fL34LfmFmTfSFJ+SZCQv0gouDRuoHYSpIzeehbIgEbX6SG0zQUaPCTK7zbj060bBLUZSnSUSFJZIrrfBrmgX7ImXIizJHWHJHghNcEdIjBQfrfZk39kW6oSgECckVr4D2U4zJl4+Ir1FsidZaCQFM5X96pPtLEjkRR6O3zJDzh0LaustZgOXQNJarKiOWOPIJWukNkqQcc0OuW3UEVOaLex0QW6rI4432cPP14N951DFcoSXkodLxTha9fEv63eYp3CZy5Ek0kgKZnqthqlPkU8tSswlA2T2miNvUISCfhFrVAD1Wuk3l+PYNRuCsMHxK7bIvO6Agg5nFiSfgLIZkAYGxJ0DscYBAtlfLMbBchEyzm38ec1WQQiXgsUkI7WnYKb7rXv8IQsS12iI3H4hCu9aonDEch4kp4O8cINaFPIE4428m06sJ5gAL+h0Rs4NRxxrUgGptCQQMcJKRNhfRklCLkKqwvuf7wUa+mqsljD7kZpJb7ZpTLhihIJBMYrvWaN0zBolwxIUDlD73m2LHGoac244IJcgTpA3irtdUEI9V1G3M/Kp/8q6boejl22RpLBBjNwaEZUiHDwpRHilBSLkZtQRiKj+OPzovV3PVSMeye+X0A7RgwVhakfRqBXKx61xclyC8nu21OXaoviOAwq7Henfn42L4h4XlPW5oHyA7pR+i2kHmddpz7YszPJjMltMtYg6ASEOVZmz6Tv2ogmSm6mgNpo8fjeQDXj1xkjRbU+Z/JGUBTncYoySsVmIinFbVNy3x8kRB5RR0Sul1r20l7zQS8WvjzZXg65UCF1wcpjphGn32OuAvA5bpLfaIKXRGol1Yqo5FtQJUBFVmCL2kjHiyeNJLQLENvEH7D5ii6L6QE62b/OQ39mKYxfXIPaCXWZig+Xe5KtGe+OajcLiG433R9UYR+w7JYjeUyKI33nCKGk3KaRYkLCvQhB7QC6IPlRtGhlOYsbsLTWM+yKPnxh0nHc4MIWXujlJN21zsu7Rz1L0jgRn6yV8UbQsOuTUsvB957X377+g56RWEIWi2Kivtx/ZOQXIr42ScB/W4tavLpfzjbn8L+Yk5Co0E7R8TobcOHNuzHKuAEq4Ft6cG8/TSBuvUCiMBgeHkJWTj/Kqcgn3eA5Gh2vwDDgjjDnNAfA42DnxuOcCDtSMkwkX4DyVIFc/yNAQA5JHIFWvg8zB6HKNngEnfe7Z3B58TsteG8tXeUdPo3v2N4C8DjNnpKq0VX7Xem2sjgrUHLCOyhj1n6IwIMPDd1mQqn8HmYNZ+gaDtVR+f12/NX6JRkBGRkbfBLLkV4x9k1G/d7z6QMbG7v0ayMK4GJD798cXB8g333yzOEAePny48EHa2tqMJiYmFj4Ic01OTnIgtQsb5PHjx4sDZGpqanGATE9Pz4LULnCQp0+eLg6Q58+eLQ6QmRcvWJDahQ7y8uXLxQEyMzOjDpC57lb7d3S9S9UcI8//lxhZqrLx0ud2gnM7SD2VDZaOyoZM9QBbS31Z6+l/nbW0OOMMuD256p5ewD0zfG3by+MA5+C01V9Hfl9B1OIMMuIOGiy40xUL7sRk7vDBlDt8MObG8lU8pqPW5fVfVHZtzhjVYyIr7m7J3UX/AUyg4pH5WPoXg3fLamxzux4AAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKOklEQVR42u2YaVRTZxrHVRQUCIQlhAABkrAEEMSpdsZl0hZcaJXWDWydui8IkR0RArIGxQVxFNzKEpAdFSK4UBZBS8W6EGutVunY0+mZM/0y83Xmw8x/nnvzYnM8tp12kpnR03vO/+Tem+S+7y/PngkTfjl+Of5/j4L7IdZpnc7StM5p0sSLE2xeXJB+kap8RIGCXhE2VEx7oq5zGk1qcjKktjkbdulFhuxusSH3gqeh4LLcoO1TGkoHZhgODs42lF+dZ6j4KMJw/Poiw6mRaEPVjRWG2k9Wk2INtbdiSHR9a7mh9vYyQ92dtw11o5yi6Xyp4cDQ9FsFl738zAqi7ZeoKm8Ho2RAgnXldog/5YoddS5Ia3WDpssDBT0+0Pb6oXQgCAeHwnD4o1moGJmDkzdVqL4dCd3om2j49G203F+JMw/fReejtej+chMuPYlD79c7cOWbVFz7cyY+/laD4W+zcOHJ+yga8FljdouUDklVJ+7MJBB3rDtsh4QqVyQ3iJB+RgSyBnZflKDgQym0/QQzSDDXQnFkeBYqOZhbBHPnDdTdjULjZ9Fof7AcHQ9joR/7HW14Az78aisGvlFj8E8puPDH1Wgfi8T+q4GZFnGt0iGZqtownwdZ/3s7qGtckdrkhp1n3LD7ggS7LxNIjyeK+72xd0CBg4NKlA/PIJBZZJXfoOqOCrWGSNR/uhjNPMwKglmNrrH3cfGrjeh6shJNj2ejbew1WkN+jJacaBGQg0NBqtP3FvMgG47YI1FH1mgSI7tTguVrA7FocTBZxBtFvd4o6fNFab8C+weUmBcehnkzZjxXJ3rfQvPnC6D77FeovT8dzY/mY9+QUk/LTWIyP8zh4VAV598cyMYKeyTXiZHRJsbaLDm/qcUEUnhZiuLLMhT3yLGnTw7N6UD+vVXvzcD2nHDE57yChJzZUOfMwnZNOMr6gnD8VhBO3QmG7t6r2NcffCMmQWRPy002AZloZpCZqrMP1kHLgRy1RcppCWLjFE9/Xd4iF3yQ3+2Lom45Snr8oN4bzL+XcXQ6yq9Ox6ErStqsgiTHAXK/w0MBOPqxEiduhGN/38wn0ducvGkpW5I1ycoiVqkYnqs6/yieYsAdq3JtEbFAyW8yeoXxddHCYOSe8yXJkNcpR75ejjXxRotkN3qTpbxQfMkb2ou+JAX2XvLH/p5AHKIst7cr/C+Lt7u8Ssu4kASkqaQplgH5RKXqGUvnQZZscMe8WaFYleyBlDZbfrMLCCz5tBBJdU7kdk5Ia3TBW+8o8Vv6XJ7eE4Vd3ijQe6OwQ4bCc3IUd/qj5Lw/tB3hf4vaKlhOS3DWcCM5mljF/O514pMIVf+X+QQiRmy+HTYfn4akFnKxdiNIZIQSCdWOiD8pxI5TBFMjwutzQhH5RgjejQtAxPzpmD8zDAvpemNaAHY3K5DXEvrPRfEOifT4EJKcJCE5WdS9OJChJ3t5kPWVk5FwehqSW+2Q2iZ4CrL9hCPiKpwQV+mMuHLx0/iJUE1H7JZAxG4O4oG4e8veVSJKbVdGj+ZcKozEVXAPkjPJzqIgA38o5EE2HJ+CHU22SG23Q1qbw1OQuGNCbDlCEEddsFHricjXg/B2jBLZDd7IafaFplGGzCo5oqJC+O+8s9nrM3r0HFI4KYDkxeLE3mIgxhjJ5EE2nbQma9gj45wDMs4Iv7PIcQ7CFXFHXJFwTIzUKg9kNnhB0+QDTbMMmgY5snQyxBUZs92y1SFYlSM8R49/hRRM8mVx4sACfrL5QShrdX+h5oN9ywc21Jo4IEsvxK4OJ2OwRyqRSG1L/DER1JVuSDwhQVqtF1nDh7fG7hY5chqNIBkfyFjtCUFuQyhic9yPP+NeTs8JeHPWkQ08SFz1VGR2CpF7idTtzG9q4YIgpNS7IfmUGEmn3KE+IsW2PeRKtZSKzyiQ3+7HBzhnlZ1VxiIa9VYQdtZREW2ei2VpbvnMKj4kEUvD5ncvrrK3fh7Dg8TrpkFDAAW9LsjvcWV1JAi7WsgKOg+k13ogdps/f39TlhLFHX4oOueHgnY5WUaGrUXG92I2B2KnzgsZdR7Y37bkH9GJkjhaSkFyZ2nY/O7F9VoN95bwIOp6W+T1uKB4QARtn5sRZFEwNGc9kdXkhcx6L6jL5JgfTrHzWijyWwKg7VSgqNMPOfUKLIwI4VNx8lFfgvBCeo0n0nUSlLYu+fvCTUKupniauJd5CyPX/dbefY1vUZIa7VHUL8KeIXde471Wvl4KTZs3BbYxLtbEGav+G3NDqcoH03UwXp9nbCI3Zvkhq5GzhidSaiRUd9yQXu+OgtZf/zVynWAOS8OWAJGqTo7O4pvGlGYBSq6IsW/YAwdI4yDF1Gvldfgir13GxwXnTvEFgXiTS7evhEE1OwxL3wmGep8fNC3e2HXai7dESo07D5Ksc0FGixhZLYqv57xv42cREG2/lCbEEB4krdUBpdckODjigbLrUhwc9sGBQR/s6ZFB2yVHUYcRQqunNqTLH3u66LyLcy058s76Ioeslk0umFHvQZlNzEMk0XyTTBNnapMQmWdcqGsQGubE8FYxb4yU9oSqKm4peZCMdkfsJ0scGpGifMQHh4Z9abyVUwdLzeBFP9q4vxHiPAfih5JuCnY9ZS5qKHPbfIwuVe+JVHKppCpK1R+4IrGaNq+j1qZRyHcL6WcduM6hPyTGzCn40PmVIRV9y6DRzaV64ZuX2uC+Pr1VsD6xQbBJXee4ZVuFY/zaAw5J7xUK0ldpBLtWZAmyV2YLsrhzTit3CbKi0wWapcm2uVEJtvmLttsWLNg2ldfCeOu8N5OtNUszbXbGFNokrymbEr/2yJStm6qtNm/T2SjM2jhWV+8T9H7Yj4LCEmhrEqTswZOY6aeyaixiGceXNYEydj5+HcBqRRhrS8LZOXfPnyRlz7BcZa+urhZcuTKI/EItyirLTEGsWOHiAlPINuLOOlkJO3dnFdubAfkzqAB2LmM/gOg5bbz5QQYHOZBiAqmUstumVrFhG3BgQE5MQiZnE0gP1iB6sXM39r6DxSfEHwEZh7FmLmH7HNmxtsORATozCRmAHfuu5SDGQYaGrvIgld+BPAtjxfK+tYlMr22eAzrNBGDKMxCWAbl27aPngUwwWXTSj8iKWW7KM5psAmA5iHGQ4eGPvw/EFOaH9EOQEy0KYApy/frID4H8u8f3Qf53Dg7kxo0b5gD53x4cyM2bN198EL1eL7h9+/aLD8Ido6OjDKTmxQa5e/fuywFy7969lwPk/v37RpCaFxzkwecPXg6QLx4+fDlAHj96xIPUvOggY2NjLwfI48ePzQEy3ltZ/YSea6KZY+SL/yRGJprMK1NNZpPxOWV8bplsMpeYzuyTzJe1HvzsrDWJbc7WZEp0ZJOhA7tnbzJJjg9cNiZwVuavIz+tIE5iGxKwEdeVze7cq4vJ2Ds+3zuyz5qOvpPN6l4/o7Jbsc04so2L2T8rYvYnhJj98fAsmIOJRZ7G0r8AHUvIFmqPl3oAAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKdklEQVR42u2ZeVRTVx7HrchOQtgXCSEkkAgGqmC1iLEsQm2lR2rVYhGoRSlrCKsgsgUE3FABWSRhESyoVAEXBAOiotSlxDoWFduetmfm9L/Ov/1nvnPzcuPJeKbT6Uw4M3r6zvmevLzLe/d+8ru/7bFgwR/HH8f/71H+yN8s57w9N+e8JTfz0gLzlxdE7SStnxagfMwJSY2W36V32c1knbLTyE/bawoHnTRFF1w0JRcXa8pHvDVVV8Wa2vFAzcFrKzT111drGm+Ga5pvR2napmM0yjvvazrubCXaoum4t5mIfL8Xq+m4v1HT9eV7mq4ZrWLI+QbNgcml98pHPIRGBalSu0mb7vuhetwNCfXWSG1zREaXA3L6nVE87I6yER4Uo0LUqJfgwLUA1N8IRsPtN9FyR4r2exHonFmPnq/eQ9+jTTg7uxXnn8TjwrOPcfnbXRj7Ph3jP2Zj8i/5mPqpGDd/2o2L38Wjcpy3zegWqZ3kSlu+XEZAXJFwxBpp7Y6Q9Tgh94wTioZdUHLJDeWjXFSphaidWIKD1yU4djMYTQSm9a4Uyi/D0PXgbfT+KQanv47FwOwWnHu6DUPfJBKYZFz9IQ3X/pyNiz9swZm5cNRNigrJtK/NAwhfqtSEMiCJR62RrnKEvNcZ+WedERsvQlSUH8ouL0YFgake88Z+tQiHbgSg8XYQaofewEfJQYhcswxrlgVg/bogZFe8hf6ZD3D+8TYMzyXi/LNY9DxZgb6nUjKH93Ey5UIKYlyYg5NLpCcfRjMgScdskNnhhJxTLogv9MbqwEBERfth7yUuykc8UXnFC/tGBaibEKFYJcHa4ACsDQrA9tRgpBasxIZ3ljP3xCWsRPdMONqJpU9o/HHyUQj2jfkOk+nMiEzmBebIlESq3d9akI8bbJDV6YrNuwTMghgQYpHSCx4oHfJC2TAflZf4UFz0QdgaArIqAIqzy9EyvQqt06vRPBWCTXGvM/ftbhWj4ZYYbXeCobgguhv0FsuRTGf1T2CMBbJMOjCbAIXaFXFldoiM9GMWEvO+mPlct84PewY8UXTWEyVn+SgfFOBThe5v0iqX4vCkPw6Ni1E7JkDNFT4K2oT4cIcf8hrEqFdLUHEu4PuV6238yFRORGwiSyJTo4M0ToVIh56mMiDvJLpgdbAEcXI+igbcn4MUfOaJ/B4eCk95oajfCxs/FCN0WSD2DnBRSfxHccmTWImHiiFvKAaFqBr0QfWQCHt7JT+veNc6kkwjIHInsieymRerNN6RSkfmchmQLXutsaPRAmk9Fsj6zJIBiQgXk0jGwactHKS2cpCpskfEW37M1irq98C2NF9ErPFH6PIArIuQIClXhNI+AfZ0S35ZFWudSKZYTqS1iBeRM7WKBdEio4K03AmXqr8pIyAu2N5ggpROM2ScsoSsz4oBCQ8XYWczG8nHbJHSwEF6syNCgyQIk/oTa/kjLCQAm3eIsTVZhPC1Euae2LilCI2z3k0eLyVaSRRAJKRW4VBfMTU6yOR3NQxIQtMipJ3UQlgju4+lAwkTY1ejLZLr7ZB81A6px52YbaUdiwxbigIV2W49RN185Ld7Izpa5z8CLreEPD6MKIRoGZEvkQfdXnqnNy7I+LcVDEhSsymxhhXkZ6yRc5r9fGulHOcQCHvsOuaAtOPOCAnU/fI7ywUEgvhOlxfylXzktvGws0zIjC0XidTk8RFEoURBREuIPIkciKyNDqL1kSvPChiQHa1myO63Qf45NvI/5+hAIoiPtNojtckRnzY6Iq3ZBdI3ljJj2ccJRLcXCjoIRKsXZE08ZDV6MmOhby75mTw+km6vN4j8DfyE9YLDGydqXXiSzjj7zhPmyB1go+gCB0VD9rrtEymGrNMJ6a3OyCSStbkxSVI7lnWEj91dfOS1e0HeogPJOMpjxqKjJdiQ5aQiU6wlWkX9RBu9XAwc3ngg2jzy+eMkBiRFZYHCQQ72jnBQetmeht8lyCOZXq5yhUzpCrlyMTbvFDFjCdm+BMRbB9LMIyCe2FGiS6axH/miRLUSUckONS/4id7hLQ0il3Eye//XmxmQ1E5L7Lloj4qrjqSsd9RldgJSdMYNeSfdkdfljvwuLrIb+AglfiJdJYHsmAB5J3jEIp7IPOaJdZEkFL8eiJRacq3dDYquqL+FJTrKqZ+IX3B4U6OBaGutnofvooqUKBndVigddYBiwhnVame6RfxQcm4xkzMKezwY597Tx0dysYiJXmtWBJBf3w+bksQIC9X5zgef+BILehAQd8hOuELR/fYvIRvZ8dThufMCoq1+Ox6sZUCyem1QqXZCzXVX1NxwfQ5SMcxFCSlTSkhW39PnhfKzAlQMCCE77IuYWH9ISTWw+vUAREb4Y3u+ALmqxchREQilGzLbnJHV7oKijhV/pVneY55AuNLWmWCmaMz+jIXqCRfU3XLHgSl37J/komaclO+XeaTG8kLF595EQijOa0sQH1KKCFE+IEAJsVBhtyfyVJ4EwgM5xI+yiT/JCEBmGwkUJxxIwHCGvNP7x8AYM230sjO6j1SpuaRD9GdAtLmj9qYbDk274/A0F4du8UgjxUPdVW/UXBageliIfcM+qLnoi9pL2k8CNSRA2QCpzU7xUNDNRV4ntQaByGp3RsYJ0nEq7ZHZxSHtgT3SujlfLY1gIpeFUUFqr0ikjffEDEjeGVvsn9JB1E/zUH+bj8M3vHFwQogDYz7YP+qLuhER9l/x1Z2P+mDfZSEqB/koOc3D7l4PJiDIVW4MRKYWot0BGSo7AmKLrFM2kPezkd5rPe7xJmMRE6MlxMNDm/wbr25EcWcIMtu9SuU9rom5/azErD7Wx7Je20/SWm1TdhxlpyfsZ8vi9rFy4qtYOdtr2PLt+9jZ2+uIqtnyLRWs3Nhi68IYuVXxOzKrkuhUi1KtojLNStbLzIo35JsVxJaay7fWmGbEHzFNSWo22ZnUYu5j1MyuVNaxxkbVKK+oRpUqjUsfvJD+WmbUKW2JHGkyc6XZ2Yle03660WjkTXOFiMqXXvOg93DmrblSKpWsiYlrKKuowqGmQ1x6WQ+j3cPmdHI2XQiHgrFpqaG/7kBB3enCPei5Cx1jz2uHqAW5dk0LoiAgTS+C6GHMqHNaUVnS7+YG123oYu1oeLWn52w6ZjmvPfuvgLwIY0JjvildjCkF1MvUANbSANiKXtPfMz8QepDJyesMSNM/giwwmHDhvyETAyhDLTIAmB8IPciNGzd/DcQQ5rf0W6DzA2AIMjV161+B/J7j1yDn/9CC3L49bSyQ/92hBfniiy9eDZC7d+++/CCDg4Os+/fvv/wg2mNmZoaCqF5ukAcPHrwaIA8fPnw1QB49eqQDUb3kILNfz74aIE8eP341QOaePmVAVC87yLNnz14NkLm5OWOA6AtEk99ROL5mZB958t/4yGsGjZdh12hGz/VN1yKD5srwv1ULjQYyO/sfR62FdHFWtH+3Nejn9T29DR23NmiTzQ3gTIyfR35fQlxIF8SivbmjwZsVB4Pe3c7ghQWLAlkYgBhve/0Hmd2ELsbwNZGbwesilxdeGTkYvIjQW+S5L/0dPf6+Qih8jgIAAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKBklEQVR42u2YaVRTZxrHVVyAkLAGQiAsYQsIWKt2UZtO3XscrSu2VgVtLSACIlYoeyBsAuICbhUUBWWTTbawR2XEBYmlFitWe3p6ZvkwZ77OfJj5z3Mvb2xOx86MncSOnt5z/ude3nvDfX959kya9Ovx6/H/e2Tcmzl9b5OdbG+ThSy6fdKMFxekV6wsHvJCRrcYYSUWj6MqbEdiLtjq4mrtdAnNYt1nrU66lDYXXUanXKfuUejy+mbpCgfm6YqvLNCVXFukO359me7U0Cpd2c11uorbm3RnboboztzeqKsYpr+H1+oq7qwhrdZVjHBaRde/1RVoA29ndLp6GxVE3eusLB0OQHafM7YVCxB5ygG7K+yxt8YRSZelyNC4Q93tjbw+fxRqg3Ho2lyUDL2Jk7eUKBtejLMj76Lyi9Wovrce9fffR9ODrWj9Zgc6Hoej+7vd6P8+Dlf+sB+Df0zC4J8S0fZ4CzL73Dcb3SJ5WpnyxJ3ZBCLBtkMC7DrtgNhKMeLrxSBrILXdGRldMqh7CWaAYK4G4cjgXJReZzB33kHF3RWo+nIV6sbWomEsBE3jH6L1URi6vt2Jvu+jMPD7PWj7LgR1DxcjX+uXYBLXytN6Kst0C3mQ0MMCRJU7IO6CIz6td0RqmzNSOwlE44KsXjfk9nmhcECB4sFZKB2aixO3XsfpO0qc0S3GuS+W4yIPsw6N9zfh8sMtaH+8HS2P1qFqfB5qxpX0DvkxeuUU0mSjgxRq/ZXnR5fzIGFHrBB9lqxxwQmfNTlj7VY/LFseQBZxQ2a3G7J7PJDX64UFs2b9R1V9uRhnRl9F+ehMVI0tQF6/Xwu9bhoDMT7MocEgJeffHMj2EivEVjhhX60TtibK+Q0tJxBVpwxZnZ7I0siR0yPH9v0B+ChxJj5JCkJ40iuITJ6DXcnz8P72Ofxn3lsXSBZT4NgNf5wengd1u+LWm8tFdvQ6c5PBHBqcrbw0tg1qDuSoJfacd0ZI+A/fOmeR9FZ3kgcyW+XI1ngjv8cPhf3+OKgNRPGVQBwkd8vTeGHp0iC8/XoQsht9ac0PRyg5qFtmfbtgtdCPXsWBWJG4FD/V6CAlg/OVLQ8iKQYk2JBiiUVLFDzAqnUT52VLA5DS4EHyRFqTHOnNcmS0EVQHZbMuV2R3uyKH3C50rx//fIRKgdw2X+S1+yG9Ovgvr62yWkSvkZOcSbYkS9J044PcVCo1D+N5kJVhEiyYG4QNsVLsqbXkN7aEwGIrbRBTYYvYc7YUP/ZIpESQ1uqMzHaC6HJHaq0n3poThBXLApHV5IPMS95IOR/0tzfWCkLpFa+S/EluJDGzCgdiZlSYEzcXKXu/SScQJ4SkC/DRcQvEVJOL1U2ALF6kwK4ya0SetMHuU7aIo2TwabUEKU1SqDvdkKPxxOZIf/7Z2AN+yGzwRnJV4D8WbhZwaVZJeoMUzKziSBIx9zI+iPZxLg8SWjoVu85bILZGgLha4ROQyJPWCC+xRcRxO0SXUWqudEZaI6XkTnKvFi/eGu8uD+JdKq1WjhVRgiL615xL/YY0/7mB9D1S8SBhx6dh9wVLxNUJsLdW9AQk/JgNdh61Q8RRe0Sfoqx2TorURlfkkjUiVBPWiFYHIL+DcytfbFHbaRkEZ5HXSIEkd5KDgWuZIkb28yA7Tk4na1hhX4MI++ptnoBwlgg/6oCIEgcCkSCh0hUZzW440OOFlSsDsXBOMAo0/sjr8EVmkxfSqn2xIdmmgVmDixFfkotBsE8zSdZq/TqKD/aPP59BrYkIic02SGi0nQj2xQpEU9uy67gjdpPiyqRIvEgty2U5shp9+GdCtgbi8BUFDmh8oSKQ1DpPLmNhU5rkGLOGp0nd6oc6EsaDhJeZY3+TDVI6SK12/CaXLvFH3DlHxH4uQexpCeIrXJBS647cDi/sKQzgn4k7QPWE6kZ+pw9vkdQaD/6ZA01vYYtKmsCsYWey1Kuv7DVfbeRBIs9aIIkAMrrtka5xYHXEHwnVzgQgxT5SQpUMGVRT8rp8sC0qkH9GXa9AUZ8Pctq9oWqQI6XOg1KyG9Ipjk50r/97xGHPEANrTDUJCNdrVY6u5EGizlkiTWOPrD4x1D2OEyDLApB0yYXcyZWPjeRqd959Crv9sPo9io/ZwSjWKlDQ5Qs1Vf70ek9yLaotDW58HKk7XFF+bdNfY8ql801mDX33e+bu23yLElNlhcxeMXK0El76XiujRYbkOjek1HDftAdyLvugkNqURQuCseTtIL4dyeXciqp+ar07uZWMQGTU1siQqZFS1+yKkqvv/DmxVuxtQhCZ8uTIXL5p3HNRiOx+J+QPSlFA0oNktVGv1eiB9EueyGz05usFB7LwlWC+fhSSW+V2yKFqnLAGB8LVGVW7Cw1lVDj7nOiLkdJwpngUf1noYBIQda+MJsSZPMjeGhHyrjqjcEiKousyFA66o2DAna8XnNtkNXsjm6yRT2m2qNcPh7R+ZA1fFPT6ILuderBGcql6GVIuuSKtWQpVhxRZPRJyW0eo+uxpMhQjodV2OK52koXxLaIJUpbcVvAg++qscYAscXBIhuIhdxwc9EDRFTlfL/I6OEtQQLf6UnbiLEIdbj8B9RNYtxeBksUayf3qXQnEBanNNJC1S6DSOFHycEBalx2SNSIktVsjvknQvbGWT8HGOw62rJ9Z0rMGSWfnU73wSIurlITG1whDoyuFO6IqrD/+pMQ6cmuBKOYDlTB+Q6pwfwjpgyxh/NZsUVworXP6MF+0J0QliF+fJPh0zWeC/etSLOI351jEhB6xjNx5ynJnRIX5jugq87CoC9NCIytJF8xCd9eaexoVpKwsX9jd1YsMVTbU5btkzH+nsDRpztKmmNUCN9ZquJIkbJ2Tk8F9N3YtZp+1MEkl/1eQMmF//wDSVWoUlRYZgpixdMkVMWvWJzkxiVmBs2H37Nh9RyYHtm5pkpb9p0AGBjiQLAIplbFlQ6vMYBsSsc1Zs2srtm7Jrn9839KkBfAZQfQw05mb6TduwTY5nWkGW9PfM2frU032Y8PTQLTaKzxI6Q8gP4YxY35uqKls3YxdP+3e84HQg1y9eu1pIHoYQ6Bn1eTnAqEHGRz83U+BGML8XE16biDXrw/9O5D/9vhlAAxBbty4YQyQX/bgQG7duvXigzQ3NwuHh4dffBDuGBkZYSDlLzbI3bt3Xw6Q0dHRlwPk3r17EyDlLzjI2FdjLwfI1/fvvxwg4w8e8CDlLzrIw4cPXw6Q8fFxY4DoeyyzZ2geJxs5Rr7+X2JkssG8Ys6GLHODgWuawZA1zWB+mWIwIhgpa4397Kw1hW2OmwyFPxp1RWxNPxILnjJd6gcwI9eRZyuIU9iGuM3ash8cxOxsz9bs2Fn/I4WQARmOwsZzr59R2c3YZgx/XXFmZwk7Oz4FTGRgkSex9E82/eGcP1eyngAAAABJRU5ErkJggg==',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKdElEQVR42u2YaVAUZxrHVeR2uGEYcBgYrpFzVUxKjkkEhBjBeKy6JKhoUMJ9iWSQWwh4BQ8QQU4FhaCGS8EDFBRvwxjjTZLdTdVu9tPu13zJf5/uebF6qc25bWW10lX/mu63Z/p9f/O8z9XTpv1+/H78/x7FD7wNsrqt5FndxvLUs9MMX16QIVt11Q1XFF+wRVy18dfJrZbjaccttZmfWGlze2y1mn6pNv+Mo7Z4UKktu6jSVg77a/dcXqCtGg3SVl8N1dZej9DW34jWNtxcqW2+tZa0Rtt8ZzWJru+s0DbfXa5t/WyZtnWcUzSdR2l3j/jcKR6c7SYqSNmQTF1z1wvlwzKsrzJFYr0NUlqtkdVph7w+BxQNKrDjvBsqhuZg92U/VF0JwMHrC3H4lhoNd8LQMr4EbZ8vQ8cXK3Hy0Vp0P4lF/8RGDHy1BRf+kozhbzIw+vdtGPs2D2P/+BBnvo5F6bDiXdEtUjkiVx/+bC6B2GP9PlMkNdggvc0W2V220PRJkX9WhuLzcpQNuaHy0hzsGfXFgasBqCGYuttqNH62CK333kLb/Wh0PlyBUw9X49On76L3yw0Y/CoeQ98k4/LfMnDmr2vQNRGGnSOeuS9ka1WOuKgbtcE8yIb9pkhuskFmux1yTtphRawnIiK8UDTgiBKCKb+gxK4hT+wd9cOus/OxPnEuwkL8ETLXD4vV/ohPD0TztWU4+cUadD9+F30Tcej5ciXanixAxzM1zaE8RFPOIE0XHWTPyBz1sfuRPEjcgVlIbbZF1nEpYnOVCPL3R0SkFwrOylE86ITSc84oP69ESY8Ki0L8+Ptr1s1DwocBWPteAH8dGTYPDaNLcXQ8FI3jc9FwzxttD4Noa3r20nQzGYj4MPvGfNUdD1bxIBsPzkJaiz1Wb3HlF8WDkEUK+2ejsNcZRX0uKD3jgpgP5vD3kkt8aJvNQ83V11F7NRBJBfP58bh0L+y/4omD11Sovx2AsrOq2wsjzaxoOiOSvgBkuoggc9WnHq3HjiF7xBRZIjzci19M9EoV/7l4sRe2n3KC5qQT8k+6oKhbibff9uHv7Rz0xp4hb+w674nyAbJUtws/vjTKh8Y8sG/YFzu6/f+8MFqioqk4kFkkQ4FlxAOpHgtU9z5N5EHe3iBFUIAvYjJdoDnl8Bxk2wkn5LQpkHvcGZpOZ0St1MFubXIiKzmiuFdOgApsrddZctlKb5T1eKCg3fefC5aahtM0SpKMZEkyIRmID3JLrR58ls2DrCkwxaZqIyS1GSHthDG/qLBQFUUyC3xw2AKJdRZIabTCphJHBM+lbfeWNzJrXbC9ywnpB1wRsdibH08sdYemxee716NMN9IUASQvkjPJjiRhVtETFebwrVD10JdFBCLFuoN6SGgxQMpxY6R3mPAgoaGe2FxrhvgD5kg4aIHkWhtktEgRX+6EkACf577EKWSeLzbne2DrEa/vA1ebbKfHh5KCSPNIHiQHkgXJWPTtxYGMfF3Bg6yvmYmkYxyEKTI6JDqQRSpsqTZHfJUl4vdbIvGQLZIPOiA8TLe9IiO8sXazCqvWeeLNhT4Iec0PwRGOF+jRUaQlDOY1ZhU58xWTKU4vDsjwVyU8SFytPlnDBJldpsj6xOz51ko4ZEEQVthywBpJh+ywPMaDv7c6zhN5nN8cc8G2ZhekVzvzYEF/8IdcJuMsEk2KIAWSfEkKkg3JVHQ/4Xzk3MQ2HmRTnQEyOmch51Mz5Jy20IGEkY/UWSGxxgYfVNsg+bAUbyz0RfB8X2janJF3wgV5x5TIaSSQGgXeL9ZFruBg92/p8e+Q3iKFkPyZ09ux6CUyCEWt/ifJvLNvPmKI7FNm0PRbQNNrxS8oPFyF9BbaTnV2SCWlH5Eh0M8Hixd5Y3uHMwpOKKE5qiS/cEbGIQWS9jnpol2YDyLirY+x7aVmfuJKkpLMGIieaCBcHjn9OI4HSWgyQm6PBQoGLVA4YMXC7xxspUyf2WSPjEZ7ZDU7YpGaLEKOnd/uisIOVwGIEzaXKvjfLVmmQs6hAISus9pD07xBms8cXsZAjASRS5zM3kmFHgeS2GKM7WesUHLRhsp6G11mJxBNlwxbjzkgp5V0VI6YBJ2PrNnohfzjBNLqTCAKpFU7YclSXdaPzXZGSo09ChvCv3/zPZtsFoY9fyByiVNrtd1fijIqUVKOmqDwvDV2XLJD+ZCdLipRrZXf7UiJcDZy22ZD064g3+CyuzerrXywepMKf9ygwpvBunD8zp88kVbniLRaGVJrpShpjvwucLlZLE3HZXhHlhjFBeGq3+Z7b/Agae2zUDpki4pRe1RcsX8OUtInR8FpKlEoq3N+UXKKGrFP3BCXNYcHCaLqNzjAjyKWF9ZluSDziAP5kgNS6uwp79gitV4KTfOCfy2IMg0TgAhDsDj9SN14AF80ZpyQoPySFDuvOWD3mAN2jchRMUzl+4ACxT0EcJrqqdNuVD+5o7zXncoQNxQTVH6HC3KPOvElS3bTbAJxJBB7Cg5SpNRToKi3poBhh8wW5Tf+0QbeL2RrlQ3JqUP05kG43FF5VYa9Nxzw8Q059l5TUCOlwM6LSlQMuKK8zw0f9bmjot8DFWfc6dwNpd3k8F1Umx1XkA/JkU3BIKuJLNIgJUvYIeUIdZwNVkhtsaD2wApJRy0+9wnjI5eRuFvrnK+6+o6KB9naZY5dYzqIqhsKVF13wcdXlNhzyQ27L7jzFW3lgAdVvaRz7qgcJMv00zb7lOqtTifeh7JbHCjCyZDWQOGagzhijZQmS6S2mvNbN7PTDMntpsOzF/IWES/8fty7yrv64nLktQQitcG5MLPNfkN2p2RDWodkY3q7+ftJdeYJm/abJa/fZZYe85Ek670dkuyYMknWunKzTE7c+ap8Sc7ybaaa6AyT7UtSTAoiE40KOUWkGuQvSTfIi8ox2Lai0DBzbYV+Suxe/YS4Wr3NcYcN3UVNiI2NOyUXzg+huKQcZU1JcvbgGezfMmBOac5KC25L2LNPOyYpC6kKlvA8WHRSsXMlq7HsmG+YCJKhmCCNkkuXLqOopAx7a/bK2fAkzExWcpuwJGbBZM6uzdi5FVuojEUlOZMDA7dm35uEEL+x4kAuX+ZAdhBIzVSQSRgD5pwmAhmzMWN2LRFATcqSwc5i35sK8cJBpsLosZivzxYzeT5TMGYogBLKSPAbvRcCMQkyMjLKg9T8J8g0wYQzfob0BGD6U2D1prw9Ef91EAdy5crVHwIRwvyUfgr0xQAIQcbGrv0YyM89fgzyxR8cyPXrN8QA+W0PDuTmzZuvBsjt27dffpCenh7J3bt3X34Q7hgfH2cgTS83yL17914NkPv3778aIA8ePNCBNL3kII8ePno1QJ48fvxqgDx7+pQHaXrZQSYmJl4NkGfPnokBMlkg6v2CwnG6yD7y5H/xkemCxsuINVhGgmZLX9AZ6gv6lhmC8l+kqPXoV0etGWxxwlZX2M9LWJvL3TcVtMiGAjg98fPIL0uIM9iCJKw3596y2LJPazY22bdPvrCQMCAjAYh42+tXZHY9thjhayLZf3ldNBXMTGCR5770b5YOum8i8zfCAAAAAElFTkSuQmCC'
    ];

    var teeMarker = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAlCAYAAAAjt+tHAAACHElEQVRYhdWXwWqjUBRATyRkRNouSjrrQNdlGGZVujWfVPIN0k/SbcmqDPMBhe4buugEsSLahfemL6KNT186zIWLMRHP8eY+9U6qqgJguVxOAQ+YyXaK2yiAEsiBMo7jAmBSVZXCZ4AvORMBzxG8FIEcyCTzOI6LSRiGCg+AM9n6HKcCGZACr7LN9Sp9gX+P5snaMXgvVpvwWj6WQGGWPojmyXqxuCAIvuH7M6fgLMtJ0zcikvVqE14hf4PZeD5wFDjQPKf2mafdrnkUeIvEjqldbtftt491jgvPHqxxd1nneImR61wlRojYCyhQoXeXH99/iUBTpilhKWIvoLA2ka59pwJNiTZYm2RHuLvfW0DNGN4DA4HuBByFO4F/sgwdxH8s0LwJfamAg4fQOAENB0tx2MPIEdxewGHpNfRWXPY6uuuqh1WjhLoChZFkWT7kZL3COPeO6fExLmUAafp2FAl9LdddYZZTA56uNuF1RPtgslhccH5+2gv28vKXp6fn1t9kMEkx5oJSdl7lgCv2RzMPICK5Bw5KKHy1CW/kK+2v5miWAWWf4XT3Dh/Nk4fPKmHAf7HfW93D6YHxXLfm+Pa7TcKA/zRL3BBoH88/CxHzgRPJIJonf0wJA/5D4FvJTEFdcVCgQ+JEKwGYV77FAt5boEXiTCQeAP3Pt9TN1RtuJdCQCCR12sypS5/awK0FDAltSl2qusRyGzjAO0ELHeYOyqR4AAAAAElFTkSuQmCC';



    //var infoWindow = new google.maps.InfoWindow();
    var createMarker = function(info, icon) {

        var marker = new google.maps.Marker({
            map: $scope.map,
            position: new google.maps.LatLng(info.holeLatitude, info.holeLongitude),
            icon: icon,
            title: info.courseName
        });
        //marker.content = '<div class="infoWindowContent">' + info.courseAddress + ', ' + info.cityName + ', ' + info.stateName + '<p> <i class="glyphicon glyphicon-phone"></i> ' + info.phone + '</p> </div>';

        /*google.maps.event.addListener(marker, 'click', function() {
            infoWindow.setContent('<h5>' + marker.title + '</h5>' + marker.content);
            infoWindow.open($scope.map, marker);
        });*/
        if (icon !== teeMarker) {
            $scope.markers.push(marker);
        } else {
            $scope.teeMarkers.push(marker);
        }
    };

    $scope.openInfoWindow = function(e, selectedMarker) {
        e.preventDefault();
        google.maps.event.trigger(selectedMarker, 'click');
    };


    $scope.createMap = function(golfCourseId) {

        console.log('Create Map called');
        $scope.holeNumber = 0;

        $http.get(url + '/GolfCourses/golfCourseLocation?golfCourseId=' + golfCourseId)
            .success(function(success) { //success
                console.log('success ' + JSON.stringify(success));

                $http.get(url + '/GolfCourses/golfCourseTeeLocation?golfCourseId=' + golfCourseId)
                    .success(function(golfCourseTeeLocation) { //success
                        console.log('success ' + JSON.stringify(golfCourseTeeLocation));

                        $scope.courseLatitude = success[0].courseLatitude;

                        $scope.courseLongitude = success[0].courseLongitude;

                        var mapOptions = {
                            zoom: 14,
                            center: new google.maps.LatLng(success[0].courseLatitude, success[0].courseLongitude),
                            mapTypeId: google.maps.MapTypeId.SATELLITE,
                            streetViewControl: false,
                            mapTypeControl: false,
                            zoomControl: true,
                            rotateControl: true,
                            zoomControlOptions: {
                                position: google.maps.ControlPosition.RIGHT_TOP,
                                style: google.maps.ZoomControlStyle.SMALL
                            },
                            panControlOptions: {
                                position: google.maps.ControlPosition.RIGHT_TOP
                            }
                        };


                        $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

                        $scope.holeLatLng = [];

                        $scope.teeLatLng = [];

                        $scope.markers = [];

                        $scope.teeMarkers = [];

                        //icons for markers
                        $scope.icons = [];
                        for (var j = 0; j <= HOLES.length; j++) {
                            $scope.icons[j] = HOLES[j];
                        }

                        $scope.courseHole = [];
                        for (var i = 0; i < success.length; i++) {
                            $scope.holeLatLng.push({
                                'courseHoleID': success[i].courseHoleID,
                                'holeLatitude': success[i].holeLatitude,
                                'holeLongitude': success[i].holeLongitude
                            });
                            $scope.courseHole.push({
                                'holeNumber': i + 1
                            });
                            createMarker($scope.holeLatLng[i], $scope.icons[i]);
                        }

                        for (var i = 0; i < golfCourseTeeLocation.length; i++) {
                            if (golfCourseTeeLocation[i].teeLatitude !== null) {
                                $scope.teeLatLng.push({
                                    'courseHoleTeeID': golfCourseTeeLocation[i].courseHoleId,
                                    'holeLatitude': golfCourseTeeLocation[i].teeLatitude,
                                    'holeLongitude': golfCourseTeeLocation[i].teeLongitude
                                });
                            }
                        }

                        for (var i = 0; i < $scope.teeLatLng.length; i++) {
                            createMarker($scope.teeLatLng[i], teeMarker);
                        }

                        //$scope.zoomHole($scope.courseHole[0].holeNumber);

                        for (var i = 0; i < $scope.holeLatLng.length; i++) {
                            $scope.markers[i].setOpacity(1.0);
                        }

                        for (var i = 0; i < $scope.teeLatLng.length; i++) {
                            $scope.teeMarkers[i].setOpacity(0.0);
                        }

                        console.log('HoleLatLng ' + JSON.stringify($scope.holeLatLng));

                        console.log('HoleTeeLatLng ' + JSON.stringify($scope.teeLatLng));

                    }).error(function(error) { //error
                        console.log('Error in fetching tee details ' + JSON.stringify(error));
                    });

            }).error(function(error) { //error
                console.log('Error in fetching course details ' + JSON.stringify(error));
            });

    };

    // Add a Hole control
    $scope.HoleNumber = function(controlDiv, map, holeNumber) {
        controlDiv.style.padding = '5px';
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#E6E6E6';
        controlUI.style.border = '1px solid';
        controlUI.style.opacity = '0.7';
        controlUI.style.borderRadius = '5px';
        controlUI.style.marginTop = '10px';
        controlUI.style.marginLeft = '10px';
        //controlUI.style.cursor = 'pointer';
        controlUI.style.textAlign = 'center';
        controlDiv.appendChild(controlUI);
        var controlText = document.createElement('div');
        controlText.style.fontFamily = 'Arial,sans-serif';
        controlText.style.fontSize = '12px';
        controlText.style.paddingLeft = '8px';
        controlText.style.paddingRight = '8px';
        controlText.style.paddingTop = '8px';
        controlText.innerHTML = 'Hole<br><p style="font-size:30px;margin-bottom:0px;">' + holeNumber + '</p>';
        controlUI.appendChild(controlText);

        // Setup click-event listener
        // google.maps.event.addDomListener(controlUI, 'click', function() {
        //     map.setCenter(london)
        // });
    };

    // Add a Course control
    $scope.Course = function(controlDiv, map) {
        controlDiv.style.padding = '5px';
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#E6E6E6';
        controlUI.style.border = '1px solid';
        controlUI.style.opacity = '0.7';
        controlUI.style.borderRadius = '5px';
        controlUI.style.marginTop = '10px';
        controlUI.style.marginLeft = '10px';
        controlUI.style.cursor = 'pointer';
        controlUI.style.textAlign = 'center';
        controlDiv.appendChild(controlUI);
        var controlText = document.createElement('div');
        controlText.style.fontFamily = 'Arial,sans-serif';
        controlText.style.fontSize = '12px';
        controlText.style.paddingLeft = '8px';
        controlText.style.paddingRight = '8px';
        controlText.innerHTML = '<p style="font-size:20px;margin-bottom:0px;"><img style="height: 15px;margin-bottom: 2px;" src="app/img/left.png"> Course</p>';
        controlUI.appendChild(controlText);

        // Setup click-event listener
        google.maps.event.addDomListener(controlUI, 'click', function() {
            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].clear();

            $scope.map.controls[google.maps.ControlPosition.LEFT_CENTER].clear();

            $scope.map.controls[google.maps.ControlPosition.RIGHT_CENTER].clear();

            for (var i = 0; i < $scope.holeLatLng.length; i++) {
                $scope.markers[i].setOpacity(1.0);
            }

            for (var i = 0; i < $scope.teeLatLng.length; i++) {
                $scope.teeMarkers[i].setOpacity(0.0);
            }

            $scope.map.setCenter(new google.maps.LatLng($scope.courseLatitude, $scope.courseLongitude));
            $scope.map.setZoom(14);
        });
    };

    // Add a HoleLeftNumber control
    $scope.HoleLeftNumber = function(controlDiv, map, holeNumber) {
        controlDiv.style.padding = '5px';
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#E6E6E6';
        controlUI.style.border = '1px solid';
        controlUI.style.opacity = '0.7';
        controlUI.style.borderRadius = '5px';
        controlUI.style.marginLeft = '5px';
        controlUI.style.cursor = 'pointer';
        controlUI.style.textAlign = 'center';
        controlDiv.appendChild(controlUI);
        var controlText = document.createElement('div');
        controlText.style.fontFamily = 'Arial,sans-serif';
        controlText.style.fontSize = '12px';
        controlText.style.paddingLeft = '8px';
        controlText.style.paddingRight = '8px';
        controlText.innerHTML = '<p style="font-size:20px;margin-bottom:0px;"><img style="height: 15px;margin-bottom: 2px;" src="app/img/left.png"> Hole ' + holeNumber + '</p>';
        controlUI.appendChild(controlText);

        //Setup click-event listener
        google.maps.event.addDomListener(controlUI, 'click', function() {
            $scope.zoomHole(holeNumber);
        });
    };

    // Add a HoleLeftNumber control
    $scope.HoleRightNumber = function(controlDiv, map, holeNumber) {
        controlDiv.style.padding = '5px';
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#E6E6E6';
        controlUI.style.border = '1px solid';
        controlUI.style.opacity = '0.7';
        controlUI.style.borderRadius = '5px';
        controlUI.style.marginRight = '5px';
        controlUI.style.cursor = 'pointer';
        controlUI.style.textAlign = 'center';
        controlDiv.appendChild(controlUI);
        var controlText = document.createElement('div');
        controlText.style.fontFamily = 'Arial,sans-serif';
        controlText.style.fontSize = '12px';
        controlText.style.paddingLeft = '8px';
        controlText.style.paddingRight = '8px';
        controlText.innerHTML = '<p style="font-size:20px;margin-bottom:0px;"> Hole ' + holeNumber + ' <img style="height: 15px;margin-bottom: 2px;" src="app/img/right.png"></p>';
        controlUI.appendChild(controlText);

        //Setup click-event listener
        google.maps.event.addDomListener(controlUI, 'click', function() {
            $scope.zoomHole(holeNumber);
            $scope.isActive(holeNumber);
        });
    };

    function radians(degrees) {
        var TAU = 2 * Math.PI;
        return degrees * TAU / 360;
    }

    function degrees(angle) {
        return angle * (180 / Math.PI);
    }

    $scope.getBearing = function(startLat, startLong, endLat, endLong) {

        startLat = radians(startLat);
        startLong = radians(startLong);
        endLat = radians(endLat);
        endLong = radians(endLong);

        var dLong = endLong - startLong;

        var dPhi = Math.log(Math.tan(endLat / 2.0 + Math.PI / 4.0) / Math.tan(startLat / 2.0 + Math.PI / 4.0));
        if (Math.abs(dLong) > Math.PI) {
            if (dLong > 0.0) {
                dLong = -(2.0 * Math.PI - dLong);
            } else {
                dLong = (2.0 * Math.PI + dLong);
            }
        }
        return (degrees(Math.atan2(dLong, dPhi)) + 360.0) % 360.0;
    };

    $scope.getMidPoint = function(startLat, startLong, endLat, endLong) {
        var dLon = radians(endLong - startLong);
        var Bx = Math.cos(radians(endLat)) * Math.cos(dLon);
        var By = Math.cos(radians(endLat)) * Math.sin(dLon);
        var midPoint = {
            Latitude: 0,
            Longitude: 0
        };
        midPoint.Latitude = degrees(Math.atan2(
            Math.sin(radians(startLat)) + Math.sin(radians(endLat)),
            Math.sqrt(
                (Math.cos(radians(startLat)) + Bx) * (Math.cos(radians(startLat)) + Bx) + By * By)));
        midPoint.Longitude = startLong + degrees(Math.atan2(By, Math.cos(radians(startLat)) + Bx));
        return midPoint;
    };

    $scope.isActive = function(holeNumber) {
        return $scope.selected === holeNumber;
    };

    // $scope.courseButton = function() {
    //     $scope.map.controls[google.maps.ControlPosition.LEFT_CENTER].clear();
    //     //Course
    //     // $scope.course = document.createElement('div');

    //     // $scope.hole = new $scope.Course($scope.course, $scope.map);

    //     // $scope.map.controls[google.maps.ControlPosition.LEFT_CENTER].push($scope.course);

    //     return $scope.selected === 'course';
    // };

    // function rotate(){
    //     var heading = $scope.map.getHeading() || 0;
    //     $scope.map.setHeading(heading + 90);
    // }

    // function autoRotate(){
    //     if($scope.map.setTilt() !== 0){
    //         window.setInterval(rotate, 2000);
    //     }
    // }

    $scope.zoomHole = function(holeNumber) {

        $scope.selected = holeNumber;

        $scope.showParSI = 'show';

        $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].clear();

        $scope.map.controls[google.maps.ControlPosition.LEFT_CENTER].clear();

        $scope.map.controls[google.maps.ControlPosition.RIGHT_CENTER].clear();

        $scope.holeNumber = holeNumber;

        for (var i = 0; i < $scope.holeLatLng.length; i++) {
            if ((holeNumber - 1) === i) {
                $scope.markers[i].setOpacity(1.0);
            } else {
                $scope.markers[i].setOpacity(0.2);
            }
        }

        for (var i = 0; i < $scope.teeLatLng.length; i++) {
            if ((holeNumber - 1) === i) {
                $scope.teeMarkers[i].setOpacity(1.0);
            } else {
                $scope.teeMarkers[i].setOpacity(0.0);
            }
        }

        //alert(Math.radians(90));

        var midPoint = $scope.getMidPoint($scope.teeLatLng[holeNumber - 1].holeLatitude, $scope.teeLatLng[holeNumber - 1].holeLongitude, $scope.holeLatLng[holeNumber - 1].holeLatitude, $scope.holeLatLng[holeNumber - 1].holeLongitude);
        console.log('midPoint : ' + JSON.stringify(midPoint));
        var midLocation = new google.maps.LatLng(midPoint.Latitude, midPoint.Longitude);
        console.log('midLocation : ' + JSON.stringify(midLocation));

        //Bearing of Hole and tee
        var maxBearing = $scope.getBearing($scope.teeLatLng[holeNumber - 1].holeLatitude, $scope.teeLatLng[holeNumber - 1].holeLongitude, $scope.holeLatLng[holeNumber - 1].holeLatitude, $scope.holeLatLng[holeNumber - 1].holeLongitude);


        console.log('maxBearing : ' + JSON.stringify(maxBearing));

        //Hole Number 
        $scope.holeDiv = document.createElement('div');

        $scope.hole = new $scope.HoleNumber($scope.holeDiv, $scope.map, $scope.holeNumber);

        $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push($scope.holeDiv);

        if (holeNumber === 1) {
            //Hole Number on Left 
            $scope.course = document.createElement('div');

            $scope.hole = new $scope.Course($scope.course, $scope.map);

            $scope.map.controls[google.maps.ControlPosition.LEFT_CENTER].push($scope.course);
        }

        if (holeNumber !== 1) {
            //Hole Number on Left 
            $scope.holeLeftDiv = document.createElement('div');

            $scope.hole = new $scope.HoleLeftNumber($scope.holeLeftDiv, $scope.map, $scope.holeNumber - 1);

            $scope.map.controls[google.maps.ControlPosition.LEFT_CENTER].push($scope.holeLeftDiv);

        }

        if (holeNumber !== $scope.holeLatLng.length) {
            //Hole Number on Right 
            $scope.holeRightDiv = document.createElement('div');

            $scope.hole = new $scope.HoleRightNumber($scope.holeRightDiv, $scope.map, $scope.holeNumber + 1);

            $scope.map.controls[google.maps.ControlPosition.RIGHT_CENTER].push($scope.holeRightDiv);

        }

        //var latLng = new google.maps.LatLng($scope.holeLatLng[holeNumber - 1].holeLatitude, $scope.holeLatLng[holeNumber - 1].holeLongitude); //Makes a latlng

        $scope.map.setZoom(17);

        $scope.map.panTo(midLocation);

        //  var bearing = $scope.map.getCameraPosition().bearing;

        // console.log('bearing : ' + JSON.stringify(bearing));

        //$scope.map.setHeading(maxBearing);



        /*$scope.map.moveCamera({
            'target': midLocation,
            'tilt': 60,
            'zoom': 18,
            'bearing': 90
        });*/
        //  $scope.map.moveCamera(CameraUpdateFactory.newLatLngZoom(midLocation, 18));

        // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
        // CameraPosition cameraPosition = new google.maps.CameraPosition.Builder()
        //     .target(midLocation) // Sets the center of the map to Mountain View
        //     .zoom(18) // Sets the zoom
        //     .bearing(90) // Sets the orientation of the camera to east
        //     .tilt(30) // Sets the tilt of the camera to 30 degrees
        //     .build(); // Creates a CameraPosition from the builder

        // $scope.map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    };



    var colorCodeArray = ['tie-row', 'team-1', 'team-2', 'team-3', 'team-4'];

    var colorCode = colorCodeArray[0];

    $scope.init = function() {
        var roundId = atob($stateParams.roundId);
        $scope.roundId = roundId;
        console.log($scope.roundId);
        golfCourseDetails();
    };

    //fetching Golf Course Details where round is been played
    function golfCourseDetails() {

        var roundId = $scope.roundId;

        $http.get(url + '/RoundGolfCourses/courseDetailsByRoundId?roundId=' + roundId)
            .success(function(roundGolfCourses) { //success
                var jsonList = [];
                var jsonString = {};
                console.log('Course details : ' + JSON.stringify(roundGolfCourses));

                $scope.golfCourseId = roundGolfCourses[0].id;

                $scope.createMap($scope.golfCourseId);

                var roundType = roundGolfCourses[0].description;

                var roundDetails = {
                    roundType: roundType,
                    scoreType: roundGolfCourses[0].scoreType,
                    roundDate: roundGolfCourses[0].createdDate,
                    cityName: roundGolfCourses[0].cityName,
                    stateName: roundGolfCourses[0].stateName
                };

                $scope.roundDetails = roundDetails;

                if ((!angular.isUndefined(roundGolfCourses)) && roundGolfCourses.length !== 0) {

                    for (var i = 0; i < roundGolfCourses.length; i++) {
                        if (!angular.isUndefined(roundGolfCourses[i])) {

                            var courseName = roundGolfCourses[i].courseName;
                            var totalHole = roundGolfCourses[i].totalHole;
                            var par = roundGolfCourses[i].par;
                            var clubName = roundGolfCourses[i].clubName;

                            jsonList.push({
                                'courseName': courseName,
                                'totalHole': totalHole,
                                'par': par,
                                'clubName': clubName
                            });
                        }
                        jsonString = jsonList;
                    }

                    console.log('round type : ' + roundType);

                    if (roundType === 'Stroke Play') {
                        displayScoreCardForStrokePlay();
                    } else if (roundType === 'Match Play') {
                        displayScoreCardForMatchPlay();
                    } else if (roundType === 'Fourball') {
                        displayScoreCardForFourBall();
                    }
                    $scope.RoundGolfCourses = jsonString;
                }
            }).error(function(error) { //error
                console.log('Error in fetching course details ' + JSON.stringify(error));
            });
    }

    function displayScoreCardForStrokePlay() {
        fetchScoreDetailsForStrokePlay();
        fetchTotalScoresForStrokePlay();
    }

    //fetching total score for each round playing member
    function fetchTotalScoresForStrokePlay() {

        var roundId = $scope.roundId;

        $http.get(url + '/ScoreCards/membersTotalScore?roundId=' + roundId)
            .success(function(response) {

                console.log('Summary : ' + JSON.stringify(response));

                var groupList = [];
                var memberList = [];

                var handicap = 36.4;

                var courseHandicap = 0;
                var courseHandicapUSGA = 0;
                var nickName = '';
                var grossScore = 0;
                var netScore = 0;
                var grossScoreWrtPar = 0;
                var netScoreWrtPar = 0;
                var tempPlayerId = 0;
                var tempGroupId = 0;
                var groupIndex = -1;

                var pieces = [];
                var playerInitials = '';

                if ((!angular.isUndefined(response)) && response.length !== 0) {

                    for (var i = 0; i < response.length; i++) {
                        if (!angular.isUndefined(response[i])) {

                            if (i === 0) {
                                tempPlayerId = response[i].id;
                                netScore = 0;
                            } else if (tempPlayerId !== response[i].id) {
                                memberList.push({
                                    'nickName': nickName,
                                    'playerInitials': playerInitials,
                                    'handicap': handicap,
                                    'courseHandicap': courseHandicapUSGA,
                                    'grossScore': grossScore,
                                    'netScore': '' + netScore,
                                    'grossScoreWrtPar': grossScoreWrtPar,
                                    'netScoreWrtPar': netScoreWrtPar
                                });
                                tempPlayerId = response.id;
                                netScore = 0;
                            }

                            if (tempGroupId !== response[i].roundGroupId) {

                                if (i !== 0) {
                                    groupList[groupIndex].memberList = memberList;
                                    memberList = [];
                                }

                                tempGroupId = response[i].roundGroupId;
                                groupList.push({
                                    groupId: tempGroupId,
                                    groupName: response[i].groupName,
                                    memberList: []
                                });
                                groupIndex++;
                            }

                            handicap = response[i].handicapIndex;

                            courseHandicap = response[i].courseHandicap;

                            if (parseFloat(courseHandicap) < 0) {
                                courseHandicapUSGA = '+' + courseHandicap.toString().split('-')[1];
                            } else {
                                courseHandicapUSGA = courseHandicap;
                            }

                            nickName = response[i].nickName;
                            grossScore = response[i].score;
                            netScore = response[i].netScore;

                            grossScoreWrtPar = response[i].grossScoreWrtPar;
                            netScoreWrtPar = response[i].netScoreWrtPar;

                            if (grossScore === 0) {
                                grossScore = '-';
                                netScore = '-';
                            }

                            pieces = [];
                            playerInitials = '';

                            pieces = response[i].nickName.split(' ');

                            for (var j = 0; j < pieces.length; j++) {

                                console.log('Pieces : ' + pieces);

                                if (j === 0) {
                                    playerInitials = pieces[j].substring(0, 1).toUpperCase();
                                } else {
                                    playerInitials = playerInitials + ' ' + (pieces[j].substring(0, 1).toUpperCase());
                                }

                                if (j === 3) {
                                    break;
                                }

                            }

                            console.log('Course Handicap : ' + courseHandicap);
                            console.log('Gross Score : ' + grossScore);
                            console.log('Net Score : ' + netScore);

                            //calculate net score
                            /*if(grossScore > courseHandicap){
                                netScore =  netScore + (grossScore - courseHandicap);
                            }*/

                        }

                    } // end of for loop


                    memberList.push({
                        'nickName': nickName,
                        'playerInitials': playerInitials,
                        'handicap': handicap,
                        'courseHandicap': courseHandicapUSGA,
                        'grossScore': grossScore,
                        'netScore': '' + netScore,
                        'grossScoreWrtPar': grossScoreWrtPar,
                        'netScoreWrtPar': netScoreWrtPar
                    });

                    groupList[groupIndex].memberList = memberList;

                    if ($scope.scoringType === 'gross') {
                        $scope.scoreOrder = 'grossScoreWrtPar';
                    } else {
                        $scope.scoreOrder = 'netScoreWrtPar';
                    }


                    console.log('Total Scores : ' + JSON.stringify(groupList));
                    $scope.userTotalScores = groupList;
                }
            }).error(function(error) { //error
                console.log('Error in fetching score for stroke play ' + JSON.stringify(error));
            });
    }


    //fetching score per hole for each round playing member
    function fetchScoreDetailsForStrokePlay() {

        var roundId = $scope.roundId;

        $http.get(url + '/ScoreCards/scorePerHole?roundId=' + roundId)
            .success(function(ScoreCards) {
                var courseHoleOrder;
                var par;
                var strokeIndex;
                var score;
                var nickName;

                var jsonScore = [];
                var jsonHole = [];

                var pieces = [];
                var playerInitials = '';
                var playerIdArray = [];
                var playerInitialsArray = [];

                if ((!angular.isUndefined(ScoreCards)) && ScoreCards.length !== 0) {

                    var tempHoleId = -1;
                    var tempPar = 0;
                    var tempSI = 0;

                    // sum of pars of course holes played by user
                    var totalPars = 0;
                    var courseHolePlayed = false;

                    var holeNo = 1;

                    for (var i = 0; i < ScoreCards.length; i++) {
                        if (!angular.isUndefined(ScoreCards[i])) {

                            courseHoleOrder = ScoreCards[i].courseHoleOrder;

                            if (ScoreCards[i].par !== null) {
                                par = ScoreCards[i].par;
                            } else {
                                par = 0;
                            }

                            strokeIndex = ScoreCards[i].strokeIndex;
                            score = ScoreCards[i].score;

                            nickName = ScoreCards[i].nickName;

                            if (playerIdArray.indexOf(ScoreCards[i].playerId) === -1) {

                                playerIdArray.push(ScoreCards[i].playerId);

                                pieces = [];
                                playerInitials = '';

                                pieces = nickName.split(' ');

                                for (var j = 0; j < pieces.length; j++) {

                                    console.log('Pieces : ' + pieces);

                                    if (j === 0) {
                                        playerInitials = pieces[j].substring(0, 1).toUpperCase();
                                    } else {
                                        playerInitials = playerInitials + ' ' + (pieces[j].substring(0, 1).toUpperCase());
                                    }

                                    if (j === 3) {
                                        break;
                                    }

                                }

                                playerInitialsArray.push(playerInitials);
                            }

                            if (courseHoleOrder !== tempHoleId) {
                                // not first row
                                if (i !== 0) {

                                    if (courseHolePlayed === true) {
                                        totalPars = totalPars + tempPar;
                                    }

                                    courseHolePlayed = false;

                                    jsonHole.push({
                                        'courseHoleOrder': holeNo++,
                                        'par': tempPar,
                                        'strokeIndex': tempSI,
                                        'jsonScore': jsonScore

                                    });
                                    jsonScore = [];
                                }
                                tempHoleId = courseHoleOrder;
                            }
                            tempPar = par;
                            tempSI = strokeIndex;

                            //adding scores per par             

                            if (score !== 0) {
                                courseHolePlayed = true;
                            } else {
                                score = '-';
                            }

                            jsonScore.push({
                                'nickName': nickName,
                                'score': score
                            });
                        }
                    }

                    $scope.playerInitialsArray = playerInitialsArray;

                    if (courseHolePlayed === true) {
                        totalPars = totalPars + tempPar;
                    }

                    jsonHole.push({
                        'courseHoleOrder': holeNo++,
                        'par': par,
                        'strokeIndex': strokeIndex,
                        'jsonScore': jsonScore
                    });

                    $scope.totalPars = totalPars;

                    console.log('User Score details : ' + JSON.stringify(jsonHole));
                    $scope.ScoreCards = jsonHole;

                    scoreGraph($scope.ScoreCards, $scope.ScoreCards);
                }

            }).error(function(error) { //error
                console.log('Error in fetching course details ' + JSON.stringify(error));
            });
    }

    function displayScoreCardForMatchPlay() {
        fetchTotalScoresForMatchPlay();
    }

    // score summary match play
    function fetchTotalScoresForMatchPlay() {

        var roundId = $scope.roundId;

        $http.get(url + '/ScoreCards/generateScoreSummaryForMatchPlay?roundId=' + roundId)
            .success(function(scoreCards) { //success

                for (var i = 0; i < scoreCards.length; i++) {
                    if (i === 0) {
                        scoreCards[i].colorCode = colorCodeArray[1];
                        $scope.winnerId = scoreCards[i].playerId;
                    } else {
                        scoreCards[i].colorCode = colorCodeArray[2];
                    }
                }

                $scope.scoreSummary = scoreCards;

                fetchScoreDetailsForMatchPlay();
            }).error(function(error) { //error
                console.log('Error fetching score card for match play  ' + JSON.stringify(error));
            });
    }

    // hole wise score match play
    function fetchScoreDetailsForMatchPlay() {

        var roundId = $scope.roundId;

        $http.get(url + '/ScoreCards/generateScoreCardForMatchPlay?roundId=' + roundId)
            .success(function(scoreCards) { //success

                var jsonScore = [];
                var jsonHole = [];

                var tempHoleOrder = 0;
                var tempPar = 0;
                var tempStrokeIndex = 0;

                var holeNo = 1;
                var totalGrossStrokes = 0;

                var nickName = '';
                var pieces = [];
                var playerInitials = '';
                var playerIdArray = [];
                var playerInitialsArray = [];

                for (var i = 0; i < scoreCards.length; i++) {

                    if (tempHoleOrder !== scoreCards[i].courseHoleOrder) {
                        if (i !== 0) {
                            jsonHole.push({
                                'courseHoleOrder': holeNo++,
                                'par': tempPar,
                                'strokeIndex': tempStrokeIndex,
                                'colorCode': colorCode,
                                'jsonScore': jsonScore
                            });
                        }
                        colorCode = colorCodeArray[0];
                        tempPar = scoreCards[i].par;
                        tempHoleOrder = scoreCards[i].courseHoleOrder;
                        tempStrokeIndex = scoreCards[i].strokeIndex;
                        jsonScore = [];
                    }

                    nickName = scoreCards[i].playerName;

                    if (playerIdArray.indexOf(scoreCards[i].playerId) === -1) {

                        playerIdArray.push(scoreCards[i].playerId);

                        pieces = [];
                        playerInitials = '';

                        pieces = nickName.split(' ');

                        for (var j = 0; j < pieces.length; j++) {

                            console.log('Pieces : ' + pieces);

                            if (j === 0) {
                                playerInitials = pieces[j].substring(0, 1).toUpperCase();
                            } else {
                                playerInitials = playerInitials + ' ' + (pieces[j].substring(0, 1).toUpperCase());
                            }

                            if (j === 3) {
                                break;
                            }

                        }

                        playerInitialsArray.push(playerInitials);
                    }

                    if (scoreCards[i].netScore === 1) {
                        if (scoreCards[i].playerId === $scope.winnerId) {
                            colorCode = colorCodeArray[1];
                        } else {
                            colorCode = colorCodeArray[2];
                        }
                    }

                    totalGrossStrokes = scoreCards[i].totalGrossStrokes;

                    if (totalGrossStrokes === 0) {
                        totalGrossStrokes = '-';
                    }

                    jsonScore.push({
                        'nickName': scoreCards[i].playerName,
                        'score': totalGrossStrokes,
                        'netStrokes': scoreCards[i].totalNetStrokes,
                        'grossScore': scoreCards[i].grossScore,
                        'netScore': scoreCards[i].netScore
                    });

                }

                jsonHole.push({
                    'courseHoleOrder': holeNo++,
                    'par': tempPar,
                    'strokeIndex': tempStrokeIndex,
                    'colorCode': colorCode,
                    'jsonScore': jsonScore
                });

                console.log('User Score details : ' + JSON.stringify(jsonHole));
                $scope.scoreDetails = jsonHole;

                scoreGraph($scope.scoreDetails, $scope.scoreSummary);

                $scope.playerInitialsArray = playerInitialsArray;

            }).error(function(error) { //error
                console.log('Error fetching score details for match play.' + JSON.stringify(error));
            });
    }

    function displayScoreCardForFourBall() {
        fetchTotalScoresForFourBall();
    }

    // score summary foursome
    function fetchTotalScoresForFourBall() {

        var roundId = $scope.roundId;

        $http.get(url + '/ScoreCards/scoreBoardSummaryFourBall?roundId=' + roundId)
            .success(function(scoreCards) { //success

                var groupWiseColorCode = [];

                for (var i = 0; i < scoreCards.length; i++) {
                    scoreCards[i].colorCode = colorCodeArray[i + 1];
                    groupWiseColorCode[scoreCards[i].groupId] = colorCodeArray[i + 1];
                }

                $scope.scoreSummary = scoreCards;
                $scope.groupWiseColorCode = groupWiseColorCode;

                fetchScoreDetailsForFourBall();

            }).error(function(error) { //error
                console.log('Error fetching score card for foursome' + JSON.stringify(error));
            });
    }

    // hole wise team score foursome
    function fetchScoreDetailsForFourBall() {

        var roundId = $scope.roundId;

        $http.get(url + '/ScoreCards/scoreBoardFourBall?roundId=' + roundId)
            .success(function(scoreCards) { //success

                var groupIdArray = [];
                var groupNameArray = [];

                var jsonScore = [];
                var jsonHole = [];

                var tempHoleOrder = 0;
                var tempPar = 0;
                var tempStrokeIndex = 0;

                var holeNo = 1;
                var totalGrossStrokes = 0;

                for (var i = 0; i < scoreCards.length; i++) {

                    if (tempHoleOrder !== scoreCards[i].courseHoleOrder) {
                        if (i !== 0) {
                            jsonHole.push({
                                'courseHoleOrder': holeNo++,
                                'par': tempPar,
                                'strokeIndex': tempStrokeIndex,
                                'colorCode': colorCode,
                                'jsonScore': jsonScore
                            });
                        }
                        colorCode = colorCodeArray[0];
                        tempPar = scoreCards[i].par;
                        tempHoleOrder = scoreCards[i].courseHoleOrder;
                        tempStrokeIndex = scoreCards[i].strokeIndex;
                        jsonScore = [];
                    }

                    if (groupIdArray.indexOf(scoreCards[i].groupId) === -1) {
                        groupIdArray.push(scoreCards[i].groupId);
                        groupNameArray.push(scoreCards[i].groupName);
                    }

                    if (scoreCards[i].teamWinnerOfNetStrokes === 1) {
                        colorCode = $scope.groupWiseColorCode[scoreCards[i].groupId];
                    }

                    totalGrossStrokes = scoreCards[i].teamLowestGrossStrokes;

                    if (totalGrossStrokes === 0) {
                        totalGrossStrokes = '-';
                    }

                    jsonScore.push({
                        'groupId': scoreCards[i].groupId,
                        'groupName': scoreCards[i].groupName,
                        'score': totalGrossStrokes
                    });

                }

                jsonHole.push({
                    'courseHoleOrder': holeNo++,
                    'par': tempPar,
                    'strokeIndex': tempStrokeIndex,
                    'colorCode': colorCode,
                    'jsonScore': jsonScore
                });

                console.log('User Score details : ' + JSON.stringify(jsonHole));
                $scope.scoreDetails = jsonHole;

                $scope.groupNameArray = groupNameArray;

                scoreGraph($scope.scoreDetails, $scope.scoreSummary);

                fetchPlayerWiseScoreDetailsForFourBall();

            }).error(function(error) { //error
                console.log('Error fetching score details for foursome.' + JSON.stringify(error));
            });
    }

    // fetch player wise hole score four ball
    function fetchPlayerWiseScoreDetailsForFourBall() {

        var roundId = $scope.roundId;

        $http.get(url + '/ScoreCards/playerWiseTotalScoreForFourBall?roundId=' + roundId)
            .success(function(scoreCards) { //success

                var jsonScore = [];
                var jsonHole = [];

                var tempGroupId = 0;
                var tempGroupName = '';
                var groupWiseScore = [];

                var tempHoleOrder = 0;
                var tempPar = 0;
                var tempStrokeIndex = 0;
                var holeNo = 1;
                var totalGrossStrokes = 0;
                var nickName = '';
                var pieces = [];
                var playerIdArray = [];
                var playerInitials = '';
                var playerInitialsArray = [];

                for (var i = 0; i < scoreCards.length; i++) {

                    if (tempHoleOrder !== scoreCards[i].courseHoleOrder) {
                        if (i !== 0 && scoreCards[i].courseHoleOrder !== 1) {
                            jsonHole.push({
                                'courseHoleOrder': holeNo++,
                                'par': tempPar,
                                'strokeIndex': tempStrokeIndex,
                                'colorCode': colorCode,
                                'jsonScore': jsonScore
                            });
                        }
                        colorCode = colorCodeArray[0];
                        tempPar = scoreCards[i].par;
                        tempHoleOrder = scoreCards[i].courseHoleOrder;
                        tempStrokeIndex = scoreCards[i].strokeIndex;
                        jsonScore = [];
                    }

                    if (tempGroupId !== scoreCards[i].groupId) {
                        if (i !== 0) {
                            groupWiseScore.push({
                                groupId: tempGroupId,
                                groupName: tempGroupName,
                                colorCode: $scope.groupWiseColorCode[tempGroupId],
                                holeWiseScore: jsonHole,
                                playerInitialsArray: playerInitialsArray
                            });
                        }
                        tempGroupId = scoreCards[i].groupId;
                        tempGroupName = scoreCards[i].groupName;
                        jsonHole = [];
                        playerIdArray = [];
                        playerInitialsArray = [];
                        holeNo = 1;
                    }

                    nickName = scoreCards[i].playerName;

                    if (playerIdArray.indexOf(scoreCards[i].playerId) === -1) {

                        playerIdArray.push(scoreCards[i].playerId);

                        pieces = [];
                        playerInitials = '';

                        pieces = nickName.split(' ');

                        for (var j = 0; j < pieces.length; j++) {

                            console.log('Pieces : ' + pieces);

                            if (j === 0) {
                                playerInitials = pieces[j].substring(0, 1).toUpperCase();
                            } else {
                                playerInitials = playerInitials + ' ' + (pieces[j].substring(0, 1).toUpperCase());
                            }

                            if (j === 3) {
                                break;
                            }

                        }

                        playerInitialsArray.push(playerInitials);
                    }

                    if (scoreCards[i].teamWinnerOfNetStrokes === 1) {
                        colorCode = $scope.groupWiseColorCode[scoreCards[i].groupId];
                    }

                    totalGrossStrokes = scoreCards[i].totalGrossStrokes;

                    if (totalGrossStrokes === 0) {
                        totalGrossStrokes = '-';
                    }

                    jsonScore.push({
                        'nickName': scoreCards[i].playerName,
                        'grossStrokes': totalGrossStrokes,
                        'netStrokes': scoreCards[i].totalNetStrokes,
                        'grossScore': scoreCards[i].teamWinnerOfGrossStrokes,
                        'netScore': scoreCards[i].teamWinnerOfNetStrokes
                    });

                }

                jsonHole.push({
                    'courseHoleOrder': holeNo++,
                    'par': tempPar,
                    'strokeIndex': tempStrokeIndex,
                    'colorCode': colorCode,
                    'jsonScore': jsonScore
                });

                groupWiseScore.push({
                    groupId: tempGroupId,
                    groupName: tempGroupName,
                    colorCode: $scope.groupWiseColorCode[tempGroupId],
                    holeWiseScore: jsonHole,
                    playerInitialsArray: playerInitialsArray
                });

                console.log('User Score details : ' + JSON.stringify(groupWiseScore));
                $scope.jsonHole = jsonHole;
                $scope.groupWiseScore = groupWiseScore;

                /*scoreGraph($scope.jsonHole, playerInitialsArray);*/

            }).error(function(error) { //error
                console.log('Error fetching score details for foursome.' + JSON.stringify(error));
            });
    }


}]);

/**=========================================================
 * Module: anchor.js
 * Disables null anchor behavior
 =========================================================*/

App.directive('href', function() {

  return {
    restrict: 'A',
    compile: function(element, attr) {
        return function(scope, element) {
          if(attr.ngClick || attr.href === '' || attr.href === '#'){
            if( !element.hasClass('dropdown-toggle') )
              element.on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
              });
          }
        };
      }
   };
});
/**=========================================================
 * Module: animate-enabled.js
 * Enable or disables ngAnimate for element with directive
 =========================================================*/

App.directive("animateEnabled", ["$animate", function ($animate) {
  return {
    link: function (scope, element, attrs) {
      scope.$watch(function () {
        return scope.$eval(attrs.animateEnabled, scope);
      }, function (newValue) {
        $animate.enabled(!!newValue, element);
      });
    }
  };
}]);
/**=========================================================
 * Module: classy-loader.js
 * Enable use of classyloader directly from data attributes
 =========================================================*/

App.directive('classyloader', ["$timeout", function($timeout) {
  'use strict';

  var $scroller       = $(window),
      inViewFlagClass = 'js-is-in-view'; // a classname to detect when a chart has been triggered after scroll

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      // run after interpolation  
      $timeout(function(){
  
        var $element = $(element),
            options  = $element.data();
        
        // At lease we need a data-percentage attribute
        if(options) {
          if( options.triggerInView ) {

            $scroller.scroll(function() {
              checkLoaderInVIew($element, options);
            });
            // if the element starts already in view
            checkLoaderInVIew($element, options);
          }
          else
            startLoader($element, options);
        }

      }, 0);

      function checkLoaderInVIew(element, options) {
        var offset = -20;
        if( ! element.hasClass(inViewFlagClass) &&
            $.Utils.isInView(element, {topoffset: offset}) ) {
          startLoader(element, options);
        }
      }
      function startLoader(element, options) {
        element.ClassyLoader(options).addClass(inViewFlagClass);
      }
    }
  };
}]);

/**=========================================================
 * Module: clear-storage.js
 * Removes a key from the browser storage via element click
 =========================================================*/

App.directive('resetKey',  ['$state','$rootScope', function($state, $rootScope) {
  'use strict';

  return {
    restrict: 'A',
    scope: {
      resetKey: '='
    },
    link: function(scope, element, attrs) {
      
      scope.resetKey = attrs.resetKey;

    },
    controller: ["$scope", "$element", function($scope, $element) {
    
      $element.on('click', function (e) {
          e.preventDefault();

          if($scope.resetKey) {
            delete $rootScope.$storage[$scope.resetKey];
            $state.go($state.current, {}, {reload: true});
          }
          else {
            $.error('No storage key specified for reset.');
          }
      });

    }]

  };

}]);

/*
 Highcharts JS v4.1.7 (2015-06-26)
 Exporting module

 (c) 2010-2014 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(f){var z=f.Chart,s=f.addEvent,A=f.removeEvent,B=HighchartsAdapter.fireEvent,j=f.createElement,p=f.discardElement,u=f.css,l=f.merge,m=f.each,q=f.extend,E=f.splat,F=Math.max,k=document,C=window,G=f.isTouchDevice,H=f.Renderer.prototype.symbols,r=f.getOptions(),x;q(r.lang,{printChart:"Print chart",downloadPNG:"Download PNG image",downloadJPEG:"Download JPEG image",downloadPDF:"Download PDF document",downloadSVG:"Download SVG vector image",contextButtonTitle:"Chart context menu"});r.navigation=
{menuStyle:{border:"1px solid #A0A0A0",background:"#FFFFFF",padding:"5px 0"},menuItemStyle:{padding:"0 10px",background:"none",color:"#303030",fontSize:G?"14px":"11px"},menuItemHoverStyle:{background:"#4572A5",color:"#FFFFFF"},buttonOptions:{symbolFill:"#E0E0E0",symbolSize:14,symbolStroke:"#666",symbolStrokeWidth:3,symbolX:12.5,symbolY:10.5,align:"right",buttonSpacing:3,height:22,theme:{fill:"white",stroke:"none"},verticalAlign:"top",width:24}};r.exporting={type:"image/png",url:"http://export.highcharts.com/",
buttons:{contextButton:{menuClassName:"highcharts-contextmenu",symbol:"menu",_titleKey:"contextButtonTitle",menuItems:[{textKey:"printChart",onclick:function(){this.print()}},{separator:!0},{textKey:"downloadPNG",onclick:function(){this.exportChart()}},{textKey:"downloadJPEG",onclick:function(){this.exportChart({type:"image/jpeg"})}},{textKey:"downloadPDF",onclick:function(){this.exportChart({type:"application/pdf"})}},{textKey:"downloadSVG",onclick:function(){this.exportChart({type:"image/svg+xml"})}}]}}};
f.post=function(b,a,e){var c,b=j("form",l({method:"post",action:b,enctype:"multipart/form-data"},e),{display:"none"},k.body);for(c in a)j("input",{type:"hidden",name:c,value:a[c]},null,b);b.submit();p(b)};q(z.prototype,{sanitizeSVG:function(b){return b.replace(/zIndex="[^"]+"/g,"").replace(/isShadow="[^"]+"/g,"").replace(/symbolName="[^"]+"/g,"").replace(/jQuery[0-9]+="[^"]+"/g,"").replace(/url\([^#]+#/g,"url(#").replace(/<svg /,'<svg xmlns:xlink="http://www.w3.org/1999/xlink" ').replace(/ (NS[0-9]+\:)?href=/g,
" xlink:href=").replace(/\n/," ").replace(/<\/svg>.*?$/,"</svg>").replace(/(fill|stroke)="rgba\(([ 0-9]+,[ 0-9]+,[ 0-9]+),([ 0-9\.]+)\)"/g,'$1="rgb($2)" $1-opacity="$3"').replace(/&nbsp;/g,"� ").replace(/&shy;/g,"­").replace(/<IMG /g,"<image ").replace(/<(\/?)TITLE>/g,"<$1title>").replace(/height=([^" ]+)/g,'height="$1"').replace(/width=([^" ]+)/g,'width="$1"').replace(/hc-svg-href="([^"]+)">/g,'xlink:href="$1"/>').replace(/ id=([^" >]+)/g,' id="$1"').replace(/class=([^" >]+)/g,'class="$1"').replace(/ transform /g,
" ").replace(/:(path|rect)/g,"$1").replace(/style="([^"]+)"/g,function(a){return a.toLowerCase()})},getSVG:function(b){var a=this,e,c,g,y,h,d=l(a.options,b);if(!k.createElementNS)k.createElementNS=function(a,b){return k.createElement(b)};c=j("div",null,{position:"absolute",top:"-9999em",width:a.chartWidth+"px",height:a.chartHeight+"px"},k.body);g=a.renderTo.style.width;h=a.renderTo.style.height;g=d.exporting.sourceWidth||d.chart.width||/px$/.test(g)&&parseInt(g,10)||600;h=d.exporting.sourceHeight||
d.chart.height||/px$/.test(h)&&parseInt(h,10)||400;q(d.chart,{animation:!1,renderTo:c,forExport:!0,width:g,height:h});d.exporting.enabled=!1;delete d.data;d.series=[];m(a.series,function(a){y=l(a.options,{animation:!1,enableMouseTracking:!1,showCheckbox:!1,visible:a.visible});y.isInternal||d.series.push(y)});b&&m(["xAxis","yAxis"],function(a){m(E(b[a]),function(b,c){d[a][c]=l(d[a][c],b)})});e=new f.Chart(d,a.callback);m(["xAxis","yAxis"],function(b){m(a[b],function(a,d){var c=e[b][d],g=a.getExtremes(),
h=g.userMin,g=g.userMax;c&&(h!==void 0||g!==void 0)&&c.setExtremes(h,g,!0,!1)})});g=e.container.innerHTML;d=null;e.destroy();p(c);g=this.sanitizeSVG(g);return g=g.replace(/(url\(#highcharts-[0-9]+)&quot;/g,"$1").replace(/&quot;/g,"'")},getSVGForExport:function(b,a){var e=this.options.exporting;return this.getSVG(l({chart:{borderRadius:0}},e.chartOptions,a,{exporting:{sourceWidth:b&&b.sourceWidth||e.sourceWidth,sourceHeight:b&&b.sourceHeight||e.sourceHeight}}))},exportChart:function(b,a){var e=this.getSVGForExport(b,
a),b=l(this.options.exporting,b);f.post(b.url,{filename:b.filename||"chart",type:b.type,width:b.width||0,scale:b.scale||2,svg:e},b.formAttributes)},print:function(){var b=this,a=b.container,e=[],c=a.parentNode,g=k.body,f=g.childNodes;if(!b.isPrinting)b.isPrinting=!0,B(b,"beforePrint"),m(f,function(a,b){if(a.nodeType===1)e[b]=a.style.display,a.style.display="none"}),g.appendChild(a),C.focus(),C.print(),setTimeout(function(){c.appendChild(a);m(f,function(a,b){if(a.nodeType===1)a.style.display=e[b]});
b.isPrinting=!1;B(b,"afterPrint")},1E3)},contextMenu:function(b,a,e,c,g,f,h){var d=this,l=d.options.navigation,D=l.menuItemStyle,n=d.chartWidth,o=d.chartHeight,k="cache-"+b,i=d[k],t=F(g,f),v,w,p,r=function(a){d.pointer.inClass(a.target,b)||w()};if(!i)d[k]=i=j("div",{className:b},{position:"absolute",zIndex:1E3,padding:t+"px"},d.container),v=j("div",null,q({MozBoxShadow:"3px 3px 10px #888",WebkitBoxShadow:"3px 3px 10px #888",boxShadow:"3px 3px 10px #888"},l.menuStyle),i),w=function(){u(i,{display:"none"});
h&&h.setState(0);d.openMenu=!1},s(i,"mouseleave",function(){p=setTimeout(w,500)}),s(i,"mouseenter",function(){clearTimeout(p)}),s(document,"mouseup",r),s(d,"destroy",function(){A(document,"mouseup",r)}),m(a,function(a){if(a){var b=a.separator?j("hr",null,null,v):j("div",{onmouseover:function(){u(this,l.menuItemHoverStyle)},onmouseout:function(){u(this,D)},onclick:function(){w();a.onclick&&a.onclick.apply(d,arguments)},innerHTML:a.text||d.options.lang[a.textKey]},q({cursor:"pointer"},D),v);d.exportDivElements.push(b)}}),
d.exportDivElements.push(v,i),d.exportMenuWidth=i.offsetWidth,d.exportMenuHeight=i.offsetHeight;a={display:"block"};e+d.exportMenuWidth>n?a.right=n-e-g-t+"px":a.left=e-t+"px";c+f+d.exportMenuHeight>o&&h.alignOptions.verticalAlign!=="top"?a.bottom=o-c-t+"px":a.top=c+f-t+"px";u(i,a);d.openMenu=!0},addButton:function(b){var a=this,e=a.renderer,c=l(a.options.navigation.buttonOptions,b),g=c.onclick,k=c.menuItems,h,d,m={stroke:c.symbolStroke,fill:c.symbolFill},j=c.symbolSize||12;if(!a.btnCount)a.btnCount=
0;if(!a.exportDivElements)a.exportDivElements=[],a.exportSVGElements=[];if(c.enabled!==!1){var n=c.theme,o=n.states,p=o&&o.hover,o=o&&o.select,i;delete n.states;g?i=function(){g.apply(a,arguments)}:k&&(i=function(){a.contextMenu(d.menuClassName,k,d.translateX,d.translateY,d.width,d.height,d);d.setState(2)});c.text&&c.symbol?n.paddingLeft=f.pick(n.paddingLeft,25):c.text||q(n,{width:c.width,height:c.height,padding:0});d=e.button(c.text,0,0,i,n,p,o).attr({title:a.options.lang[c._titleKey],"stroke-linecap":"round"});
d.menuClassName=b.menuClassName||"highcharts-menu-"+a.btnCount++;c.symbol&&(h=e.symbol(c.symbol,c.symbolX-j/2,c.symbolY-j/2,j,j).attr(q(m,{"stroke-width":c.symbolStrokeWidth||1,zIndex:1})).add(d));d.add().align(q(c,{width:d.width,x:f.pick(c.x,x)}),!0,"spacingBox");x+=(d.width+c.buttonSpacing)*(c.align==="right"?-1:1);a.exportSVGElements.push(d,h)}},destroyExport:function(b){var b=b.target,a,e;for(a=0;a<b.exportSVGElements.length;a++)if(e=b.exportSVGElements[a])e.onclick=e.ontouchstart=null,b.exportSVGElements[a]=
e.destroy();for(a=0;a<b.exportDivElements.length;a++)e=b.exportDivElements[a],A(e,"mouseleave"),b.exportDivElements[a]=e.onmouseout=e.onmouseover=e.ontouchstart=e.onclick=null,p(e)}});H.menu=function(b,a,e,c){return["M",b,a+2.5,"L",b+e,a+2.5,"M",b,a+c/2+0.5,"L",b+e,a+c/2+0.5,"M",b,a+c-1.5,"L",b+e,a+c-1.5]};z.prototype.callbacks.push(function(b){var a,e=b.options.exporting,c=e.buttons;x=0;if(e.enabled!==!1){for(a in c)b.addButton(c[a]);s(b,"destroy",b.destroyExport)}})});

/**=========================================================
 * Module: filestyle.js
 * Initializes the fielstyle plugin
 =========================================================*/

App.directive('filestyle', function() {
  return {
    restrict: 'A',
    controller: ["$scope", "$element", function($scope, $element) {
      var options = $element.data();
      
      // old usage support
        options.classInput = $element.data('classinput') || options.classInput;
      
      $element.filestyle(options);
    }]
  };
});

/**=========================================================
 * Module: flatdoc.js
 * Creates the flatdoc markup and initializes the plugin
 =========================================================*/

App.directive('flatdoc', ['$location', function($location) {
  return {
    restrict: "EA",
    template: "<div role='flatdoc'><div role='flatdoc-menu'></div><div role='flatdoc-content'></div></div>",
    link: function(scope, element, attrs) {

      Flatdoc.run({
        fetcher: Flatdoc.file(attrs.src)
      });
      
      var $root = $('html, body');
      $(document).on('flatdoc:ready', function() {
        var docMenu = $('[role="flatdoc-menu"]');
        docMenu.find('a').on('click', function(e) {
          e.preventDefault(); e.stopPropagation();
          
          var $this = $(this);
          
          docMenu.find('a.active').removeClass('active');
          $this.addClass('active');

          $root.animate({
                scrollTop: $(this.getAttribute('href')).offset().top - ($('.topnavbar').height() + 10)
            }, 800);
        });

      });
    }
  };

}]);
/**=========================================================
 * Module: form-wizard.js
 * Handles form wizard plugin and validation
 =========================================================*/

App.directive('formWizard', ["$parse", function($parse){
  'use strict';

  return {
    restrict: 'EA',
    scope: true,
    link: function(scope, element, attribute) {
      var validate = $parse(attribute.validateSteps)(scope),
          wiz = new Wizard(attribute.steps, !!validate, element);
      scope.wizard = wiz.init();

    }
  };

  function Wizard (quantity, validate, element) {
    
    var self = this;
    self.quantity = parseInt(quantity,10);
    self.validate = validate;
    self.element = element;
    
    self.init = function() {
      self.createsteps(self.quantity);
      self.go(1); // always start at fist step
      return self;
    };

    self.go = function(step) {
      
      if ( angular.isDefined(self.steps[step]) ) {

        if(self.validate && step !== 1) {
          var form = $(self.element),
              group = form.children().children('div').get(step - 2);

          if (false === form.parsley().validate( group.id )) {
            return false;
          }
        }

        self.cleanall();
        self.steps[step] = true;
      }
    };

    self.active = function(step) {
      return !!self.steps[step];
    };

    self.cleanall = function() {
      for(var i in self.steps){
        self.steps[i] = false;
      }
    };

    self.createsteps = function(q) {
      self.steps = [];
      for(var i = 1; i <= q; i++) self.steps[i] = false;
    };

  }

}]);

/**=========================================================
 * Module: fullscreen.js
 * Toggle the fullscreen mode on/off
 =========================================================*/

App.directive('toggleFullscreen', function() {
  'use strict';

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {

      element.on('click', function (e) {
          e.preventDefault();

          if (screenfull.enabled) {
            
            screenfull.toggle();
            
            // Switch icon indicator
            if(screenfull.isFullscreen)
              $(this).children('em').removeClass('fa-expand').addClass('fa-compress');
            else
              $(this).children('em').removeClass('fa-compress').addClass('fa-expand');

          } else {
            $.error('Fullscreen not enabled');
          }

      });
    }
  };

});


/**=========================================================
 * Module: gmap.js
 * Init Google Map plugin
 =========================================================*/

App.directive('gmap', ['$window','gmap', function($window, gmap){
  'use strict';

  // Map Style definition
  // Get more styles from http://snazzymaps.com/style/29/light-monochrome
  // - Just replace and assign to 'MapStyles' the new style array
  var MapStyles = [{featureType:'water',stylers:[{visibility:'on'},{color:'#bdd1f9'}]},{featureType:'all',elementType:'labels.text.fill',stylers:[{color:'#334165'}]},{featureType:'landscape',stylers:[{color:'#e9ebf1'}]},{featureType:'road.highway',elementType:'geometry',stylers:[{color:'#c5c6c6'}]},{featureType:'road.arterial',elementType:'geometry',stylers:[{color:'#fff'}]},{featureType:'road.local',elementType:'geometry',stylers:[{color:'#fff'}]},{featureType:'transit',elementType:'geometry',stylers:[{color:'#d8dbe0'}]},{featureType:'poi',elementType:'geometry',stylers:[{color:'#cfd5e0'}]},{featureType:'administrative',stylers:[{visibility:'on'},{lightness:33}]},{featureType:'poi.park',elementType:'labels',stylers:[{visibility:'on'},{lightness:20}]},{featureType:'road',stylers:[{color:'#d8dbe0',lightness:20}]}];
  
  gmap.setStyle( MapStyles );

  // Center Map marker on resolution change

  $($window).resize(function() {

    gmap.autocenter();

  });

  return {
    restrict: 'A',
    priority: 1000,
    scope: {
      mapAddress: '='
    },
    link: function (scope, element, attrs) {

      scope.$watch('mapAddress', function(newVal){

        element.attr('data-address', newVal);
        gmap.init(element);

      });

    }
  };

}]);


/*
 Highcharts JS v4.1.7 (2015-06-26)

 (c) 2009-2014 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(){function z(){var a,b=arguments,c,d={},e=function(a,b){var c,d;typeof a!=="object"&&(a={});for(d in b)b.hasOwnProperty(d)&&(c=b[d],a[d]=c&&typeof c==="object"&&Object.prototype.toString.call(c)!=="[object Array]"&&d!=="renderTo"&&typeof c.nodeType!=="number"?e(a[d]||{},c):b[d]);return a};b[0]===!0&&(d=b[1],b=Array.prototype.slice.call(b,2));c=b.length;for(a=0;a<c;a++)d=e(d,b[a]);return d}function D(a,b){return parseInt(a,b||10)}function Da(a){return typeof a==="string"}function da(a){return a&&
typeof a==="object"}function Ha(a){return Object.prototype.toString.call(a)==="[object Array]"}function ra(a){return typeof a==="number"}function Ea(a){return W.log(a)/W.LN10}function ia(a){return W.pow(10,a)}function ja(a,b){for(var c=a.length;c--;)if(a[c]===b){a.splice(c,1);break}}function q(a){return a!==y&&a!==null}function J(a,b,c){var d,e;if(Da(b))q(c)?a.setAttribute(b,c):a&&a.getAttribute&&(e=a.getAttribute(b));else if(q(b)&&da(b))for(d in b)a.setAttribute(d,b[d]);return e}function sa(a){return Ha(a)?
a:[a]}function L(a,b){if(ya&&!ca&&b&&b.opacity!==y)b.filter="alpha(opacity="+b.opacity*100+")";x(a.style,b)}function $(a,b,c,d,e){a=B.createElement(a);b&&x(a,b);e&&L(a,{padding:0,border:P,margin:0});c&&L(a,c);d&&d.appendChild(a);return a}function ka(a,b){var c=function(){return y};c.prototype=new a;x(c.prototype,b);return c}function Ia(a,b){return Array((b||2)+1-String(a).length).join(0)+a}function Wa(a){return(eb&&eb(a)||ob||0)*6E4}function Ja(a,b){for(var c="{",d=!1,e,f,g,h,i,j=[];(c=a.indexOf(c))!==
-1;){e=a.slice(0,c);if(d){f=e.split(":");g=f.shift().split(".");i=g.length;e=b;for(h=0;h<i;h++)e=e[g[h]];if(f.length)f=f.join(":"),g=/\.([0-9])/,h=T.lang,i=void 0,/f$/.test(f)?(i=(i=f.match(g))?i[1]:-1,e!==null&&(e=A.numberFormat(e,i,h.decimalPoint,f.indexOf(",")>-1?h.thousandsSep:""))):e=Oa(f,e)}j.push(e);a=a.slice(c+1);c=(d=!d)?"}":"{"}j.push(a);return j.join("")}function pb(a){return W.pow(10,V(W.log(a)/W.LN10))}function qb(a,b,c,d,e){var f,g=a,c=p(c,1);f=a/c;b||(b=[1,2,2.5,5,10],d===!1&&(c===
1?b=[1,2,5,10]:c<=0.1&&(b=[1/c])));for(d=0;d<b.length;d++)if(g=b[d],e&&g*c>=a||!e&&f<=(b[d]+(b[d+1]||b[d]))/2)break;g*=c;return g}function rb(a,b){var c=a.length,d,e;for(e=0;e<c;e++)a[e].ss_i=e;a.sort(function(a,c){d=b(a,c);return d===0?a.ss_i-c.ss_i:d});for(e=0;e<c;e++)delete a[e].ss_i}function Pa(a){for(var b=a.length,c=a[0];b--;)a[b]<c&&(c=a[b]);return c}function Fa(a){for(var b=a.length,c=a[0];b--;)a[b]>c&&(c=a[b]);return c}function Qa(a,b){for(var c in a)a[c]&&a[c]!==b&&a[c].destroy&&a[c].destroy(),
delete a[c]}function Ra(a){fb||(fb=$(Ka));a&&fb.appendChild(a);fb.innerHTML=""}function la(a,b){var c="Highcharts error #"+a+":"+a;if(b)throw c;K.console&&console.log(c)}function ea(a){return parseFloat(a.toPrecision(14))}function Sa(a,b){za=p(a,b.animation)}function Db(){var a=T.global,b=a.useUTC,c=b?"getUTC":"get",d=b?"setUTC":"set";Aa=a.Date||window.Date;ob=b&&a.timezoneOffset;eb=b&&a.getTimezoneOffset;gb=function(a,c,d,h,i,j){var k;b?(k=Aa.UTC.apply(0,arguments),k+=
Wa(k)):k=(new Aa(a,c,p(d,1),p(h,0),p(i,0),p(j,0))).getTime();return k};sb=c+"Minutes";tb=c+"Hours";ub=c+"Day";Xa=c+"Date";Ya=c+"Month";Za=c+"FullYear";Eb=d+"Milliseconds";Fb=d+"Seconds";Gb=d+"Minutes";Hb=d+"Hours";vb=d+"Date";wb=d+"Month";xb=d+"FullYear"}function Q(){}function Ta(a,b,c,d){this.axis=a;this.pos=b;this.type=c||"";this.isNew=!0;!c&&!d&&this.addLabel()}function Ib(a,b,c,d,e){var f=a.chart.inverted;this.axis=a;this.isNegative=c;this.options=b;this.x=d;this.total=null;this.points={};this.stack=
e;this.alignOptions={align:b.align||(f?c?"left":"right":"center"),verticalAlign:b.verticalAlign||(f?"middle":c?"bottom":"top"),y:p(b.y,f?4:c?14:-6),x:p(b.x,f?c?-6:6:0)};this.textAlign=b.textAlign||(f?c?"right":"left":"center")}var y,B=document,K=window,W=Math,r=W.round,V=W.floor,ta=W.ceil,u=W.max,C=W.min,O=W.abs,X=W.cos,aa=W.sin,ma=W.PI,ha=ma*2/360,Ba=navigator.userAgent,Jb=K.opera,ya=/(msie|trident)/i.test(Ba)&&!Jb,hb=B.documentMode===8,ib=/AppleWebKit/.test(Ba),La=/Firefox/.test(Ba),Kb=/(Mobile|Android|Windows Phone)/.test(Ba),
Ca="http://www.w3.org/2000/svg",ca=!!B.createElementNS&&!!B.createElementNS(Ca,"svg").createSVGRect,Ob=La&&parseInt(Ba.split("Firefox/")[1],10)<4,fa=!ca&&!ya&&!!B.createElement("canvas").getContext,$a,ab,Lb={},yb=0,fb,T,Oa,za,zb,E,na=function(){return y},Y=[],bb=0,Ka="div",P="none",Pb=/^[0-9]+$/,jb=["plotTop","marginRight","marginBottom","plotLeft"],Qb="stroke-width",Aa,gb,ob,eb,sb,tb,ub,Xa,Ya,Za,Eb,Fb,Gb,Hb,vb,wb,xb,M={},A;A=K.Highcharts=K.Highcharts?la(16,!0):{};A.seriesTypes=M;var x=A.extend=function(a,
b){var c;a||(a={});for(c in b)a[c]=b[c];return a},p=A.pick=function(){var a=arguments,b,c,d=a.length;for(b=0;b<d;b++)if(c=a[b],c!==y&&c!==null)return c},cb=A.wrap=function(a,b,c){var d=a[b];a[b]=function(){var a=Array.prototype.slice.call(arguments);a.unshift(d);return c.apply(this,a)}};Oa=function(a,b,c){if(!q(b)||isNaN(b))return"Invalid date";var a=p(a,"%Y-%m-%d %H:%M:%S"),d=new Aa(b-Wa(b)),e,f=d[tb](),g=d[ub](),h=d[Xa](),i=d[Ya](),j=d[Za](),k=T.lang,l=k.weekdays,d=x({a:l[g].substr(0,3),A:l[g],
d:Ia(h),e:h,w:g,b:k.shortMonths[i],B:k.months[i],m:Ia(i+1),y:j.toString().substr(2,2),Y:j,H:Ia(f),I:Ia(f%12||12),l:f%12||12,M:Ia(d[sb]()),p:f<12?"AM":"PM",P:f<12?"am":"pm",S:Ia(d.getSeconds()),L:Ia(r(b%1E3),3)},A.dateFormats);for(e in d)for(;a.indexOf("%"+e)!==-1;)a=a.replace("%"+e,typeof d[e]==="function"?d[e](b):d[e]);return c?a.substr(0,1).toUpperCase()+a.substr(1):a};E={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5,month:24192E5,year:314496E5};A.numberFormat=function(a,b,
c,d){var e=T.lang,a=+a||0,f=b===-1?C((a.toString().split(".")[1]||"").length,20):isNaN(b=O(b))?2:b,b=c===void 0?e.decimalPoint:c,d=d===void 0?e.thousandsSep:d,e=a<0?"-":"",c=String(D(a=O(a).toFixed(f))),g=c.length>3?c.length%3:0;return e+(g?c.substr(0,g)+d:"")+c.substr(g).replace(/(\d{3})(?=\d)/g,"$1"+d)+(f?b+O(a-c).toFixed(f).slice(2):"")};zb={init:function(a,b,c){var b=b||"",d=a.shift,e=b.indexOf("C")>-1,f=e?7:3,g,b=b.split(" "),c=[].concat(c),h,i,j=function(a){for(g=a.length;g--;)a[g]==="M"&&a.splice(g+
1,0,a[g+1],a[g+2],a[g+1],a[g+2])};e&&(j(b),j(c));a.isArea&&(h=b.splice(b.length-6,6),i=c.splice(c.length-6,6));if(d<=c.length/f&&b.length===c.length)for(;d--;)c=[].concat(c).splice(0,f).concat(c);a.shift=0;if(b.length)for(a=c.length;b.length<a;)d=[].concat(b).splice(b.length-f,f),e&&(d[f-6]=d[f-2],d[f-5]=d[f-1]),b=b.concat(d);h&&(b=b.concat(h),c=c.concat(i));return[b,c]},step:function(a,b,c,d){var e=[],f=a.length;if(c===1)e=d;else if(f===b.length&&c<1)for(;f--;)d=parseFloat(a[f]),e[f]=isNaN(d)?a[f]:
c*parseFloat(b[f]-d)+d;else e=b;return e}};(function(a){K.HighchartsAdapter=K.HighchartsAdapter||a&&{init:function(b){var c=a.fx;a.extend(a.easing,{easeOutQuad:function(a,b,c,g,h){return-g*(b/=h)*(b-2)+c}});a.each(["cur","_default","width","height","opacity"],function(b,e){var f=c.step,g;e==="cur"?f=c.prototype:e==="_default"&&a.Tween&&(f=a.Tween.propHooks[e],e="set");(g=f[e])&&(f[e]=function(a){var c,a=b?a:this;if(a.prop!=="align")return c=a.elem,c.attr?c.attr(a.prop,e==="cur"?y:a.now):g.apply(this,
arguments)})});cb(a.cssHooks.opacity,"get",function(a,b,c){return b.attr?b.opacity||0:a.call(this,b,c)});this.addAnimSetter("d",function(a){var c=a.elem,f;if(!a.started)f=b.init(c,c.d,c.toD),a.start=f[0],a.end=f[1],a.started=!0;c.attr("d",b.step(a.start,a.end,a.pos,c.toD))});this.each=Array.prototype.forEach?function(a,b){return Array.prototype.forEach.call(a,b)}:function(a,b){var c,g=a.length;for(c=0;c<g;c++)if(b.call(a[c],a[c],c,a)===!1)return c};a.fn.highcharts=function(){var a="Chart",b=arguments,
c,g;if(this[0]){Da(b[0])&&(a=b[0],b=Array.prototype.slice.call(b,1));c=b[0];if(c!==y)c.chart=c.chart||{},c.chart.renderTo=this[0],new A[a](c,b[1]),g=this;c===y&&(g=Y[J(this[0],"data-highcharts-chart")])}return g}},addAnimSetter:function(b,c){a.Tween?a.Tween.propHooks[b]={set:c}:a.fx.step[b]=c},getScript:a.getScript,inArray:a.inArray,adapterRun:function(b,c){return a(b)[c]()},grep:a.grep,map:function(a,c){for(var d=[],e=0,f=a.length;e<f;e++)d[e]=c.call(a[e],a[e],e,a);return d},offset:function(b){return a(b).offset()},
addEvent:function(b,c,d){a(b).bind(c,d)},removeEvent:function(b,c,d){var e=B.removeEventListener?"removeEventListener":"detachEvent";B[e]&&b&&!b[e]&&(b[e]=function(){});a(b).unbind(c,d)},fireEvent:function(b,c,d,e){var f=a.Event(c),g="detached"+c,h;!ya&&d&&(delete d.layerX,delete d.layerY,delete d.returnValue);x(f,d);b[c]&&(b[g]=b[c],b[c]=null);a.each(["preventDefault","stopPropagation"],function(a,b){var c=f[b];f[b]=function(){try{c.call(f)}catch(a){b==="preventDefault"&&(h=!0)}}});a(b).trigger(f);
b[g]&&(b[c]=b[g],b[g]=null);e&&!f.isDefaultPrevented()&&!h&&e(f)},washMouseEvent:function(a){var c=a.originalEvent||a;if(c.pageX===y)c.pageX=a.pageX,c.pageY=a.pageY;return c},animate:function(b,c,d){var e=a(b);if(!b.style)b.style={};if(c.d)b.toD=c.d,c.d=1;e.stop();c.opacity!==y&&b.attr&&(c.opacity+="px");b.hasAnim=1;e.animate(c,d)},stop:function(b){b.hasAnim&&a(b).stop()}}})(K.jQuery);var U=K.HighchartsAdapter,F=U||{};U&&U.init.call(U,zb);var kb=F.adapterRun,Rb=F.getScript,Ma=F.inArray,o=A.each=F.each,
lb=F.grep,Sb=F.offset,Ua=F.map,H=F.addEvent,Z=F.removeEvent,I=F.fireEvent,Tb=F.washMouseEvent,mb=F.animate,db=F.stop;T={colors:"#7cb5ec,#434348,#90ed7d,#f7a35c,#8085e9,#f15c80,#e4d354,#2b908f,#f45b5b,#91e8e1".split(","),symbols:["circle","diamond","square","triangle","triangle-down"],lang:{loading:"Loading...",months:"January,February,March,April,May,June,July,August,September,October,November,December".split(","),shortMonths:"Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),weekdays:"Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),
decimalPoint:".",numericSymbols:"k,M,G,T,P,E".split(","),resetZoom:"Reset zoom",resetZoomTitle:"Reset zoom level 1:1",thousandsSep:" "},global:{useUTC:!0,canvasToolsURL:"http://code.highcharts.com/4.1.7/modules/canvas-tools.js",VMLRadialGradientURL:"http://code.highcharts.com/4.1.7/gfx/vml-radial-gradient.png"},chart:{borderColor:"#4572A7",borderRadius:0,defaultSeriesType:"line",ignoreHiddenSeries:!0,spacing:[10,10,15,10],backgroundColor:"#FFFFFF",plotBorderColor:"#C0C0C0",resetZoomButton:{theme:{zIndex:20},
position:{align:"right",x:-10,y:10}}},title:{text:"Chart title",align:"center",margin:15,style:{color:"#333333",fontSize:"18px"}},subtitle:{text:"",align:"center",style:{color:"#555555"}},plotOptions:{line:{allowPointSelect:!1,showCheckbox:!1,animation:{duration:1E3},events:{},lineWidth:2,marker:{lineWidth:0,radius:4,lineColor:"#FFFFFF",states:{hover:{enabled:!0,lineWidthPlus:1,radiusPlus:2},select:{fillColor:"#FFFFFF",lineColor:"#000000",lineWidth:2}}},point:{events:{}},dataLabels:{align:"center",
formatter:function(){return this.y===null?"":A.numberFormat(this.y,-1)},style:{color:"contrast",fontSize:"11px",fontWeight:"bold",textShadow:"0 0 6px contrast, 0 0 3px contrast"},verticalAlign:"bottom",x:0,y:0,padding:5},cropThreshold:300,pointRange:0,states:{hover:{lineWidthPlus:1,marker:{},halo:{size:10,opacity:0.25}},select:{marker:{}}},stickyTracking:!0,turboThreshold:1E3}},labels:{style:{position:"absolute",color:"#3E576F"}},legend:{enabled:!0,align:"center",layout:"horizontal",labelFormatter:function(){return this.name},
borderColor:"#909090",borderRadius:0,navigation:{activeColor:"#274b6d",inactiveColor:"#CCC"},shadow:!1,itemStyle:{color:"#333333",fontSize:"12px",fontWeight:"bold"},itemHoverStyle:{color:"#000"},itemHiddenStyle:{color:"#CCC"},itemCheckboxStyle:{position:"absolute",width:"13px",height:"13px"},symbolPadding:5,verticalAlign:"bottom",x:0,y:0,title:{style:{fontWeight:"bold"}}},loading:{labelStyle:{fontWeight:"bold",position:"relative",top:"45%"},style:{position:"absolute",backgroundColor:"white",opacity:0.5,
textAlign:"center"}},tooltip:{enabled:!0,animation:ca,backgroundColor:"rgba(249, 249, 249, .85)",borderWidth:1,borderRadius:3,dateTimeLabelFormats:{millisecond:"%A, %b %e, %H:%M:%S.%L",second:"%A, %b %e, %H:%M:%S",minute:"%A, %b %e, %H:%M",hour:"%A, %b %e, %H:%M",day:"%A, %b %e, %Y",week:"Week from %A, %b %e, %Y",month:"%B %Y",year:"%Y"},footerFormat:"",headerFormat:'<span style="font-size: 10px">{point.key}</span><br/>',pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}</b><br/>',
shadow:!0,snap:Kb?25:10,style:{color:"#333333",cursor:"default",fontSize:"12px",padding:"8px",whiteSpace:"nowrap"}},credits:{enabled:!0,text:"",href:"",position:{align:"right",x:-10,verticalAlign:"bottom",y:-5},style:{cursor:"pointer",color:"#909090",fontSize:"9px"}}};var ba=T.plotOptions,U=ba.line;Db();var Ub=/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,Vb=/#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/,
Wb=/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/,oa=function(a){var b=[],c,d;(function(a){a&&a.stops?d=Ua(a.stops,function(a){return oa(a[1])}):(c=Ub.exec(a))?b=[D(c[1]),D(c[2]),D(c[3]),parseFloat(c[4],10)]:(c=Vb.exec(a))?b=[D(c[1],16),D(c[2],16),D(c[3],16),1]:(c=Wb.exec(a))&&(b=[D(c[1]),D(c[2]),D(c[3]),1])})(a);return{get:function(c){var f;d?(f=z(a),f.stops=[].concat(f.stops),o(d,function(a,b){f.stops[b]=[f.stops[b][0],a.get(c)]})):f=b&&!isNaN(b[0])?c==="rgb"?"rgb("+b[0]+","+
b[1]+","+b[2]+")":c==="a"?b[3]:"rgba("+b.join(",")+")":a;return f},brighten:function(a){if(d)o(d,function(b){b.brighten(a)});else if(ra(a)&&a!==0){var c;for(c=0;c<3;c++)b[c]+=D(a*255),b[c]<0&&(b[c]=0),b[c]>255&&(b[c]=255)}return this},rgba:b,setOpacity:function(a){b[3]=a;return this},raw:a}};Q.prototype={opacity:1,textProps:"fontSize,fontWeight,fontFamily,fontStyle,color,lineHeight,width,textDecoration,textShadow".split(","),init:function(a,b){this.element=b==="span"?$(b):B.createElementNS(Ca,b);
this.renderer=a},animate:function(a,b,c){b=p(b,za,!0);db(this);if(b){b=z(b,{});if(c)b.complete=c;mb(this,a,b)}else this.attr(a),c&&c();return this},colorGradient:function(a,b,c){var d=this.renderer,e,f,g,h,i,j,k,l,m,n,v=[];a.linearGradient?f="linearGradient":a.radialGradient&&(f="radialGradient");if(f){g=a[f];h=d.gradients;j=a.stops;m=c.radialReference;Ha(g)&&(a[f]=g={x1:g[0],y1:g[1],x2:g[2],y2:g[3],gradientUnits:"userSpaceOnUse"});f==="radialGradient"&&m&&!q(g.gradientUnits)&&(g=z(g,{cx:m[0]-m[2]/
2+g.cx*m[2],cy:m[1]-m[2]/2+g.cy*m[2],r:g.r*m[2],gradientUnits:"userSpaceOnUse"}));for(n in g)n!=="id"&&v.push(n,g[n]);for(n in j)v.push(j[n]);v=v.join(",");h[v]?a=h[v].attr("id"):(g.id=a="highcharts-"+yb++,h[v]=i=d.createElement(f).attr(g).add(d.defs),i.stops=[],o(j,function(a){a[1].indexOf("rgba")===0?(e=oa(a[1]),k=e.get("rgb"),l=e.get("a")):(k=a[1],l=1);a=d.createElement("stop").attr({offset:a[0],"stop-color":k,"stop-opacity":l}).add(i);i.stops.push(a)}));c.setAttribute(b,"url("+d.url+"#"+a+")")}},
applyTextShadow:function(a){var b=this.element,c,d=a.indexOf("contrast")!==-1,e={},f=this.renderer.forExport||b.style.textShadow!==y&&!ya;if(d)e.textShadow=a=a.replace(/contrast/g,this.renderer.getContrast(b.style.fill));if(ib)e.textRendering="geometricPrecision";f?L(b,e):(this.fakeTS=!0,this.ySetter=this.xSetter,c=[].slice.call(b.getElementsByTagName("tspan")),o(a.split(/\s?,\s?/g),function(a){var d=b.firstChild,e,f,a=a.split(" ");e=a[a.length-1];(f=a[a.length-2])&&o(c,function(a,c){var g;c===0&&
(a.setAttribute("x",b.getAttribute("x")),c=b.getAttribute("y"),a.setAttribute("y",c||0),c===null&&b.setAttribute("y",0));g=a.cloneNode(1);J(g,{"class":"highcharts-text-shadow",fill:e,stroke:e,"stroke-opacity":1/u(D(f),3),"stroke-width":f,"stroke-linejoin":"round"});b.insertBefore(g,d)})}))},attr:function(a,b){var c,d,e=this.element,f,g=this,h;typeof a==="string"&&b!==y&&(c=a,a={},a[c]=b);if(typeof a==="string")g=(this[a+"Getter"]||this._defaultGetter).call(this,a,e);else{for(c in a){d=a[c];h=!1;this.symbolName&&
/^(x|y|width|height|r|start|end|innerR|anchorX|anchorY)/.test(c)&&(f||(this.symbolAttr(a),f=!0),h=!0);if(this.rotation&&(c==="x"||c==="y"))this.doTransform=!0;h||(this[c+"Setter"]||this._defaultSetter).call(this,d,c,e);this.shadows&&/^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(c)&&this.updateShadows(c,d)}if(this.doTransform)this.updateTransform(),this.doTransform=!1}return g},updateShadows:function(a,b){for(var c=this.shadows,d=c.length;d--;)c[d].setAttribute(a,a==="height"?u(b-(c[d].cutHeight||
0),0):a==="d"?this.d:b)},addClass:function(a){var b=this.element,c=J(b,"class")||"";c.indexOf(a)===-1&&J(b,"class",c+" "+a);return this},symbolAttr:function(a){var b=this;o("x,y,r,start,end,width,height,innerR,anchorX,anchorY".split(","),function(c){b[c]=p(a[c],b[c])});b.attr({d:b.renderer.symbols[b.symbolName](b.x,b.y,b.width,b.height,b)})},clip:function(a){return this.attr("clip-path",a?"url("+this.renderer.url+"#"+a.id+")":P)},crisp:function(a){var b,c={},d,e=a.strokeWidth||this.strokeWidth||0;
d=r(e)%2/2;a.x=V(a.x||this.x||0)+d;a.y=V(a.y||this.y||0)+d;a.width=V((a.width||this.width||0)-2*d);a.height=V((a.height||this.height||0)-2*d);a.strokeWidth=e;for(b in a)this[b]!==a[b]&&(this[b]=c[b]=a[b]);return c},css:function(a){var b=this.styles,c={},d=this.element,e,f,g="";e=!b;if(a&&a.color)a.fill=a.color;if(b)for(f in a)a[f]!==b[f]&&(c[f]=a[f],e=!0);if(e){e=this.textWidth=a&&a.width&&d.nodeName.toLowerCase()==="text"&&D(a.width)||this.textWidth;b&&(a=x(b,c));this.styles=a;e&&(fa||!ca&&this.renderer.forExport)&&
delete a.width;if(ya&&!ca)L(this.element,a);else{b=function(a,b){return"-"+b.toLowerCase()};for(f in a)g+=f.replace(/([A-Z])/g,b)+":"+a[f]+";";J(d,"style",g)}e&&this.added&&this.renderer.buildText(this)}return this},on:function(a,b){var c=this,d=c.element;ab&&a==="click"?(d.ontouchstart=function(a){c.touchEventFired=Aa.now();a.preventDefault();b.call(d,a)},d.onclick=function(a){(Ba.indexOf("Android")===-1||Aa.now()-(c.touchEventFired||0)>1100)&&b.call(d,a)}):d["on"+a]=b;return this},setRadialReference:function(a){this.element.radialReference=
a;return this},translate:function(a,b){return this.attr({translateX:a,translateY:b})},invert:function(){this.inverted=!0;this.updateTransform();return this},updateTransform:function(){var a=this.translateX||0,b=this.translateY||0,c=this.scaleX,d=this.scaleY,e=this.inverted,f=this.rotation,g=this.element;e&&(a+=this.attr("width"),b+=this.attr("height"));a=["translate("+a+","+b+")"];e?a.push("rotate(90) scale(-1,1)"):f&&a.push("rotate("+f+" "+(g.getAttribute("x")||0)+" "+(g.getAttribute("y")||0)+")");
(q(c)||q(d))&&a.push("scale("+p(c,1)+" "+p(d,1)+")");a.length&&g.setAttribute("transform",a.join(" "))},toFront:function(){var a=this.element;a.parentNode.appendChild(a);return this},align:function(a,b,c){var d,e,f,g,h={};e=this.renderer;f=e.alignedObjects;if(a){if(this.alignOptions=a,this.alignByTranslate=b,!c||Da(c))this.alignTo=d=c||"renderer",ja(f,this),f.push(this),c=null}else a=this.alignOptions,b=this.alignByTranslate,d=this.alignTo;c=p(c,e[d],e);d=a.align;e=a.verticalAlign;f=(c.x||0)+(a.x||
0);g=(c.y||0)+(a.y||0);if(d==="right"||d==="center")f+=(c.width-(a.width||0))/{right:1,center:2}[d];h[b?"translateX":"x"]=r(f);if(e==="bottom"||e==="middle")g+=(c.height-(a.height||0))/({bottom:1,middle:2}[e]||1);h[b?"translateY":"y"]=r(g);this[this.placed?"animate":"attr"](h);this.placed=!0;this.alignAttr=h;return this},getBBox:function(a){var b,c=this.renderer,d,e=this.rotation,f=this.element,g=this.styles,h=e*ha;d=this.textStr;var i,j=f.style,k,l;d!==y&&(l=["",e||0,g&&g.fontSize,f.style.width].join(","),
l=d===""||Pb.test(d)?"num:"+d.toString().length+l:d+l);l&&!a&&(b=c.cache[l]);if(!b){if(f.namespaceURI===Ca||c.forExport){try{k=this.fakeTS&&function(a){o(f.querySelectorAll(".highcharts-text-shadow"),function(b){b.style.display=a})},La&&j.textShadow?(i=j.textShadow,j.textShadow=""):k&&k(P),b=f.getBBox?x({},f.getBBox()):{width:f.offsetWidth,height:f.offsetHeight},i?j.textShadow=i:k&&k("")}catch(m){}if(!b||b.width<0)b={width:0,height:0}}else b=this.htmlGetBBox();if(c.isSVG){a=b.width;d=b.height;if(ya&&
g&&g.fontSize==="11px"&&d.toPrecision(3)==="16.9")b.height=d=14;if(e)b.width=O(d*aa(h))+O(a*X(h)),b.height=O(d*X(h))+O(a*aa(h))}c.cache[l]=b}return b},show:function(a){a&&this.element.namespaceURI===Ca?this.element.removeAttribute("visibility"):this.attr({visibility:a?"inherit":"visible"});return this},hide:function(){return this.attr({visibility:"hidden"})},fadeOut:function(a){var b=this;b.animate({opacity:0},{duration:a||150,complete:function(){b.attr({y:-9999})}})},add:function(a){var b=this.renderer,
c=this.element,d;if(a)this.parentGroup=a;this.parentInverted=a&&a.inverted;this.textStr!==void 0&&b.buildText(this);this.added=!0;if(!a||a.handleZ||this.zIndex)d=this.zIndexSetter();d||(a?a.element:b.box).appendChild(c);if(this.onAdd)this.onAdd();return this},safeRemoveChild:function(a){var b=a.parentNode;b&&b.removeChild(a)},destroy:function(){var a=this,b=a.element||{},c=a.shadows,d=a.renderer.isSVG&&b.nodeName==="SPAN"&&a.parentGroup,e,f;b.onclick=b.onmouseout=b.onmouseover=b.onmousemove=b.point=
null;db(a);if(a.clipPath)a.clipPath=a.clipPath.destroy();if(a.stops){for(f=0;f<a.stops.length;f++)a.stops[f]=a.stops[f].destroy();a.stops=null}a.safeRemoveChild(b);for(c&&o(c,function(b){a.safeRemoveChild(b)});d&&d.div&&d.div.childNodes.length===0;)b=d.parentGroup,a.safeRemoveChild(d.div),delete d.div,d=b;a.alignTo&&ja(a.renderer.alignedObjects,a);for(e in a)delete a[e];return null},shadow:function(a,b,c){var d=[],e,f,g=this.element,h,i,j,k;if(a){i=p(a.width,3);j=(a.opacity||0.15)/i;k=this.parentInverted?
"(-1,-1)":"("+p(a.offsetX,1)+", "+p(a.offsetY,1)+")";for(e=1;e<=i;e++){f=g.cloneNode(0);h=i*2+1-2*e;J(f,{isShadow:"true",stroke:a.color||"black","stroke-opacity":j*e,"stroke-width":h,transform:"translate"+k,fill:P});if(c)J(f,"height",u(J(f,"height")-h,0)),f.cutHeight=h;b?b.element.appendChild(f):g.parentNode.insertBefore(f,g);d.push(f)}this.shadows=d}return this},xGetter:function(a){this.element.nodeName==="circle"&&(a={x:"cx",y:"cy"}[a]||a);return this._defaultGetter(a)},_defaultGetter:function(a){a=
p(this[a],this.element?this.element.getAttribute(a):null,0);/^[\-0-9\.]+$/.test(a)&&(a=parseFloat(a));return a},dSetter:function(a,b,c){a&&a.join&&(a=a.join(" "));/(NaN| {2}|^$)/.test(a)&&(a="M 0 0");c.setAttribute(b,a);this[b]=a},dashstyleSetter:function(a){var b;if(a=a&&a.toLowerCase()){a=a.replace("shortdashdotdot","3,1,1,1,1,1,").replace("shortdashdot","3,1,1,1").replace("shortdot","1,1,").replace("shortdash","3,1,").replace("longdash","8,3,").replace(/dot/g,"1,3,").replace("dash","4,3,").replace(/,$/,
"").split(",");for(b=a.length;b--;)a[b]=D(a[b])*this["stroke-width"];a=a.join(",").replace("NaN","none");this.element.setAttribute("stroke-dasharray",a)}},alignSetter:function(a){this.element.setAttribute("text-anchor",{left:"start",center:"middle",right:"end"}[a])},opacitySetter:function(a,b,c){this[b]=a;c.setAttribute(b,a)},titleSetter:function(a){var b=this.element.getElementsByTagName("title")[0];b||(b=B.createElementNS(Ca,"title"),this.element.appendChild(b));b.appendChild(B.createTextNode(String(p(a),
"").replace(/<[^>]*>/g,"")))},textSetter:function(a){if(a!==this.textStr)delete this.bBox,this.textStr=a,this.added&&this.renderer.buildText(this)},fillSetter:function(a,b,c){typeof a==="string"?c.setAttribute(b,a):a&&this.colorGradient(a,b,c)},zIndexSetter:function(a,b){var c=this.renderer,d=this.parentGroup,c=(d||c).element||c.box,e,f,g=this.element,h;e=this.added;var i;q(a)&&(g.setAttribute(b,a),a=+a,this[b]===a&&(e=!1),this[b]=a);if(e){if((a=this.zIndex)&&d)d.handleZ=!0;d=c.childNodes;for(i=0;i<
d.length&&!h;i++)if(e=d[i],f=J(e,"zIndex"),e!==g&&(D(f)>a||!q(a)&&q(f)))c.insertBefore(g,e),h=!0;h||c.appendChild(g)}return h},_defaultSetter:function(a,b,c){c.setAttribute(b,a)}};Q.prototype.yGetter=Q.prototype.xGetter;Q.prototype.translateXSetter=Q.prototype.translateYSetter=Q.prototype.rotationSetter=Q.prototype.verticalAlignSetter=Q.prototype.scaleXSetter=Q.prototype.scaleYSetter=function(a,b){this[b]=a;this.doTransform=!0};Q.prototype["stroke-widthSetter"]=Q.prototype.strokeSetter=function(a,
b,c){this[b]=a;if(this.stroke&&this["stroke-width"])this.strokeWidth=this["stroke-width"],Q.prototype.fillSetter.call(this,this.stroke,"stroke",c),c.setAttribute("stroke-width",this["stroke-width"]),this.hasStroke=!0;else if(b==="stroke-width"&&a===0&&this.hasStroke)c.removeAttribute("stroke"),this.hasStroke=!1};var ua=function(){this.init.apply(this,arguments)};ua.prototype={Element:Q,init:function(a,b,c,d,e){var f=location,g,d=this.createElement("svg").attr({version:"1.1"}).css(this.getStyle(d));
g=d.element;a.appendChild(g);a.innerHTML.indexOf("xmlns")===-1&&J(g,"xmlns",Ca);this.isSVG=!0;this.box=g;this.boxWrapper=d;this.alignedObjects=[];this.url=(La||ib)&&B.getElementsByTagName("base").length?f.href.replace(/#.*?$/,"").replace(/([\('\)])/g,"\\$1").replace(/ /g,"%20"):"";this.createElement("desc").add().element.appendChild(B.createTextNode("Created with Highcharts 4.1.7"));this.defs=this.createElement("defs").add();this.forExport=e;this.gradients={};this.cache={};this.setSize(b,c,!1);var h;
if(La&&a.getBoundingClientRect)this.subPixelFix=b=function(){L(a,{left:0,top:0});h=a.getBoundingClientRect();L(a,{left:ta(h.left)-h.left+"px",top:ta(h.top)-h.top+"px"})},b(),H(K,"resize",b)},getStyle:function(a){return this.style=x({fontFamily:'"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',fontSize:"12px"},a)},isHidden:function(){return!this.boxWrapper.getBBox().width},destroy:function(){var a=this.defs;this.box=null;this.boxWrapper=this.boxWrapper.destroy();Qa(this.gradients||
{});this.gradients=null;if(a)this.defs=a.destroy();this.subPixelFix&&Z(K,"resize",this.subPixelFix);return this.alignedObjects=null},createElement:function(a){var b=new this.Element;b.init(this,a);return b},draw:function(){},buildText:function(a){for(var b=a.element,c=this,d=c.forExport,e=p(a.textStr,"").toString(),f=e.indexOf("<")!==-1,g=b.childNodes,h,i,j=J(b,"x"),k=a.styles,l=a.textWidth,m=k&&k.lineHeight,n=k&&k.textShadow,v=k&&k.textOverflow==="ellipsis",s=g.length,S=l&&!a.added&&this.box,N=function(a){return m?
D(m):c.fontMetrics(/(px|em)$/.test(a&&a.style.fontSize)?a.style.fontSize:k&&k.fontSize||c.style.fontSize||12,a).h},t=function(a){return a.replace(/&lt;/g,"<").replace(/&gt;/g,">")};s--;)b.removeChild(g[s]);!f&&!n&&!v&&e.indexOf(" ")===-1?b.appendChild(B.createTextNode(t(e))):(h=/<.*style="([^"]+)".*>/,i=/<.*href="(http[^"]+)".*>/,S&&S.appendChild(b),e=f?e.replace(/<(b|strong)>/g,'<span style="font-weight:bold">').replace(/<(i|em)>/g,'<span style="font-style:italic">').replace(/<a/g,"<span").replace(/<\/(b|strong|i|em|a)>/g,
"</span>").split(/<br.*?>/g):[e],e[e.length-1]===""&&e.pop(),o(e,function(e,f){var g,m=0,e=e.replace(/<span/g,"|||<span").replace(/<\/span>/g,"</span>|||");g=e.split("|||");o(g,function(e){if(e!==""||g.length===1){var n={},s=B.createElementNS(Ca,"tspan"),p;h.test(e)&&(p=e.match(h)[1].replace(/(;| |^)color([ :])/,"$1fill$2"),J(s,"style",p));i.test(e)&&!d&&(J(s,"onclick",'location.href="'+e.match(i)[1]+'"'),L(s,{cursor:"pointer"}));e=t(e.replace(/<(.|\n)*?>/g,"")||" ");if(e!==" "){s.appendChild(B.createTextNode(e));
if(m)n.dx=0;else if(f&&j!==null)n.x=j;J(s,n);b.appendChild(s);!m&&f&&(!ca&&d&&L(s,{display:"block"}),J(s,"dy",N(s)));if(l){for(var n=e.replace(/([^\^])-/g,"$1- ").split(" "),o=g.length>1||f||n.length>1&&k.whiteSpace!=="nowrap",S,w,q,u=[],y=N(s),r=1,x=a.rotation,z=e,C=z.length;(o||v)&&(n.length||u.length);)a.rotation=0,S=a.getBBox(!0),q=S.width,!ca&&c.forExport&&(q=c.measureSpanWidth(s.firstChild.data,a.styles)),S=q>l,w===void 0&&(w=S),v&&w?(C/=2,z===""||!S&&C<0.5?n=[]:(S&&(w=!0),z=e.substring(0,z.length+
(S?-1:1)*ta(C)),n=[z+(l>3?"…":"")],s.removeChild(s.firstChild))):!S||n.length===1?(n=u,u=[],n.length&&(r++,s=B.createElementNS(Ca,"tspan"),J(s,{dy:y,x:j}),p&&J(s,"style",p),b.appendChild(s)),q>l&&(l=q)):(s.removeChild(s.firstChild),u.unshift(n.pop())),n.length&&s.appendChild(B.createTextNode(n.join(" ").replace(/- /g,"-")));w&&a.attr("title",a.textStr);a.rotation=x}m++}}})}),S&&S.removeChild(b),n&&a.applyTextShadow&&a.applyTextShadow(n))},getContrast:function(a){a=oa(a).rgba;return a[0]+a[1]+a[2]>
384?"#000000":"#FFFFFF"},button:function(a,b,c,d,e,f,g,h,i){var j=this.label(a,b,c,i,null,null,null,null,"button"),k=0,l,m,n,v,s,p,a={x1:0,y1:0,x2:0,y2:1},e=z({"stroke-width":1,stroke:"#CCCCCC",fill:{linearGradient:a,stops:[[0,"#FEFEFE"],[1,"#F6F6F6"]]},r:2,padding:5,style:{color:"black"}},e);n=e.style;delete e.style;f=z(e,{stroke:"#68A",fill:{linearGradient:a,stops:[[0,"#FFF"],[1,"#ACF"]]}},f);v=f.style;delete f.style;g=z(e,{stroke:"#68A",fill:{linearGradient:a,stops:[[0,"#9BD"],[1,"#CDF"]]}},g);
s=g.style;delete g.style;h=z(e,{style:{color:"#CCC"}},h);p=h.style;delete h.style;H(j.element,ya?"mouseover":"mouseenter",function(){k!==3&&j.attr(f).css(v)});H(j.element,ya?"mouseout":"mouseleave",function(){k!==3&&(l=[e,f,g][k],m=[n,v,s][k],j.attr(l).css(m))});j.setState=function(a){(j.state=k=a)?a===2?j.attr(g).css(s):a===3&&j.attr(h).css(p):j.attr(e).css(n)};return j.on("click",function(){k!==3&&d.call(j)}).attr(e).css(x({cursor:"default"},n))},crispLine:function(a,b){a[1]===a[4]&&(a[1]=a[4]=
r(a[1])-b%2/2);a[2]===a[5]&&(a[2]=a[5]=r(a[2])+b%2/2);return a},path:function(a){var b={fill:P};Ha(a)?b.d=a:da(a)&&x(b,a);return this.createElement("path").attr(b)},circle:function(a,b,c){a=da(a)?a:{x:a,y:b,r:c};b=this.createElement("circle");b.xSetter=function(a){this.element.setAttribute("cx",a)};b.ySetter=function(a){this.element.setAttribute("cy",a)};return b.attr(a)},arc:function(a,b,c,d,e,f){if(da(a))b=a.y,c=a.r,d=a.innerR,e=a.start,f=a.end,a=a.x;a=this.symbol("arc",a||0,b||0,c||0,c||0,{innerR:d||
0,start:e||0,end:f||0});a.r=c;return a},rect:function(a,b,c,d,e,f){var e=da(a)?a.r:e,g=this.createElement("rect"),a=da(a)?a:a===y?{}:{x:a,y:b,width:u(c,0),height:u(d,0)};if(f!==y)a.strokeWidth=f,a=g.crisp(a);if(e)a.r=e;g.rSetter=function(a){J(this.element,{rx:a,ry:a})};return g.attr(a)},setSize:function(a,b,c){var d=this.alignedObjects,e=d.length;this.width=a;this.height=b;for(this.boxWrapper[p(c,!0)?"animate":"attr"]({width:a,height:b});e--;)d[e].align()},g:function(a){var b=this.createElement("g");
return q(a)?b.attr({"class":"highcharts-"+a}):b},image:function(a,b,c,d,e){var f={preserveAspectRatio:P};arguments.length>1&&x(f,{x:b,y:c,width:d,height:e});f=this.createElement("image").attr(f);f.element.setAttributeNS?f.element.setAttributeNS("http://www.w3.org/1999/xlink","href",a):f.element.setAttribute("hc-svg-href",a);return f},symbol:function(a,b,c,d,e,f){var g,h=this.symbols[a],h=h&&h(r(b),r(c),d,e,f),i=/^url\((.*?)\)$/,j,k;if(h)g=this.path(h),x(g,{symbolName:a,x:b,y:c,width:d,height:e}),
f&&x(g,f);else if(i.test(a))k=function(a,b){a.element&&(a.attr({width:b[0],height:b[1]}),a.alignByTranslate||a.translate(r((d-b[0])/2),r((e-b[1])/2)))},j=a.match(i)[1],a=Lb[j]||f&&f.width&&f.height&&[f.width,f.height],g=this.image(j).attr({x:b,y:c}),g.isImg=!0,a?k(g,a):(g.attr({width:0,height:0}),$("img",{onload:function(){k(g,Lb[j]=[this.width,this.height])},src:j}));return g},symbols:{circle:function(a,b,c,d){var e=0.166*c;return["M",a+c/2,b,"C",a+c+e,b,a+c+e,b+d,a+c/2,b+d,"C",a-e,b+d,a-e,b,a+c/
2,b,"Z"]},square:function(a,b,c,d){return["M",a,b,"L",a+c,b,a+c,b+d,a,b+d,"Z"]},triangle:function(a,b,c,d){return["M",a+c/2,b,"L",a+c,b+d,a,b+d,"Z"]},"triangle-down":function(a,b,c,d){return["M",a,b,"L",a+c,b,a+c/2,b+d,"Z"]},diamond:function(a,b,c,d){return["M",a+c/2,b,"L",a+c,b+d/2,a+c/2,b+d,a,b+d/2,"Z"]},arc:function(a,b,c,d,e){var f=e.start,c=e.r||c||d,g=e.end-0.001,d=e.innerR,h=e.open,i=X(f),j=aa(f),k=X(g),g=aa(g),e=e.end-f<ma?0:1;return["M",a+c*i,b+c*j,"A",c,c,0,e,1,a+c*k,b+c*g,h?"M":"L",a+d*
k,b+d*g,"A",d,d,0,e,0,a+d*i,b+d*j,h?"":"Z"]},callout:function(a,b,c,d,e){var f=C(e&&e.r||0,c,d),g=f+6,h=e&&e.anchorX,e=e&&e.anchorY,i;i=["M",a+f,b,"L",a+c-f,b,"C",a+c,b,a+c,b,a+c,b+f,"L",a+c,b+d-f,"C",a+c,b+d,a+c,b+d,a+c-f,b+d,"L",a+f,b+d,"C",a,b+d,a,b+d,a,b+d-f,"L",a,b+f,"C",a,b,a,b,a+f,b];h&&h>c&&e>b+g&&e<b+d-g?i.splice(13,3,"L",a+c,e-6,a+c+6,e,a+c,e+6,a+c,b+d-f):h&&h<0&&e>b+g&&e<b+d-g?i.splice(33,3,"L",a,e+6,a-6,e,a,e-6,a,b+f):e&&e>d&&h>a+g&&h<a+c-g?i.splice(23,3,"L",h+6,b+d,h,b+d+6,h-6,b+d,a+
f,b+d):e&&e<0&&h>a+g&&h<a+c-g&&i.splice(3,3,"L",h-6,b,h,b-6,h+6,b,c-f,b);return i}},clipRect:function(a,b,c,d){var e="highcharts-"+yb++,f=this.createElement("clipPath").attr({id:e}).add(this.defs),a=this.rect(a,b,c,d,0).add(f);a.id=e;a.clipPath=f;a.count=0;return a},text:function(a,b,c,d){var e=fa||!ca&&this.forExport,f={};if(d&&!this.forExport)return this.html(a,b,c);f.x=Math.round(b||0);if(c)f.y=Math.round(c);if(a||a===0)f.text=a;a=this.createElement("text").attr(f);e&&a.css({position:"absolute"});
if(!d)a.xSetter=function(a,b,c){var d=c.getElementsByTagName("tspan"),e,f=c.getAttribute(b),m;for(m=0;m<d.length;m++)e=d[m],e.getAttribute(b)===f&&e.setAttribute(b,a);c.setAttribute(b,a)};return a},fontMetrics:function(a,b){var c,d,a=a||this.style.fontSize;b&&K.getComputedStyle&&(b=b.element||b,a=(c=K.getComputedStyle(b,""))&&c.fontSize);a=/px/.test(a)?D(a):/em/.test(a)?parseFloat(a)*12:12;c=a<24?a+3:r(a*1.2);d=r(c*0.8);return{h:c,b:d,f:a}},rotCorr:function(a,b,c){var d=a;b&&c&&(d=u(d*X(b*ha),4));
return{x:-a/3*aa(b*ha),y:d}},label:function(a,b,c,d,e,f,g,h,i){function j(){var a,b;a=v.element.style;p=(ga===void 0||u===void 0||n.styles.textAlign)&&q(v.textStr)&&v.getBBox();n.width=(ga||p.width||0)+2*t+w;n.height=(u||p.height||0)+2*t;A=t+m.fontMetrics(a&&a.fontSize,v).b;if(D){if(!s)a=r(-N*t)+B,b=(h?-A:0)+B,n.box=s=d?m.symbol(d,a,b,n.width,n.height,G):m.rect(a,b,n.width,n.height,0,G[Qb]),s.attr("fill",P).add(n);s.isImg||s.attr(x({width:r(n.width),height:r(n.height)},G));G=null}}function k(){var a=
n.styles,a=a&&a.textAlign,b=w+t*(1-N),c;c=h?0:A;if(q(ga)&&p&&(a==="center"||a==="right"))b+={center:0.5,right:1}[a]*(ga-p.width);if(b!==v.x||c!==v.y)v.attr("x",b),c!==y&&v.attr("y",c);v.x=b;v.y=c}function l(a,b){s?s.attr(a,b):G[a]=b}var m=this,n=m.g(i),v=m.text("",0,0,g).attr({zIndex:1}),s,p,N=0,t=3,w=0,ga,u,Ab,C,B=0,G={},A,D;n.onAdd=function(){v.add(n);n.attr({text:a||a===0?a:"",x:b,y:c});s&&q(e)&&n.attr({anchorX:e,anchorY:f})};n.widthSetter=function(a){ga=a};n.heightSetter=function(a){u=a};n.paddingSetter=
function(a){if(q(a)&&a!==t)t=n.padding=a,k()};n.paddingLeftSetter=function(a){q(a)&&a!==w&&(w=a,k())};n.alignSetter=function(a){N={left:0,center:0.5,right:1}[a]};n.textSetter=function(a){a!==y&&v.textSetter(a);j();k()};n["stroke-widthSetter"]=function(a,b){a&&(D=!0);B=a%2/2;l(b,a)};n.strokeSetter=n.fillSetter=n.rSetter=function(a,b){b==="fill"&&a&&(D=!0);l(b,a)};n.anchorXSetter=function(a,b){e=a;l(b,r(a)-B-Ab)};n.anchorYSetter=function(a,b){f=a;l(b,a-C)};n.xSetter=function(a){n.x=a;N&&(a-=N*((ga||
p.width)+t));Ab=r(a);n.attr("translateX",Ab)};n.ySetter=function(a){C=n.y=r(a);n.attr("translateY",C)};var F=n.css;return x(n,{css:function(a){if(a){var b={},a=z(a);o(n.textProps,function(c){a[c]!==y&&(b[c]=a[c],delete a[c])});v.css(b)}return F.call(n,a)},getBBox:function(){return{width:p.width+2*t,height:p.height+2*t,x:p.x-t,y:p.y-t}},shadow:function(a){s&&s.shadow(a);return n},destroy:function(){Z(n.element,"mouseenter");Z(n.element,"mouseleave");v&&(v=v.destroy());s&&(s=s.destroy());Q.prototype.destroy.call(n);
n=m=j=k=l=null}})}};$a=ua;x(Q.prototype,{htmlCss:function(a){var b=this.element;if(b=a&&b.tagName==="SPAN"&&a.width)delete a.width,this.textWidth=b,this.updateTransform();if(a&&a.textOverflow==="ellipsis")a.whiteSpace="nowrap",a.overflow="hidden";this.styles=x(this.styles,a);L(this.element,a);return this},htmlGetBBox:function(){var a=this.element;if(a.nodeName==="text")a.style.position="absolute";return{x:a.offsetLeft,y:a.offsetTop,width:a.offsetWidth,height:a.offsetHeight}},htmlUpdateTransform:function(){if(this.added){var a=
this.renderer,b=this.element,c=this.translateX||0,d=this.translateY||0,e=this.x||0,f=this.y||0,g=this.textAlign||"left",h={left:0,center:0.5,right:1}[g],i=this.shadows,j=this.styles;L(b,{marginLeft:c,marginTop:d});i&&o(i,function(a){L(a,{marginLeft:c+1,marginTop:d+1})});this.inverted&&o(b.childNodes,function(c){a.invertChild(c,b)});if(b.tagName==="SPAN"){var k=this.rotation,l,m=D(this.textWidth),n=[k,g,b.innerHTML,this.textWidth].join(",");if(n!==this.cTT){l=a.fontMetrics(b.style.fontSize).b;q(k)&&
this.setSpanRotation(k,h,l);i=p(this.elemWidth,b.offsetWidth);if(i>m&&/[ \-]/.test(b.textContent||b.innerText))L(b,{width:m+"px",display:"block",whiteSpace:j&&j.whiteSpace||"normal"}),i=m;this.getSpanCorrection(i,l,h,k,g)}L(b,{left:e+(this.xCorr||0)+"px",top:f+(this.yCorr||0)+"px"});if(ib)l=b.offsetHeight;this.cTT=n}}else this.alignOnAdd=!0},setSpanRotation:function(a,b,c){var d={},e=ya?"-ms-transform":ib?"-webkit-transform":La?"MozTransform":Jb?"-o-transform":"";d[e]=d.transform="rotate("+a+"deg)";
d[e+(La?"Origin":"-origin")]=d.transformOrigin=b*100+"% "+c+"px";L(this.element,d)},getSpanCorrection:function(a,b,c){this.xCorr=-a*c;this.yCorr=-b}});x(ua.prototype,{html:function(a,b,c){var d=this.createElement("span"),e=d.element,f=d.renderer;d.textSetter=function(a){a!==e.innerHTML&&delete this.bBox;e.innerHTML=this.textStr=a};d.xSetter=d.ySetter=d.alignSetter=d.rotationSetter=function(a,b){b==="align"&&(b="textAlign");d[b]=a;d.htmlUpdateTransform()};d.attr({text:a,x:r(b),y:r(c)}).css({position:"absolute",
fontFamily:this.style.fontFamily,fontSize:this.style.fontSize});e.style.whiteSpace="nowrap";d.css=d.htmlCss;if(f.isSVG)d.add=function(a){var b,c=f.box.parentNode,j=[];if(this.parentGroup=a){if(b=a.div,!b){for(;a;)j.push(a),a=a.parentGroup;o(j.reverse(),function(a){var d,e=J(a.element,"class");e&&(e={className:e});b=a.div=a.div||$(Ka,e,{position:"absolute",left:(a.translateX||0)+"px",top:(a.translateY||0)+"px"},b||c);d=b.style;x(a,{translateXSetter:function(b,c){d.left=b+"px";a[c]=b;a.doTransform=
!0},translateYSetter:function(b,c){d.top=b+"px";a[c]=b;a.doTransform=!0},visibilitySetter:function(a,b){d[b]=a}})})}}else b=c;b.appendChild(e);d.added=!0;d.alignOnAdd&&d.htmlUpdateTransform();return d};return d}});if(!ca&&!fa){F={init:function(a,b){var c=["<",b,' filled="f" stroked="f"'],d=["position: ","absolute",";"],e=b===Ka;(b==="shape"||e)&&d.push("left:0;top:0;width:1px;height:1px;");d.push("visibility: ",e?"hidden":"visible");c.push(' style="',d.join(""),'"/>');if(b)c=e||b==="span"||b==="img"?
c.join(""):a.prepVML(c),this.element=$(c);this.renderer=a},add:function(a){var b=this.renderer,c=this.element,d=b.box,d=a?a.element||a:d;a&&a.inverted&&b.invertChild(c,d);d.appendChild(c);this.added=!0;this.alignOnAdd&&!this.deferUpdateTransform&&this.updateTransform();if(this.onAdd)this.onAdd();return this},updateTransform:Q.prototype.htmlUpdateTransform,setSpanRotation:function(){var a=this.rotation,b=X(a*ha),c=aa(a*ha);L(this.element,{filter:a?["progid:DXImageTransform.Microsoft.Matrix(M11=",b,
", M12=",-c,", M21=",c,", M22=",b,", sizingMethod='auto expand')"].join(""):P})},getSpanCorrection:function(a,b,c,d,e){var f=d?X(d*ha):1,g=d?aa(d*ha):0,h=p(this.elemHeight,this.element.offsetHeight),i;this.xCorr=f<0&&-a;this.yCorr=g<0&&-h;i=f*g<0;this.xCorr+=g*b*(i?1-c:c);this.yCorr-=f*b*(d?i?c:1-c:1);e&&e!=="left"&&(this.xCorr-=a*c*(f<0?-1:1),d&&(this.yCorr-=h*c*(g<0?-1:1)),L(this.element,{textAlign:e}))},pathToVML:function(a){for(var b=a.length,c=[];b--;)if(ra(a[b]))c[b]=r(a[b]*10)-5;else if(a[b]===
"Z")c[b]="x";else if(c[b]=a[b],a.isArc&&(a[b]==="wa"||a[b]==="at"))c[b+5]===c[b+7]&&(c[b+7]+=a[b+7]>a[b+5]?1:-1),c[b+6]===c[b+8]&&(c[b+8]+=a[b+8]>a[b+6]?1:-1);return c.join(" ")||"x"},clip:function(a){var b=this,c;a?(c=a.members,ja(c,b),c.push(b),b.destroyClip=function(){ja(c,b)},a=a.getCSS(b)):(b.destroyClip&&b.destroyClip(),a={clip:hb?"inherit":"rect(auto)"});return b.css(a)},css:Q.prototype.htmlCss,safeRemoveChild:function(a){a.parentNode&&Ra(a)},destroy:function(){this.destroyClip&&this.destroyClip();
return Q.prototype.destroy.apply(this)},on:function(a,b){this.element["on"+a]=function(){var a=K.event;a.target=a.srcElement;b(a)};return this},cutOffPath:function(a,b){var c,a=a.split(/[ ,]/);c=a.length;if(c===9||c===11)a[c-4]=a[c-2]=D(a[c-2])-10*b;return a.join(" ")},shadow:function(a,b,c){var d=[],e,f=this.element,g=this.renderer,h,i=f.style,j,k=f.path,l,m,n,v;k&&typeof k.value!=="string"&&(k="x");m=k;if(a){n=p(a.width,3);v=(a.opacity||0.15)/n;for(e=1;e<=3;e++){l=n*2+1-2*e;c&&(m=this.cutOffPath(k.value,
l+0.5));j=['<shape isShadow="true" strokeweight="',l,'" filled="false" path="',m,'" coordsize="10 10" style="',f.style.cssText,'" />'];h=$(g.prepVML(j),null,{left:D(i.left)+p(a.offsetX,1),top:D(i.top)+p(a.offsetY,1)});if(c)h.cutOff=l+1;j=['<stroke color="',a.color||"black",'" opacity="',v*e,'"/>'];$(g.prepVML(j),null,null,h);b?b.element.appendChild(h):f.parentNode.insertBefore(h,f);d.push(h)}this.shadows=d}return this},updateShadows:na,setAttr:function(a,b){hb?this.element[a]=b:this.element.setAttribute(a,
b)},classSetter:function(a){this.element.className=a},dashstyleSetter:function(a,b,c){(c.getElementsByTagName("stroke")[0]||$(this.renderer.prepVML(["<stroke/>"]),null,null,c))[b]=a||"solid";this[b]=a},dSetter:function(a,b,c){var d=this.shadows,a=a||[];this.d=a.join&&a.join(" ");c.path=a=this.pathToVML(a);if(d)for(c=d.length;c--;)d[c].path=d[c].cutOff?this.cutOffPath(a,d[c].cutOff):a;this.setAttr(b,a)},fillSetter:function(a,b,c){var d=c.nodeName;if(d==="SPAN")c.style.color=a;else if(d!=="IMG")c.filled=
a!==P,this.setAttr("fillcolor",this.renderer.color(a,c,b,this))},opacitySetter:na,rotationSetter:function(a,b,c){c=c.style;this[b]=c[b]=a;c.left=-r(aa(a*ha)+1)+"px";c.top=r(X(a*ha))+"px"},strokeSetter:function(a,b,c){this.setAttr("strokecolor",this.renderer.color(a,c,b))},"stroke-widthSetter":function(a,b,c){c.stroked=!!a;this[b]=a;ra(a)&&(a+="px");this.setAttr("strokeweight",a)},titleSetter:function(a,b){this.setAttr(b,a)},visibilitySetter:function(a,b,c){a==="inherit"&&(a="visible");this.shadows&&
o(this.shadows,function(c){c.style[b]=a});c.nodeName==="DIV"&&(a=a==="hidden"?"-999em":0,hb||(c.style[b]=a?"visible":"hidden"),b="top");c.style[b]=a},xSetter:function(a,b,c){this[b]=a;b==="x"?b="left":b==="y"&&(b="top");this.updateClipping?(this[b]=a,this.updateClipping()):c.style[b]=a},zIndexSetter:function(a,b,c){c.style[b]=a}};A.VMLElement=F=ka(Q,F);F.prototype.ySetter=F.prototype.widthSetter=F.prototype.heightSetter=F.prototype.xSetter;var Na={Element:F,isIE8:Ba.indexOf("MSIE 8.0")>-1,init:function(a,
b,c,d){var e;this.alignedObjects=[];d=this.createElement(Ka).css(x(this.getStyle(d),{position:"relative"}));e=d.element;a.appendChild(d.element);this.isVML=!0;this.box=e;this.boxWrapper=d;this.cache={};this.setSize(b,c,!1);if(!B.namespaces.hcv){B.namespaces.add("hcv","urn:schemas-microsoft-com:vml");try{B.createStyleSheet().cssText="hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}catch(f){B.styleSheets[0].cssText+="hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}}},
isHidden:function(){return!this.box.offsetWidth},clipRect:function(a,b,c,d){var e=this.createElement(),f=da(a);return x(e,{members:[],count:0,left:(f?a.x:a)+1,top:(f?a.y:b)+1,width:(f?a.width:c)-1,height:(f?a.height:d)-1,getCSS:function(a){var b=a.element,c=b.nodeName,a=a.inverted,d=this.top-(c==="shape"?b.offsetTop:0),e=this.left,b=e+this.width,f=d+this.height,d={clip:"rect("+r(a?e:d)+"px,"+r(a?f:b)+"px,"+r(a?b:f)+"px,"+r(a?d:e)+"px)"};!a&&hb&&c==="DIV"&&x(d,{width:b+"px",height:f+"px"});return d},
updateClipping:function(){o(e.members,function(a){a.element&&a.css(e.getCSS(a))})}})},color:function(a,b,c,d){var e=this,f,g=/^rgba/,h,i,j=P;a&&a.linearGradient?i="gradient":a&&a.radialGradient&&(i="pattern");if(i){var k,l,m=a.linearGradient||a.radialGradient,n,v,s,p,N,t="",a=a.stops,w,ga=[],q=function(){h=['<fill colors="'+ga.join(",")+'" opacity="',s,'" o:opacity2="',v,'" type="',i,'" ',t,'focus="100%" method="any" />'];$(e.prepVML(h),null,null,b)};n=a[0];w=a[a.length-1];n[0]>0&&a.unshift([0,n[1]]);
w[0]<1&&a.push([1,w[1]]);o(a,function(a,b){g.test(a[1])?(f=oa(a[1]),k=f.get("rgb"),l=f.get("a")):(k=a[1],l=1);ga.push(a[0]*100+"% "+k);b?(s=l,p=k):(v=l,N=k)});if(c==="fill")if(i==="gradient")c=m.x1||m[0]||0,a=m.y1||m[1]||0,n=m.x2||m[2]||0,m=m.y2||m[3]||0,t='angle="'+(90-W.atan((m-a)/(n-c))*180/ma)+'"',q();else{var j=m.r,u=j*2,y=j*2,r=m.cx,x=m.cy,C=b.radialReference,z,j=function(){C&&(z=d.getBBox(),r+=(C[0]-z.x)/z.width-0.5,x+=(C[1]-z.y)/z.height-0.5,u*=C[2]/z.width,y*=C[2]/z.height);t='src="'+T.global.VMLRadialGradientURL+
'" size="'+u+","+y+'" origin="0.5,0.5" position="'+r+","+x+'" color2="'+N+'" ';q()};d.added?j():d.onAdd=j;j=p}else j=k}else if(g.test(a)&&b.tagName!=="IMG")f=oa(a),h=["<",c,' opacity="',f.get("a"),'"/>'],$(this.prepVML(h),null,null,b),j=f.get("rgb");else{j=b.getElementsByTagName(c);if(j.length)j[0].opacity=1,j[0].type="solid";j=a}return j},prepVML:function(a){var b=this.isIE8,a=a.join("");b?(a=a.replace("/>",' xmlns="urn:schemas-microsoft-com:vml" />'),a=a.indexOf('style="')===-1?a.replace("/>",' style="display:inline-block;behavior:url(#default#VML);" />'):
a.replace('style="','style="display:inline-block;behavior:url(#default#VML);')):a=a.replace("<","<hcv:");return a},text:ua.prototype.html,path:function(a){var b={coordsize:"10 10"};Ha(a)?b.d=a:da(a)&&x(b,a);return this.createElement("shape").attr(b)},circle:function(a,b,c){var d=this.symbol("circle");if(da(a))c=a.r,b=a.y,a=a.x;d.isCircle=!0;d.r=c;return d.attr({x:a,y:b})},g:function(a){var b;a&&(b={className:"highcharts-"+a,"class":"highcharts-"+a});return this.createElement(Ka).attr(b)},image:function(a,
b,c,d,e){var f=this.createElement("img").attr({src:a});arguments.length>1&&f.attr({x:b,y:c,width:d,height:e});return f},createElement:function(a){return a==="rect"?this.symbol(a):ua.prototype.createElement.call(this,a)},invertChild:function(a,b){var c=this,d=b.style,e=a.tagName==="IMG"&&a.style;L(a,{flip:"x",left:D(d.width)-(e?D(e.top):1),top:D(d.height)-(e?D(e.left):1),rotation:-90});o(a.childNodes,function(b){c.invertChild(b,a)})},symbols:{arc:function(a,b,c,d,e){var f=e.start,g=e.end,h=e.r||c||
d,c=e.innerR,d=X(f),i=aa(f),j=X(g),k=aa(g);if(g-f===0)return["x"];f=["wa",a-h,b-h,a+h,b+h,a+h*d,b+h*i,a+h*j,b+h*k];e.open&&!c&&f.push("e","M",a,b);f.push("at",a-c,b-c,a+c,b+c,a+c*j,b+c*k,a+c*d,b+c*i,"x","e");f.isArc=!0;return f},circle:function(a,b,c,d,e){e&&(c=d=2*e.r);e&&e.isCircle&&(a-=c/2,b-=d/2);return["wa",a,b,a+c,b+d,a+c,b+d/2,a+c,b+d/2,"e"]},rect:function(a,b,c,d,e){return ua.prototype.symbols[!q(e)||!e.r?"square":"callout"].call(0,a,b,c,d,e)}}};A.VMLRenderer=F=function(){this.init.apply(this,
arguments)};F.prototype=z(ua.prototype,Na);$a=F}ua.prototype.measureSpanWidth=function(a,b){var c=B.createElement("span"),d;d=B.createTextNode(a);c.appendChild(d);L(c,b);this.box.appendChild(c);d=c.offsetWidth;Ra(c);return d};var Mb;if(fa)A.CanVGRenderer=F=function(){Ca="http://www.w3.org/1999/xhtml"},F.prototype.symbols={},Mb=function(){function a(){var a=b.length,d;for(d=0;d<a;d++)b[d]();b=[]}var b=[];return{push:function(c,d){b.length===0&&Rb(d,a);b.push(c)}}}(),$a=F;Ta.prototype={addLabel:function(){var a=
this.axis,b=a.options,c=a.chart,d=a.categories,e=a.names,f=this.pos,g=b.labels,h=a.tickPositions,i=f===h[0],j=f===h[h.length-1],e=d?p(d[f],e[f],f):f,d=this.label,h=h.info,k;a.isDatetimeAxis&&h&&(k=b.dateTimeLabelFormats[h.higherRanks[f]||h.unitName]);this.isFirst=i;this.isLast=j;b=a.labelFormatter.call({axis:a,chart:c,isFirst:i,isLast:j,dateTimeLabelFormat:k,value:a.isLog?ea(ia(e)):e});q(d)?d&&d.attr({text:b}):(this.labelLength=(this.label=d=q(b)&&g.enabled?c.renderer.text(b,0,0,g.useHTML).css(z(g.style)).add(a.labelGroup):
null)&&d.getBBox().width,this.rotation=0)},getLabelSize:function(){return this.label?this.label.getBBox()[this.axis.horiz?"height":"width"]:0},handleOverflow:function(a){var b=this.axis,c=a.x,d=b.chart.chartWidth,e=b.chart.spacing,f=p(b.labelLeft,C(b.pos,e[3])),e=p(b.labelRight,u(b.pos+b.len,d-e[1])),g=this.label,h=this.rotation,i={left:0,center:0.5,right:1}[b.labelAlign],j=g.getBBox().width,k=b.slotWidth,l=1,m,n={};if(h)h<0&&c-i*j<f?m=r(c/X(h*ha)-f):h>0&&c+i*j>e&&(m=r((d-c)/X(h*ha)));else if(d=c+
(1-i)*j,c-i*j<f?k=a.x+k*(1-i)-f:d>e&&(k=e-a.x+k*i,l=-1),k=C(b.slotWidth,k),k<b.slotWidth&&b.labelAlign==="center"&&(a.x+=l*(b.slotWidth-k-i*(b.slotWidth-C(j,k)))),j>k||b.autoRotation&&g.styles.width)m=k;if(m){n.width=m;if(!b.options.labels.style.textOverflow)n.textOverflow="ellipsis";g.css(n)}},getPosition:function(a,b,c,d){var e=this.axis,f=e.chart,g=d&&f.oldChartHeight||f.chartHeight;return{x:a?e.translate(b+c,null,null,d)+e.transB:e.left+e.offset+(e.opposite?(d&&f.oldChartWidth||f.chartWidth)-
e.right-e.left:0),y:a?g-e.bottom+e.offset-(e.opposite?e.height:0):g-e.translate(b+c,null,null,d)-e.transB}},getLabelPosition:function(a,b,c,d,e,f,g,h){var i=this.axis,j=i.transA,k=i.reversed,l=i.staggerLines,m=i.tickRotCorr||{x:0,y:0},c=p(e.y,m.y+(i.side===2?8:-(c.getBBox().height/2))),a=a+e.x+m.x-(f&&d?f*j*(k?-1:1):0),b=b+c-(f&&!d?f*j*(k?1:-1):0);l&&(b+=g/(h||1)%l*(i.labelOffset/l));return{x:a,y:r(b)}},getMarkPath:function(a,b,c,d,e,f){return f.crispLine(["M",a,b,"L",a+(e?0:-c),b+(e?c:0)],d)},render:function(a,
b,c){var d=this.axis,e=d.options,f=d.chart.renderer,g=d.horiz,h=this.type,i=this.label,j=this.pos,k=e.labels,l=this.gridLine,m=h?h+"Grid":"grid",n=h?h+"Tick":"tick",v=e[m+"LineWidth"],s=e[m+"LineColor"],o=e[m+"LineDashStyle"],N=e[n+"Length"],m=e[n+"Width"]||0,t=e[n+"Color"],w=e[n+"Position"],n=this.mark,ga=k.step,q=!0,u=d.tickmarkOffset,r=this.getPosition(g,j,u,b),x=r.x,r=r.y,z=g&&x===d.pos+d.len||!g&&r===d.pos?-1:1,c=p(c,1);this.isActive=!0;if(v){j=d.getPlotLinePath(j+u,v*z,b,!0);if(l===y){l={stroke:s,
"stroke-width":v};if(o)l.dashstyle=o;if(!h)l.zIndex=1;if(b)l.opacity=0;this.gridLine=l=v?f.path(j).attr(l).add(d.gridGroup):null}if(!b&&l&&j)l[this.isNew?"attr":"animate"]({d:j,opacity:c})}if(m&&N)w==="inside"&&(N=-N),d.opposite&&(N=-N),h=this.getMarkPath(x,r,N,m*z,g,f),n?n.animate({d:h,opacity:c}):this.mark=f.path(h).attr({stroke:t,"stroke-width":m,opacity:c}).add(d.axisGroup);if(i&&!isNaN(x))i.xy=r=this.getLabelPosition(x,r,i,g,k,u,a,ga),this.isFirst&&!this.isLast&&!p(e.showFirstLabel,1)||this.isLast&&
!this.isFirst&&!p(e.showLastLabel,1)?q=!1:g&&!d.isRadial&&!k.step&&!k.rotation&&!b&&c!==0&&this.handleOverflow(r),ga&&a%ga&&(q=!1),q&&!isNaN(r.y)?(r.opacity=c,i[this.isNew?"attr":"animate"](r),this.isNew=!1):i.attr("y",-9999)},destroy:function(){Qa(this,this.axis)}};A.PlotLineOrBand=function(a,b){this.axis=a;if(b)this.options=b,this.id=b.id};A.PlotLineOrBand.prototype={render:function(){var a=this,b=a.axis,c=b.horiz,d=a.options,e=d.label,f=a.label,g=d.width,h=d.to,i=d.from,j=q(i)&&q(h),k=d.value,
l=d.dashStyle,m=a.svgElem,n=[],v,s=d.color,p=d.zIndex,o=d.events,t={},w=b.chart.renderer;b.isLog&&(i=Ea(i),h=Ea(h),k=Ea(k));if(g){if(n=b.getPlotLinePath(k,g),t={stroke:s,"stroke-width":g},l)t.dashstyle=l}else if(j){n=b.getPlotBandPath(i,h,d);if(s)t.fill=s;if(d.borderWidth)t.stroke=d.borderColor,t["stroke-width"]=d.borderWidth}else return;if(q(p))t.zIndex=p;if(m)if(n)m.animate({d:n},null,m.onGetPath);else{if(m.hide(),m.onGetPath=function(){m.show()},f)a.label=f=f.destroy()}else if(n&&n.length&&(a.svgElem=
m=w.path(n).attr(t).add(),o))for(v in d=function(b){m.on(b,function(c){o[b].apply(a,[c])})},o)d(v);if(e&&q(e.text)&&n&&n.length&&b.width>0&&b.height>0){e=z({align:c&&j&&"center",x:c?!j&&4:10,verticalAlign:!c&&j&&"middle",y:c?j?16:10:j?6:-4,rotation:c&&!j&&90},e);if(!f){t={align:e.textAlign||e.align,rotation:e.rotation};if(q(p))t.zIndex=p;a.label=f=w.text(e.text,0,0,e.useHTML).attr(t).css(e.style).add()}b=[n[1],n[4],j?n[6]:n[1]];j=[n[2],n[5],j?n[7]:n[2]];n=Pa(b);c=Pa(j);f.align(e,!1,{x:n,y:c,width:Fa(b)-
n,height:Fa(j)-c});f.show()}else f&&f.hide();return a},destroy:function(){ja(this.axis.plotLinesAndBands,this);delete this.axis;Qa(this)}};var va=A.Axis=function(){this.init.apply(this,arguments)};va.prototype={defaultOptions:{dateTimeLabelFormats:{millisecond:"%H:%M:%S.%L",second:"%H:%M:%S",minute:"%H:%M",hour:"%H:%M",day:"%e. %b",week:"%e. %b",month:"%b '%y",year:"%Y"},endOnTick:!1,gridLineColor:"#D8D8D8",labels:{enabled:!0,style:{color:"#606060",cursor:"default",fontSize:"11px"},x:0,y:15},lineColor:"#C0D0E0",
lineWidth:1,minPadding:0.01,maxPadding:0.01,minorGridLineColor:"#E0E0E0",minorGridLineWidth:1,minorTickColor:"#A0A0A0",minorTickLength:2,minorTickPosition:"outside",startOfWeek:1,startOnTick:!1,tickColor:"#C0D0E0",tickLength:10,tickmarkPlacement:"between",tickPixelInterval:100,tickPosition:"outside",tickWidth:1,title:{align:"middle",style:{color:"#707070"}},type:"linear"},defaultYAxisOptions:{endOnTick:!0,gridLineWidth:1,tickPixelInterval:72,showLastLabel:!0,labels:{x:-8,y:3},lineWidth:0,maxPadding:0.05,
minPadding:0.05,startOnTick:!0,tickWidth:0,title:{rotation:270,text:"Values"},stackLabels:{enabled:!1,formatter:function(){return A.numberFormat(this.total,-1)},style:z(ba.line.dataLabels.style,{color:"#000000"})}},defaultLeftAxisOptions:{labels:{x:-15,y:null},title:{rotation:270}},defaultRightAxisOptions:{labels:{x:15,y:null},title:{rotation:90}},defaultBottomAxisOptions:{labels:{autoRotation:[-45],x:0,y:null},title:{rotation:0}},defaultTopAxisOptions:{labels:{autoRotation:[-45],x:0,y:-15},title:{rotation:0}},
init:function(a,b){var c=b.isX;this.horiz=a.inverted?!c:c;this.coll=(this.isXAxis=c)?"xAxis":"yAxis";this.opposite=b.opposite;this.side=b.side||(this.horiz?this.opposite?0:2:this.opposite?1:3);this.setOptions(b);var d=this.options,e=d.type;this.labelFormatter=d.labels.formatter||this.defaultLabelFormatter;this.userOptions=b;this.minPixelPadding=0;this.chart=a;this.reversed=d.reversed;this.zoomEnabled=d.zoomEnabled!==!1;this.categories=d.categories||e==="category";this.names=this.names||[];this.isLog=
e==="logarithmic";this.isDatetimeAxis=e==="datetime";this.isLinked=q(d.linkedTo);this.ticks={};this.labelEdge=[];this.minorTicks={};this.plotLinesAndBands=[];this.alternateBands={};this.len=0;this.minRange=this.userMinRange=d.minRange||d.maxZoom;this.range=d.range;this.offset=d.offset||0;this.stacks={};this.oldStacks={};this.min=this.max=null;this.crosshair=p(d.crosshair,sa(a.options.tooltip.crosshairs)[c?0:1],!1);var f,d=this.options.events;Ma(this,a.axes)===-1&&(c&&!this.isColorAxis?a.axes.splice(a.xAxis.length,
0,this):a.axes.push(this),a[this.coll].push(this));this.series=this.series||[];if(a.inverted&&c&&this.reversed===y)this.reversed=!0;this.removePlotLine=this.removePlotBand=this.removePlotBandOrLine;for(f in d)H(this,f,d[f]);if(this.isLog)this.val2lin=Ea,this.lin2val=ia},setOptions:function(a){this.options=z(this.defaultOptions,this.isXAxis?{}:this.defaultYAxisOptions,[this.defaultTopAxisOptions,this.defaultRightAxisOptions,this.defaultBottomAxisOptions,this.defaultLeftAxisOptions][this.side],z(T[this.coll],
a))},defaultLabelFormatter:function(){var a=this.axis,b=this.value,c=a.categories,d=this.dateTimeLabelFormat,e=T.lang.numericSymbols,f=e&&e.length,g,h=a.options.labels.format,a=a.isLog?b:a.tickInterval;if(h)g=Ja(h,this);else if(c)g=b;else if(d)g=Oa(d,b);else if(f&&a>=1E3)for(;f--&&g===y;)c=Math.pow(1E3,f+1),a>=c&&b*10%c===0&&e[f]!==null&&(g=A.numberFormat(b/c,-1)+e[f]);g===y&&(g=O(b)>=1E4?A.numberFormat(b,-1):A.numberFormat(b,-1,y,""));return g},getSeriesExtremes:function(){var a=this,b=a.chart;a.hasVisibleSeries=
!1;a.dataMin=a.dataMax=a.ignoreMinPadding=a.ignoreMaxPadding=null;a.buildStacks&&a.buildStacks();o(a.series,function(c){if(c.visible||!b.options.chart.ignoreHiddenSeries){var d;d=c.options.threshold;var e;a.hasVisibleSeries=!0;a.isLog&&d<=0&&(d=null);if(a.isXAxis){if(d=c.xData,d.length)a.dataMin=C(p(a.dataMin,d[0]),Pa(d)),a.dataMax=u(p(a.dataMax,d[0]),Fa(d))}else{c.getExtremes();e=c.dataMax;c=c.dataMin;if(q(c)&&q(e))a.dataMin=C(p(a.dataMin,c),c),a.dataMax=u(p(a.dataMax,e),e);if(q(d))if(a.dataMin>=
d)a.dataMin=d,a.ignoreMinPadding=!0;else if(a.dataMax<d)a.dataMax=d,a.ignoreMaxPadding=!0}}})},translate:function(a,b,c,d,e,f){var g=this.linkedParent||this,h=1,i=0,j=d?g.oldTransA:g.transA,d=d?g.oldMin:g.min,k=g.minPixelPadding,e=(g.doPostTranslate||g.isLog&&e)&&g.lin2val;if(!j)j=g.transA;if(c)h*=-1,i=g.len;g.reversed&&(h*=-1,i-=h*(g.sector||g.len));b?(a=a*h+i,a-=k,a=a/j+d,e&&(a=g.lin2val(a))):(e&&(a=g.val2lin(a)),f==="between"&&(f=0.5),a=h*(a-d)*j+i+h*k+(ra(f)?j*f*g.pointRange:0));return a},toPixels:function(a,
b){return this.translate(a,!1,!this.horiz,null,!0)+(b?0:this.pos)},toValue:function(a,b){return this.translate(a-(b?0:this.pos),!0,!this.horiz,null,!0)},getPlotLinePath:function(a,b,c,d,e){var f=this.chart,g=this.left,h=this.top,i,j,k=c&&f.oldChartHeight||f.chartHeight,l=c&&f.oldChartWidth||f.chartWidth,m;i=this.transB;var n=function(a,b,c){if(a<b||a>c)d?a=C(u(b,a),c):m=!0;return a},e=p(e,this.translate(a,null,null,c)),a=c=r(e+i);i=j=r(k-e-i);isNaN(e)?m=!0:this.horiz?(i=h,j=k-this.bottom,a=c=n(a,
g,g+this.width)):(a=g,c=l-this.right,i=j=n(i,h,h+this.height));return m&&!d?null:f.renderer.crispLine(["M",a,i,"L",c,j],b||1)},getLinearTickPositions:function(a,b,c){var d,e=ea(V(b/a)*a),f=ea(ta(c/a)*a),g=[];if(b===c&&ra(b))return[b];for(b=e;b<=f;){g.push(b);b=ea(b+a);if(b===d)break;d=b}return g},getMinorTickPositions:function(){var a=this.options,b=this.tickPositions,c=this.minorTickInterval,d=[],e,f=this.min;e=this.max;var g=e-f;if(g&&g/c<this.len/3)if(this.isLog){a=b.length;for(e=1;e<a;e++)d=d.concat(this.getLogTickPositions(c,
b[e-1],b[e],!0))}else if(this.isDatetimeAxis&&a.minorTickInterval==="auto")d=d.concat(this.getTimeTicks(this.normalizeTimeTickInterval(c),f,e,a.startOfWeek));else for(b=f+(b[0]-f)%c;b<=e;b+=c)d.push(b);this.trimTicks(d);return d},adjustForMinRange:function(){var a=this.options,b=this.min,c=this.max,d,e=this.dataMax-this.dataMin>=this.minRange,f,g,h,i,j;if(this.isXAxis&&this.minRange===y&&!this.isLog)q(a.min)||q(a.max)?this.minRange=null:(o(this.series,function(a){i=a.xData;for(g=j=a.xIncrement?1:
i.length-1;g>0;g--)if(h=i[g]-i[g-1],f===y||h<f)f=h}),this.minRange=C(f*5,this.dataMax-this.dataMin));if(c-b<this.minRange){var k=this.minRange;d=(k-c+b)/2;d=[b-d,p(a.min,b-d)];if(e)d[2]=this.dataMin;b=Fa(d);c=[b+k,p(a.max,b+k)];if(e)c[2]=this.dataMax;c=Pa(c);c-b<k&&(d[0]=c-k,d[1]=p(a.min,c-k),b=Fa(d))}this.min=b;this.max=c},setAxisTranslation:function(a){var b=this,c=b.max-b.min,d=b.axisPointRange||0,e,f=0,g=0,h=b.linkedParent,i=!!b.categories,j=b.transA,k=b.isXAxis;if(k||i||d)if(h?(f=h.minPointOffset,
g=h.pointRangePadding):o(b.series,function(a){var h=i?1:k?a.pointRange:b.axisPointRange||0,j=a.options.pointPlacement,v=a.closestPointRange;h>c&&(h=0);d=u(d,h);b.single||(f=u(f,Da(j)?0:h/2),g=u(g,j==="on"?0:h));!a.noSharedTooltip&&q(v)&&(e=q(e)?C(e,v):v)}),h=b.ordinalSlope&&e?b.ordinalSlope/e:1,b.minPointOffset=f*=h,b.pointRangePadding=g*=h,b.pointRange=C(d,c),k)b.closestPointRange=e;if(a)b.oldTransA=j;b.translationSlope=b.transA=j=b.len/(c+g||1);b.transB=b.horiz?b.left:b.bottom;b.minPixelPadding=
j*f},setTickInterval:function(a){var b=this,c=b.chart,d=b.options,e=b.isLog,f=b.isDatetimeAxis,g=b.isXAxis,h=b.isLinked,i=d.maxPadding,j=d.minPadding,k=d.tickInterval,l=d.tickPixelInterval,m=b.categories;!f&&!m&&!h&&this.getTickAmount();h?(b.linkedParent=c[b.coll][d.linkedTo],c=b.linkedParent.getExtremes(),b.min=p(c.min,c.dataMin),b.max=p(c.max,c.dataMax),d.type!==b.linkedParent.options.type&&la(11,1)):(b.min=p(b.userMin,d.min,b.dataMin),b.max=p(b.userMax,d.max,b.dataMax));if(e)!a&&C(b.min,p(b.dataMin,
b.min))<=0&&la(10,1),b.min=ea(Ea(b.min)),b.max=ea(Ea(b.max));if(b.range&&q(b.max))b.userMin=b.min=u(b.min,b.max-b.range),b.userMax=b.max,b.range=null;b.beforePadding&&b.beforePadding();b.adjustForMinRange();if(!m&&!b.axisPointRange&&!b.usePercentage&&!h&&q(b.min)&&q(b.max)&&(c=b.max-b.min)){if(!q(d.min)&&!q(b.userMin)&&j&&(b.dataMin<0||!b.ignoreMinPadding))b.min-=c*j;if(!q(d.max)&&!q(b.userMax)&&i&&(b.dataMax>0||!b.ignoreMaxPadding))b.max+=c*i}if(ra(d.floor))b.min=u(b.min,d.floor);if(ra(d.ceiling))b.max=
C(b.max,d.ceiling);b.tickInterval=b.min===b.max||b.min===void 0||b.max===void 0?1:h&&!k&&l===b.linkedParent.options.tickPixelInterval?k=b.linkedParent.tickInterval:p(k,this.tickAmount?(b.max-b.min)/u(this.tickAmount-1,1):void 0,m?1:(b.max-b.min)*l/u(b.len,l));g&&!a&&o(b.series,function(a){a.processData(b.min!==b.oldMin||b.max!==b.oldMax)});b.setAxisTranslation(!0);b.beforeSetTickPositions&&b.beforeSetTickPositions();if(b.postProcessTickInterval)b.tickInterval=b.postProcessTickInterval(b.tickInterval);
if(b.pointRange)b.tickInterval=u(b.pointRange,b.tickInterval);a=p(d.minTickInterval,b.isDatetimeAxis&&b.closestPointRange);if(!k&&b.tickInterval<a)b.tickInterval=a;if(!f&&!e&&!k)b.tickInterval=qb(b.tickInterval,null,pb(b.tickInterval),p(d.allowDecimals,!(b.tickInterval>0.5&&b.tickInterval<5&&b.max>1E3&&b.max<9999)),!!this.tickAmount);if(!this.tickAmount&&this.len)b.tickInterval=b.unsquish();this.setTickPositions()},setTickPositions:function(){var a=this.options,b,c=a.tickPositions,d=a.tickPositioner,
e=a.startOnTick,f=a.endOnTick,g;this.tickmarkOffset=this.categories&&a.tickmarkPlacement==="between"&&this.tickInterval===1?0.5:0;this.minorTickInterval=a.minorTickInterval==="auto"&&this.tickInterval?this.tickInterval/5:a.minorTickInterval;this.tickPositions=b=c&&c.slice();if(!b&&(this.tickPositions=b=this.isDatetimeAxis?this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval,a.units),this.min,this.max,a.startOfWeek,this.ordinalPositions,this.closestPointRange,!0):this.isLog?this.getLogTickPositions(this.tickInterval,
this.min,this.max):this.getLinearTickPositions(this.tickInterval,this.min,this.max),d&&(d=d.apply(this,[this.min,this.max]))))this.tickPositions=b=d;if(!this.isLinked)this.trimTicks(b,e,f),this.min===this.max&&q(this.min)&&!this.tickAmount&&(g=!0,this.min-=0.5,this.max+=0.5),this.single=g,!c&&!d&&this.adjustTickAmount()},trimTicks:function(a,b,c){var d=a[0],e=a[a.length-1],f=this.minPointOffset||0;b?this.min=d:this.min-f>d&&a.shift();c?this.max=e:this.max+f<e&&a.pop();a.length===0&&q(d)&&a.push((e+
d)/2)},getTickAmount:function(){var a={},b,c=this.options,d=c.tickAmount,e=c.tickPixelInterval;!q(c.tickInterval)&&this.len<e&&!this.isRadial&&!this.isLog&&c.startOnTick&&c.endOnTick&&(d=2);!d&&this.chart.options.chart.alignTicks!==!1&&c.alignTicks!==!1&&(o(this.chart[this.coll],function(c){var d=c.options,e=c.horiz,d=[e?d.left:d.top,e?d.width:d.height,d.pane].join(",");a[d]?c.series.length&&(b=!0):a[d]=1}),b&&(d=ta(this.len/e)+1));if(d<4)this.finalTickAmt=d,d=5;this.tickAmount=d},adjustTickAmount:function(){var a=
this.tickInterval,b=this.tickPositions,c=this.tickAmount,d=this.finalTickAmt,e=b&&b.length;if(e<c){for(;b.length<c;)b.push(ea(b[b.length-1]+a));this.transA*=(e-1)/(c-1);this.max=b[b.length-1]}else e>c&&(this.tickInterval*=2,this.setTickPositions());if(q(d)){for(a=c=b.length;a--;)(d===3&&a%2===1||d<=2&&a>0&&a<c-1)&&b.splice(a,1);this.finalTickAmt=y}},setScale:function(){var a=this.stacks,b,c,d,e;this.oldMin=this.min;this.oldMax=this.max;this.oldAxisLength=this.len;this.setAxisSize();e=this.len!==this.oldAxisLength;
o(this.series,function(a){if(a.isDirtyData||a.isDirty||a.xAxis.isDirty)d=!0});if(e||d||this.isLinked||this.forceRedraw||this.userMin!==this.oldUserMin||this.userMax!==this.oldUserMax){if(!this.isXAxis)for(b in a)for(c in a[b])a[b][c].total=null,a[b][c].cum=0;this.forceRedraw=!1;this.getSeriesExtremes();this.setTickInterval();this.oldUserMin=this.userMin;this.oldUserMax=this.userMax;if(!this.isDirty)this.isDirty=e||this.min!==this.oldMin||this.max!==this.oldMax}else if(!this.isXAxis){if(this.oldStacks)a=
this.stacks=this.oldStacks;for(b in a)for(c in a[b])a[b][c].cum=a[b][c].total}},setExtremes:function(a,b,c,d,e){var f=this,g=f.chart,c=p(c,!0);o(f.series,function(a){delete a.kdTree});e=x(e,{min:a,max:b});I(f,"setExtremes",e,function(){f.userMin=a;f.userMax=b;f.eventArgs=e;f.isDirtyExtremes=!0;c&&g.redraw(d)})},zoom:function(a,b){var c=this.dataMin,d=this.dataMax,e=this.options;this.allowZoomOutside||(q(c)&&a<=C(c,p(e.min,c))&&(a=y),q(d)&&b>=u(d,p(e.max,d))&&(b=y));this.displayBtn=a!==y||b!==y;this.setExtremes(a,
b,!1,y,{trigger:"zoom"});return!0},setAxisSize:function(){var a=this.chart,b=this.options,c=b.offsetLeft||0,d=this.horiz,e=p(b.width,a.plotWidth-c+(b.offsetRight||0)),f=p(b.height,a.plotHeight),g=p(b.top,a.plotTop),b=p(b.left,a.plotLeft+c),c=/%$/;c.test(f)&&(f=parseFloat(f)/100*a.plotHeight);c.test(g)&&(g=parseFloat(g)/100*a.plotHeight+a.plotTop);this.left=b;this.top=g;this.width=e;this.height=f;this.bottom=a.chartHeight-f-g;this.right=a.chartWidth-e-b;this.len=u(d?e:f,0);this.pos=d?b:g},getExtremes:function(){var a=
this.isLog;return{min:a?ea(ia(this.min)):this.min,max:a?ea(ia(this.max)):this.max,dataMin:this.dataMin,dataMax:this.dataMax,userMin:this.userMin,userMax:this.userMax}},getThreshold:function(a){var b=this.isLog,c=b?ia(this.min):this.min,b=b?ia(this.max):this.max;a===null?a=b<0?b:c:c>a?a=c:b<a&&(a=b);return this.translate(a,0,1,0,1)},autoLabelAlign:function(a){a=(p(a,0)-this.side*90+720)%360;return a>15&&a<165?"right":a>195&&a<345?"left":"center"},unsquish:function(){var a=this.ticks,b=this.options.labels,
c=this.horiz,d=this.tickInterval,e=d,f=this.len/(((this.categories?1:0)+this.max-this.min)/d),g,h=b.rotation,i=this.chart.renderer.fontMetrics(b.style.fontSize,a[0]&&a[0].label),j,k=Number.MAX_VALUE,l,m=function(a){a/=f||1;a=a>1?ta(a):1;return a*d};c?(l=q(h)?[h]:f<p(b.autoRotationLimit,80)&&!b.staggerLines&&!b.step&&b.autoRotation)&&o(l,function(a){var b;if(a===h||a&&a>=-90&&a<=90)j=m(O(i.h/aa(ha*a))),b=j+O(a/360),b<k&&(k=b,g=a,e=j)}):e=m(i.h);this.autoRotation=l;this.labelRotation=g;return e},renderUnsquish:function(){var a=
this.chart,b=a.renderer,c=this.tickPositions,d=this.ticks,e=this.options.labels,f=this.horiz,g=a.margin,h=this.categories?c.length:c.length-1,i=this.slotWidth=f&&!e.step&&!e.rotation&&(this.staggerLines||1)*a.plotWidth/h||!f&&(g[3]&&g[3]-a.spacing[3]||a.chartWidth*0.33),j=u(1,r(i-2*(e.padding||5))),k={},g=b.fontMetrics(e.style.fontSize,d[0]&&d[0].label),h=e.style.textOverflow,l,m=0;if(!Da(e.rotation))k.rotation=e.rotation;if(this.autoRotation)o(c,function(a){if((a=d[a])&&a.labelLength>m)m=a.labelLength}),
m>j&&m>g.h?k.rotation=this.labelRotation:this.labelRotation=0;else if(i&&(l={width:j+"px"},!h)){l.textOverflow="clip";for(i=c.length;!f&&i--;)if(j=c[i],j=d[j].label)if(j.styles.textOverflow==="ellipsis"&&j.css({textOverflow:"clip"}),j.getBBox().height>this.len/c.length-(g.h-g.f))j.specCss={textOverflow:"ellipsis"}}if(k.rotation&&(l={width:(m>a.chartHeight*0.5?a.chartHeight*0.33:a.chartHeight)+"px"},!h))l.textOverflow="ellipsis";this.labelAlign=k.align=e.align||this.autoLabelAlign(this.labelRotation);
o(c,function(a){var b=(a=d[a])&&a.label;if(b)l&&b.css(z(l,b.specCss)),delete b.specCss,b.attr(k),a.rotation=k.rotation});this.tickRotCorr=b.rotCorr(g.b,this.labelRotation||0,this.side===2)},hasData:function(){return this.hasVisibleSeries||q(this.min)&&q(this.max)&&!!this.tickPositions},getOffset:function(){var a=this,b=a.chart,c=b.renderer,d=a.options,e=a.tickPositions,f=a.ticks,g=a.horiz,h=a.side,i=b.inverted?[1,0,3,2][h]:h,j,k,l=0,m,n=0,v=d.title,s=d.labels,S=0,N=b.axisOffset,b=b.clipOffset,t=[-1,
1,1,-1][h],w;j=a.hasData();a.showAxis=k=j||p(d.showEmpty,!0);a.staggerLines=a.horiz&&s.staggerLines;if(!a.axisGroup)a.gridGroup=c.g("grid").attr({zIndex:d.gridZIndex||1}).add(),a.axisGroup=c.g("axis").attr({zIndex:d.zIndex||2}).add(),a.labelGroup=c.g("axis-labels").attr({zIndex:s.zIndex||7}).addClass("highcharts-"+a.coll.toLowerCase()+"-labels").add();if(j||a.isLinked){if(o(e,function(b){f[b]?f[b].addLabel():f[b]=new Ta(a,b)}),a.renderUnsquish(),o(e,function(b){if(h===0||h===2||{1:"left",3:"right"}[h]===
a.labelAlign)S=u(f[b].getLabelSize(),S)}),a.staggerLines)S*=a.staggerLines,a.labelOffset=S}else for(w in f)f[w].destroy(),delete f[w];if(v&&v.text&&v.enabled!==!1){if(!a.axisTitle)a.axisTitle=c.text(v.text,0,0,v.useHTML).attr({zIndex:7,rotation:v.rotation||0,align:v.textAlign||{low:"left",middle:"center",high:"right"}[v.align]}).addClass("highcharts-"+this.coll.toLowerCase()+"-title").css(v.style).add(a.axisGroup),a.axisTitle.isNew=!0;if(k)l=a.axisTitle.getBBox()[g?"height":"width"],m=v.offset,n=
q(m)?0:p(v.margin,g?5:10);a.axisTitle[k?"show":"hide"]()}a.offset=t*p(d.offset,N[h]);a.tickRotCorr=a.tickRotCorr||{x:0,y:0};c=h===2?a.tickRotCorr.y:0;g=S+n+(S&&t*(g?p(s.y,a.tickRotCorr.y+8):s.x)-c);a.axisTitleMargin=p(m,g);N[h]=u(N[h],a.axisTitleMargin+l+t*a.offset,g);l=V(d.lineWidth/2)*2;d.offset&&(l=u(0,l-d.offset));b[i]=u(b[i],l)},getLinePath:function(a){var b=this.chart,c=this.opposite,d=this.offset,e=this.horiz,f=this.left+(c?this.width:0)+d,d=b.chartHeight-this.bottom-(c?this.height:0)+d;c&&
(a*=-1);return b.renderer.crispLine(["M",e?this.left:f,e?d:this.top,"L",e?b.chartWidth-this.right:f,e?d:b.chartHeight-this.bottom],a)},getTitlePosition:function(){var a=this.horiz,b=this.left,c=this.top,d=this.len,e=this.options.title,f=a?b:c,g=this.opposite,h=this.offset,i=e.x||0,j=e.y||0,k=D(e.style.fontSize||12),d={low:f+(a?0:d),middle:f+d/2,high:f+(a?d:0)}[e.align],b=(a?c+this.height:b)+(a?1:-1)*(g?-1:1)*this.axisTitleMargin+(this.side===2?k:0);return{x:a?d+i:b+(g?this.width:0)+h+i,y:a?b+j-(g?
this.height:0)+h:d+j}},render:function(){var a=this,b=a.chart,c=b.renderer,d=a.options,e=a.isLog,f=a.isLinked,g=a.tickPositions,h=a.axisTitle,i=a.ticks,j=a.minorTicks,k=a.alternateBands,l=d.stackLabels,m=d.alternateGridColor,n=a.tickmarkOffset,v=d.lineWidth,s,p=b.hasRendered&&q(a.oldMin)&&!isNaN(a.oldMin),N=a.showAxis,t,w;a.labelEdge.length=0;a.overlap=!1;o([i,j,k],function(a){for(var b in a)a[b].isActive=!1});if(a.hasData()||f){a.minorTickInterval&&!a.categories&&o(a.getMinorTickPositions(),function(b){j[b]||
(j[b]=new Ta(a,b,"minor"));p&&j[b].isNew&&j[b].render(null,!0);j[b].render(null,!1,1)});if(g.length&&(o(g,function(b,c){if(!f||b>=a.min&&b<=a.max)i[b]||(i[b]=new Ta(a,b)),p&&i[b].isNew&&i[b].render(c,!0,0.1),i[b].render(c)}),n&&(a.min===0||a.single)))i[-1]||(i[-1]=new Ta(a,-1,null,!0)),i[-1].render(-1);m&&o(g,function(b,c){if(c%2===0&&b<a.max)k[b]||(k[b]=new A.PlotLineOrBand(a)),t=b+n,w=g[c+1]!==y?g[c+1]+n:a.max,k[b].options={from:e?ia(t):t,to:e?ia(w):w,color:m},k[b].render(),k[b].isActive=!0});if(!a._addedPlotLB)o((d.plotLines||
[]).concat(d.plotBands||[]),function(b){a.addPlotBandOrLine(b)}),a._addedPlotLB=!0}o([i,j,k],function(a){var c,d,e=[],f=za?za.duration||500:0,g=function(){for(d=e.length;d--;)a[e[d]]&&!a[e[d]].isActive&&(a[e[d]].destroy(),delete a[e[d]])};for(c in a)if(!a[c].isActive)a[c].render(c,!1,0),a[c].isActive=!1,e.push(c);a===k||!b.hasRendered||!f?g():f&&setTimeout(g,f)});if(v)s=a.getLinePath(v),a.axisLine?a.axisLine.animate({d:s}):a.axisLine=c.path(s).attr({stroke:d.lineColor,"stroke-width":v,zIndex:7}).add(a.axisGroup),
a.axisLine[N?"show":"hide"]();if(h&&N)h[h.isNew?"attr":"animate"](a.getTitlePosition()),h.isNew=!1;l&&l.enabled&&a.renderStackTotals();a.isDirty=!1},redraw:function(){this.render();o(this.plotLinesAndBands,function(a){a.render()});o(this.series,function(a){a.isDirty=!0})},destroy:function(a){var b=this,c=b.stacks,d,e=b.plotLinesAndBands;a||Z(b);for(d in c)Qa(c[d]),c[d]=null;o([b.ticks,b.minorTicks,b.alternateBands],function(a){Qa(a)});for(a=e.length;a--;)e[a].destroy();o("stackTotalGroup,axisLine,axisTitle,axisGroup,cross,gridGroup,labelGroup".split(","),
function(a){b[a]&&(b[a]=b[a].destroy())});this.cross&&this.cross.destroy()},drawCrosshair:function(a,b){var c,d=this.crosshair,e=d.animation;if(!this.crosshair||(q(b)||!p(this.crosshair.snap,!0))===!1||b&&b.series&&b.series[this.coll]!==this)this.hideCrosshair();else if(p(d.snap,!0)?q(b)&&(c=this.isXAxis?b.plotX:this.len-b.plotY):c=this.horiz?a.chartX-this.pos:this.len-a.chartY+this.pos,c=this.isRadial?this.getPlotLinePath(this.isXAxis?b.x:p(b.stackY,b.y))||null:this.getPlotLinePath(null,null,null,
null,c)||null,c===null)this.hideCrosshair();else if(this.cross)this.cross.attr({visibility:"visible"})[e?"animate":"attr"]({d:c},e);else{e=this.categories&&!this.isRadial;e={"stroke-width":d.width||(e?this.transA:1),stroke:d.color||(e?"rgba(155,200,255,0.2)":"#C0C0C0"),zIndex:d.zIndex||2};if(d.dashStyle)e.dashstyle=d.dashStyle;this.cross=this.chart.renderer.path(c).attr(e).add()}},hideCrosshair:function(){this.cross&&this.cross.hide()}};x(va.prototype,{getPlotBandPath:function(a,b){var c=this.getPlotLinePath(b,
null,null,!0),d=this.getPlotLinePath(a,null,null,!0);d&&c&&d.toString()!==c.toString()?d.push(c[4],c[5],c[1],c[2]):d=null;return d},addPlotBand:function(a){return this.addPlotBandOrLine(a,"plotBands")},addPlotLine:function(a){return this.addPlotBandOrLine(a,"plotLines")},addPlotBandOrLine:function(a,b){var c=(new A.PlotLineOrBand(this,a)).render(),d=this.userOptions;c&&(b&&(d[b]=d[b]||[],d[b].push(a)),this.plotLinesAndBands.push(c));return c},removePlotBandOrLine:function(a){for(var b=this.plotLinesAndBands,
c=this.options,d=this.userOptions,e=b.length;e--;)b[e].id===a&&b[e].destroy();o([c.plotLines||[],d.plotLines||[],c.plotBands||[],d.plotBands||[]],function(b){for(e=b.length;e--;)b[e].id===a&&ja(b,b[e])})}});va.prototype.getTimeTicks=function(a,b,c,d){var e=[],f={},g=T.global.useUTC,h,i=new Aa(b-Wa(b)),j=a.unitRange,k=a.count;if(q(b)){i[Eb](j>=E.second?0:k*V(i.getMilliseconds()/k));if(j>=E.second)i[Fb](j>=E.minute?0:k*V(i.getSeconds()/k));if(j>=E.minute)i[Gb](j>=E.hour?0:k*V(i[sb]()/k));if(j>=E.hour)i[Hb](j>=
E.day?0:k*V(i[tb]()/k));if(j>=E.day)i[vb](j>=E.month?1:k*V(i[Xa]()/k));j>=E.month&&(i[wb](j>=E.year?0:k*V(i[Ya]()/k)),h=i[Za]());j>=E.year&&(h-=h%k,i[xb](h));if(j===E.week)i[vb](i[Xa]()-i[ub]()+p(d,1));b=1;if(ob||eb)i=i.getTime(),i=new Aa(i+Wa(i));h=i[Za]();for(var d=i.getTime(),l=i[Ya](),m=i[Xa](),n=(E.day+(g?Wa(i):i.getTimezoneOffset()*6E4))%E.day;d<c;)e.push(d),j===E.year?d=gb(h+b*k,0):j===E.month?d=gb(h,l+b*k):!g&&(j===E.day||j===E.week)?d=gb(h,l,m+b*k*(j===E.day?1:7)):d+=j*k,b++;e.push(d);o(lb(e,
function(a){return j<=E.hour&&a%E.day===n}),function(a){f[a]="day"})}e.info=x(a,{higherRanks:f,totalRange:j*k});return e};va.prototype.normalizeTimeTickInterval=function(a,b){var c=b||[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",[1,2]],["week",[1,2]],["month",[1,2,3,4,6]],["year",null]],d=c[c.length-1],e=E[d[0]],f=d[1],g;for(g=0;g<c.length;g++)if(d=c[g],e=E[d[0]],f=d[1],c[g+1]&&a<=(e*f[f.length-1]+E[c[g+1][0]])/
2)break;e===E.year&&a<5*e&&(f=[1,2,5]);c=qb(a/e,f,d[0]==="year"?u(pb(a/e),1):1);return{unitRange:e,count:c,unitName:d[0]}};va.prototype.getLogTickPositions=function(a,b,c,d){var e=this.options,f=this.len,g=[];if(!d)this._minorAutoInterval=null;if(a>=0.5)a=r(a),g=this.getLinearTickPositions(a,b,c);else if(a>=0.08)for(var f=V(b),h,i,j,k,l,e=a>0.3?[1,2,4]:a>0.15?[1,2,4,6,8]:[1,2,3,4,5,6,7,8,9];f<c+1&&!l;f++){i=e.length;for(h=0;h<i&&!l;h++)j=Ea(ia(f)*e[h]),j>b&&(!d||k<=c)&&k!==y&&g.push(k),k>c&&(l=!0),
k=j}else if(b=ia(b),c=ia(c),a=e[d?"minorTickInterval":"tickInterval"],a=p(a==="auto"?null:a,this._minorAutoInterval,(c-b)*(e.tickPixelInterval/(d?5:1))/((d?f/this.tickPositions.length:f)||1)),a=qb(a,null,pb(a)),g=Ua(this.getLinearTickPositions(a,b,c),Ea),!d)this._minorAutoInterval=a/5;if(!d)this.tickInterval=a;return g};var Nb=A.Tooltip=function(){this.init.apply(this,arguments)};Nb.prototype={init:function(a,b){var c=b.borderWidth,d=b.style,e=D(d.padding);this.chart=a;this.options=b;this.crosshairs=
[];this.now={x:0,y:0};this.isHidden=!0;this.label=a.renderer.label("",0,0,b.shape||"callout",null,null,b.useHTML,null,"tooltip").attr({padding:e,fill:b.backgroundColor,"stroke-width":c,r:b.borderRadius,zIndex:8}).css(d).css({padding:0}).add().attr({y:-9999});fa||this.label.shadow(b.shadow);this.shared=b.shared},destroy:function(){if(this.label)this.label=this.label.destroy();clearTimeout(this.hideTimer);clearTimeout(this.tooltipTimeout)},move:function(a,b,c,d){var e=this,f=e.now,g=e.options.animation!==
!1&&!e.isHidden&&(O(a-f.x)>1||O(b-f.y)>1),h=e.followPointer||e.len>1;x(f,{x:g?(2*f.x+a)/3:a,y:g?(f.y+b)/2:b,anchorX:h?y:g?(2*f.anchorX+c)/3:c,anchorY:h?y:g?(f.anchorY+d)/2:d});e.label.attr(f);if(g)clearTimeout(this.tooltipTimeout),this.tooltipTimeout=setTimeout(function(){e&&e.move(a,b,c,d)},32)},hide:function(a){var b=this;clearTimeout(this.hideTimer);if(!this.isHidden)this.hideTimer=setTimeout(function(){b.label.fadeOut();b.isHidden=!0},p(a,this.options.hideDelay,500))},getAnchor:function(a,b){var c,
d=this.chart,e=d.inverted,f=d.plotTop,g=d.plotLeft,h=0,i=0,j,k,a=sa(a);c=a[0].tooltipPos;this.followPointer&&b&&(b.chartX===y&&(b=d.pointer.normalize(b)),c=[b.chartX-d.plotLeft,b.chartY-f]);c||(o(a,function(a){j=a.series.yAxis;k=a.series.xAxis;h+=a.plotX+(!e&&k?k.left-g:0);i+=(a.plotLow?(a.plotLow+a.plotHigh)/2:a.plotY)+(!e&&j?j.top-f:0)}),h/=a.length,i/=a.length,c=[e?d.plotWidth-i:h,this.shared&&!e&&a.length>1&&b?b.chartY-f:e?d.plotHeight-h:i]);return Ua(c,r)},getPosition:function(a,b,c){var d=this.chart,
e=this.distance,f={},g=c.h||0,h,i=["y",d.chartHeight,b,c.plotY+d.plotTop],j=["x",d.chartWidth,a,c.plotX+d.plotLeft],k=p(c.ttBelow,d.inverted&&!c.negative||!d.inverted&&c.negative),l=function(a,b,c,d){var h=c<d-e,i=d+e+c<b,j=d-e-c;d+=e;if(k&&i)f[a]=d;else if(!k&&h)f[a]=j;else if(h)f[a]=j-g<0?j:j-g;else if(i)f[a]=d+g+c>b?d:d+g;else return!1},m=function(a,b,c,d){if(d<e||d>b-e)return!1;else f[a]=d<c/2?1:d>b-c/2?b-c-2:d-c/2},n=function(a){var b=i;i=j;j=b;h=a},v=function(){l.apply(0,i)!==!1?m.apply(0,j)===
!1&&!h&&(n(!0),v()):h?f.x=f.y=0:(n(!0),v())};(d.inverted||this.len>1)&&n();v();return f},defaultFormatter:function(a){var b=this.points||sa(this),c;c=[a.tooltipFooterHeaderFormatter(b[0])];c=c.concat(a.bodyFormatter(b));c.push(a.tooltipFooterHeaderFormatter(b[0],!0));return c.join("")},refresh:function(a,b){var c=this.chart,d=this.label,e=this.options,f,g,h,i={},j,k=[];j=e.formatter||this.defaultFormatter;var i=c.hoverPoints,l,m=this.shared;clearTimeout(this.hideTimer);this.followPointer=sa(a)[0].series.tooltipOptions.followPointer;
h=this.getAnchor(a,b);f=h[0];g=h[1];m&&(!a.series||!a.series.noSharedTooltip)?(c.hoverPoints=a,i&&o(i,function(a){a.setState()}),o(a,function(a){a.setState("hover");k.push(a.getLabelConfig())}),i={x:a[0].category,y:a[0].y},i.points=k,this.len=k.length,a=a[0]):i=a.getLabelConfig();j=j.call(i,this);i=a.series;this.distance=p(i.tooltipOptions.distance,16);j===!1?this.hide():(this.isHidden&&(db(d),d.attr("opacity",1).show()),d.attr({text:j}),l=e.borderColor||a.color||i.color||"#606060",d.attr({stroke:l}),
this.updatePosition({plotX:f,plotY:g,negative:a.negative,ttBelow:a.ttBelow,h:h[2]||0}),this.isHidden=!1);I(c,"tooltipRefresh",{text:j,x:f+c.plotLeft,y:g+c.plotTop,borderColor:l})},updatePosition:function(a){var b=this.chart,c=this.label,c=(this.options.positioner||this.getPosition).call(this,c.width,c.height,a);this.move(r(c.x),r(c.y||0),a.plotX+b.plotLeft,a.plotY+b.plotTop)},getXDateFormat:function(a,b,c){var d,b=b.dateTimeLabelFormats,e=c&&c.closestPointRange,f,g={millisecond:15,second:12,minute:9,
hour:6,day:3},h,i="millisecond";if(e){h=Oa("%m-%d %H:%M:%S.%L",a.x);for(f in E){if(e===E.week&&+Oa("%w",a.x)===c.options.startOfWeek&&h.substr(6)==="00:00:00.000"){f="week";break}else if(E[f]>e){f=i;break}else if(g[f]&&h.substr(g[f])!=="01-01 00:00:00.000".substr(g[f]))break;f!=="week"&&(i=f)}f&&(d=b[f])}else d=b.day;return d||b.year},tooltipFooterHeaderFormatter:function(a,b){var c=b?"footer":"header",d=a.series,e=d.tooltipOptions,f=e.xDateFormat,g=d.xAxis,h=g&&g.options.type==="datetime"&&ra(a.key),
c=e[c+"Format"];h&&!f&&(f=this.getXDateFormat(a,e,g));h&&f&&(c=c.replace("{point.key}","{point.key:"+f+"}"));return Ja(c,{point:a,series:d})},bodyFormatter:function(a){return Ua(a,function(a){var c=a.series.tooltipOptions;return(c.pointFormatter||a.point.tooltipFormatter).call(a.point,c.pointFormat)})}};var pa;ab=B.documentElement.ontouchstart!==y;var Va=A.Pointer=function(a,b){this.init(a,b)};Va.prototype={init:function(a,b){var c=b.chart,d=c.events,e=fa?"":c.zoomType,c=a.inverted,f;this.options=
b;this.chart=a;this.zoomX=f=/x/.test(e);this.zoomY=e=/y/.test(e);this.zoomHor=f&&!c||e&&c;this.zoomVert=e&&!c||f&&c;this.hasZoom=f||e;this.runChartClick=d&&!!d.click;this.pinchDown=[];this.lastValidTouch={};if(A.Tooltip&&b.tooltip.enabled)a.tooltip=new Nb(a,b.tooltip),this.followTouchMove=p(b.tooltip.followTouchMove,!0);this.setDOMEvents()},normalize:function(a,b){var c,d,a=a||window.event,a=Tb(a);if(!a.target)a.target=a.srcElement;d=a.touches?a.touches.length?a.touches.item(0):a.changedTouches[0]:
a;if(!b)this.chartPosition=b=Sb(this.chart.container);d.pageX===y?(c=u(a.x,a.clientX-b.left),d=a.y):(c=d.pageX-b.left,d=d.pageY-b.top);return x(a,{chartX:r(c),chartY:r(d)})},getCoordinates:function(a){var b={xAxis:[],yAxis:[]};o(this.chart.axes,function(c){b[c.isXAxis?"xAxis":"yAxis"].push({axis:c,value:c.toValue(a[c.horiz?"chartX":"chartY"])})});return b},runPointActions:function(a){var b=this.chart,c=b.series,d=b.tooltip,e=d?d.shared:!1,f=b.hoverPoint,g=b.hoverSeries,h,i=b.chartWidth,j,k,l=[],m,
n;if(!e&&!g)for(h=0;h<c.length;h++)if(c[h].directTouch||!c[h].options.stickyTracking)c=[];!e&&g&&g.directTouch&&f?m=f:(o(c,function(b){j=b.noSharedTooltip&&e;k=!e&&b.directTouch;b.visible&&!j&&!k&&p(b.options.enableMouseTracking,!0)&&(n=b.searchPoint(a,!j&&b.kdDimensions===1))&&l.push(n)}),o(l,function(a){if(a&&typeof a.dist==="number"&&a.dist<i)i=a.dist,m=a}));if(m&&(m!==this.prevKDPoint||d&&d.isHidden)){if(e&&!m.series.noSharedTooltip){for(h=l.length;h--;)(l[h].clientX!==m.clientX||l[h].series.noSharedTooltip)&&
l.splice(h,1);l.length&&d&&d.refresh(l,a);o(l,function(b){if(b!==m)b.onMouseOver(a)});(g&&g.directTouch&&f||m).onMouseOver(a)}else d&&d.refresh(m,a),m.onMouseOver(a);this.prevKDPoint=m}else c=g&&g.tooltipOptions.followPointer,d&&c&&!d.isHidden&&(c=d.getAnchor([{}],a),d.updatePosition({plotX:c[0],plotY:c[1]}));if(d&&!this._onDocumentMouseMove)this._onDocumentMouseMove=function(a){if(Y[pa])Y[pa].pointer.onDocumentMouseMove(a)},H(B,"mousemove",this._onDocumentMouseMove);o(b.axes,function(b){b.drawCrosshair(a,
p(m,f))})},reset:function(a,b){var c=this.chart,d=c.hoverSeries,e=c.hoverPoint,f=c.hoverPoints,g=c.tooltip,h=g&&g.shared?f:e;(a=a&&g&&h)&&sa(h)[0].plotX===y&&(a=!1);if(a)g.refresh(h),e&&(e.setState(e.state,!0),o(c.axes,function(a){p(a.options.crosshair&&a.options.crosshair.snap,!0)?a.drawCrosshair(null,e):a.hideCrosshair()}));else{if(e)e.onMouseOut();f&&o(f,function(a){a.setState()});if(d)d.onMouseOut();g&&g.hide(b);if(this._onDocumentMouseMove)Z(B,"mousemove",this._onDocumentMouseMove),this._onDocumentMouseMove=
null;o(c.axes,function(a){a.hideCrosshair()});this.hoverX=c.hoverPoints=c.hoverPoint=null}},scaleGroups:function(a,b){var c=this.chart,d;o(c.series,function(e){d=a||e.getPlotBox();e.xAxis&&e.xAxis.zoomEnabled&&(e.group.attr(d),e.markerGroup&&(e.markerGroup.attr(d),e.markerGroup.clip(b?c.clipRect:null)),e.dataLabelsGroup&&e.dataLabelsGroup.attr(d))});c.clipRect.attr(b||c.clipBox)},dragStart:function(a){var b=this.chart;b.mouseIsDown=a.type;b.cancelClick=!1;b.mouseDownX=this.mouseDownX=a.chartX;b.mouseDownY=
this.mouseDownY=a.chartY},drag:function(a){var b=this.chart,c=b.options.chart,d=a.chartX,e=a.chartY,f=this.zoomHor,g=this.zoomVert,h=b.plotLeft,i=b.plotTop,j=b.plotWidth,k=b.plotHeight,l,m=this.mouseDownX,n=this.mouseDownY,v=c.panKey&&a[c.panKey+"Key"];d<h?d=h:d>h+j&&(d=h+j);e<i?e=i:e>i+k&&(e=i+k);this.hasDragged=Math.sqrt(Math.pow(m-d,2)+Math.pow(n-e,2));if(this.hasDragged>10){l=b.isInsidePlot(m-h,n-i);if(b.hasCartesianSeries&&(this.zoomX||this.zoomY)&&l&&!v&&!this.selectionMarker)this.selectionMarker=
b.renderer.rect(h,i,f?1:j,g?1:k,0).attr({fill:c.selectionMarkerFill||"rgba(69,114,167,0.25)",zIndex:7}).add();this.selectionMarker&&f&&(d-=m,this.selectionMarker.attr({width:O(d),x:(d>0?0:d)+m}));this.selectionMarker&&g&&(d=e-n,this.selectionMarker.attr({height:O(d),y:(d>0?0:d)+n}));l&&!this.selectionMarker&&c.panning&&b.pan(a,c.panning)}},drop:function(a){var b=this,c=this.chart,d=this.hasPinched;if(this.selectionMarker){var e={xAxis:[],yAxis:[],originalEvent:a.originalEvent||a},f=this.selectionMarker,
g=f.attr?f.attr("x"):f.x,h=f.attr?f.attr("y"):f.y,i=f.attr?f.attr("width"):f.width,j=f.attr?f.attr("height"):f.height,k;if(this.hasDragged||d)o(c.axes,function(c){if(c.zoomEnabled&&q(c.min)&&(d||b[{xAxis:"zoomX",yAxis:"zoomY"}[c.coll]])){var f=c.horiz,n=a.type==="touchend"?c.minPixelPadding:0,v=c.toValue((f?g:h)+n),f=c.toValue((f?g+i:h+j)-n);e[c.coll].push({axis:c,min:C(v,f),max:u(v,f)});k=!0}}),k&&I(c,"selection",e,function(a){c.zoom(x(a,d?{animation:!1}:null))});this.selectionMarker=this.selectionMarker.destroy();
d&&this.scaleGroups()}if(c)L(c.container,{cursor:c._cursor}),c.cancelClick=this.hasDragged>10,c.mouseIsDown=this.hasDragged=this.hasPinched=!1,this.pinchDown=[]},onContainerMouseDown:function(a){a=this.normalize(a);a.preventDefault&&a.preventDefault();this.dragStart(a)},onDocumentMouseUp:function(a){Y[pa]&&Y[pa].pointer.drop(a)},onDocumentMouseMove:function(a){var b=this.chart,c=this.chartPosition,a=this.normalize(a,c);c&&!this.inClass(a.target,"highcharts-tracker")&&!b.isInsidePlot(a.chartX-b.plotLeft,
a.chartY-b.plotTop)&&this.reset()},onContainerMouseLeave:function(){var a=Y[pa];if(a)a.pointer.reset(),a.pointer.chartPosition=null},onContainerMouseMove:function(a){var b=this.chart;pa=b.index;a=this.normalize(a);a.returnValue=!1;b.mouseIsDown==="mousedown"&&this.drag(a);(this.inClass(a.target,"highcharts-tracker")||b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop))&&!b.openMenu&&this.runPointActions(a)},inClass:function(a,b){for(var c;a;){if(c=J(a,"class"))if(c.indexOf(b)!==-1)return!0;else if(c.indexOf("highcharts-container")!==
-1)return!1;a=a.parentNode}},onTrackerMouseOut:function(a){var b=this.chart.hoverSeries,c=(a=a.relatedTarget||a.toElement)&&a.point&&a.point.series;if(b&&!b.options.stickyTracking&&!this.inClass(a,"highcharts-tooltip")&&c!==b)b.onMouseOut()},onContainerClick:function(a){var b=this.chart,c=b.hoverPoint,d=b.plotLeft,e=b.plotTop,a=this.normalize(a);a.originalEvent=a;b.cancelClick||(c&&this.inClass(a.target,"highcharts-tracker")?(I(c.series,"click",x(a,{point:c})),b.hoverPoint&&c.firePointEvent("click",
a)):(x(a,this.getCoordinates(a)),b.isInsidePlot(a.chartX-d,a.chartY-e)&&I(b,"click",a)))},setDOMEvents:function(){var a=this,b=a.chart.container;b.onmousedown=function(b){a.onContainerMouseDown(b)};b.onmousemove=function(b){a.onContainerMouseMove(b)};b.onclick=function(b){a.onContainerClick(b)};H(b,"mouseleave",a.onContainerMouseLeave);bb===1&&H(B,"mouseup",a.onDocumentMouseUp);if(ab)b.ontouchstart=function(b){a.onContainerTouchStart(b)},b.ontouchmove=function(b){a.onContainerTouchMove(b)},bb===1&&
H(B,"touchend",a.onDocumentTouchEnd)},destroy:function(){var a;Z(this.chart.container,"mouseleave",this.onContainerMouseLeave);bb||(Z(B,"mouseup",this.onDocumentMouseUp),Z(B,"touchend",this.onDocumentTouchEnd));clearInterval(this.tooltipTimeout);for(a in this)this[a]=null}};x(A.Pointer.prototype,{pinchTranslate:function(a,b,c,d,e,f){(this.zoomHor||this.pinchHor)&&this.pinchTranslateDirection(!0,a,b,c,d,e,f);(this.zoomVert||this.pinchVert)&&this.pinchTranslateDirection(!1,a,b,c,d,e,f)},pinchTranslateDirection:function(a,
b,c,d,e,f,g,h){var i=this.chart,j=a?"x":"y",k=a?"X":"Y",l="chart"+k,m=a?"width":"height",n=i["plot"+(a?"Left":"Top")],v,s,p=h||1,o=i.inverted,t=i.bounds[a?"h":"v"],w=b.length===1,q=b[0][l],u=c[0][l],r=!w&&b[1][l],y=!w&&c[1][l],x,c=function(){!w&&O(q-r)>20&&(p=h||O(u-y)/O(q-r));s=(n-u)/p+q;v=i["plot"+(a?"Width":"Height")]/p};c();b=s;b<t.min?(b=t.min,x=!0):b+v>t.max&&(b=t.max-v,x=!0);x?(u-=0.8*(u-g[j][0]),w||(y-=0.8*(y-g[j][1])),c()):g[j]=[u,y];o||(f[j]=s-n,f[m]=v);f=o?1/p:p;e[m]=v;e[j]=b;d[o?a?"scaleY":
"scaleX":"scale"+k]=p;d["translate"+k]=f*n+(u-f*q)},pinch:function(a){var b=this,c=b.chart,d=b.pinchDown,e=a.touches,f=e.length,g=b.lastValidTouch,h=b.hasZoom,i=b.selectionMarker,j={},k=f===1&&(b.inClass(a.target,"highcharts-tracker")&&c.runTrackerClick||b.runChartClick),l={};if(f>1)b.initiated=!0;h&&b.initiated&&!k&&a.preventDefault();Ua(e,function(a){return b.normalize(a)});if(a.type==="touchstart")o(e,function(a,b){d[b]={chartX:a.chartX,chartY:a.chartY}}),g.x=[d[0].chartX,d[1]&&d[1].chartX],g.y=
[d[0].chartY,d[1]&&d[1].chartY],o(c.axes,function(a){if(a.zoomEnabled){var b=c.bounds[a.horiz?"h":"v"],d=a.minPixelPadding,e=a.toPixels(p(a.options.min,a.dataMin)),f=a.toPixels(p(a.options.max,a.dataMax)),g=C(e,f),e=u(e,f);b.min=C(a.pos,g-d);b.max=u(a.pos+a.len,e+d)}}),b.res=!0;else if(d.length){if(!i)b.selectionMarker=i=x({destroy:na},c.plotBox);b.pinchTranslate(d,e,j,i,l,g);b.hasPinched=h;b.scaleGroups(j,l);if(!h&&b.followTouchMove&&f===1)this.runPointActions(b.normalize(a));else if(b.res)b.res=
!1,this.reset(!1,0)}},touch:function(a,b){var c=this.chart;pa=c.index;a.touches.length===1?(a=this.normalize(a),c.isInsidePlot(a.chartX-c.plotLeft,a.chartY-c.plotTop)&&!c.openMenu?(b&&this.runPointActions(a),this.pinch(a)):b&&this.reset()):a.touches.length===2&&this.pinch(a)},onContainerTouchStart:function(a){this.touch(a,!0)},onContainerTouchMove:function(a){this.touch(a)},onDocumentTouchEnd:function(a){Y[pa]&&Y[pa].pointer.drop(a)}});if(K.PointerEvent||K.MSPointerEvent){var wa={},Bb=!!K.PointerEvent,
Xb=function(){var a,b=[];b.item=function(a){return this[a]};for(a in wa)wa.hasOwnProperty(a)&&b.push({pageX:wa[a].pageX,pageY:wa[a].pageY,target:wa[a].target});return b},Cb=function(a,b,c,d){a=a.originalEvent||a;if((a.pointerType==="touch"||a.pointerType===a.MSPOINTER_TYPE_TOUCH)&&Y[pa])d(a),d=Y[pa].pointer,d[b]({type:c,target:a.currentTarget,preventDefault:na,touches:Xb()})};x(Va.prototype,{onContainerPointerDown:function(a){Cb(a,"onContainerTouchStart","touchstart",function(a){wa[a.pointerId]={pageX:a.pageX,
pageY:a.pageY,target:a.currentTarget}})},onContainerPointerMove:function(a){Cb(a,"onContainerTouchMove","touchmove",function(a){wa[a.pointerId]={pageX:a.pageX,pageY:a.pageY};if(!wa[a.pointerId].target)wa[a.pointerId].target=a.currentTarget})},onDocumentPointerUp:function(a){Cb(a,"onDocumentTouchEnd","touchend",function(a){delete wa[a.pointerId]})},batchMSEvents:function(a){a(this.chart.container,Bb?"pointerdown":"MSPointerDown",this.onContainerPointerDown);a(this.chart.container,Bb?"pointermove":
"MSPointerMove",this.onContainerPointerMove);a(B,Bb?"pointerup":"MSPointerUp",this.onDocumentPointerUp)}});cb(Va.prototype,"init",function(a,b,c){a.call(this,b,c);this.hasZoom&&L(b.container,{"-ms-touch-action":P,"touch-action":P})});cb(Va.prototype,"setDOMEvents",function(a){a.apply(this);(this.hasZoom||this.followTouchMove)&&this.batchMSEvents(H)});cb(Va.prototype,"destroy",function(a){this.batchMSEvents(Z);a.call(this)})}var nb=A.Legend=function(a,b){this.init(a,b)};nb.prototype={init:function(a,
b){var c=this,d=b.itemStyle,e=b.itemMarginTop||0;this.options=b;if(b.enabled)c.itemStyle=d,c.itemHiddenStyle=z(d,b.itemHiddenStyle),c.itemMarginTop=e,c.padding=d=p(b.padding,8),c.initialItemX=d,c.initialItemY=d-5,c.maxItemWidth=0,c.chart=a,c.itemHeight=0,c.symbolWidth=p(b.symbolWidth,16),c.pages=[],c.render(),H(c.chart,"endResize",function(){c.positionCheckboxes()})},colorizeItem:function(a,b){var c=this.options,d=a.legendItem,e=a.legendLine,f=a.legendSymbol,g=this.itemHiddenStyle.color,c=b?c.itemStyle.color:
g,h=b?a.legendColor||a.color||"#CCC":g,g=a.options&&a.options.marker,i={fill:h},j;d&&d.css({fill:c,color:c});e&&e.attr({stroke:h});if(f){if(g&&f.isMarker)for(j in i.stroke=h,g=a.convertAttribs(g),g)d=g[j],d!==y&&(i[j]=d);f.attr(i)}},positionItem:function(a){var b=this.options,c=b.symbolPadding,b=!b.rtl,d=a._legendItemPos,e=d[0],d=d[1],f=a.checkbox;(a=a.legendGroup)&&a.element&&a.translate(b?e:this.legendWidth-e-2*c-4,d);if(f)f.x=e,f.y=d},destroyItem:function(a){var b=a.checkbox;o(["legendItem","legendLine",
"legendSymbol","legendGroup"],function(b){a[b]&&(a[b]=a[b].destroy())});b&&Ra(a.checkbox)},destroy:function(){var a=this.group,b=this.box;if(b)this.box=b.destroy();if(a)this.group=a.destroy()},positionCheckboxes:function(a){var b=this.group.alignAttr,c,d=this.clipHeight||this.legendHeight;if(b)c=b.translateY,o(this.allItems,function(e){var f=e.checkbox,g;f&&(g=c+f.y+(a||0)+3,L(f,{left:b.translateX+e.checkboxOffset+f.x-20+"px",top:g+"px",display:g>c-6&&g<c+d-6?"":P}))})},renderTitle:function(){var a=
this.padding,b=this.options.title,c=0;if(b.text){if(!this.title)this.title=this.chart.renderer.label(b.text,a-3,a-4,null,null,null,null,null,"legend-title").attr({zIndex:1}).css(b.style).add(this.group);a=this.title.getBBox();c=a.height;this.offsetWidth=a.width;this.contentGroup.attr({translateY:c})}this.titleHeight=c},setText:function(a){var b=this.options;a.legendItem.attr({text:b.labelFormat?Ja(b.labelFormat,a):b.labelFormatter.call(a)})},renderItem:function(a){var b=this.chart,c=b.renderer,d=
this.options,e=d.layout==="horizontal",f=this.symbolWidth,g=d.symbolPadding,h=this.itemStyle,i=this.itemHiddenStyle,j=this.padding,k=e?p(d.itemDistance,20):0,l=!d.rtl,m=d.width,n=d.itemMarginBottom||0,v=this.itemMarginTop,s=this.initialItemX,o=a.legendItem,q=a.series&&a.series.drawLegendSymbol?a.series:a,t=q.options,t=this.createCheckboxForItem&&t&&t.showCheckbox,w=d.useHTML;if(!o){a.legendGroup=c.g("legend-item").attr({zIndex:1}).add(this.scrollGroup);a.legendItem=o=c.text("",l?f+g:-g,this.baseline||
0,w).css(z(a.visible?h:i)).attr({align:l?"left":"right",zIndex:2}).add(a.legendGroup);if(!this.baseline)this.fontMetrics=c.fontMetrics(h.fontSize,o),this.baseline=this.fontMetrics.f+3+v,o.attr("y",this.baseline);q.drawLegendSymbol(this,a);this.setItemEvents&&this.setItemEvents(a,o,w,h,i);this.colorizeItem(a,a.visible);t&&this.createCheckboxForItem(a)}this.setText(a);c=o.getBBox();f=a.checkboxOffset=d.itemWidth||a.legendItemWidth||f+g+c.width+k+(t?20:0);this.itemHeight=g=r(a.legendItemHeight||c.height);
if(e&&this.itemX-s+f>(m||b.chartWidth-2*j-s-d.x))this.itemX=s,this.itemY+=v+this.lastLineHeight+n,this.lastLineHeight=0;this.maxItemWidth=u(this.maxItemWidth,f);this.lastItemY=v+this.itemY+n;this.lastLineHeight=u(g,this.lastLineHeight);a._legendItemPos=[this.itemX,this.itemY];e?this.itemX+=f:(this.itemY+=v+g+n,this.lastLineHeight=g);this.offsetWidth=m||u((e?this.itemX-s-k:f)+j,this.offsetWidth)},getAllItems:function(){var a=[];o(this.chart.series,function(b){var c=b.options;if(p(c.showInLegend,!q(c.linkedTo)?
y:!1,!0))a=a.concat(b.legendItems||(c.legendType==="point"?b.data:b))});return a},adjustMargins:function(a,b){var c=this.chart,d=this.options,e=d.align[0]+d.verticalAlign[0]+d.layout[0];this.display&&!d.floating&&o([/(lth|ct|rth)/,/(rtv|rm|rbv)/,/(rbh|cb|lbh)/,/(lbv|lm|ltv)/],function(f,g){f.test(e)&&!q(a[g])&&(c[jb[g]]=u(c[jb[g]],c.legend[(g+1)%2?"legendHeight":"legendWidth"]+[1,-1,-1,1][g]*d[g%2?"x":"y"]+p(d.margin,12)+b[g]))})},render:function(){var a=this,b=a.chart,c=b.renderer,d=a.group,e,f,
g,h,i=a.box,j=a.options,k=a.padding,l=j.borderWidth,m=j.backgroundColor;a.itemX=a.initialItemX;a.itemY=a.initialItemY;a.offsetWidth=0;a.lastItemY=0;if(!d)a.group=d=c.g("legend").attr({zIndex:7}).add(),a.contentGroup=c.g().attr({zIndex:1}).add(d),a.scrollGroup=c.g().add(a.contentGroup);a.renderTitle();e=a.getAllItems();rb(e,function(a,b){return(a.options&&a.options.legendIndex||0)-(b.options&&b.options.legendIndex||0)});j.reversed&&e.reverse();a.allItems=e;a.display=f=!!e.length;a.lastLineHeight=0;
o(e,function(b){a.renderItem(b)});g=(j.width||a.offsetWidth)+k;h=a.lastItemY+a.lastLineHeight+a.titleHeight;h=a.handleOverflow(h);h+=k;if(l||m){if(i){if(g>0&&h>0)i[i.isNew?"attr":"animate"](i.crisp({width:g,height:h})),i.isNew=!1}else a.box=i=c.rect(0,0,g,h,j.borderRadius,l||0).attr({stroke:j.borderColor,"stroke-width":l||0,fill:m||P}).add(d).shadow(j.shadow),i.isNew=!0;i[f?"show":"hide"]()}a.legendWidth=g;a.legendHeight=h;o(e,function(b){a.positionItem(b)});f&&d.align(x({width:g,height:h},j),!0,
"spacingBox");b.isResizing||this.positionCheckboxes()},handleOverflow:function(a){var b=this,c=this.chart,d=c.renderer,e=this.options,f=e.y,f=c.spacingBox.height+(e.verticalAlign==="top"?-f:f)-this.padding,g=e.maxHeight,h,i=this.clipRect,j=e.navigation,k=p(j.animation,!0),l=j.arrowSize||12,m=this.nav,n=this.pages,v=this.padding,s,q=this.allItems,N=function(a){i.attr({height:a});if(b.contentGroup.div)b.contentGroup.div.style.clip="rect("+v+"px,9999px,"+(v+a)+"px,0)"};e.layout==="horizontal"&&(f/=2);
g&&(f=C(f,g));n.length=0;if(a>f){this.clipHeight=h=u(f-20-this.titleHeight-v,0);this.currentPage=p(this.currentPage,1);this.fullHeight=a;o(q,function(a,b){var c=a._legendItemPos[1],d=r(a.legendItem.getBBox().height),e=n.length;if(!e||c-n[e-1]>h&&(s||c)!==n[e-1])n.push(s||c),e++;b===q.length-1&&c+d-n[e-1]>h&&n.push(c);c!==s&&(s=c)});if(!i)i=b.clipRect=d.clipRect(0,v,9999,0),b.contentGroup.clip(i);N(h);if(!m)this.nav=m=d.g().attr({zIndex:1}).add(this.group),this.up=d.symbol("triangle",0,0,l,l).on("click",
function(){b.scroll(-1,k)}).add(m),this.pager=d.text("",15,10).css(j.style).add(m),this.down=d.symbol("triangle-down",0,0,l,l).on("click",function(){b.scroll(1,k)}).add(m);b.scroll(0);a=f}else if(m)N(c.chartHeight),m.hide(),this.scrollGroup.attr({translateY:1}),this.clipHeight=0;return a},scroll:function(a,b){var c=this.pages,d=c.length,e=this.currentPage+a,f=this.clipHeight,g=this.options.navigation,h=g.activeColor,g=g.inactiveColor,i=this.pager,j=this.padding;e>d&&(e=d);if(e>0)b!==y&&Sa(b,this.chart),
this.nav.attr({translateX:j,translateY:f+this.padding+7+this.titleHeight,visibility:"visible"}),this.up.attr({fill:e===1?g:h}).css({cursor:e===1?"default":"pointer"}),i.attr({text:e+"/"+d}),this.down.attr({x:18+this.pager.getBBox().width,fill:e===d?g:h}).css({cursor:e===d?"default":"pointer"}),c=-c[e-1]+this.initialItemY,this.scrollGroup.animate({translateY:c}),this.currentPage=e,this.positionCheckboxes(c)}};Na=A.LegendSymbolMixin={drawRectangle:function(a,b){var c=a.options.symbolHeight||a.fontMetrics.f;
b.legendSymbol=this.chart.renderer.rect(0,a.baseline-c+1,a.symbolWidth,c,a.options.symbolRadius||0).attr({zIndex:3}).add(b.legendGroup)},drawLineMarker:function(a){var b=this.options,c=b.marker,d;d=a.symbolWidth;var e=this.chart.renderer,f=this.legendGroup,a=a.baseline-r(a.fontMetrics.b*0.3),g;if(b.lineWidth){g={"stroke-width":b.lineWidth};if(b.dashStyle)g.dashstyle=b.dashStyle;this.legendLine=e.path(["M",0,a,"L",d,a]).attr(g).add(f)}if(c&&c.enabled!==!1)b=c.radius,this.legendSymbol=d=e.symbol(this.symbol,
d/2-b,a-b,2*b,2*b).add(f),d.isMarker=!0}};(/Trident\/7\.0/.test(Ba)||La)&&cb(nb.prototype,"positionItem",function(a,b){var c=this,d=function(){b._legendItemPos&&a.call(c,b)};d();setTimeout(d)});F=A.Chart=function(){this.init.apply(this,arguments)};F.prototype={callbacks:[],init:function(a,b){var c,d=a.series;a.series=null;c=z(T,a);c.series=a.series=d;this.userOptions=a;d=c.chart;this.margin=this.splashArray("margin",d);this.spacing=this.splashArray("spacing",d);var e=d.events;this.bounds={h:{},v:{}};
this.callback=b;this.isResizing=0;this.options=c;this.axes=[];this.series=[];this.hasCartesianSeries=d.showAxes;var f=this,g;f.index=Y.length;Y.push(f);bb++;d.reflow!==!1&&H(f,"load",function(){f.initReflow()});if(e)for(g in e)H(f,g,e[g]);f.xAxis=[];f.yAxis=[];f.animation=fa?!1:p(d.animation,!0);f.pointCount=f.colorCounter=f.symbolCounter=0;f.firstRender()},initSeries:function(a){var b=this.options.chart;(b=M[a.type||b.type||b.defaultSeriesType])||la(17,!0);b=new b;b.init(this,a);return b},isInsidePlot:function(a,
b,c){var d=c?b:a,a=c?a:b;return d>=0&&d<=this.plotWidth&&a>=0&&a<=this.plotHeight},redraw:function(a){var b=this.axes,c=this.series,d=this.pointer,e=this.legend,f=this.isDirtyLegend,g,h,i=this.hasCartesianSeries,j=this.isDirtyBox,k=c.length,l=k,m=this.renderer,n=m.isHidden(),p=[];Sa(a,this);n&&this.cloneRenderTo();for(this.layOutTitles();l--;)if(a=c[l],a.options.stacking&&(g=!0,a.isDirty)){h=!0;break}if(h)for(l=k;l--;)if(a=c[l],a.options.stacking)a.isDirty=!0;o(c,function(a){a.isDirty&&a.options.legendType===
"point"&&(a.updateTotals&&a.updateTotals(),f=!0)});if(f&&e.options.enabled)e.render(),this.isDirtyLegend=!1;g&&this.getStacks();if(i&&!this.isResizing)this.maxTicks=null,o(b,function(a){a.setScale()});this.getMargins();i&&(o(b,function(a){a.isDirty&&(j=!0)}),o(b,function(a){if(a.isDirtyExtremes)a.isDirtyExtremes=!1,p.push(function(){I(a,"afterSetExtremes",x(a.eventArgs,a.getExtremes()));delete a.eventArgs});(j||g)&&a.redraw()}));j&&this.drawChartBox();o(c,function(a){a.isDirty&&a.visible&&(!a.isCartesian||
a.xAxis)&&a.redraw()});d&&d.reset(!0);m.draw();I(this,"redraw");n&&this.cloneRenderTo(!0);o(p,function(a){a.call()})},get:function(a){var b=this.axes,c=this.series,d,e;for(d=0;d<b.length;d++)if(b[d].options.id===a)return b[d];for(d=0;d<c.length;d++)if(c[d].options.id===a)return c[d];for(d=0;d<c.length;d++){e=c[d].points||[];for(b=0;b<e.length;b++)if(e[b].id===a)return e[b]}return null},getAxes:function(){var a=this,b=this.options,c=b.xAxis=sa(b.xAxis||{}),b=b.yAxis=sa(b.yAxis||{});o(c,function(a,
b){a.index=b;a.isX=!0});o(b,function(a,b){a.index=b});c=c.concat(b);o(c,function(b){new va(a,b)})},getSelectedPoints:function(){var a=[];o(this.series,function(b){a=a.concat(lb(b.points||[],function(a){return a.selected}))});return a},getSelectedSeries:function(){return lb(this.series,function(a){return a.selected})},getStacks:function(){var a=this;o(a.yAxis,function(a){if(a.stacks&&a.hasVisibleSeries)a.oldStacks=a.stacks});o(a.series,function(b){if(b.options.stacking&&(b.visible===!0||a.options.chart.ignoreHiddenSeries===
!1))b.stackKey=b.type+p(b.options.stack,"")})},setTitle:function(a,b,c){var g;var d=this,e=d.options,f;f=e.title=z(e.title,a);g=e.subtitle=z(e.subtitle,b),e=g;o([["title",a,f],["subtitle",b,e]],function(a){var b=a[0],c=d[b],e=a[1],a=a[2];c&&e&&(d[b]=c=c.destroy());a&&a.text&&!c&&(d[b]=d.renderer.text(a.text,0,0,a.useHTML).attr({align:a.align,"class":"highcharts-"+b,zIndex:a.zIndex||4}).css(a.style).add())});d.layOutTitles(c)},layOutTitles:function(a){var b=0,c=this.title,d=this.subtitle,e=this.options,
f=e.title,e=e.subtitle,g=this.renderer,h=this.spacingBox.width-44;if(c&&(c.css({width:(f.width||h)+"px"}).align(x({y:g.fontMetrics(f.style.fontSize,c).b-3},f),!1,"spacingBox"),!f.floating&&!f.verticalAlign))b=c.getBBox().height;d&&(d.css({width:(e.width||h)+"px"}).align(x({y:b+(f.margin-13)+g.fontMetrics(f.style.fontSize,d).b},e),!1,"spacingBox"),!e.floating&&!e.verticalAlign&&(b=ta(b+d.getBBox().height)));c=this.titleOffset!==b;this.titleOffset=b;if(!this.isDirtyBox&&c)this.isDirtyBox=c,this.hasRendered&&
p(a,!0)&&this.isDirtyBox&&this.redraw()},getChartSize:function(){var a=this.options.chart,b=a.width,a=a.height,c=this.renderToClone||this.renderTo;if(!q(b))this.containerWidth=kb(c,"width");if(!q(a))this.containerHeight=kb(c,"height");this.chartWidth=u(0,b||this.containerWidth||600);this.chartHeight=u(0,p(a,this.containerHeight>19?this.containerHeight:400))},cloneRenderTo:function(a){var b=this.renderToClone,c=this.container;a?b&&(this.renderTo.appendChild(c),Ra(b),delete this.renderToClone):(c&&
c.parentNode===this.renderTo&&this.renderTo.removeChild(c),this.renderToClone=b=this.renderTo.cloneNode(0),L(b,{position:"absolute",top:"-9999px",display:"block"}),b.style.setProperty&&b.style.setProperty("display","block","important"),B.body.appendChild(b),c&&b.appendChild(c))},getContainer:function(){var a,b=this.options.chart,c,d,e;this.renderTo=a=b.renderTo;e="highcharts-"+yb++;if(Da(a))this.renderTo=a=B.getElementById(a);a||la(13,!0);c=D(J(a,"data-highcharts-chart"));!isNaN(c)&&Y[c]&&Y[c].hasRendered&&
Y[c].destroy();J(a,"data-highcharts-chart",this.index);a.innerHTML="";!b.skipClone&&!a.offsetWidth&&this.cloneRenderTo();this.getChartSize();c=this.chartWidth;d=this.chartHeight;this.container=a=$(Ka,{className:"highcharts-container"+(b.className?" "+b.className:""),id:e},x({position:"relative",overflow:"hidden",width:c+"px",height:d+"px",textAlign:"left",lineHeight:"normal",zIndex:0,"-webkit-tap-highlight-color":"rgba(0,0,0,0)"},b.style),this.renderToClone||a);this._cursor=a.style.cursor;this.renderer=
b.forExport?new ua(a,c,d,b.style,!0):new $a(a,c,d,b.style);fa&&this.renderer.create(this,a,c,d);this.renderer.chartIndex=this.index},getMargins:function(a){var b=this.spacing,c=this.margin,d=this.titleOffset;this.resetMargins();if(d&&!q(c[0]))this.plotTop=u(this.plotTop,d+this.options.title.margin+b[0]);this.legend.adjustMargins(c,b);this.extraBottomMargin&&(this.marginBottom+=this.extraBottomMargin);this.extraTopMargin&&(this.plotTop+=this.extraTopMargin);a||this.getAxisMargins()},getAxisMargins:function(){var a=
this,b=a.axisOffset=[0,0,0,0],c=a.margin;a.hasCartesianSeries&&o(a.axes,function(a){a.getOffset()});o(jb,function(d,e){q(c[e])||(a[d]+=b[e])});a.setChartSize()},reflow:function(a){var b=this,c=b.options.chart,d=b.renderTo,e=c.width||kb(d,"width"),f=c.height||kb(d,"height"),c=a?a.target:K,d=function(){if(b.container)b.setSize(e,f,!1),b.hasUserSize=null};if(!b.hasUserSize&&!b.isPrinting&&e&&f&&(c===K||c===B)){if(e!==b.containerWidth||f!==b.containerHeight)clearTimeout(b.reflowTimeout),a?b.reflowTimeout=
setTimeout(d,100):d();b.containerWidth=e;b.containerHeight=f}},initReflow:function(){var a=this,b=function(b){a.reflow(b)};H(K,"resize",b);H(a,"destroy",function(){Z(K,"resize",b)})},setSize:function(a,b,c){var d=this,e,f,g;d.isResizing+=1;g=function(){d&&I(d,"endResize",null,function(){d.isResizing-=1})};Sa(c,d);d.oldChartHeight=d.chartHeight;d.oldChartWidth=d.chartWidth;if(q(a))d.chartWidth=e=u(0,r(a)),d.hasUserSize=!!e;if(q(b))d.chartHeight=f=u(0,r(b));(za?mb:L)(d.container,{width:e+"px",height:f+
"px"},za);d.setChartSize(!0);d.renderer.setSize(e,f,c);d.maxTicks=null;o(d.axes,function(a){a.isDirty=!0;a.setScale()});o(d.series,function(a){a.isDirty=!0});d.isDirtyLegend=!0;d.isDirtyBox=!0;d.layOutTitles();d.getMargins();d.redraw(c);d.oldChartHeight=null;I(d,"resize");za===!1?g():setTimeout(g,za&&za.duration||500)},setChartSize:function(a){var b=this.inverted,c=this.renderer,d=this.chartWidth,e=this.chartHeight,f=this.options.chart,g=this.spacing,h=this.clipOffset,i,j,k,l;this.plotLeft=i=r(this.plotLeft);
this.plotTop=j=r(this.plotTop);this.plotWidth=k=u(0,r(d-i-this.marginRight));this.plotHeight=l=u(0,r(e-j-this.marginBottom));this.plotSizeX=b?l:k;this.plotSizeY=b?k:l;this.plotBorderWidth=f.plotBorderWidth||0;this.spacingBox=c.spacingBox={x:g[3],y:g[0],width:d-g[3]-g[1],height:e-g[0]-g[2]};this.plotBox=c.plotBox={x:i,y:j,width:k,height:l};d=2*V(this.plotBorderWidth/2);b=ta(u(d,h[3])/2);c=ta(u(d,h[0])/2);this.clipBox={x:b,y:c,width:V(this.plotSizeX-u(d,h[1])/2-b),height:u(0,V(this.plotSizeY-u(d,h[2])/
2-c))};a||o(this.axes,function(a){a.setAxisSize();a.setAxisTranslation()})},resetMargins:function(){var a=this;o(jb,function(b,c){a[b]=p(a.margin[c],a.spacing[c])});a.axisOffset=[0,0,0,0];a.clipOffset=[0,0,0,0]},drawChartBox:function(){var a=this.options.chart,b=this.renderer,c=this.chartWidth,d=this.chartHeight,e=this.chartBackground,f=this.plotBackground,g=this.plotBorder,h=this.plotBGImage,i=a.borderWidth||0,j=a.backgroundColor,k=a.plotBackgroundColor,l=a.plotBackgroundImage,m=a.plotBorderWidth||
0,n,p=this.plotLeft,o=this.plotTop,q=this.plotWidth,u=this.plotHeight,t=this.plotBox,w=this.clipRect,r=this.clipBox;n=i+(a.shadow?8:0);if(i||j)if(e)e.animate(e.crisp({width:c-n,height:d-n}));else{e={fill:j||P};if(i)e.stroke=a.borderColor,e["stroke-width"]=i;this.chartBackground=b.rect(n/2,n/2,c-n,d-n,a.borderRadius,i).attr(e).addClass("highcharts-background").add().shadow(a.shadow)}if(k)f?f.animate(t):this.plotBackground=b.rect(p,o,q,u,0).attr({fill:k}).add().shadow(a.plotShadow);if(l)h?h.animate(t):
this.plotBGImage=b.image(l,p,o,q,u).add();w?w.animate({width:r.width,height:r.height}):this.clipRect=b.clipRect(r);if(m)g?g.animate(g.crisp({x:p,y:o,width:q,height:u,strokeWidth:-m})):this.plotBorder=b.rect(p,o,q,u,0,-m).attr({stroke:a.plotBorderColor,"stroke-width":m,fill:P,zIndex:1}).add();this.isDirtyBox=!1},propFromSeries:function(){var a=this,b=a.options.chart,c,d=a.options.series,e,f;o(["inverted","angular","polar"],function(g){c=M[b.type||b.defaultSeriesType];f=a[g]||b[g]||c&&c.prototype[g];
for(e=d&&d.length;!f&&e--;)(c=M[d[e].type])&&c.prototype[g]&&(f=!0);a[g]=f})},linkSeries:function(){var a=this,b=a.series;o(b,function(a){a.linkedSeries.length=0});o(b,function(b){var d=b.options.linkedTo;if(Da(d)&&(d=d===":previous"?a.series[b.index-1]:a.get(d)))d.linkedSeries.push(b),b.linkedParent=d})},renderSeries:function(){o(this.series,function(a){a.translate();a.render()})},renderLabels:function(){var a=this,b=a.options.labels;b.items&&o(b.items,function(c){var d=x(b.style,c.style),e=D(d.left)+
a.plotLeft,f=D(d.top)+a.plotTop+12;delete d.left;delete d.top;a.renderer.text(c.html,e,f).attr({zIndex:2}).css(d).add()})},render:function(){var a=this.axes,b=this.renderer,c=this.options,d,e,f,g;this.setTitle();this.legend=new nb(this,c.legend);this.getStacks();this.getMargins(!0);this.setChartSize();d=this.plotWidth;e=this.plotHeight-=13;o(a,function(a){a.setScale()});this.getAxisMargins();f=d/this.plotWidth>1.1;g=e/this.plotHeight>1.1;if(f||g)this.maxTicks=null,o(a,function(a){(a.horiz&&f||!a.horiz&&
g)&&a.setTickInterval(!0)}),this.getMargins();this.drawChartBox();this.hasCartesianSeries&&o(a,function(a){a.render()});if(!this.seriesGroup)this.seriesGroup=b.g("series-group").attr({zIndex:3}).add();this.renderSeries();this.renderLabels();this.showCredits(c.credits);this.hasRendered=!0},showCredits:function(a){if(a.enabled&&!this.credits)this.credits=this.renderer.text(a.text,0,0).on("click",function(){if(a.href)location.href=a.href}).attr({align:a.position.align,zIndex:8}).css(a.style).add().align(a.position)},
destroy:function(){var a=this,b=a.axes,c=a.series,d=a.container,e,f=d&&d.parentNode;I(a,"destroy");Y[a.index]=y;bb--;a.renderTo.removeAttribute("data-highcharts-chart");Z(a);for(e=b.length;e--;)b[e]=b[e].destroy();for(e=c.length;e--;)c[e]=c[e].destroy();o("title,subtitle,chartBackground,plotBackground,plotBGImage,plotBorder,seriesGroup,clipRect,credits,pointer,scroller,rangeSelector,legend,resetZoomButton,tooltip,renderer".split(","),function(b){var c=a[b];c&&c.destroy&&(a[b]=c.destroy())});if(d)d.innerHTML=
"",Z(d),f&&Ra(d);for(e in a)delete a[e]},isReadyToRender:function(){var a=this;return!ca&&K==K.top&&B.readyState!=="complete"||fa&&!K.canvg?(fa?Mb.push(function(){a.firstRender()},a.options.global.canvasToolsURL):B.attachEvent("onreadystatechange",function(){B.detachEvent("onreadystatechange",a.firstRender);B.readyState==="complete"&&a.firstRender()}),!1):!0},firstRender:function(){var a=this,b=a.options,c=a.callback;if(a.isReadyToRender()){a.getContainer();I(a,"init");a.resetMargins();a.setChartSize();
a.propFromSeries();a.getAxes();o(b.series||[],function(b){a.initSeries(b)});a.linkSeries();I(a,"beforeRender");if(A.Pointer)a.pointer=new Va(a,b);a.render();a.renderer.draw();c&&c.apply(a,[a]);o(a.callbacks,function(b){a.index!==y&&b.apply(a,[a])});I(a,"load");a.cloneRenderTo(!0)}},splashArray:function(a,b){var c=b[a],c=da(c)?c:[c,c,c,c];return[p(b[a+"Top"],c[0]),p(b[a+"Right"],c[1]),p(b[a+"Bottom"],c[2]),p(b[a+"Left"],c[3])]}};var Yb=A.CenteredSeriesMixin={getCenter:function(){var a=this.options,
b=this.chart,c=2*(a.slicedOffset||0),d=b.plotWidth-2*c,b=b.plotHeight-2*c,e=a.center,e=[p(e[0],"50%"),p(e[1],"50%"),a.size||"100%",a.innerSize||0],f=C(d,b),g,h;for(g=0;g<4;++g)h=e[g],a=g<2||g===2&&/%$/.test(h),e[g]=(/%$/.test(h)?[d,b,f,e[2]][g]*parseFloat(h)/100:parseFloat(h))+(a?c:0);return e}},Ga=function(){};Ga.prototype={init:function(a,b,c){this.series=a;this.color=a.color;this.applyOptions(b,c);this.pointAttr={};if(a.options.colorByPoint&&(b=a.options.colors||a.chart.options.colors,this.color=
this.color||b[a.colorCounter++],a.colorCounter===b.length))a.colorCounter=0;a.chart.pointCount++;return this},applyOptions:function(a,b){var c=this.series,d=c.options.pointValKey||c.pointValKey,a=Ga.prototype.optionsToObject.call(this,a);x(this,a);this.options=this.options?x(this.options,a):a;if(d)this.y=this[d];if(this.x===y&&c)this.x=b===y?c.autoIncrement():b;return this},optionsToObject:function(a){var b={},c=this.series,d=c.options.keys,e=d||c.pointArrayMap||["y"],f=e.length,g=0,h=0;if(typeof a===
"number"||a===null)b[e[0]]=a;else if(Ha(a)){if(!d&&a.length>f){c=typeof a[0];if(c==="string")b.name=a[0];else if(c==="number")b.x=a[0];g++}for(;h<f;)b[e[h++]]=a[g++]}else if(typeof a==="object"){b=a;if(a.dataLabels)c._hasPointLabels=!0;if(a.marker)c._hasPointMarkers=!0}return b},destroy:function(){var a=this.series.chart,b=a.hoverPoints,c;a.pointCount--;if(b&&(this.setState(),ja(b,this),!b.length))a.hoverPoints=null;if(this===a.hoverPoint)this.onMouseOut();if(this.graphic||this.dataLabel)Z(this),
this.destroyElements();this.legendItem&&a.legend.destroyItem(this);for(c in this)this[c]=null},destroyElements:function(){for(var a="graphic,dataLabel,dataLabelUpper,group,connector,shadowGroup".split(","),b,c=6;c--;)b=a[c],this[b]&&(this[b]=this[b].destroy())},getLabelConfig:function(){return{x:this.category,y:this.y,key:this.name||this.category,series:this.series,point:this,percentage:this.percentage,total:this.total||this.stackTotal}},tooltipFormatter:function(a){var b=this.series,c=b.tooltipOptions,
d=p(c.valueDecimals,""),e=c.valuePrefix||"",f=c.valueSuffix||"";o(b.pointArrayMap||["y"],function(b){b="{point."+b;if(e||f)a=a.replace(b+"}",e+b+"}"+f);a=a.replace(b+"}",b+":,."+d+"f}")});return Ja(a,{point:this,series:this.series})},firePointEvent:function(a,b,c){var d=this,e=this.series.options;(e.point.events[a]||d.options&&d.options.events&&d.options.events[a])&&this.importEvents();a==="click"&&e.allowPointSelect&&(c=function(a){d.select&&d.select(null,a.ctrlKey||a.metaKey||a.shiftKey)});I(this,
a,b,c)}};var R=A.Series=function(){};R.prototype={isCartesian:!0,type:"line",pointClass:Ga,sorted:!0,requireSorting:!0,pointAttrToOptions:{stroke:"lineColor","stroke-width":"lineWidth",fill:"fillColor",r:"radius"},axisTypes:["xAxis","yAxis"],colorCounter:0,parallelArrays:["x","y"],init:function(a,b){var c=this,d,e,f=a.series,g=function(a,b){return p(a.options.index,a._i)-p(b.options.index,b._i)};c.chart=a;c.options=b=c.setOptions(b);c.linkedSeries=[];c.bindAxes();x(c,{name:b.name,state:"",pointAttr:{},
visible:b.visible!==!1,selected:b.selected===!0});if(fa)b.animation=!1;e=b.events;for(d in e)H(c,d,e[d]);if(e&&e.click||b.point&&b.point.events&&b.point.events.click||b.allowPointSelect)a.runTrackerClick=!0;c.getColor();c.getSymbol();o(c.parallelArrays,function(a){c[a+"Data"]=[]});c.setData(b.data,!1);if(c.isCartesian)a.hasCartesianSeries=!0;f.push(c);c._i=f.length-1;rb(f,g);this.yAxis&&rb(this.yAxis.series,g);o(f,function(a,b){a.index=b;a.name=a.name||"Series "+(b+1)})},bindAxes:function(){var a=
this,b=a.options,c=a.chart,d;o(a.axisTypes||[],function(e){o(c[e],function(c){d=c.options;if(b[e]===d.index||b[e]!==y&&b[e]===d.id||b[e]===y&&d.index===0)c.series.push(a),a[e]=c,c.isDirty=!0});!a[e]&&a.optionalAxis!==e&&la(18,!0)})},updateParallelArrays:function(a,b){var c=a.series,d=arguments;o(c.parallelArrays,typeof b==="number"?function(d){var f=d==="y"&&c.toYData?c.toYData(a):a[d];c[d+"Data"][b]=f}:function(a){Array.prototype[b].apply(c[a+"Data"],Array.prototype.slice.call(d,2))})},autoIncrement:function(){var a=
this.options,b=this.xIncrement,c,d=a.pointIntervalUnit,b=p(b,a.pointStart,0);this.pointInterval=c=p(this.pointInterval,a.pointInterval,1);if(d==="month"||d==="year")a=new Aa(b),a=d==="month"?+a[wb](a[Ya]()+c):+a[xb](a[Za]()+c),c=a-b;this.xIncrement=b+c;return b},getSegments:function(){var a=-1,b=[],c,d=this.points,e=d.length;if(e)if(this.options.connectNulls){for(c=e;c--;)d[c].y===null&&d.splice(c,1);d.length&&(b=[d])}else o(d,function(c,g){c.y===null?(g>a+1&&b.push(d.slice(a+1,g)),a=g):g===e-1&&
b.push(d.slice(a+1,g+1))});this.segments=b},setOptions:function(a){var b=this.chart,c=b.options.plotOptions,b=b.userOptions||{},d=b.plotOptions||{},e=c[this.type];this.userOptions=a;c=z(e,c.series,a);this.tooltipOptions=z(T.tooltip,T.plotOptions[this.type].tooltip,b.tooltip,d.series&&d.series.tooltip,d[this.type]&&d[this.type].tooltip,a.tooltip);e.marker===null&&delete c.marker;this.zoneAxis=c.zoneAxis;a=this.zones=(c.zones||[]).slice();if((c.negativeColor||c.negativeFillColor)&&!c.zones)a.push({value:c[this.zoneAxis+
"Threshold"]||c.threshold||0,color:c.negativeColor,fillColor:c.negativeFillColor});a.length&&q(a[a.length-1].value)&&a.push({color:this.color,fillColor:this.fillColor});return c},getCyclic:function(a,b,c){var d=this.userOptions,e="_"+a+"Index",f=a+"Counter";b||(q(d[e])?b=d[e]:(d[e]=b=this.chart[f]%c.length,this.chart[f]+=1),b=c[b]);this[a]=b},getColor:function(){this.options.colorByPoint||this.getCyclic("color",this.options.color||ba[this.type].color,this.chart.options.colors)},getSymbol:function(){var a=
this.options.marker;this.getCyclic("symbol",a.symbol,this.chart.options.symbols);if(/^url/.test(this.symbol))a.radius=0},drawLegendSymbol:Na.drawLineMarker,setData:function(a,b,c,d){var e=this,f=e.points,g=f&&f.length||0,h,i=e.options,j=e.chart,k=null,l=e.xAxis,m=l&&!!l.categories,n=i.turboThreshold,v=this.xData,s=this.yData,q=(h=e.pointArrayMap)&&h.length,a=a||[];h=a.length;b=p(b,!0);if(d!==!1&&h&&g===h&&!e.cropped&&!e.hasGroupedData&&e.visible)o(a,function(a,b){f[b].update&&f[b].update(a,!1,null,
!1)});else{e.xIncrement=null;e.pointRange=m?1:i.pointRange;e.colorCounter=0;o(this.parallelArrays,function(a){e[a+"Data"].length=0});if(n&&h>n){for(c=0;k===null&&c<h;)k=a[c],c++;if(ra(k)){m=p(i.pointStart,0);i=p(i.pointInterval,1);for(c=0;c<h;c++)v[c]=m,s[c]=a[c],m+=i;e.xIncrement=m}else if(Ha(k))if(q)for(c=0;c<h;c++)i=a[c],v[c]=i[0],s[c]=i.slice(1,q+1);else for(c=0;c<h;c++)i=a[c],v[c]=i[0],s[c]=i[1];else la(12)}else for(c=0;c<h;c++)if(a[c]!==y&&(i={series:e},e.pointClass.prototype.applyOptions.apply(i,
[a[c]]),e.updateParallelArrays(i,c),m&&i.name))l.names[i.x]=i.name;Da(s[0])&&la(14,!0);e.data=[];e.options.data=a;for(c=g;c--;)f[c]&&f[c].destroy&&f[c].destroy();if(l)l.minRange=l.userMinRange;e.isDirty=e.isDirtyData=j.isDirtyBox=!0;c=!1}b&&j.redraw(c)},processData:function(a){var b=this.xData,c=this.yData,d=b.length,e;e=0;var f,g,h=this.xAxis,i,j=this.options;i=j.cropThreshold;var k=this.isCartesian,l,m;if(k&&!this.isDirty&&!h.isDirty&&!this.yAxis.isDirty&&!a)return!1;if(h)a=h.getExtremes(),l=a.min,
m=a.max;if(k&&this.sorted&&(!i||d>i||this.forceCrop))if(b[d-1]<l||b[0]>m)b=[],c=[];else if(b[0]<l||b[d-1]>m)e=this.cropData(this.xData,this.yData,l,m),b=e.xData,c=e.yData,e=e.start,f=!0;for(i=b.length-1;i>=0;i--)d=b[i]-b[i-1],d>0&&(g===y||d<g)?g=d:d<0&&this.requireSorting&&la(15);this.cropped=f;this.cropStart=e;this.processedXData=b;this.processedYData=c;if(j.pointRange===null)this.pointRange=g||1;this.closestPointRange=g},cropData:function(a,b,c,d){var e=a.length,f=0,g=e,h=p(this.cropShoulder,1),
i;for(i=0;i<e;i++)if(a[i]>=c){f=u(0,i-h);break}for(;i<e;i++)if(a[i]>d){g=i+h;break}return{xData:a.slice(f,g),yData:b.slice(f,g),start:f,end:g}},generatePoints:function(){var a=this.options.data,b=this.data,c,d=this.processedXData,e=this.processedYData,f=this.pointClass,g=d.length,h=this.cropStart||0,i,j=this.hasGroupedData,k,l=[],m;if(!b&&!j)b=[],b.length=a.length,b=this.data=b;for(m=0;m<g;m++)i=h+m,j?l[m]=(new f).init(this,[d[m]].concat(sa(e[m]))):(b[i]?k=b[i]:a[i]!==y&&(b[i]=k=(new f).init(this,
a[i],d[m])),l[m]=k),l[m].index=i;if(b&&(g!==(c=b.length)||j))for(m=0;m<c;m++)if(m===h&&!j&&(m+=g),b[m])b[m].destroyElements(),b[m].plotX=y;this.data=b;this.points=l},getExtremes:function(a){var b=this.yAxis,c=this.processedXData,d,e=[],f=0;d=this.xAxis.getExtremes();var g=d.min,h=d.max,i,j,k,l,a=a||this.stackedYData||this.processedYData;d=a.length;for(l=0;l<d;l++)if(j=c[l],k=a[l],i=k!==null&&k!==y&&(!b.isLog||k.length||k>0),j=this.getExtremesFromAll||this.options.getExtremesFromAll||this.cropped||
(c[l+1]||j)>=g&&(c[l-1]||j)<=h,i&&j)if(i=k.length)for(;i--;)k[i]!==null&&(e[f++]=k[i]);else e[f++]=k;this.dataMin=Pa(e);this.dataMax=Fa(e)},translate:function(){this.processedXData||this.processData();this.generatePoints();for(var a=this.options,b=a.stacking,c=this.xAxis,d=c.categories,e=this.yAxis,f=this.points,g=f.length,h=!!this.modifyValue,i=a.pointPlacement,j=i==="between"||ra(i),k=a.threshold,l=a.startFromThreshold?k:0,m,n,v,o=Number.MAX_VALUE,a=0;a<g;a++){var r=f[a],x=r.x,t=r.y;n=r.low;var w=
b&&e.stacks[(this.negStacks&&t<(l?0:k)?"-":"")+this.stackKey];if(e.isLog&&t!==null&&t<=0)r.y=t=null,la(10);r.plotX=m=C(u(-1E5,c.translate(x,0,0,0,1,i,this.type==="flags")),1E5);if(b&&this.visible&&w&&w[x])w=w[x],t=w.points[this.index+","+a],n=t[0],t=t[1],n===l&&(n=p(k,e.min)),e.isLog&&n<=0&&(n=null),r.total=r.stackTotal=w.total,r.percentage=w.total&&r.y/w.total*100,r.stackY=t,w.setOffset(this.pointXOffset||0,this.barW||0);r.yBottom=q(n)?e.translate(n,0,1,0,1):null;h&&(t=this.modifyValue(t,r));r.plotY=
n=typeof t==="number"&&t!==Infinity?C(u(-1E5,e.translate(t,0,1,0,1)),1E5):y;r.isInside=n!==y&&n>=0&&n<=e.len&&m>=0&&m<=c.len;r.clientX=j?c.translate(x,0,0,0,1):m;r.negative=r.y<(k||0);r.category=d&&d[r.x]!==y?d[r.x]:r.x;a&&(o=C(o,O(m-v)));v=m}this.closestPointRangePx=o;this.getSegments()},setClip:function(a){var b=this.chart,c=b.renderer,d=b.inverted,e=this.clipBox,f=e||b.clipBox,g=this.sharedClipKey||["_sharedClip",a&&a.duration,a&&a.easing,f.height].join(","),h=b[g],i=b[g+"m"];if(!h){if(a)f.width=
0,b[g+"m"]=i=c.clipRect(-99,d?-b.plotLeft:-b.plotTop,99,d?b.chartWidth:b.chartHeight);b[g]=h=c.clipRect(f)}a&&(h.count+=1);if(this.options.clip!==!1)this.group.clip(a||e?h:b.clipRect),this.markerGroup.clip(i),this.sharedClipKey=g;a||(h.count-=1,h.count<=0&&g&&b[g]&&(e||(b[g]=b[g].destroy()),b[g+"m"]&&(b[g+"m"]=b[g+"m"].destroy())))},animate:function(a){var b=this.chart,c=this.options.animation,d;if(c&&!da(c))c=ba[this.type].animation;a?this.setClip(c):(d=this.sharedClipKey,(a=b[d])&&a.animate({width:b.plotSizeX},
c),b[d+"m"]&&b[d+"m"].animate({width:b.plotSizeX+99},c),this.animate=null)},afterAnimate:function(){this.setClip();I(this,"afterAnimate")},drawPoints:function(){var a,b=this.points,c=this.chart,d,e,f,g,h,i,j,k,l=this.options.marker,m=this.pointAttr[""],n,v,o,r=this.markerGroup,q=p(l.enabled,this.xAxis.isRadial,this.closestPointRangePx>2*l.radius);if(l.enabled!==!1||this._hasPointMarkers)for(f=b.length;f--;)if(g=b[f],d=V(g.plotX),e=g.plotY,k=g.graphic,n=g.marker||{},v=!!g.marker,a=q&&n.enabled===y||
n.enabled,o=g.isInside,a&&e!==y&&!isNaN(e)&&g.y!==null)if(a=g.pointAttr[g.selected?"select":""]||m,h=a.r,i=p(n.symbol,this.symbol),j=i.indexOf("url")===0,k)k[o?"show":"hide"](!0).animate(x({x:d-h,y:e-h},k.symbolName?{width:2*h,height:2*h}:{}));else{if(o&&(h>0||j))g.graphic=c.renderer.symbol(i,d-h,e-h,2*h,2*h,v?n:l).attr(a).add(r)}else if(k)g.graphic=k.destroy()},convertAttribs:function(a,b,c,d){var e=this.pointAttrToOptions,f,g,h={},a=a||{},b=b||{},c=c||{},d=d||{};for(f in e)g=e[f],h[f]=p(a[g],b[f],
c[f],d[f]);return h},getAttribs:function(){var a=this,b=a.options,c=ba[a.type].marker?b.marker:b,d=c.states,e=d.hover,f,g=a.color,h=a.options.negativeColor;f={stroke:g,fill:g};var i=a.points||[],j,k=[],l,m=a.pointAttrToOptions;l=a.hasPointSpecificOptions;var n=c.lineColor,p=c.fillColor;j=b.turboThreshold;var s=a.zones,r=a.zoneAxis||"y",u;b.marker?(e.radius=e.radius||c.radius+e.radiusPlus,e.lineWidth=e.lineWidth||c.lineWidth+e.lineWidthPlus):(e.color=e.color||oa(e.color||g).brighten(e.brightness).get(),
e.negativeColor=e.negativeColor||oa(e.negativeColor||h).brighten(e.brightness).get());k[""]=a.convertAttribs(c,f);o(["hover","select"],function(b){k[b]=a.convertAttribs(d[b],k[""])});a.pointAttr=k;g=i.length;if(!j||g<j||l)for(;g--;){j=i[g];if((c=j.options&&j.options.marker||j.options)&&c.enabled===!1)c.radius=0;if(s.length){l=0;for(f=s[l];j[r]>=f.value;)f=s[++l];j.color=j.fillColor=f.color}l=b.colorByPoint||j.color;if(j.options)for(u in m)q(c[m[u]])&&(l=!0);if(l){c=c||{};l=[];d=c.states||{};f=d.hover=
d.hover||{};if(!b.marker)f.color=f.color||!j.options.color&&e[j.negative&&h?"negativeColor":"color"]||oa(j.color).brighten(f.brightness||e.brightness).get();f={color:j.color};if(!p)f.fillColor=j.color;if(!n)f.lineColor=j.color;c.hasOwnProperty("color")&&!c.color&&delete c.color;l[""]=a.convertAttribs(x(f,c),k[""]);l.hover=a.convertAttribs(d.hover,k.hover,l[""]);l.select=a.convertAttribs(d.select,k.select,l[""])}else l=k;j.pointAttr=l}},destroy:function(){var a=this,b=a.chart,c=/AppleWebKit\/533/.test(Ba),
d,e=a.data||[],f,g,h;I(a,"destroy");Z(a);o(a.axisTypes||[],function(b){if(h=a[b])ja(h.series,a),h.isDirty=h.forceRedraw=!0});a.legendItem&&a.chart.legend.destroyItem(a);for(d=e.length;d--;)(f=e[d])&&f.destroy&&f.destroy();a.points=null;clearTimeout(a.animationTimeout);for(g in a)a[g]instanceof Q&&!a[g].survive&&(d=c&&g==="group"?"hide":"destroy",a[g][d]());if(b.hoverSeries===a)b.hoverSeries=null;ja(b.series,a);for(g in a)delete a[g]},getSegmentPath:function(a){var b=this,c=[],d=b.options.step;o(a,
function(e,f){var g=e.plotX,h=e.plotY,i;b.getPointSpline?c.push.apply(c,b.getPointSpline(a,e,f)):(c.push(f?"L":"M"),d&&f&&(i=a[f-1],d==="right"?c.push(i.plotX,h):d==="center"?c.push((i.plotX+g)/2,i.plotY,(i.plotX+g)/2,h):c.push(g,i.plotY)),c.push(e.plotX,e.plotY))});return c},getGraphPath:function(){var a=this,b=[],c,d=[];o(a.segments,function(e){c=a.getSegmentPath(e);e.length>1?b=b.concat(c):d.push(e[0])});a.singlePoints=d;return a.graphPath=b},drawGraph:function(){var a=this,b=this.options,c=[["graph",
b.lineColor||this.color,b.dashStyle]],d=b.lineWidth,e=b.linecap!=="square",f=this.getGraphPath(),g=this.fillGraph&&this.color||P;o(this.zones,function(d,e){c.push(["zoneGraph"+e,d.color||a.color,d.dashStyle||b.dashStyle])});o(c,function(c,i){var j=c[0],k=a[j];if(k)db(k),k.animate({d:f});else if((d||g)&&f.length)k={stroke:c[1],"stroke-width":d,fill:g,zIndex:1},c[2]?k.dashstyle=c[2]:e&&(k["stroke-linecap"]=k["stroke-linejoin"]="round"),a[j]=a.chart.renderer.path(f).attr(k).add(a.group).shadow(i<2&&
b.shadow)})},applyZones:function(){var a=this,b=this.chart,c=b.renderer,d=this.zones,e,f,g=this.clips||[],h,i=this.graph,j=this.area,k=u(b.chartWidth,b.chartHeight),l=this[(this.zoneAxis||"y")+"Axis"],m,n=l.reversed,v=b.inverted,s=l.horiz,q,x,t,w=!1;if(d.length&&(i||j))i&&i.hide(),j&&j.hide(),m=l.getExtremes(),o(d,function(d,o){e=n?s?b.plotWidth:0:s?0:l.toPixels(m.min);e=C(u(p(f,e),0),k);f=C(u(r(l.toPixels(p(d.value,m.max),!0)),0),k);w&&(e=f=l.toPixels(m.max));q=Math.abs(e-f);x=C(e,f);t=u(e,f);if(l.isXAxis){if(h=
{x:v?t:x,y:0,width:q,height:k},!s)h.x=b.plotHeight-h.x}else if(h={x:0,y:v?t:x,width:k,height:q},s)h.y=b.plotWidth-h.y;b.inverted&&c.isVML&&(h=l.isXAxis?{x:0,y:n?x:t,height:h.width,width:b.chartWidth}:{x:h.y-b.plotLeft-b.spacingBox.x,y:0,width:h.height,height:b.chartHeight});g[o]?g[o].animate(h):(g[o]=c.clipRect(h),i&&a["zoneGraph"+o].clip(g[o]),j&&a["zoneArea"+o].clip(g[o]));w=d.value>m.max}),this.clips=g},invertGroups:function(){function a(){var a={width:b.yAxis.len,height:b.xAxis.len};o(["group",
"markerGroup"],function(c){b[c]&&b[c].attr(a).invert()})}var b=this,c=b.chart;if(b.xAxis)H(c,"resize",a),H(b,"destroy",function(){Z(c,"resize",a)}),a(),b.invertGroups=a},plotGroup:function(a,b,c,d,e){var f=this[a],g=!f;g&&(this[a]=f=this.chart.renderer.g(b).attr({visibility:c,zIndex:d||0.1}).add(e));f[g?"attr":"animate"](this.getPlotBox());return f},getPlotBox:function(){var a=this.chart,b=this.xAxis,c=this.yAxis;if(a.inverted)b=c,c=this.xAxis;return{translateX:b?b.left:a.plotLeft,translateY:c?c.top:
a.plotTop,scaleX:1,scaleY:1}},render:function(){var a=this,b=a.chart,c,d=a.options,e=(c=d.animation)&&!!a.animate&&b.renderer.isSVG&&p(c.duration,500)||0,f=a.visible?"visible":"hidden",g=d.zIndex,h=a.hasRendered,i=b.seriesGroup;c=a.plotGroup("group","series",f,g,i);a.markerGroup=a.plotGroup("markerGroup","markers",f,g,i);e&&a.animate(!0);a.getAttribs();c.inverted=a.isCartesian?b.inverted:!1;a.drawGraph&&(a.drawGraph(),a.applyZones());o(a.points,function(a){a.redraw&&a.redraw()});a.drawDataLabels&&
a.drawDataLabels();a.visible&&a.drawPoints();a.drawTracker&&a.options.enableMouseTracking!==!1&&a.drawTracker();b.inverted&&a.invertGroups();d.clip!==!1&&!a.sharedClipKey&&!h&&c.clip(b.clipRect);e&&a.animate();if(!h)e?a.animationTimeout=setTimeout(function(){a.afterAnimate()},e):a.afterAnimate();a.isDirty=a.isDirtyData=!1;a.hasRendered=!0},redraw:function(){var a=this.chart,b=this.isDirtyData,c=this.isDirty,d=this.group,e=this.xAxis,f=this.yAxis;d&&(a.inverted&&d.attr({width:a.plotWidth,height:a.plotHeight}),
d.animate({translateX:p(e&&e.left,a.plotLeft),translateY:p(f&&f.top,a.plotTop)}));this.translate();this.render();b&&I(this,"updatedData");(c||b)&&delete this.kdTree},kdDimensions:1,kdAxisArray:["clientX","plotY"],searchPoint:function(a,b){var c=this.xAxis,d=this.yAxis,e=this.chart.inverted;return this.searchKDTree({clientX:e?c.len-a.chartY+c.pos:a.chartX-c.pos,plotY:e?d.len-a.chartX+d.pos:a.chartY-d.pos},b)},buildKDTree:function(){function a(b,d,g){var h,i;if(i=b&&b.length)return h=c.kdAxisArray[d%
g],b.sort(function(a,b){return a[h]-b[h]}),i=Math.floor(i/2),{point:b[i],left:a(b.slice(0,i),d+1,g),right:a(b.slice(i+1),d+1,g)}}function b(){var b=lb(c.points,function(a){return a.y!==null});c.kdTree=a(b,d,d)}var c=this,d=c.kdDimensions;delete c.kdTree;c.options.kdSync?b():setTimeout(b)},searchKDTree:function(a,b){function c(a,b,j,k){var l=b.point,m=d.kdAxisArray[j%k],n,p,o=l;p=q(a[e])&&q(l[e])?Math.pow(a[e]-l[e],2):null;n=q(a[f])&&q(l[f])?Math.pow(a[f]-l[f],2):null;n=(p||0)+(n||0);l.dist=q(n)?Math.sqrt(n):
Number.MAX_VALUE;l.distX=q(p)?Math.sqrt(p):Number.MAX_VALUE;m=a[m]-l[m];n=m<0?"left":"right";p=m<0?"right":"left";b[n]&&(n=c(a,b[n],j+1,k),o=n[g]<o[g]?n:l);b[p]&&Math.sqrt(m*m)<o[g]&&(a=c(a,b[p],j+1,k),o=a[g]<o[g]?a:o);return o}var d=this,e=this.kdAxisArray[0],f=this.kdAxisArray[1],g=b?"distX":"dist";this.kdTree||this.buildKDTree();if(this.kdTree)return c(a,this.kdTree,this.kdDimensions,this.kdDimensions)}};Ib.prototype={destroy:function(){Qa(this,this.axis)},render:function(a){var b=this.options,
c=b.format,c=c?Ja(c,this):b.formatter.call(this);this.label?this.label.attr({text:c,visibility:"hidden"}):this.label=this.axis.chart.renderer.text(c,null,null,b.useHTML).css(b.style).attr({align:this.textAlign,rotation:b.rotation,visibility:"hidden"}).add(a)},setOffset:function(a,b){var c=this.axis,d=c.chart,e=d.inverted,f=c.reversed,f=this.isNegative&&!f||!this.isNegative&&f,g=c.translate(c.usePercentage?100:this.total,0,0,0,1),c=c.translate(0),c=O(g-c),h=d.xAxis[0].translate(this.x)+a,i=d.plotHeight,
f={x:e?f?g:g-c:h,y:e?i-h-b:f?i-g-c:i-g,width:e?c:b,height:e?b:c};if(e=this.label)e.align(this.alignOptions,null,f),f=e.alignAttr,e[this.options.crop===!1||d.isInsidePlot(f.x,f.y)?"show":"hide"](!0)}};va.prototype.buildStacks=function(){var a=this.series,b=p(this.options.reversedStacks,!0),c=a.length;if(!this.isXAxis){for(this.usePercentage=!1;c--;)a[b?c:a.length-c-1].setStackedPoints();if(this.usePercentage)for(c=0;c<a.length;c++)a[c].setPercentStacks()}};va.prototype.renderStackTotals=function(){var a=
this.chart,b=a.renderer,c=this.stacks,d,e,f=this.stackTotalGroup;if(!f)this.stackTotalGroup=f=b.g("stack-labels").attr({visibility:"visible",zIndex:6}).add();f.translate(a.plotLeft,a.plotTop);for(d in c)for(e in a=c[d],a)a[e].render(f)};R.prototype.setStackedPoints=function(){if(this.options.stacking&&!(this.visible!==!0&&this.chart.options.chart.ignoreHiddenSeries!==!1)){var a=this.processedXData,b=this.processedYData,c=[],d=b.length,e=this.options,f=e.threshold,g=e.startFromThreshold?f:0,h=e.stack,
e=e.stacking,i=this.stackKey,j="-"+i,k=this.negStacks,l=this.yAxis,m=l.stacks,n=l.oldStacks,o,s,q,r,t,w;for(r=0;r<d;r++){t=a[r];w=b[r];q=this.index+","+r;s=(o=k&&w<(g?0:f))?j:i;m[s]||(m[s]={});if(!m[s][t])n[s]&&n[s][t]?(m[s][t]=n[s][t],m[s][t].total=null):m[s][t]=new Ib(l,l.options.stackLabels,o,t,h);s=m[s][t];s.points[q]=[p(s.cum,g)];e==="percent"?(o=o?i:j,k&&m[o]&&m[o][t]?(o=m[o][t],s.total=o.total=u(o.total,s.total)+O(w)||0):s.total=ea(s.total+(O(w)||0))):s.total=ea(s.total+(w||0));s.cum=p(s.cum,
g)+(w||0);s.points[q].push(s.cum);c[r]=s.cum}if(e==="percent")l.usePercentage=!0;this.stackedYData=c;l.oldStacks={}}};R.prototype.setPercentStacks=function(){var a=this,b=a.stackKey,c=a.yAxis.stacks,d=a.processedXData;o([b,"-"+b],function(b){var e;for(var f=d.length,g,h;f--;)if(g=d[f],e=(h=c[b]&&c[b][g])&&h.points[a.index+","+f],g=e)h=h.total?100/h.total:0,g[0]=ea(g[0]*h),g[1]=ea(g[1]*h),a.stackedYData[f]=g[1]})};x(F.prototype,{addSeries:function(a,b,c){var d,e=this;a&&(b=p(b,!0),I(e,"addSeries",
{options:a},function(){d=e.initSeries(a);e.isDirtyLegend=!0;e.linkSeries();b&&e.redraw(c)}));return d},addAxis:function(a,b,c,d){var e=b?"xAxis":"yAxis",f=this.options;new va(this,z(a,{index:this[e].length,isX:b}));f[e]=sa(f[e]||{});f[e].push(a);p(c,!0)&&this.redraw(d)},showLoading:function(a){var b=this,c=b.options,d=b.loadingDiv,e=c.loading,f=function(){d&&L(d,{left:b.plotLeft+"px",top:b.plotTop+"px",width:b.plotWidth+"px",height:b.plotHeight+"px"})};if(!d)b.loadingDiv=d=$(Ka,{className:"highcharts-loading"},
x(e.style,{zIndex:10,display:P}),b.container),b.loadingSpan=$("span",null,e.labelStyle,d),H(b,"redraw",f);b.loadingSpan.innerHTML=a||c.lang.loading;if(!b.loadingShown)L(d,{opacity:0,display:""}),mb(d,{opacity:e.style.opacity},{duration:e.showDuration||0}),b.loadingShown=!0;f()},hideLoading:function(){var a=this.options,b=this.loadingDiv;b&&mb(b,{opacity:0},{duration:a.loading.hideDuration||100,complete:function(){L(b,{display:P})}});this.loadingShown=!1}});x(Ga.prototype,{update:function(a,b,c,d){function e(){f.applyOptions(a);
if(f.y===null&&h)f.graphic=h.destroy();if(da(a)&&!Ha(a))f.redraw=function(){if(h)if(a&&a.marker&&a.marker.symbol)f.graphic=h.destroy();else h.attr(f.pointAttr[f.state||""])[f.visible===!1?"hide":"show"]();if(a&&a.dataLabels&&f.dataLabel)f.dataLabel=f.dataLabel.destroy();f.redraw=null};i=f.index;g.updateParallelArrays(f,i);if(l&&f.name)l[f.x]=f.name;k.data[i]=f.options;g.isDirty=g.isDirtyData=!0;if(!g.fixedBox&&g.hasCartesianSeries)j.isDirtyBox=!0;if(k.legendType==="point")j.isDirtyLegend=!0;b&&j.redraw(c)}
var f=this,g=f.series,h=f.graphic,i,j=g.chart,k=g.options,l=g.xAxis&&g.xAxis.names,b=p(b,!0);d===!1?e():f.firePointEvent("update",{options:a},e)},remove:function(a,b){this.series.removePoint(Ma(this,this.series.data),a,b)}});x(R.prototype,{addPoint:function(a,b,c,d){var e=this,f=e.options,g=e.data,h=e.graph,i=e.area,j=e.chart,k=e.xAxis&&e.xAxis.names,l=h&&h.shift||0,m=["graph","area"],h=f.data,n,v=e.xData;Sa(d,j);if(c){for(d=e.zones.length;d--;)m.push("zoneGraph"+d,"zoneArea"+d);o(m,function(a){if(e[a])e[a].shift=
l+1})}if(i)i.isArea=!0;b=p(b,!0);i={series:e};e.pointClass.prototype.applyOptions.apply(i,[a]);m=i.x;d=v.length;if(e.requireSorting&&m<v[d-1])for(n=!0;d&&v[d-1]>m;)d--;e.updateParallelArrays(i,"splice",d,0,0);e.updateParallelArrays(i,d);if(k&&i.name)k[m]=i.name;h.splice(d,0,a);n&&(e.data.splice(d,0,null),e.processData());f.legendType==="point"&&e.generatePoints();c&&(g[0]&&g[0].remove?g[0].remove(!1):(g.shift(),e.updateParallelArrays(i,"shift"),h.shift()));e.isDirty=!0;e.isDirtyData=!0;b&&(e.getAttribs(),
j.redraw())},removePoint:function(a,b,c){var d=this,e=d.data,f=e[a],g=d.points,h=d.chart,i=function(){e.length===g.length&&g.splice(a,1);e.splice(a,1);d.options.data.splice(a,1);d.updateParallelArrays(f||{series:d},"splice",a,1);f&&f.destroy();d.isDirty=!0;d.isDirtyData=!0;b&&h.redraw()};Sa(c,h);b=p(b,!0);f?f.firePointEvent("remove",null,i):i()},remove:function(a,b){var c=this,d=c.chart,a=p(a,!0);if(!c.isRemoving)c.isRemoving=!0,I(c,"remove",null,function(){c.destroy();d.isDirtyLegend=d.isDirtyBox=
!0;d.linkSeries();a&&d.redraw(b)});c.isRemoving=!1},update:function(a,b){var c=this,d=this.chart,e=this.userOptions,f=this.type,g=M[f].prototype,h=["group","markerGroup","dataLabelsGroup"],i;if(a.type&&a.type!==f||a.zIndex!==void 0)h.length=0;o(h,function(a){h[a]=c[a];delete c[a]});a=z(e,{animation:!1,index:this.index,pointStart:this.xData[0]},{data:this.options.data},a);this.remove(!1);for(i in g)this[i]=y;x(this,M[a.type||f].prototype);o(h,function(a){c[a]=h[a]});this.init(d,a);d.linkSeries();p(b,
!0)&&d.redraw(!1)}});x(va.prototype,{update:function(a,b){var c=this.chart,a=c.options[this.coll][this.options.index]=z(this.userOptions,a);this.destroy(!0);this._addedPlotLB=this.chart._labelPanes=y;this.init(c,x(a,{events:y}));c.isDirtyBox=!0;p(b,!0)&&c.redraw()},remove:function(a){for(var b=this.chart,c=this.coll,d=this.series,e=d.length;e--;)d[e]&&d[e].remove(!1);ja(b.axes,this);ja(b[c],this);b.options[c].splice(this.options.index,1);o(b[c],function(a,b){a.options.index=b});this.destroy();b.isDirtyBox=
!0;p(a,!0)&&b.redraw()},setTitle:function(a,b){this.update({title:a},b)},setCategories:function(a,b){this.update({categories:a},b)}});var xa=ka(R);M.line=xa;ba.area=z(U,{threshold:0});var qa=ka(R,{type:"area",getSegments:function(){var a=this,b=[],c=[],d=[],e=this.xAxis,f=this.yAxis,g=f.stacks[this.stackKey],h={},i,j,k=this.points,l=this.options.connectNulls,m,n;if(this.options.stacking&&!this.cropped){for(m=0;m<k.length;m++)h[k[m].x]=k[m];for(n in g)g[n].total!==null&&d.push(+n);d.sort(function(a,
b){return a-b});o(d,function(b){var d=0,k;if(!l||h[b]&&h[b].y!==null)if(h[b])c.push(h[b]);else{for(m=a.index;m<=f.series.length;m++)if(k=g[b].points[m+","+b]){d=k[1];break}i=e.translate(b);j=f.toPixels(d,!0);c.push({y:null,plotX:i,clientX:i,plotY:j,yBottom:j,onMouseOver:na})}});c.length&&b.push(c)}else R.prototype.getSegments.call(this),b=this.segments;this.segments=b},getSegmentPath:function(a){var b=R.prototype.getSegmentPath.call(this,a),c=[].concat(b),d,e=this.options;d=b.length;var f=this.yAxis.getThreshold(e.threshold),
g;d===3&&c.push("L",b[1],b[2]);if(e.stacking&&!this.closedStacks)for(d=a.length-1;d>=0;d--)g=p(a[d].yBottom,f),d<a.length-1&&e.step&&c.push(a[d+1].plotX,g),c.push(a[d].plotX,g);else this.closeSegment(c,a,f);this.areaPath=this.areaPath.concat(c);return b},closeSegment:function(a,b,c){a.push("L",b[b.length-1].plotX,c,"L",b[0].plotX,c)},drawGraph:function(){this.areaPath=[];R.prototype.drawGraph.apply(this);var a=this,b=this.areaPath,c=this.options,d=[["area",this.color,c.fillColor]];o(this.zones,function(b,
f){d.push(["zoneArea"+f,b.color||a.color,b.fillColor||c.fillColor])});o(d,function(d){var f=d[0],g=a[f];g?g.animate({d:b}):a[f]=a.chart.renderer.path(b).attr({fill:p(d[2],oa(d[1]).setOpacity(p(c.fillOpacity,0.75)).get()),zIndex:0}).add(a.group)})},drawLegendSymbol:Na.drawRectangle});M.area=qa;ba.spline=z(U);xa=ka(R,{type:"spline",getPointSpline:function(a,b,c){var d=b.plotX,e=b.plotY,f=a[c-1],g=a[c+1],h,i,j,k;if(f&&g){a=f.plotY;j=g.plotX;var g=g.plotY,l;h=(1.5*d+f.plotX)/2.5;i=(1.5*e+a)/2.5;j=(1.5*
d+j)/2.5;k=(1.5*e+g)/2.5;l=(k-i)*(j-d)/(j-h)+e-k;i+=l;k+=l;i>a&&i>e?(i=u(a,e),k=2*e-i):i<a&&i<e&&(i=C(a,e),k=2*e-i);k>g&&k>e?(k=u(g,e),i=2*e-k):k<g&&k<e&&(k=C(g,e),i=2*e-k);b.rightContX=j;b.rightContY=k}c?(b=["C",f.rightContX||f.plotX,f.rightContY||f.plotY,h||d,i||e,d,e],f.rightContX=f.rightContY=null):b=["M",d,e];return b}});M.spline=xa;ba.areaspline=z(ba.area);qa=qa.prototype;xa=ka(xa,{type:"areaspline",closedStacks:!0,getSegmentPath:qa.getSegmentPath,closeSegment:qa.closeSegment,drawGraph:qa.drawGraph,
drawLegendSymbol:Na.drawRectangle});M.areaspline=xa;ba.column=z(U,{borderColor:"#FFFFFF",borderRadius:0,groupPadding:0.2,marker:null,pointPadding:0.1,minPointLength:0,cropThreshold:50,pointRange:null,states:{hover:{brightness:0.1,shadow:!1,halo:!1},select:{color:"#C0C0C0",borderColor:"#000000",shadow:!1}},dataLabels:{align:null,verticalAlign:null,y:null},startFromThreshold:!0,stickyTracking:!1,tooltip:{distance:6},threshold:0});xa=ka(R,{type:"column",pointAttrToOptions:{stroke:"borderColor",fill:"color",
r:"borderRadius"},cropShoulder:0,directTouch:!0,trackerGroups:["group","dataLabelsGroup"],negStacks:!0,init:function(){R.prototype.init.apply(this,arguments);var a=this,b=a.chart;b.hasRendered&&o(b.series,function(b){if(b.type===a.type)b.isDirty=!0})},getColumnMetrics:function(){var a=this,b=a.options,c=a.xAxis,d=a.yAxis,e=c.reversed,f,g={},h,i=0;b.grouping===!1?i=1:o(a.chart.series,function(b){var c=b.options,e=b.yAxis;if(b.type===a.type&&b.visible&&d.len===e.len&&d.pos===e.pos)c.stacking?(f=b.stackKey,
g[f]===y&&(g[f]=i++),h=g[f]):c.grouping!==!1&&(h=i++),b.columnIndex=h});var c=C(O(c.transA)*(c.ordinalSlope||b.pointRange||c.closestPointRange||c.tickInterval||1),c.len),j=c*b.groupPadding,k=(c-2*j)/i,l=b.pointWidth,b=q(l)?(k-l)/2:k*b.pointPadding,l=p(l,k-2*b);return a.columnMetrics={width:l,offset:b+(j+((e?i-(a.columnIndex||0):a.columnIndex)||0)*k-c/2)*(e?-1:1)}},translate:function(){var a=this,b=a.chart,c=a.options,d=a.borderWidth=p(c.borderWidth,a.closestPointRange*a.xAxis.transA<2?0:1),e=a.yAxis,
f=a.translatedThreshold=e.getThreshold(c.threshold),g=p(c.minPointLength,5),h=a.getColumnMetrics(),i=h.width,j=a.barW=u(i,1+2*d),k=a.pointXOffset=h.offset,l=-(d%2?0.5:0),m=d%2?0.5:1;b.inverted&&(f-=0.5,b.renderer.isVML&&(m+=1));c.pointPadding&&(j=ta(j));R.prototype.translate.apply(a);o(a.points,function(c){var d=p(c.yBottom,f),h=999+O(d),h=C(u(-h,c.plotY),e.len+h),o=c.plotX+k,q=j,t=C(h,d),w,x;w=u(h,d)-t;O(w)<g&&g&&(w=g,x=!e.reversed&&!c.negative||e.reversed&&c.negative,t=r(O(t-f)>g?d-g:f-(x?g:0)));
c.barX=o;c.pointWidth=i;q=r(o+q)+l;o=r(o)+l;q-=o;d=O(t)<0.5;w=C(r(t+w)+m,9E4);t=r(t)+m;w-=t;d&&(t-=1,w+=1);c.tooltipPos=b.inverted?[e.len+e.pos-b.plotLeft-h,a.xAxis.len-o-q/2,w]:[o+q/2,h+e.pos-b.plotTop,w];c.shapeType="rect";c.shapeArgs={x:o,y:t,width:q,height:w}})},getSymbol:na,drawLegendSymbol:Na.drawRectangle,drawGraph:na,drawPoints:function(){var a=this,b=this.chart,c=a.options,d=b.renderer,e=c.animationLimit||250,f,g;o(a.points,function(h){var i=h.plotY,j=h.graphic;if(i!==y&&!isNaN(i)&&h.y!==
null)f=h.shapeArgs,i=q(a.borderWidth)?{"stroke-width":a.borderWidth}:{},g=h.pointAttr[h.selected?"select":""]||a.pointAttr[""],j?(db(j),j.attr(i)[b.pointCount<e?"animate":"attr"](z(f))):h.graphic=d[h.shapeType](f).attr(i).attr(g).add(a.group).shadow(c.shadow,null,c.stacking&&!c.borderRadius);else if(j)h.graphic=j.destroy()})},animate:function(a){var b=this.yAxis,c=this.options,d=this.chart.inverted,e={};if(ca)a?(e.scaleY=0.001,a=C(b.pos+b.len,u(b.pos,b.toPixels(c.threshold))),d?e.translateX=a-b.len:
e.translateY=a,this.group.attr(e)):(e.scaleY=1,e[d?"translateX":"translateY"]=b.pos,this.group.animate(e,this.options.animation),this.animate=null)},remove:function(){var a=this,b=a.chart;b.hasRendered&&o(b.series,function(b){if(b.type===a.type)b.isDirty=!0});R.prototype.remove.apply(a,arguments)}});M.column=xa;ba.bar=z(ba.column);qa=ka(xa,{type:"bar",inverted:!0});M.bar=qa;ba.scatter=z(U,{lineWidth:0,marker:{enabled:!0},tooltip:{headerFormat:'<span style="color:{series.color}">\u25CF</span> <span style="font-size: 10px;"> {series.name}</span><br/>',
pointFormat:"x: <b>{point.x}</b><br/>y: <b>{point.y}</b><br/>"}});qa=ka(R,{type:"scatter",sorted:!1,requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","markerGroup","dataLabelsGroup"],takeOrdinalPosition:!1,kdDimensions:2,drawGraph:function(){this.options.lineWidth&&R.prototype.drawGraph.call(this)}});M.scatter=qa;ba.pie=z(U,{borderColor:"#FFFFFF",borderWidth:1,center:[null,null],clip:!1,colorByPoint:!0,dataLabels:{distance:30,enabled:!0,formatter:function(){return this.point.name},x:0},
ignoreHiddenPoint:!0,legendType:"point",marker:null,size:null,showInLegend:!1,slicedOffset:10,states:{hover:{brightness:0.1,shadow:!1}},stickyTracking:!1,tooltip:{followPointer:!0}});U={type:"pie",isCartesian:!1,pointClass:ka(Ga,{init:function(){Ga.prototype.init.apply(this,arguments);var a=this,b;x(a,{visible:a.visible!==!1,name:p(a.name,"Slice")});b=function(b){a.slice(b.type==="select")};H(a,"select",b);H(a,"unselect",b);return a},setVisible:function(a,b){var c=this,d=c.series,e=d.chart,f=d.options.ignoreHiddenPoint,
b=p(b,f);if(a!==c.visible){c.visible=c.options.visible=a=a===y?!c.visible:a;d.options.data[Ma(c,d.data)]=c.options;o(["graphic","dataLabel","connector","shadowGroup"],function(b){if(c[b])c[b][a?"show":"hide"](!0)});c.legendItem&&e.legend.colorizeItem(c,a);!a&&c.state==="hover"&&c.setState("");if(f)d.isDirty=!0;b&&e.redraw()}},slice:function(a,b,c){var d=this.series;Sa(c,d.chart);p(b,!0);this.sliced=this.options.sliced=a=q(a)?a:!this.sliced;d.options.data[Ma(this,d.data)]=this.options;a=a?this.slicedTranslation:
{translateX:0,translateY:0};this.graphic.animate(a);this.shadowGroup&&this.shadowGroup.animate(a)},haloPath:function(a){var b=this.shapeArgs,c=this.series.chart;return this.sliced||!this.visible?[]:this.series.chart.renderer.symbols.arc(c.plotLeft+b.x,c.plotTop+b.y,b.r+a,b.r+a,{innerR:this.shapeArgs.r,start:b.start,end:b.end})}}),requireSorting:!1,directTouch:!0,noSharedTooltip:!0,trackerGroups:["group","dataLabelsGroup"],axisTypes:[],pointAttrToOptions:{stroke:"borderColor","stroke-width":"borderWidth",
fill:"color"},getColor:na,animate:function(a){var b=this,c=b.points,d=b.startAngleRad;if(!a)o(c,function(a){var c=a.graphic,g=a.shapeArgs;c&&(c.attr({r:a.startR||b.center[3]/2,start:d,end:d}),c.animate({r:g.r,start:g.start,end:g.end},b.options.animation))}),b.animate=null},setData:function(a,b,c,d){R.prototype.setData.call(this,a,!1,c,d);this.processData();this.generatePoints();p(b,!0)&&this.chart.redraw(c)},updateTotals:function(){var a,b=0,c=this.points,d=c.length,e,f=this.options.ignoreHiddenPoint;
for(a=0;a<d;a++)e=c[a],b+=f&&!e.visible?0:e.y;this.total=b;for(a=0;a<d;a++)e=c[a],e.percentage=b>0&&(e.visible||!f)?e.y/b*100:0,e.total=b},generatePoints:function(){R.prototype.generatePoints.call(this);this.updateTotals()},translate:function(a){this.generatePoints();var b=0,c=this.options,d=c.slicedOffset,e=d+c.borderWidth,f,g,h,i=c.startAngle||0,j=this.startAngleRad=ma/180*(i-90),i=(this.endAngleRad=ma/180*(p(c.endAngle,i+360)-90))-j,k=this.points,l=c.dataLabels.distance,c=c.ignoreHiddenPoint,m,
n=k.length,o;if(!a)this.center=a=this.getCenter();this.getX=function(b,c){h=W.asin(C((b-a[1])/(a[2]/2+l),1));return a[0]+(c?-1:1)*X(h)*(a[2]/2+l)};for(m=0;m<n;m++){o=k[m];f=j+b*i;if(!c||o.visible)b+=o.percentage/100;g=j+b*i;o.shapeType="arc";o.shapeArgs={x:a[0],y:a[1],r:a[2]/2,innerR:a[3]/2,start:r(f*1E3)/1E3,end:r(g*1E3)/1E3};h=(g+f)/2;h>1.5*ma?h-=2*ma:h<-ma/2&&(h+=2*ma);o.slicedTranslation={translateX:r(X(h)*d),translateY:r(aa(h)*d)};f=X(h)*a[2]/2;g=aa(h)*a[2]/2;o.tooltipPos=[a[0]+f*0.7,a[1]+g*
0.7];o.half=h<-ma/2||h>ma/2?1:0;o.angle=h;e=C(e,l/2);o.labelPos=[a[0]+f+X(h)*l,a[1]+g+aa(h)*l,a[0]+f+X(h)*e,a[1]+g+aa(h)*e,a[0]+f,a[1]+g,l<0?"center":o.half?"right":"left",h]}},drawGraph:null,drawPoints:function(){var a=this,b=a.chart.renderer,c,d,e=a.options.shadow,f,g,h;if(e&&!a.shadowGroup)a.shadowGroup=b.g("shadow").add(a.group);o(a.points,function(i){d=i.graphic;g=i.shapeArgs;f=i.shadowGroup;if(e&&!f)f=i.shadowGroup=b.g("shadow").add(a.shadowGroup);c=i.sliced?i.slicedTranslation:{translateX:0,
translateY:0};f&&f.attr(c);if(d)d.animate(x(g,c));else{h={"stroke-linejoin":"round"};if(!i.visible)h.visibility="hidden";i.graphic=d=b[i.shapeType](g).setRadialReference(a.center).attr(i.pointAttr[i.selected?"select":""]).attr(h).attr(c).add(a.group).shadow(e,f)}})},searchPoint:na,sortByAngle:function(a,b){a.sort(function(a,d){return a.angle!==void 0&&(d.angle-a.angle)*b})},drawLegendSymbol:Na.drawRectangle,getCenter:Yb.getCenter,getSymbol:na};U=ka(R,U);M.pie=U;R.prototype.drawDataLabels=function(){var a=
this,b=a.options,c=b.cursor,d=b.dataLabels,e=a.points,f,g,h=a.hasRendered||0,i,j,k=a.chart.renderer;if(d.enabled||a._hasPointLabels)a.dlProcessOptions&&a.dlProcessOptions(d),j=a.plotGroup("dataLabelsGroup","data-labels",d.defer?"hidden":"visible",d.zIndex||6),p(d.defer,!0)&&(j.attr({opacity:+h}),h||H(a,"afterAnimate",function(){a.visible&&j.show();j[b.animation?"animate":"attr"]({opacity:1},{duration:200})})),g=d,o(e,function(e){var h,n=e.dataLabel,o,s,r=e.connector,u=!0,t,w={};f=e.dlOptions||e.options&&
e.options.dataLabels;h=p(f&&f.enabled,g.enabled);if(n&&!h)e.dataLabel=n.destroy();else if(h){d=z(g,f);t=d.style;h=d.rotation;o=e.getLabelConfig();i=d.format?Ja(d.format,o):d.formatter.call(o,d);t.color=p(d.color,t.color,a.color,"black");if(n)if(q(i))n.attr({text:i}),u=!1;else{if(e.dataLabel=n=n.destroy(),r)e.connector=r.destroy()}else if(q(i)){n={fill:d.backgroundColor,stroke:d.borderColor,"stroke-width":d.borderWidth,r:d.borderRadius||0,rotation:h,padding:d.padding,zIndex:1};if(t.color==="contrast")w.color=
d.inside||d.distance<0||b.stacking?k.getContrast(e.color||a.color):"#000000";if(c)w.cursor=c;for(s in n)n[s]===y&&delete n[s];n=e.dataLabel=k[h?"text":"label"](i,0,-999,d.shape,null,null,d.useHTML).attr(n).css(x(t,w)).add(j).shadow(d.shadow)}n&&a.alignDataLabel(e,n,d,null,u)}})};R.prototype.alignDataLabel=function(a,b,c,d,e){var f=this.chart,g=f.inverted,h=p(a.plotX,-999),i=p(a.plotY,-999),j=b.getBBox(),k=f.renderer.fontMetrics(c.style.fontSize).b,l=this.visible&&(a.series.forceDL||f.isInsidePlot(h,
r(i),g)||d&&f.isInsidePlot(h,g?d.x+1:d.y+d.height-1,g));if(l)d=x({x:g?f.plotWidth-i:h,y:r(g?f.plotHeight-h:i),width:0,height:0},d),x(c,{width:j.width,height:j.height}),c.rotation?(a=f.renderer.rotCorr(k,c.rotation),b[e?"attr":"animate"]({x:d.x+c.x+d.width/2+a.x,y:d.y+c.y+d.height/2}).attr({align:c.align})):(b.align(c,null,d),g=b.alignAttr,p(c.overflow,"justify")==="justify"?this.justifyDataLabel(b,c,g,j,d,e):p(c.crop,!0)&&(l=f.isInsidePlot(g.x,g.y)&&f.isInsidePlot(g.x+j.width,g.y+j.height)),c.shape&&
b.attr({anchorX:a.plotX,anchorY:a.plotY}));if(!l)b.attr({y:-999}),b.placed=!1};R.prototype.justifyDataLabel=function(a,b,c,d,e,f){var g=this.chart,h=b.align,i=b.verticalAlign,j,k,l=a.box?0:a.padding||0;j=c.x+l;if(j<0)h==="right"?b.align="left":b.x=-j,k=!0;j=c.x+d.width-l;if(j>g.plotWidth)h==="left"?b.align="right":b.x=g.plotWidth-j,k=!0;j=c.y+l;if(j<0)i==="bottom"?b.verticalAlign="top":b.y=-j,k=!0;j=c.y+d.height-l;if(j>g.plotHeight)i==="top"?b.verticalAlign="bottom":b.y=g.plotHeight-j,k=!0;if(k)a.placed=
!f,a.align(b,null,e)};if(M.pie)M.pie.prototype.drawDataLabels=function(){var a=this,b=a.data,c,d=a.chart,e=a.options.dataLabels,f=p(e.connectorPadding,10),g=p(e.connectorWidth,1),h=d.plotWidth,i=d.plotHeight,j,k,l=p(e.softConnector,!0),m=e.distance,n=a.center,q=n[2]/2,s=n[1],x=m>0,y,t,w,z=[[],[]],B,A,D,F,G,E=[0,0,0,0],L=function(a,b){return b.y-a.y};if(a.visible&&(e.enabled||a._hasPointLabels)){R.prototype.drawDataLabels.apply(a);o(b,function(a){a.dataLabel&&a.visible&&z[a.half].push(a)});for(F=2;F--;){var I=
[],M=[],H=z[F],K=H.length,J;if(K){a.sortByAngle(H,F-0.5);for(G=b=0;!b&&H[G];)b=H[G]&&H[G].dataLabel&&(H[G].dataLabel.getBBox().height||21),G++;if(m>0){t=C(s+q+m,d.plotHeight);for(G=u(0,s-q-m);G<=t;G+=b)I.push(G);t=I.length;if(K>t){c=[].concat(H);c.sort(L);for(G=K;G--;)c[G].rank=G;for(G=K;G--;)H[G].rank>=t&&H.splice(G,1);K=H.length}for(G=0;G<K;G++){c=H[G];w=c.labelPos;c=9999;var Q,P;for(P=0;P<t;P++)Q=O(I[P]-w[1]),Q<c&&(c=Q,J=P);if(J<G&&I[G]!==null)J=G;else for(t<K-G+J&&I[G]!==null&&(J=t-K+G);I[J]===
null;)J++;M.push({i:J,y:I[J]});I[J]=null}M.sort(L)}for(G=0;G<K;G++){c=H[G];w=c.labelPos;y=c.dataLabel;D=c.visible===!1?"hidden":"inherit";c=w[1];if(m>0){if(t=M.pop(),J=t.i,A=t.y,c>A&&I[J+1]!==null||c<A&&I[J-1]!==null)A=C(u(0,c),d.plotHeight)}else A=c;B=e.justify?n[0]+(F?-1:1)*(q+m):a.getX(A===s-q-m||A===s+q+m?c:A,F);y._attr={visibility:D,align:w[6]};y._pos={x:B+e.x+({left:f,right:-f}[w[6]]||0),y:A+e.y-10};y.connX=B;y.connY=A;if(this.options.size===null)t=y.width,B-t<f?E[3]=u(r(t-B+f),E[3]):B+t>h-
f&&(E[1]=u(r(B+t-h+f),E[1])),A-b/2<0?E[0]=u(r(-A+b/2),E[0]):A+b/2>i&&(E[2]=u(r(A+b/2-i),E[2]))}}}if(Fa(E)===0||this.verifyDataLabelOverflow(E))this.placeDataLabels(),x&&g&&o(this.points,function(b){j=b.connector;w=b.labelPos;if((y=b.dataLabel)&&y._pos&&b.visible)D=y._attr.visibility,B=y.connX,A=y.connY,k=l?["M",B+(w[6]==="left"?5:-5),A,"C",B,A,2*w[2]-w[4],2*w[3]-w[5],w[2],w[3],"L",w[4],w[5]]:["M",B+(w[6]==="left"?5:-5),A,"L",w[2],w[3],"L",w[4],w[5]],j?(j.animate({d:k}),j.attr("visibility",D)):b.connector=
j=a.chart.renderer.path(k).attr({"stroke-width":g,stroke:e.connectorColor||b.color||"#606060",visibility:D}).add(a.dataLabelsGroup);else if(j)b.connector=j.destroy()})}},M.pie.prototype.placeDataLabels=function(){o(this.points,function(a){var b=a.dataLabel;if(b&&a.visible)(a=b._pos)?(b.attr(b._attr),b[b.moved?"animate":"attr"](a),b.moved=!0):b&&b.attr({y:-999})})},M.pie.prototype.alignDataLabel=na,M.pie.prototype.verifyDataLabelOverflow=function(a){var b=this.center,c=this.options,d=c.center,e=c.minSize||
80,f=e,g;d[0]!==null?f=u(b[2]-u(a[1],a[3]),e):(f=u(b[2]-a[1]-a[3],e),b[0]+=(a[3]-a[1])/2);d[1]!==null?f=u(C(f,b[2]-u(a[0],a[2])),e):(f=u(C(f,b[2]-a[0]-a[2]),e),b[1]+=(a[0]-a[2])/2);f<b[2]?(b[2]=f,b[3]=/%$/.test(c.innerSize||0)?f*parseFloat(c.innerSize||0)/100:parseFloat(c.innerSize||0),this.translate(b),o(this.points,function(a){if(a.dataLabel)a.dataLabel._pos=null}),this.drawDataLabels&&this.drawDataLabels()):g=!0;return g};if(M.column)M.column.prototype.alignDataLabel=function(a,b,c,d,e){var f=
this.chart.inverted,g=a.series,h=a.dlBox||a.shapeArgs,i=p(a.below,a.plotY>p(this.translatedThreshold,g.yAxis.len)),j=p(c.inside,!!this.options.stacking);if(h&&(d=z(h),f&&(d={x:g.yAxis.len-d.y-d.height,y:g.xAxis.len-d.x-d.width,width:d.height,height:d.width}),!j))f?(d.x+=i?0:d.width,d.width=0):(d.y+=i?d.height:0,d.height=0);c.align=p(c.align,!f||j?"center":i?"right":"left");c.verticalAlign=p(c.verticalAlign,f||j?"middle":i?"top":"bottom");R.prototype.alignDataLabel.call(this,a,b,c,d,e)};(function(a){var b=
a.Chart,c=a.each,d=a.pick,e=HighchartsAdapter.addEvent;b.prototype.callbacks.push(function(a){function b(){var e=[];c(a.series,function(a){var b=a.options.dataLabels;(b.enabled||a._hasPointLabels)&&!b.allowOverlap&&a.visible&&c(a.points,function(a){if(a.dataLabel)a.dataLabel.labelrank=d(a.labelrank,a.shapeArgs&&a.shapeArgs.height),e.push(a.dataLabel)})});a.hideOverlappingLabels(e)}b();e(a,"redraw",b)});b.prototype.hideOverlappingLabels=function(a){var b=a.length,c,d,e,k;for(d=0;d<b;d++)if(c=a[d])c.oldOpacity=
c.opacity,c.newOpacity=1;a.sort(function(a,b){return b.labelrank-a.labelrank});for(d=0;d<b;d++){e=a[d];for(c=d+1;c<b;++c)if(k=a[c],e&&k&&e.placed&&k.placed&&e.newOpacity!==0&&k.newOpacity!==0&&!(k.alignAttr.x>e.alignAttr.x+e.width||k.alignAttr.x+k.width<e.alignAttr.x||k.alignAttr.y>e.alignAttr.y+e.height||k.alignAttr.y+k.height<e.alignAttr.y))(e.labelrank<k.labelrank?e:k).newOpacity=0}for(d=0;d<b;d++)if(c=a[d]){if(c.oldOpacity!==c.newOpacity&&c.placed)c.alignAttr.opacity=c.newOpacity,c[c.isOld&&c.newOpacity?
"animate":"attr"](c.alignAttr);c.isOld=!0}}})(A);U=A.TrackerMixin={drawTrackerPoint:function(){var a=this,b=a.chart,c=b.pointer,d=a.options.cursor,e=d&&{cursor:d},f=function(a){for(var c=a.target,d;c&&!d;)d=c.point,c=c.parentNode;if(d!==y&&d!==b.hoverPoint)d.onMouseOver(a)};o(a.points,function(a){if(a.graphic)a.graphic.element.point=a;if(a.dataLabel)a.dataLabel.element.point=a});if(!a._hasTracking)o(a.trackerGroups,function(b){if(a[b]&&(a[b].addClass("highcharts-tracker").on("mouseover",f).on("mouseout",
function(a){c.onTrackerMouseOut(a)}).css(e),ab))a[b].on("touchstart",f)}),a._hasTracking=!0},drawTrackerGraph:function(){var a=this,b=a.options,c=b.trackByArea,d=[].concat(c?a.areaPath:a.graphPath),e=d.length,f=a.chart,g=f.pointer,h=f.renderer,i=f.options.tooltip.snap,j=a.tracker,k=b.cursor,l=k&&{cursor:k},k=a.singlePoints,m,n=function(){if(f.hoverSeries!==a)a.onMouseOver()},p="rgba(192,192,192,"+(ca?1.0E-4:0.002)+")";if(e&&!c)for(m=e+1;m--;)d[m]==="M"&&d.splice(m+1,0,d[m+1]-i,d[m+2],"L"),(m&&d[m]===
"M"||m===e)&&d.splice(m,0,"L",d[m-2]+i,d[m-1]);for(m=0;m<k.length;m++)e=k[m],d.push("M",e.plotX-i,e.plotY,"L",e.plotX+i,e.plotY);j?j.attr({d:d}):(a.tracker=h.path(d).attr({"stroke-linejoin":"round",visibility:a.visible?"visible":"hidden",stroke:p,fill:c?p:P,"stroke-width":b.lineWidth+(c?0:2*i),zIndex:2}).add(a.group),o([a.tracker,a.markerGroup],function(a){a.addClass("highcharts-tracker").on("mouseover",n).on("mouseout",function(a){g.onTrackerMouseOut(a)}).css(l);if(ab)a.on("touchstart",n)}))}};if(M.column)xa.prototype.drawTracker=
U.drawTrackerPoint;if(M.pie)M.pie.prototype.drawTracker=U.drawTrackerPoint;if(M.scatter)qa.prototype.drawTracker=U.drawTrackerPoint;x(nb.prototype,{setItemEvents:function(a,b,c,d,e){var f=this;(c?b:a.legendGroup).on("mouseover",function(){a.setState("hover");b.css(f.options.itemHoverStyle)}).on("mouseout",function(){b.css(a.visible?d:e);a.setState()}).on("click",function(b){var c=function(){a.setVisible()},b={browserEvent:b};a.firePointEvent?a.firePointEvent("legendItemClick",b,c):I(a,"legendItemClick",
b,c)})},createCheckboxForItem:function(a){a.checkbox=$("input",{type:"checkbox",checked:a.selected,defaultChecked:a.selected},this.options.itemCheckboxStyle,this.chart.container);H(a.checkbox,"click",function(b){I(a.series||a,"checkboxClick",{checked:b.target.checked,item:a},function(){a.select()})})}});T.legend.itemStyle.cursor="pointer";x(F.prototype,{showResetZoom:function(){var a=this,b=T.lang,c=a.options.chart.resetZoomButton,d=c.theme,e=d.states,f=c.relativeTo==="chart"?null:"plotBox";this.resetZoomButton=
a.renderer.button(b.resetZoom,null,null,function(){a.zoomOut()},d,e&&e.hover).attr({align:c.position.align,title:b.resetZoomTitle}).add().align(c.position,!1,f)},zoomOut:function(){var a=this;I(a,"selection",{resetSelection:!0},function(){a.zoom()})},zoom:function(a){var b,c=this.pointer,d=!1,e;!a||a.resetSelection?o(this.axes,function(a){b=a.zoom()}):o(a.xAxis.concat(a.yAxis),function(a){var e=a.axis,h=e.isXAxis;if(c[h?"zoomX":"zoomY"]||c[h?"pinchX":"pinchY"])b=e.zoom(a.min,a.max),e.displayBtn&&
(d=!0)});e=this.resetZoomButton;if(d&&!e)this.showResetZoom();else if(!d&&da(e))this.resetZoomButton=e.destroy();b&&this.redraw(p(this.options.chart.animation,a&&a.animation,this.pointCount<100))},pan:function(a,b){var c=this,d=c.hoverPoints,e;d&&o(d,function(a){a.setState()});o(b==="xy"?[1,0]:[1],function(b){var d=a[b?"chartX":"chartY"],h=c[b?"xAxis":"yAxis"][0],i=c[b?"mouseDownX":"mouseDownY"],j=(h.pointRange||0)/2,k=h.getExtremes(),l=h.toValue(i-d,!0)+j,j=h.toValue(i+c[b?"plotWidth":"plotHeight"]-
d,!0)-j,i=i>d;if(h.series.length&&(i||l>C(k.dataMin,k.min))&&(!i||j<u(k.dataMax,k.max)))h.setExtremes(l,j,!1,!1,{trigger:"pan"}),e=!0;c[b?"mouseDownX":"mouseDownY"]=d});e&&c.redraw(!1);L(c.container,{cursor:"move"})}});x(Ga.prototype,{select:function(a,b){var c=this,d=c.series,e=d.chart,a=p(a,!c.selected);c.firePointEvent(a?"select":"unselect",{accumulate:b},function(){c.selected=c.options.selected=a;d.options.data[Ma(c,d.data)]=c.options;c.setState(a&&"select");b||o(e.getSelectedPoints(),function(a){if(a.selected&&
a!==c)a.selected=a.options.selected=!1,d.options.data[Ma(a,d.data)]=a.options,a.setState(""),a.firePointEvent("unselect")})})},onMouseOver:function(a){var b=this.series,c=b.chart,d=c.tooltip,e=c.hoverPoint;if(c.hoverSeries!==b)b.onMouseOver();if(e&&e!==this)e.onMouseOut();if(this.series)this.firePointEvent("mouseOver"),d&&(!d.shared||b.noSharedTooltip)&&d.refresh(this,a),this.setState("hover"),c.hoverPoint=this},onMouseOut:function(){var a=this.series.chart,b=a.hoverPoints;this.firePointEvent("mouseOut");
if(!b||Ma(this,b)===-1)this.setState(),a.hoverPoint=null},importEvents:function(){if(!this.hasImportedEvents){var a=z(this.series.options.point,this.options).events,b;this.events=a;for(b in a)H(this,b,a[b]);this.hasImportedEvents=!0}},setState:function(a,b){var c=this.plotX,d=this.plotY,e=this.series,f=e.options.states,g=ba[e.type].marker&&e.options.marker,h=g&&!g.enabled,i=g&&g.states[a],j=i&&i.enabled===!1,k=e.stateMarkerGraphic,l=this.marker||{},m=e.chart,n=e.halo,o,a=a||"";o=this.pointAttr[a]||
e.pointAttr[a];if(!(a===this.state&&!b||this.selected&&a!=="select"||f[a]&&f[a].enabled===!1||a&&(j||h&&i.enabled===!1)||a&&l.states&&l.states[a]&&l.states[a].enabled===!1)){if(this.graphic)g=g&&this.graphic.symbolName&&o.r,this.graphic.attr(z(o,g?{x:c-g,y:d-g,width:2*g,height:2*g}:{})),k&&k.hide();else{if(a&&i)if(g=i.radius,l=l.symbol||e.symbol,k&&k.currentSymbol!==l&&(k=k.destroy()),k)k[b?"animate":"attr"]({x:c-g,y:d-g});else if(l)e.stateMarkerGraphic=k=m.renderer.symbol(l,c-g,d-g,2*g,2*g).attr(o).add(e.markerGroup),
k.currentSymbol=l;if(k)k[a&&m.isInsidePlot(c,d,m.inverted)?"show":"hide"](),k.element.point=this}if((c=f[a]&&f[a].halo)&&c.size){if(!n)e.halo=n=m.renderer.path().add(m.seriesGroup);n.attr(x({fill:oa(this.color||e.color).setOpacity(c.opacity).get()},c.attributes))[b?"animate":"attr"]({d:this.haloPath(c.size)})}else n&&n.attr({d:[]});this.state=a}},haloPath:function(a){var b=this.series,c=b.chart,d=b.getPlotBox(),e=c.inverted;return c.renderer.symbols.circle(d.translateX+(e?b.yAxis.len-this.plotY:this.plotX)-
a,d.translateY+(e?b.xAxis.len-this.plotX:this.plotY)-a,a*2,a*2)}});x(R.prototype,{onMouseOver:function(){var a=this.chart,b=a.hoverSeries;if(b&&b!==this)b.onMouseOut();this.options.events.mouseOver&&I(this,"mouseOver");this.setState("hover");a.hoverSeries=this},onMouseOut:function(){var a=this.options,b=this.chart,c=b.tooltip,d=b.hoverPoint;b.hoverSeries=null;if(d)d.onMouseOut();this&&a.events.mouseOut&&I(this,"mouseOut");c&&!a.stickyTracking&&(!c.shared||this.noSharedTooltip)&&c.hide();this.setState()},
setState:function(a){var b=this.options,c=this.graph,d=b.states,e=b.lineWidth,b=0,a=a||"";if(this.state!==a&&(this.state=a,!(d[a]&&d[a].enabled===!1)&&(a&&(e=d[a].lineWidth||e+(d[a].lineWidthPlus||0)),c&&!c.dashstyle))){a={"stroke-width":e};for(c.attr(a);this["zoneGraph"+b];)this["zoneGraph"+b].attr(a),b+=1}},setVisible:function(a,b){var c=this,d=c.chart,e=c.legendItem,f,g=d.options.chart.ignoreHiddenSeries,h=c.visible;f=(c.visible=a=c.userOptions.visible=a===y?!h:a)?"show":"hide";o(["group","dataLabelsGroup",
"markerGroup","tracker"],function(a){if(c[a])c[a][f]()});if(d.hoverSeries===c||(d.hoverPoint&&d.hoverPoint.series)===c)c.onMouseOut();e&&d.legend.colorizeItem(c,a);c.isDirty=!0;c.options.stacking&&o(d.series,function(a){if(a.options.stacking&&a.visible)a.isDirty=!0});o(c.linkedSeries,function(b){b.setVisible(a,!1)});if(g)d.isDirtyBox=!0;b!==!1&&d.redraw();I(c,f)},show:function(){this.setVisible(!0)},hide:function(){this.setVisible(!1)},select:function(a){this.selected=a=a===y?!this.selected:a;if(this.checkbox)this.checkbox.checked=
a;I(this,a?"select":"unselect")},drawTracker:U.drawTrackerGraph});x(A,{Color:oa,Point:Ga,Tick:Ta,Renderer:$a,SVGElement:Q,SVGRenderer:ua,arrayMin:Pa,arrayMax:Fa,charts:Y,dateFormat:Oa,error:la,format:Ja,pathAnim:zb,getOptions:function(){return T},hasBidiBug:Ob,isTouchDevice:Kb,setOptions:function(a){T=z(!0,T,a);Db();return T},addEvent:H,removeEvent:Z,createElement:$,discardElement:Ra,css:L,each:o,map:Ua,merge:z,splat:sa,extendClass:ka,pInt:D,svg:ca,canvas:fa,vml:!ca&&!fa,product:"Highcharts",version:"4.1.7"})})();

/**=========================================================
 * Module: load-css.js
 * Request and load into the current page a css file
 =========================================================*/

App.directive('loadCss', function() {
  'use strict';

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.on('click', function (e) {
          if(element.is('a')) e.preventDefault();
          var uri = attrs.loadCss,
              link;

          if(uri) {
            link = createLink(uri);
            if ( !link ) {
              $.error('Error creating stylesheet link element.');
            }
          }
          else {
            $.error('No stylesheet location defined.');
          }

      });

    }
  };

  function createLink(uri) {
    var linkId = 'autoloaded-stylesheet',
        oldLink = $('#'+linkId).attr('id', linkId + '-old');

    $('head').append($('<link/>').attr({
      'id':   linkId,
      'rel':  'stylesheet',
      'href': uri
    }));

    if( oldLink.length ) {
      oldLink.remove();
    }

    return $('#'+linkId);
  }


});

/**
 * @ngdoc directive
 * @name golfCourseAmericaWebAppApp.directive:mapScroll
 * @description
 * # mapScroll
 */
App.directive('mapScroll', ["$window", function ($window) {
    'use strict';
    
    return function(scope, element, attrs) {
        angular.element($window).bind('scroll', function() {
        	console.log('Scroll called');
             if (this.pageYOffset >= 600) {
                 scope.boolChangeClass = true;
                 console.log('Scrolled below header.');
             } else {
                 scope.boolChangeClass = false;
                 console.log('Header is in view.');
             }
            scope.$apply();
        });
    };
  }]);

/**=========================================================
 * Module: markdownarea.js
 * Markdown Editor from UIKit adapted for Bootstrap Layout.
 =========================================================*/

App.directive('markdownarea', function() {
  'use strict';
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var area         = $(element),
          Markdownarea = $.fn["markdownarea"],
          options      = $.Utils.options(attrs.markdownarea);
      
      var obj = new Markdownarea(area, $.Utils.options(attrs.markdownarea));

    }
  };
});


// Markdown plugin defintion
// Customized to work with bootstrap 
// classnames
// ----------------------------------- 

(function($, window, document){
    'use strict';

    var Markdownarea = function(element, options){

        var $element = $(element);

        if($element.data("markdownarea")) return;

        this.element = $element;
        this.options = $.extend({}, Markdownarea.defaults, options);

        this.marked     = this.options.marked || marked;
        this.CodeMirror = this.options.CodeMirror || CodeMirror;

        this.marked.setOptions({
          gfm           : true,
          tables        : true,
          breaks        : true,
          pedantic      : true,
          sanitize      : false,
          smartLists    : true,
          smartypants   : false,
          langPrefix    : 'lang-'
        });

        this.init();

        this.element.data("markdownarea", this);
    };

    $.extend(Markdownarea.prototype, {

        init: function(){

            var $this = this, tpl = Markdownarea.template;

            tpl = tpl.replace(/\{\:lblPreview\}/g, this.options.lblPreview);
            tpl = tpl.replace(/\{\:lblCodeview\}/g, this.options.lblCodeview);

            this.markdownarea = $(tpl);
            this.content      = this.markdownarea.find(".uk-markdownarea-content");
            this.toolbar      = this.markdownarea.find(".uk-markdownarea-toolbar");
            this.preview      = this.markdownarea.find(".uk-markdownarea-preview").children().eq(0);
            this.code         = this.markdownarea.find(".uk-markdownarea-code");

            this.element.before(this.markdownarea).appendTo(this.code);

            this.editor = this.CodeMirror.fromTextArea(this.element[0], this.options.codemirror);

            this.editor.markdownarea = this;

            this.editor.on("change", (function(){
                var render = function(){

                    var value   = $this.editor.getValue();

                    $this.currentvalue  = String(value);

                    $this.element.trigger("markdownarea-before", [$this]);

                    $this.applyPlugins();

                    $this.marked($this.currentvalue, function (err, markdown) {

                      if (err) throw err;

                      $this.preview.html(markdown);
                      $this.element.val($this.editor.getValue()).trigger("markdownarea-update", [$this]);
                    });
                };
                render();
                return $.Utils.debounce(render, 150);
            })());

            this.code.find(".CodeMirror").css("height", this.options.height);

            this._buildtoolbar();
            this.fit();

            $(window).on("resize", $.Utils.debounce(function(){
                $this.fit();
            }, 200));


            var previewContainer = $this.preview.parent(),
                codeContent      = this.code.find('.CodeMirror-sizer'),
                codeScroll       = this.code.find('.CodeMirror-scroll').on('scroll',$.Utils.debounce(function() {

                    if($this.markdownarea.attr("data-mode")=="tab") return;

                    // calc position
                    var codeHeight       = codeContent.height()   - codeScroll.height(),
                        previewHeight    = previewContainer[0].scrollHeight - previewContainer.height(),
                        ratio            = previewHeight / codeHeight,
                        previewPostition = codeScroll.scrollTop() * ratio;

                    // apply new scroll
                    previewContainer.scrollTop(previewPostition);
            }, 10));

            this.markdownarea.on("click", ".uk-markdown-button-markdown, .uk-markdown-button-preview", function(e){

                e.preventDefault();

                if($this.markdownarea.attr("data-mode")=="tab") {

                    $this.markdownarea.find(".uk-markdown-button-markdown, .uk-markdown-button-preview").removeClass("uk-active").filter(this).addClass("uk-active");

                    $this.activetab = $(this).hasClass("uk-markdown-button-markdown") ? "code":"preview";
                    $this.markdownarea.attr("data-active-tab", $this.activetab);
                }
            });

            this.preview.parent().css("height", this.code.height());
        },

        applyPlugins: function(){

            var $this   = this,
                plugins = Object.keys(Markdownarea.plugins),
                plgs    = Markdownarea.plugins;

            this.markers = {};

            if(plugins.length) {

                var lines = this.currentvalue.split("\n");

                plugins.forEach(function(name){
                    this.markers[name] = [];
                }, this);

                for(var line=0,max=lines.length;line<max;line++) {

                    (function(line){
                        plugins.forEach(function(name){

                            var i = 0;

                            lines[line] = lines[line].replace(plgs[name].identifier, function(){

                                var replacement =  plgs[name].cb({
                                    "area" : $this,
                                    "found": arguments,
                                    "line" : line,
                                    "pos"  : i++,
                                    "uid"  : [name, line, i, (new Date().getTime())+"RAND"+(Math.ceil(Math.random() *100000))].join('-'),
                                    "replace": function(strwith){
                                        var src   = this.area.editor.getLine(this.line),
                                            start = src.indexOf(this.found[0]),
                                            end   = start + this.found[0].length;

                                        this.area.editor.replaceRange(strwith, {"line": this.line, "ch":start}, {"line": this.line, "ch":end} );
                                    }
                                });

                                return replacement;
                            });
                        });
                    }(line));
                }

                this.currentvalue = lines.join("\n");

            }
        },

        _buildtoolbar: function(){

            if(!(this.options.toolbar && this.options.toolbar.length)) return;

            var $this = this, bar = [];

            this.options.toolbar.forEach(function(cmd){
                if(Markdownarea.commands[cmd]) {

                   var title = Markdownarea.commands[cmd].title ? Markdownarea.commands[cmd].title : cmd;

                   bar.push('<li><a data-markdownarea-cmd="'+cmd+'" title="'+title+'" data-toggle="tooltip">'+Markdownarea.commands[cmd].label+'</a></li>');

                   if(Markdownarea.commands[cmd].shortcut) {
                       $this.registerShortcut(Markdownarea.commands[cmd].shortcut, Markdownarea.commands[cmd].action);
                   }
                }
            });

            this.toolbar.html(bar.join("\n"));

            this.markdownarea.on("click", "a[data-markdownarea-cmd]", function(){
                var cmd = $(this).data("markdownareaCmd");

                if(cmd && Markdownarea.commands[cmd] && (!$this.activetab || $this.activetab=="code" || cmd=="fullscreen")) {
                    Markdownarea.commands[cmd].action.apply($this, [$this.editor]);
                }

            });
        },

        fit: function() {

            var mode = this.options.mode;

            if(mode=="split" && this.markdownarea.width() < this.options.maxsplitsize) {
                mode = "tab";
            }

            if(mode=="tab") {

                if(!this.activetab) {
                    this.activetab = "code";
                    this.markdownarea.attr("data-active-tab", this.activetab);
                }

                this.markdownarea.find(".uk-markdown-button-markdown, .uk-markdown-button-preview").removeClass("uk-active")
                                 .filter(this.activetab=="code" ? '.uk-markdown-button-markdown':'.uk-markdown-button-preview').addClass("uk-active");

            }

            this.editor.refresh();
            this.preview.parent().css("height", this.code.height());

            this.markdownarea.attr("data-mode", mode);
        },

        registerShortcut: function(combination, callback){

            var $this = this;

            combination = $.isArray(combination) ? combination : [combination];

            for(var i=0,max=combination.length;i < max;i++) {
                var map = {};

                map[combination[i]] = function(){
                    callback.apply($this, [$this.editor]);
                };

                $this.editor.addKeyMap(map);
            }
        },

        getMode: function(){
            var pos = this.editor.getDoc().getCursor();

            return this.editor.getTokenAt(pos).state.base.htmlState ? 'html':'markdown';
        }
    });

    //jQuery plugin

    $.fn.markdownarea = function(options){

        return this.each(function(){

            var ele = $(this);

            if(!ele.data("markdownarea")) {
                var obj = new Markdownarea(ele, options);
            }
        });
    };

    var baseReplacer = function(replace, editor){
        var text     = editor.getSelection(),
            markdown = replace.replace('$1', text);

        editor.replaceSelection(markdown, 'end');
    };

    Markdownarea.commands = {
        "fullscreen": {
            "title"  : 'Fullscreen',
            "label"  : '<i class="fa fa-expand"></i>',
            "action" : function(editor){

                editor.markdownarea.markdownarea.toggleClass("uk-markdownarea-fullscreen");

                // dont use uk- to avoid rules declaration
                $('html').toggleClass("markdownarea-fullscreen");
                $('html, body').scrollTop(0);

                var wrap = editor.getWrapperElement();

                if(editor.markdownarea.markdownarea.hasClass("uk-markdownarea-fullscreen")) {

                    editor.state.fullScreenRestore = {scrollTop: window.pageYOffset, scrollLeft: window.pageXOffset, width: wrap.style.width, height: wrap.style.height};
                    wrap.style.width  = "";
                    wrap.style.height = editor.markdownarea.content.height()+"px";
                    document.documentElement.style.overflow = "hidden";

                } else {

                    document.documentElement.style.overflow = "";
                    var info = editor.state.fullScreenRestore;
                    wrap.style.width = info.width; wrap.style.height = info.height;
                    window.scrollTo(info.scrollLeft, info.scrollTop);
                }

                editor.refresh();
                editor.markdownarea.preview.parent().css("height", editor.markdownarea.code.height());
            }
        },

        "bold" : {
            "title"  : "Bold",
            "label"  : '<i class="fa fa-bold"></i>',
            "shortcut": ['Ctrl-B', 'Cmd-B'],
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? "<strong>$1</strong>":"**$1**", editor);
            }
        },
        "italic" : {
            "title"  : "Italic",
            "label"  : '<i class="fa fa-italic"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? "<em>$1</em>":"*$1*", editor);
            }
        },
        "strike" : {
            "title"  : "Strikethrough",
            "label"  : '<i class="fa fa-strikethrough"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? "<del>$1</del>":"~~$1~~", editor);
            }
        },
        "blockquote" : {
            "title"  : "Blockquote",
            "label"  : '<i class="fa fa-quote-right"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? "<blockquote><p>$1</p></blockquote>":"> $1", editor);
            }
        },
        "link" : {
            "title"  : "Link",
            "label"  : '<i class="fa fa-link"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? '<a href="http://">$1</a>':"[$1](http://)", editor);
            }
        },
        "picture" : {
            "title"  : "Picture",
            "label"  : '<i class="fa fa-picture-o"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? '<img src="http://" alt="$1">':"![$1](http://)", editor);
            }
        },
        "listUl" : {
            "title"  : "Unordered List",
            "label"  : '<i class="fa fa-list-ul"></i>',
            "action" : function(editor){
                if(this.getMode() == 'markdown') baseReplacer("* $1", editor);
            }
        },
        "listOl" : {
            "title"  : "Ordered List",
            "label"  : '<i class="fa fa-list-ol"></i>',
            "action" : function(editor){
                if(this.getMode() == 'markdown') baseReplacer("1. $1", editor);
            }
        }
    };

    Markdownarea.defaults = {
        "mode"         : "split",
        "height"       : 500,
        "maxsplitsize" : 1000,
        "codemirror"   : { mode: 'gfm', tabMode: 'indent', tabindex: "2", lineWrapping: true, dragDrop: false, autoCloseTags: true, matchTags: true },
        "toolbar"      : [ "bold", "italic", "strike", "link", "picture", "blockquote", "listUl", "listOl" ],
        "lblPreview"   : "Preview",
        "lblCodeview"  : "Markdown"
    };

    Markdownarea.template = '<div class="uk-markdownarea uk-clearfix" data-mode="split">' +
                                '<div class="uk-markdownarea-navbar">' +
                                    '<ul class="uk-markdownarea-navbar-nav uk-markdownarea-toolbar"></ul>' +
                                    '<div class="uk-markdownarea-navbar-flip">' +
                                        '<ul class="uk-markdownarea-navbar-nav">' +
                                            '<li class="uk-markdown-button-markdown"><a>{:lblCodeview}</a></li>' +
                                            '<li class="uk-markdown-button-preview"><a>{:lblPreview}</a></li>' +
                                            '<li><a data-markdownarea-cmd="fullscreen" data-toggle="tooltip" title="Zen Mode"><i class="fa fa-expand"></i></a></li>' +
                                        '</ul>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="uk-markdownarea-content">' +
                                    '<div class="uk-markdownarea-code"></div>' +
                                    '<div class="uk-markdownarea-preview"><div></div></div>' +
                                '</div>' +
                            '</div>';

    Markdownarea.plugins   = {};
    Markdownarea.addPlugin = function(name, identifier, callback) {
        Markdownarea.plugins[name] = {"identifier":identifier, "cb":callback};
    };

    $.fn["markdownarea"] = Markdownarea;

    // init code
    $(function() {

        $("textarea[data-uk-markdownarea]").each(function() {
            var area = $(this), obj;

            if (!area.data("markdownarea")) {
                obj = new Markdownarea(area, $.Utils.options(area.attr("data-uk-markdownarea")));
            }
        });
    });

    return Markdownarea;

}(jQuery, window, document));

/**=========================================================
 * Module: masked,js
 * Initializes the masked inputs
 =========================================================*/

App.directive('masked', function() {
  return {
    restrict: 'A',
    controller: ["$scope", "$element", function($scope, $element) {
      var $elem = $($element);
      if($.fn.inputmask)
        $elem.inputmask();
    }]
  };
});

/**=========================================================
 * Module: navbar-search.js
 * Navbar search toggler * Auto dismiss on ESC key
 =========================================================*/

App.directive('searchOpen', ['navSearch', function(navSearch) {
  'use strict';

  return {
    restrict: 'A',
    controller: ["$scope", "$element", function($scope, $element) {
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', navSearch.toggle);
    }]
  };

}]).directive('searchDismiss', ['navSearch', function(navSearch) {
  'use strict';

  var inputSelector = '.navbar-form input[type="text"]';

  return {
    restrict: 'A',
    controller: ["$scope", "$element", function($scope, $element) {

      $(inputSelector)
        .on('click', function (e) { e.stopPropagation(); })
        .on('keyup', function(e) {
          if (e.keyCode == 27) // ESC
            navSearch.dismiss();
        });
        
      // click anywhere closes the search
      $(document).on('click', navSearch.dismiss);
      // dismissable options
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', navSearch.dismiss);
    }]
  };

}]);


/**=========================================================
 * Module: nestable.js
 * Initializes the nestable plugin
 =========================================================*/

App.directive('nestable', ["$timeout", function($timeout) {
  return {
    restrict: 'A',
    scope: {
      'nestableControl': '='
    },
    controller: ["$scope", "$element", function($scope, $element) {
      var options = $element.data();
      
      $timeout(function(){
        $element.nestable();
      });

      if ( $scope.nestableControl ) {
        var nest = $scope.nestableControl;
        nest.serialize = function() { return $element.nestable('serialize'); };
        nest.expandAll = runMethod('expandAll');
        nest.collapseAll = runMethod('collapseAll');

        $element.on('change', function(){
          if ( typeof nest.onchange === 'function')
            $timeout(function() {
              nest.onchange.apply(arguments);
            });
        });
      }
      
      function runMethod(name) {
        return function() {
          $element.nestable(name);
        };
      }
    }]
  };

}]);

/**=========================================================
 * Module: notify.js
 * Create a notifications that fade out automatically.
 * Based on Notify addon from UIKit (http://getuikit.com/docs/addons_notify.html)
 =========================================================*/

App.directive('notify', ["$window", function($window){

  return {
    restrict: 'A',
    controller: ["$scope", "$element", function ($scope, $element) {
      
      $element.on('click', function (e) {
        e.preventDefault();
        notifyNow($element);
      });

    }]
  };

  function notifyNow(elem) {
    var $element = $(elem),
        message = $element.data('message'),
        options = $element.data('options');

    if(!message)
      $.error('Notify: No message specified');

    $.notify(message, options || {});
  }


}]);


/**
 * Notify Addon definition as jQuery plugin
 * Adapted version to work with Bootstrap classes
 * More information http://getuikit.com/docs/addons_notify.html
 */

(function($, window, document){

    var containers = {},
        messages   = {},

        notify     =  function(options){

            if ($.type(options) == 'string') {
                options = { message: options };
            }

            if (arguments[1]) {
                options = $.extend(options, $.type(arguments[1]) == 'string' ? {status:arguments[1]} : arguments[1]);
            }

            return (new Message(options)).show();
        },
        closeAll  = function(group, instantly){
            if(group) {
                for(var id in messages) { if(group===messages[id].group) messages[id].close(instantly); }
            } else {
                for(var id in messages) { messages[id].close(instantly); }
            }
        };

    var Message = function(options){

        var $this = this;

        this.options = $.extend({}, Message.defaults, options);

        this.uuid    = "ID"+(new Date().getTime())+"RAND"+(Math.ceil(Math.random() * 100000));
        this.element = $([
            // @geedmo: alert-dismissable enables bs close icon
            '<div class="uk-notify-message alert-dismissable">',
                '<a class="close">&times;</a>',
                '<div>'+this.options.message+'</div>',
            '</div>'

        ].join('')).data("notifyMessage", this);

        // status
        if (this.options.status) {
            this.element.addClass('alert alert-'+this.options.status);
            this.currentstatus = this.options.status;
        }

        this.group = this.options.group;

        messages[this.uuid] = this;

        if(!containers[this.options.pos]) {
            containers[this.options.pos] = $('<div class="uk-notify uk-notify-'+this.options.pos+'"></div>').appendTo('body').on("click", ".uk-notify-message", function(){
                $(this).data("notifyMessage").close();
            });
        }
    };


    $.extend(Message.prototype, {

        uuid: false,
        element: false,
        timout: false,
        currentstatus: "",
        group: false,

        show: function() {

            if (this.element.is(":visible")) return;

            var $this = this;

            containers[this.options.pos].show().prepend(this.element);

            var marginbottom = parseInt(this.element.css("margin-bottom"), 10);

            this.element.css({"opacity":0, "margin-top": -1*this.element.outerHeight(), "margin-bottom":0}).animate({"opacity":1, "margin-top": 0, "margin-bottom":marginbottom}, function(){

                if ($this.options.timeout) {

                    var closefn = function(){ $this.close(); };

                    $this.timeout = setTimeout(closefn, $this.options.timeout);

                    $this.element.hover(
                        function() { clearTimeout($this.timeout); },
                        function() { $this.timeout = setTimeout(closefn, $this.options.timeout);  }
                    );
                }

            });

            return this;
        },

        close: function(instantly) {

            var $this    = this,
                finalize = function(){
                    $this.element.remove();

                    if(!containers[$this.options.pos].children().length) {
                        containers[$this.options.pos].hide();
                    }

                    delete messages[$this.uuid];
                };

            if(this.timeout) clearTimeout(this.timeout);

            if(instantly) {
                finalize();
            } else {
                this.element.animate({"opacity":0, "margin-top": -1* this.element.outerHeight(), "margin-bottom":0}, function(){
                    finalize();
                });
            }
        },

        content: function(html){

            var container = this.element.find(">div");

            if(!html) {
                return container.html();
            }

            container.html(html);

            return this;
        },

        status: function(status) {

            if(!status) {
                return this.currentstatus;
            }

            this.element.removeClass('alert alert-'+this.currentstatus).addClass('alert alert-'+status);

            this.currentstatus = status;

            return this;
        }
    });

    Message.defaults = {
        message: "",
        status: "normal",
        timeout: 5000,
        group: null,
        pos: 'top-center'
    };


    $["notify"]          = notify;
    $["notify"].message  = Message;
    $["notify"].closeAll = closeAll;

    return notify;

}(jQuery, window, document));

/**=========================================================
 * Module: now.js
 * Provides a simple way to display the current time formatted
 =========================================================*/

App.directive("now", ['dateFilter', '$interval', function(dateFilter, $interval){
    return {
      restrict: 'E',
      link: function(scope, element, attrs){
        
        var format = attrs.format;

        function updateTime() {
          var dt = dateFilter(new Date(), format);
          element.text(dt);
        }

        updateTime();
        $interval(updateTime, 1000);
      }
    };
}]);
/**=========================================================
 * Module panel-tools.js
 * Directive tools to control panels. 
 * Allows collapse, refresh and dismiss (remove)
 * Saves panel state in browser storage
 =========================================================*/

App.directive('paneltool', ["$compile", "$timeout", function($compile, $timeout){
  var templates = {
    /* jshint multistr: true */
    collapse:"<a href='#' panel-collapse='' data-toggle='tooltip' title='Collapse Panel' ng-click='{{panelId}} = !{{panelId}}' ng-init='{{panelId}}=false'> \
                <em ng-show='{{panelId}}' class='fa fa-plus'></em> \
                <em ng-show='!{{panelId}}' class='fa fa-minus'></em> \
              </a>",
    dismiss: "<a href='#' panel-dismiss='' data-toggle='tooltip' title='Close Panel'>\
               <em class='fa fa-times'></em>\
             </a>",
    refresh: "<a href='#' panel-refresh='' data-toggle='tooltip' data-spinner='{{spinner}}' title='Refresh Panel'>\
               <em class='fa fa-refresh'></em>\
             </a>"
  };

  function getTemplate( elem, attrs ){
    var temp = '';
    attrs = attrs || {};
    if(attrs.toolCollapse)
      temp += templates.collapse.replace(/{{panelId}}/g, (elem.parent().parent().attr('id')) );
    if(attrs.toolDismiss)
      temp += templates.dismiss;
    if(attrs.toolRefresh)
      temp += templates.refresh.replace(/{{spinner}}/g, attrs.toolRefresh);
    return temp;
  }
  
  return {
    restrict: 'E',
    link: function (scope, element, attrs) {

      var tools = scope.panelTools || attrs;
  
      $timeout(function() {
        element.html(getTemplate(element, tools )).show();
        $compile(element.contents())(scope);
        
        element.addClass('pull-right');
      });

    }
  };
}])
/**=========================================================
 * Dismiss panels * [panel-dismiss]
 =========================================================*/
.directive('panelDismiss', ["$q", function($q){
  'use strict';
  return {
    restrict: 'A',
    controller: ["$scope", "$element", function ($scope, $element) {
      var removeEvent   = 'panel-remove',
          removedEvent  = 'panel-removed';

      $element.on('click', function () {

        // find the first parent panel
        var parent = $(this).closest('.panel');

        removeElement();

        function removeElement() {
          var deferred = $q.defer();
          var promise = deferred.promise;
          
          // Communicate event destroying panel
          $scope.$emit(removeEvent, parent.attr('id'), deferred);
          promise.then(destroyMiddleware);
        }

        // Run the animation before destroy the panel
        function destroyMiddleware() {
          if($.support.animation) {
            parent.animo({animation: 'bounceOut'}, destroyPanel);
          }
          else destroyPanel();
        }

        function destroyPanel() {

          var col = parent.parent();
          parent.remove();
          // remove the parent if it is a row and is empty and not a sortable (portlet)
          col
            .filter(function() {
            var el = $(this);
            return (el.is('[class*="col-"]:not(.sortable)') && el.children('*').length === 0);
          }).remove();

          // Communicate event destroyed panel
          $scope.$emit(removedEvent, parent.attr('id'));

        }
      });
    }]
  };
}])
/**=========================================================
 * Collapse panels * [panel-collapse]
 =========================================================*/
.directive('panelCollapse', ['$timeout', function($timeout){
  'use strict';
  
  var storageKeyName = 'panelState',
      storage;
  
  return {
    restrict: 'A',
    controller: ["$scope", "$element", function ($scope, $element) {

      // Prepare the panel to be collapsible
      var $elem   = $($element),
          parent  = $elem.closest('.panel'), // find the first parent panel
          panelId = parent.attr('id');

      storage = $scope.$storage;

      // Load the saved state if exists
      var currentState = loadPanelState( panelId );
      if ( typeof currentState !== undefined) {
        $timeout(function(){
            $scope[panelId] = currentState; },
          10);
      }

      // bind events to switch icons
      $element.bind('click', function() {

        savePanelState( panelId, !$scope[panelId] );

      });
    }]
  };

  function savePanelState(id, state) {
    if(!id) return false;
    var data = angular.fromJson(storage[storageKeyName]);
    if(!data) { data = {}; }
    data[id] = state;
    storage[storageKeyName] = angular.toJson(data);
  }

  function loadPanelState(id) {
    if(!id) return false;
    var data = angular.fromJson(storage[storageKeyName]);
    if(data) {
      return data[id];
    }
  }

}])
/**=========================================================
 * Refresh panels
 * [panel-refresh] * [data-spinner="standard"]
 =========================================================*/
.directive('panelRefresh', ["$q", function($q){
  'use strict';
  
  return {
    restrict: 'A',
    controller: ["$scope", "$element", function ($scope, $element) {
      
      var refreshEvent   = 'panel-refresh',
          whirlClass     = 'whirl',
          defaultSpinner = 'standard';


      // catch clicks to toggle panel refresh
      $element.on('click', function () {
        var $this   = $(this),
            panel   = $this.parents('.panel').eq(0),
            spinner = $this.data('spinner') || defaultSpinner
            ;

        // start showing the spinner
        panel.addClass(whirlClass + ' ' + spinner);

        // Emit event when refresh clicked
        $scope.$emit(refreshEvent, panel.attr('id'));

      });

      // listen to remove spinner
      $scope.$on('removeSpinner', removeSpinner);

      // method to clear the spinner when done
      function removeSpinner (ev, id) {
        if (!id) return;
        var newid = id.charAt(0) == '#' ? id : ('#'+id);
        angular
          .element(newid)
          .removeClass(whirlClass);
      }
    }]
  };
}]);

/**=========================================================
 * Module: play-animation.js
 * Provides a simple way to run animation with a trigger
 * Requires animo.js
 =========================================================*/
 
App.directive('animate', ["$window", function($window){

  'use strict';

  var $scroller = $(window).add('body, .wrapper');
  
  return {
    restrict: 'A',
    link: function (scope, elem, attrs) {

      // Parse animations params and attach trigger to scroll
      var $elem     = $(elem),
          offset    = $elem.data('offset'),
          delay     = $elem.data('delay')     || 100, // milliseconds
          animation = $elem.data('play')      || 'bounce';
      
      if(typeof offset !== 'undefined') {
        
        // test if the element starts visible
        testAnimation($elem);
        // test on scroll
        $scroller.scroll(function(){
          testAnimation($elem);
        });

      }

      // Test an element visibilty and trigger the given animation
      function testAnimation(element) {
          if ( !element.hasClass('anim-running') &&
              $.Utils.isInView(element, {topoffset: offset})) {
          element
            .addClass('anim-running');

          setTimeout(function() {
            element
              .addClass('anim-done')
              .animo( { animation: animation, duration: 0.7} );
          }, delay);

        }
      }

      // Run click triggered animations
      $elem.on('click', function() {

        var $elem     = $(this),
            targetSel = $elem.data('target'),
            animation = $elem.data('play') || 'bounce',
            target    = $(targetSel);

        if(target && target) {
          target.animo( { animation: animation } );
        }
        
      });
    }
  };

}]);

/**=========================================================
 * Module: scroll.js
 * Make a content box scrollable
 =========================================================*/

App.directive('scrollable', function(){
  return {
    restrict: 'EA',
    link: function(scope, elem, attrs) {
      var defaultHeight = 250;
      elem.slimScroll({
          height: (attrs.height || defaultHeight)
      });
    }
  };
});
/**=========================================================
 * Module: sidebar.js
 * Wraps the sidebar and handles collapsed state
 =========================================================*/

App.directive('sidebar', ['$window', 'APP_MEDIAQUERY', function($window, mq) {
  
  var $win  = $($window);
  var $html = $('html');
  var $body = $('body');
  var $scope;
  var $sidebar;

  return {
    restrict: 'EA',
    template: '<nav class="sidebar" ng-transclude></nav>',
    transclude: true,
    replace: true,
    link: function(scope, element, attrs) {
      
      $scope   = scope;
      $sidebar = element;

      var eventName = isTouch() ? 'click' : 'mouseenter' ;
      var subNav = $();
      $sidebar.on( eventName, '.nav > li', function() {

        if( isSidebarCollapsed() && !isMobile() ) {

          subNav.trigger('mouseleave');
          subNav = toggleMenuItem( $(this) );

        }

      });

      scope.$on('closeSidebarMenu', function() {
        removeFloatingNav();
        $('.sidebar li.open').removeClass('open');
      });
    }
  };


  // Open the collapse sidebar submenu items when on touch devices 
  // - desktop only opens on hover
  function toggleTouchItem($element){
    $element
      .siblings('li')
      .removeClass('open')
      .end()
      .toggleClass('open');
  }

  // Handles hover to open items under collapsed menu
  // ----------------------------------- 
  function toggleMenuItem($listItem) {

    removeFloatingNav();

    var ul = $listItem.children('ul');
    
    if( !ul.length ) return $();
    if( $listItem.hasClass('open') ) {
      toggleTouchItem($listItem);
      return $();
    }

    var $aside = $('.aside');
    //var mar =  $scope.app.layout.isFixed ?  parseInt( $aside.css('padding-top'), 0) : 0;
    var mar = parseInt( $aside.css('padding-top'), 0);
    var subNav = ul.clone().appendTo( $aside );
    
    toggleTouchItem($listItem);

    var itemTop = ($listItem.position().top + mar) - $sidebar.scrollTop();
    var vwHeight = $win.height();

    subNav
      .addClass('nav-floating')
      .css({
        position: $scope.app.layout.isFixed ? 'fixed' : 'absolute',
        top:      itemTop,
        bottom:   (subNav.outerHeight(true) + itemTop > vwHeight) ? 0 : 'auto'
      });

    subNav.on('mouseleave', function() {
      toggleTouchItem($listItem);
      subNav.remove();
    });

    return subNav;
  }

  function removeFloatingNav() {
    $('.sidebar-subnav.nav-floating').remove();
  }

  function isTouch() {
    return $html.hasClass('touch');
  }
  function isSidebarCollapsed() {
    return $body.hasClass('aside-collapsed');
  }
  function isSidebarToggled() {
    return $body.hasClass('aside-toggled');
  }
  function isMobile() {
    return $win.width() < mq.tablet;
  }
}]);
/**=========================================================
 * Module: skycons.js
 * Include any animated weather icon from Skycons
 =========================================================*/

App.directive('skycon', function(){

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var skycons = new Skycons({'color': (attrs.color || 'white')});

      element.html('<canvas width="' + attrs.width + '" height="' + attrs.height + '"></canvas>');

      skycons.add(element.children()[0], attrs.skycon);

      skycons.play();

    }
  };
});
/**=========================================================
 * Module: sparkline.js
 * SparkLines Mini Charts
 =========================================================*/
 
App.directive('sparkline', ['$timeout', '$window', function($timeout, $window){

  'use strict';

  return {
    restrict: 'EA',
    controller: ["$scope", "$element", function ($scope, $element) {
      var runSL = function(){
        initSparLine($element);
      };

      $timeout(runSL);
    }]
  };

  function initSparLine($element) {
    var options = $element.data();

    options.type = options.type || 'bar'; // default chart is bar
    options.disableHiddenCheck = true;

    $element.sparkline('html', options);

    if(options.resize) {
      $(window).resize(function(){
        $element.sparkline('html', options);
      });
    }
  }

}]);

/**=========================================================
 * Module: table-checkall.js
 * Tables check all checkbox
 =========================================================*/

App.directive('checkAll', function() {
  'use strict';
  
  return {
    restrict: 'A',
    controller: ["$scope", "$element", function($scope, $element){
      
      $element.on('change', function() {
        var $this = $(this),
            index= $this.index() + 1,
            checkbox = $this.find('input[type="checkbox"]'),
            table = $this.parents('table');
        // Make sure to affect only the correct checkbox column
        table.find('tbody > tr > td:nth-child('+index+') input[type="checkbox"]')
          .prop('checked', checkbox[0].checked);

      });
    }]
  };

});
/**=========================================================
 * Module: tags-input.js
 * Initializes the tag inputs plugin
 =========================================================*/

App.directive('tagsinput', ["$timeout", function($timeout) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {

      element.on('itemAdded itemRemoved', function(){
        // check if view value is not empty and is a string
        // and update the view from string to an array of tags
        if(ngModel.$viewValue && ngModel.$viewValue.split) {
          ngModel.$setViewValue( ngModel.$viewValue.split(',') );
          ngModel.$render();
        }
      });

      $timeout(function(){
        element.tagsinput();
      });

    }
  };
}]);

/**=========================================================
 * Module: toggle-state.js
 * Toggle a classname from the BODY Useful to change a state that 
 * affects globally the entire layout or more than one item 
 * Targeted elements must have [toggle-state="CLASS-NAME-TO-TOGGLE"]
 * User no-persist to avoid saving the sate in browser storage
 =========================================================*/

App.directive('toggleState', ['toggleStateService', function(toggle) {
  'use strict';
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {

      var $body = $('body');

      $(element)
        .on('click', function (e) {
          e.preventDefault();
          var classname = attrs.toggleState;
          
          if(classname) {
            if( $body.hasClass(classname) ) {
              $body.removeClass(classname);
              if( ! attrs.noPersist)
                toggle.removeState(classname);
            }
            else {
              $body.addClass(classname);
              if( ! attrs.noPersist)
                toggle.addState(classname);
            }
            
          }

      });
    }
  };
  
}]);

/**=========================================================
 * Module: masked,js
 * Initializes the jQuery UI slider controls
 =========================================================*/

App.directive('uiSlider', function() {
  return {
    restrict: 'A',
    controller: ["$scope", "$element", function($scope, $element) {
      var $elem = $($element);
      if($.fn.slider)
        $elem.slider();
    }]
  };
});

/**=========================================================
 * Module: validate-form.js
 * Initializes the validation plugin Parsley
 =========================================================*/

App.directive('validateForm', function() {
  return {
    restrict: 'A',
    controller: ["$scope", "$element", function($scope, $element) {
      var $elem = $($element);
      if($.fn.parsley)
        $elem.parsley();
    }]
  };
});

/**=========================================================
 * Module: vector-map.js.js
 * Init jQuery Vector Map plugin
 =========================================================*/

App.directive('vectorMap', ['vectorMap', function(vectorMap){
  'use strict';

  var defaultColors = {
      markerColor:  '#23b7e5',      // the marker points
      bgColor:      'transparent',      // the background
      scaleColors:  ['#878c9a'],    // the color of the region in the serie
      regionFill:   '#bbbec6'       // the base region color
  };

  return {
    restrict: 'EA',
    link: function(scope, element, attrs) {

      var mapHeight   = attrs.height || '300',
          options     = {
            markerColor:  attrs.markerColor  || defaultColors.markerColor,
            bgColor:      attrs.bgColor      || defaultColors.bgColor,
            scale:        attrs.scale        || 1,
            scaleColors:  attrs.scaleColors  || defaultColors.scaleColors,
            regionFill:   attrs.regionFill   || defaultColors.regionFill,
            mapName:      attrs.mapName      || 'world_mill_en'
          };
      
      element.css('height', mapHeight);
      
      vectorMap.init( element , options, scope.seriesData, scope.markersData);

    }
  };

}]);
App.service('browser', function(){
  "use strict";

  var matched, browser;

  var uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(opr)[\/]([\w.]+)/.exec( ua ) ||
      /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
      /(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
      /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
      /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
      /(msie) ([\w.]+)/.exec( ua ) ||
      ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
      ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
      [];

    var platform_match = /(ipad)/.exec( ua ) ||
      /(iphone)/.exec( ua ) ||
      /(android)/.exec( ua ) ||
      /(windows phone)/.exec( ua ) ||
      /(win)/.exec( ua ) ||
      /(mac)/.exec( ua ) ||
      /(linux)/.exec( ua ) ||
      /(cros)/i.exec( ua ) ||
      [];

    return {
      browser: match[ 3 ] || match[ 1 ] || "",
      version: match[ 2 ] || "0",
      platform: platform_match[ 0 ] || ""
    };
  };

  matched = uaMatch( window.navigator.userAgent );
  browser = {};

  if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
    browser.versionNumber = parseInt(matched.version);
  }

  if ( matched.platform ) {
    browser[ matched.platform ] = true;
  }

  // These are all considered mobile platforms, meaning they run a mobile browser
  if ( browser.android || browser.ipad || browser.iphone || browser[ "windows phone" ] ) {
    browser.mobile = true;
  }

  // These are all considered desktop platforms, meaning they run a desktop browser
  if ( browser.cros || browser.mac || browser.linux || browser.win ) {
    browser.desktop = true;
  }

  // Chrome, Opera 15+ and Safari are webkit based browsers
  if ( browser.chrome || browser.opr || browser.safari ) {
    browser.webkit = true;
  }

  // IE11 has a new token so we will assign it msie to avoid breaking changes
  if ( browser.rv )
  {
    var ie = "msie";

    matched.browser = ie;
    browser[ie] = true;
  }

  // Opera 15+ are identified as opr
  if ( browser.opr )
  {
    var opera = "opera";

    matched.browser = opera;
    browser[opera] = true;
  }

  // Stock Android browsers are marked as Safari on Android.
  if ( browser.safari && browser.android )
  {
    var android = "android";

    matched.browser = android;
    browser[android] = true;
  }

  // Assign the name and platform variable
  browser.name = matched.browser;
  browser.platform = matched.platform;


  return browser;

});
/**=========================================================
 * Module: colors.js
 * Services to retrieve global colors
 =========================================================*/
 
App.factory('colors', ['APP_COLORS', function(colors) {
  
  return {
    byName: function(name) {
      return (colors[name] || '#fff');
    }
  };

}]);

/**=========================================================
 * Module: google-map.js
 * Services to share gmap functions
 =========================================================*/

App.service('gmap', function() {

  return {
    setStyle: function(style) {
      this.MapStyles = style;
    },
    autocenter: function() {
      var refs = this.gMapRefs;
      if(refs && refs.length) {
        for( var r in refs) {
          var mapRef = refs[r];
          var currMapCenter = mapRef.getCenter();
          if(mapRef && currMapCenter) {
              google.maps.event.trigger(mapRef, 'resize');
              mapRef.setCenter(currMapCenter);
          }
        }
      }
    },
    init: function (element) { //initGmap

      var self      = this,
          $element  = $(element),
          addresses = $element.data('address') && $element.data('address').split(';'),
          titles    = $element.data('title') && $element.data('title').split(';'),
          zoom      = $element.data('zoom') || 14,
          maptype   = $element.data('maptype') || 'ROADMAP', // or 'TERRAIN'
          markers   = [];

      if(addresses) {
        for(var a in addresses)  {
            if(typeof addresses[a] == 'string') {
                markers.push({
                    address:  addresses[a],
                    html:     (titles && titles[a]) || '',
                    popup:    true   /* Always popup */
                  });
            }
        }

        var options = {
            controls: {
                   panControl:         true,
                   zoomControl:        true,
                   mapTypeControl:     true,
                   scaleControl:       true,
                   streetViewControl:  true,
                   overviewMapControl: true
               },
            scrollwheel: false,
            maptype: maptype,
            markers: markers,
            zoom: zoom
            // More options https://github.com/marioestrada/jQuery-gMap
        };

        var gMap = $element.gMap(options);

        var ref = gMap.data('gMap.reference');
        // save in the map references list
        if( ! self.gMapRefs )
          self.gMapRefs = [];
        self.gMapRefs.push(ref);

        // set the styles
        if($element.data('styled') !== undefined) {
          
          ref.setOptions({
            styles: self.MapStyles
          });

        }
      }
    }
  };
});
/**=========================================================
 * Module: nav-search.js
 * Services to share navbar search functions
 =========================================================*/
 
App.service('navSearch', function() {
  var navbarFormSelector = 'form.navbar-form';
  return {
    toggle: function() {
      
      var navbarForm = $(navbarFormSelector);

      navbarForm.toggleClass('open');
      
      var isOpen = navbarForm.hasClass('open');
      
      navbarForm.find('input')[isOpen ? 'focus' : 'blur']();

    },

    dismiss: function() {
      $(navbarFormSelector)
        .removeClass('open')      // Close control
        .find('input[type="text"]').blur() // remove focus
        .val('')                    // Empty input
        ;
    }
  };

});
/**=========================================================
 * Module: helpers.js
 * Provides helper functions for routes definition
 =========================================================*/

App.provider('RouteHelpers', ['APP_REQUIRES', function (appRequires) {
  "use strict";

  // Set here the base of the relative path
  // for all app views
  this.basepath = function (uri) {
    return 'app/views/' + uri;
  };

  // Generates a resolve object by passing script names
  // previously configured in constant.APP_REQUIRES
  this.resolveFor = function () {
    var _args = arguments;
    return {
      deps: ['$ocLazyLoad','$q', function ($ocLL, $q) {
        // Creates a promise chain for each argument
        var promise = $q.when(1); // empty promise
        for(var i=0, len=_args.length; i < len; i ++){
          promise = andThen(_args[i]);
        }
        return promise;

        // creates promise to chain dynamically
        function andThen(_arg) {
          // also support a function that returns a promise
          if(typeof _arg == 'function')
              return promise.then(_arg);
          else
              return promise.then(function() {
                // if is a module, pass the name. If not, pass the array
                var whatToLoad = getRequired(_arg);
                // simple error check
                if(!whatToLoad) return $.error('Route resolve: Bad resource name [' + _arg + ']');
                // finally, return a promise
                return $ocLL.load( whatToLoad );
              });
        }
        // check and returns required data
        // analyze module items with the form [name: '', files: []]
        // and also simple array of script files (for not angular js)
        function getRequired(name) {
          if (appRequires.modules)
              for(var m in appRequires.modules)
                  if(appRequires.modules[m].name && appRequires.modules[m].name === name)
                      return appRequires.modules[m];
          return appRequires.scripts && appRequires.scripts[name];
        }

      }]};
  }; // resolveFor

  // not necessary, only used in config block for routes
  this.$get = function(){};

}]);


/**=========================================================
 * Module: toggle-state.js
 * Services to share toggle state functionality
 =========================================================*/

App.service('toggleStateService', ['$rootScope', function($rootScope) {

  var storageKeyName  = 'toggleState';

  // Helper object to check for words in a phrase //
  var WordChecker = {
    hasWord: function (phrase, word) {
      return new RegExp('(^|\\s)' + word + '(\\s|$)').test(phrase);
    },
    addWord: function (phrase, word) {
      if (!this.hasWord(phrase, word)) {
        return (phrase + (phrase ? ' ' : '') + word);
      }
    },
    removeWord: function (phrase, word) {
      if (this.hasWord(phrase, word)) {
        return phrase.replace(new RegExp('(^|\\s)*' + word + '(\\s|$)*', 'g'), '');
      }
    }
  };

  // Return service public methods
  return {
    // Add a state to the browser storage to be restored later
    addState: function(classname){
      var data = angular.fromJson($rootScope.$storage[storageKeyName]);
      
      if(!data)  {
        data = classname;
      }
      else {
        data = WordChecker.addWord(data, classname);
      }

      $rootScope.$storage[storageKeyName] = angular.toJson(data);
    },

    // Remove a state from the browser storage
    removeState: function(classname){
      var data = $rootScope.$storage[storageKeyName];
      // nothing to remove
      if(!data) return;

      data = WordChecker.removeWord(data, classname);

      $rootScope.$storage[storageKeyName] = angular.toJson(data);
    },
    
    // Load the state string and restore the classlist
    restoreState: function($elem) {
      var data = angular.fromJson($rootScope.$storage[storageKeyName]);
      
      // nothing to restore
      if(!data) return;
      $elem.addClass(data);
    }

  };

}]);
/**=========================================================
 * Module: utils.js
 * jQuery Utility functions library 
 * adapted from the core of UIKit
 =========================================================*/

(function($, window, doc){
    'use strict';
    
    var $html = $("html"), $win = $(window);

    $.support.transition = (function() {

        var transitionEnd = (function() {

            var element = doc.body || doc.documentElement,
                transEndEventNames = {
                    WebkitTransition: 'webkitTransitionEnd',
                    MozTransition: 'transitionend',
                    OTransition: 'oTransitionEnd otransitionend',
                    transition: 'transitionend'
                }, name;

            for (name in transEndEventNames) {
                if (element.style[name] !== undefined) return transEndEventNames[name];
            }
        }());

        return transitionEnd && { end: transitionEnd };
    })();

    $.support.animation = (function() {

        var animationEnd = (function() {

            var element = doc.body || doc.documentElement,
                animEndEventNames = {
                    WebkitAnimation: 'webkitAnimationEnd',
                    MozAnimation: 'animationend',
                    OAnimation: 'oAnimationEnd oanimationend',
                    animation: 'animationend'
                }, name;

            for (name in animEndEventNames) {
                if (element.style[name] !== undefined) return animEndEventNames[name];
            }
        }());

        return animationEnd && { end: animationEnd };
    })();

    $.support.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame || function(callback){ window.setTimeout(callback, 1000/60); };
    $.support.touch                 = (
        ('ontouchstart' in window && navigator.userAgent.toLowerCase().match(/mobile|tablet/)) ||
        (window.DocumentTouch && document instanceof window.DocumentTouch)  ||
        (window.navigator['msPointerEnabled'] && window.navigator['msMaxTouchPoints'] > 0) || //IE 10
        (window.navigator['pointerEnabled'] && window.navigator['maxTouchPoints'] > 0) || //IE >=11
        false
    );
    $.support.mutationobserver      = (window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver || null);

    $.Utils = {};

    $.Utils.debounce = function(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    $.Utils.removeCssRules = function(selectorRegEx) {
        var idx, idxs, stylesheet, _i, _j, _k, _len, _len1, _len2, _ref;

        if(!selectorRegEx) return;

        setTimeout(function(){
            try {
              _ref = document.styleSheets;
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                stylesheet = _ref[_i];
                idxs = [];
                stylesheet.cssRules = stylesheet.cssRules;
                for (idx = _j = 0, _len1 = stylesheet.cssRules.length; _j < _len1; idx = ++_j) {
                  if (stylesheet.cssRules[idx].type === CSSRule.STYLE_RULE && selectorRegEx.test(stylesheet.cssRules[idx].selectorText)) {
                    idxs.unshift(idx);
                  }
                }
                for (_k = 0, _len2 = idxs.length; _k < _len2; _k++) {
                  stylesheet.deleteRule(idxs[_k]);
                }
              }
            } catch (_error) {}
        }, 0);
    };

    $.Utils.isInView = function(element, options) {

        var $element = $(element);

        if (!$element.is(':visible')) {
            return false;
        }

        var window_left = $win.scrollLeft(),
            window_top  = $win.scrollTop(),
            offset      = $element.offset(),
            left        = offset.left,
            top         = offset.top;

        options = $.extend({topoffset:0, leftoffset:0}, options);

        if (top + $element.height() >= window_top && top - options.topoffset <= window_top + $win.height() &&
            left + $element.width() >= window_left && left - options.leftoffset <= window_left + $win.width()) {
          return true;
        } else {
          return false;
        }
    };

    $.Utils.options = function(string) {

        if ($.isPlainObject(string)) return string;

        var start = (string ? string.indexOf("{") : -1), options = {};

        if (start != -1) {
            try {
                options = (new Function("", "var json = " + string.substr(start) + "; return JSON.parse(JSON.stringify(json));"))();
            } catch (e) {}
        }

        return options;
    };

    $.Utils.events       = {};
    $.Utils.events.click = $.support.touch ? 'tap' : 'click';

    $.langdirection = $html.attr("dir") == "rtl" ? "right" : "left";

    $(function(){

        // Check for dom modifications
        if(!$.support.mutationobserver) return;

        // Install an observer for custom needs of dom changes
        var observer = new $.support.mutationobserver($.Utils.debounce(function(mutations) {
            $(doc).trigger("domready");
        }, 300));

        // pass in the target node, as well as the observer options
        observer.observe(document.body, { childList: true, subtree: true });

    });

    // add touch identifier class
    $html.addClass($.support.touch ? "touch" : "no-touch");

}(jQuery, window, document));
/**=========================================================
 * Module: vector-map.js
 * Services to initialize vector map plugin
 =========================================================*/

App.service('vectorMap', function() {
  'use strict';
  return {
    init: function($element, opts, series, markers) {
          $element.vectorMap({
            map:             opts.mapName,
            backgroundColor: opts.bgColor,
            zoomMin:         1,
            zoomMax:         8,
            zoomOnScroll:    false,
            regionStyle: {
              initial: {
                'fill':           opts.regionFill,
                'fill-opacity':   1,
                'stroke':         'none',
                'stroke-width':   1.5,
                'stroke-opacity': 1
              },
              hover: {
                'fill-opacity': 0.8
              },
              selected: {
                fill: 'blue'
              },
              selectedHover: {
              }
            },
            focusOn:{ x:0.4, y:0.6, scale: opts.scale},
            markerStyle: {
              initial: {
                fill: opts.markerColor,
                stroke: opts.markerColor
              }
            },
            onRegionLabelShow: function(e, el, code) {
              if ( series && series[code] )
                el.html(el.html() + ': ' + series[code] + ' visitors');
            },
            markers: markers,
            series: {
                regions: [{
                    values: series,
                    scale: opts.scaleColors,
                    normalizeFunction: 'polynomial'
                }]
            },
          });
        }
  };
});
// To run this code, edit file 
// index.html or index.jade and change
// html data-ng-app attribute from
// angle to myAppName
// ----------------------------------- 

var myApp = angular.module('myAppName', ['angle']);

myApp.run(["$log", function($log) {

  $log.log('I\'m a line from custom.js');

}]);

myApp.config(["RouteHelpersProvider", function(RouteHelpersProvider) {

  // Custom Route definition
  
}]);

myApp.controller('oneOfMyOwnController', ["$scope", function($scope) {
  /* controller code */
}]);

myApp.directive('oneOfMyOwnDirectives', function() {
  /*directive code*/
});

myApp.config(["$stateProvider", function($stateProvider /* ... */) {
  /* specific routes here (see file config.js) */
}]);